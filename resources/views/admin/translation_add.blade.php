@extends('admin.layout.final')
@section('title')
Translation Management
@endsection
@section('pageTitle')
Translation Management
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('translation')}}">Translation Management</a></li>
            <li class="breadcrumb-item active">Add Translation</li>
            
        </ol>
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">Add Translation</h4>
            </div>
            <form name="translationAddForm" id="translationAddForm" method="post" action="{{backUrl('translation/store')}}">
                @csrf
                <!-- <div class="card-body">
                    <h4 class="card-title">Person Info</h4>
                </div>
                <hr> -->
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label class="control-label">Module:</label>
                            <select class="form-control custom-select" name="module_id" id="module_id" data-placeholder="Choose a Module" tabindex="1">
                                <option value="">Please Select</option>
                                @if(isset($modules) && !empty($modules))
                                @foreach($modules as $modulesVal)
                                <option value="{{$modulesVal->id}}">{{$modulesVal->name}}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="code_lable">Code String:</label>
                            <textarea class="form-control" rows="5" id="code_lable" name="code_lable">{{old('code_lable')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="english_text">English String:</label>
                            <textarea class="form-control" rows="5" id="english_text" name="english_text">{{old('english_text')}}</textarea>
                        </div>
                        <div class="form-group">
                          <label for="translated_text">Translated String:</label>
                          <textarea class="form-control" rows="5" id="translated_text" name="translated_text">{{old('translated_text')}}</textarea>
                      </div>
                  </div>
                  <div class="card-body">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                    <a href="{{backUrl('translation')}}" class="btn btn-dark">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#translationAddForm'):'' !!}

@toastr_render
@endsection