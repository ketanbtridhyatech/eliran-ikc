@extends('admin.layout.final')
@section('title')
{{translate('Title Management')}}
@endsection
@section('pageTitle')
{{translate('Title Management')}}
@endsection
@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Title Management')}}</li>
        </ol>
        <a href="{{backUrl('titles/add')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> {{translate('Create New')}}</a>
        @if(! $listing->isEmpty())
        
                <a href="javascript:void(0)" id="export_csv" name="export_csv" class="btn btn-success d-none d-lg-block m-l-15">{{translate('Export to CSV')}}</a>
          
        @endif
    </div>
</div>
@endsection
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
         <div class="card-body">
         	 <div class="col-md-12">
                <form action="" method="GET" role="search" name="search_filter" id="search_filter">

                    <div class="form-body">
                        <div class="row">
                            
                           
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Name')}} :</label>
                                    <input type="text" class="form-control" id="search_by_title_name" name="search_by_title_name" value="{{isset($request->search_by_title_name)?$request->search_by_title_name:'' }}">
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Remark')}} :</label>
                                    <input type="text" class="form-control" id="search_by_title_remark" name="search_by_title_remark" value="{{isset($request->search_by_title_remark)?$request->search_by_title_remark:'' }}">
                                </div>
                            </div>
                            
                        </div>
                       

                        
                        <div class="card-body">
                           <input type="hidden" name="submit_type" id="submit_type" value="1">
                            <button name="search_submit" value="1" class="btn btn-success" id="search_submit"> <i class="fa fa-check"></i> {{translate('Search')}}</button>
                            <a href="{{backUrl('titles')}}" class="btn btn-dark">{{translate('Reset')}}</a>
                        </div>
                    </div>
                </form>
            </div>
            <h6 class="card-subtitle"></h6>
            <div class="table-responsive">
               
                <table id="demo-foo-addrow" class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg" data-paging="true" data-paging-size="7" style="">
                    <thead>
                        <tr class="footable-header">
                            <!-- <th><input type="checkbox" class="control-input" name="checkAllUsers" id="checkAllUsers" value="all"></th> -->
                            <th class="footable-first-visible">No.</th>
                            <th>@sortablelink('TitleName', translate('Title Name'))</th>
                            <th>@sortablelink('TitleCode', translate('Title Code'))</th>
                            <th>@sortablelink('Remark', translate('Remark'))</th>
                            <th>@sortablelink('TitleDesc', translate('Desc'))</th>
                            
                            <th>{{translate('Action')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($listing) && !empty($listing))
                        @php $i=(($listing->currentPage() - 1) * $listing->perPage()+1);@endphp
                        @foreach($listing as $listingVal)
                        <tr>
                           <!--  <td>
                                <input type="checkbox" class="control-input usercheck" name="userCheck[]" id="userCheck{{$listingVal->id}}" value="{{$listingVal->id}}">
                            </td> -->
                            <td class="footable-first-visible">{{$i++}}</td>
                           
                            <td>{{isset($listingVal->TitleName)?$listingVal->TitleName:'N/A'}}</td>
                            <td>{{isset($listingVal->TitleCode)?$listingVal->TitleCode:'N/A'}}</td>
                            <td>{{isset($listingVal->Remark)?$listingVal->Remark:'N/A'}}</td>
                            <td>{{isset($listingVal->TitleDesc)?$listingVal->TitleDesc:'N/A'}}</td>
                             
                            <td>
                               <?php if(!empty($userSkill)){?>
                                <a href="{{backUrl('titles/edit/'.$listingVal->id)}}">
                                    <i class="fas fa-edit" data-toggle="tooltip" data-original-title="{{translate('Edit')}}"></i>
                                </a>
                                <a onclick="return confirm('Are you sure want to delete this record?')" href="{{backUrl('titles/delete/'.$listingVal->id)}}">
                                    <i class="far fa-trash-alt" data-toggle="tooltip" data-original-title="{{translate('Delete')}}"></i>
                                </a>
                                <?php }?>
                            </td>
                            
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr class="footable-paging">
                            <td colspan="8">
                                <div class="footable-pagination-wrapper">
                                    <div class="text-right">{!! $listing->appends(\Request::except('page'))->render() !!}</div>
                                    <div class="divider">
                                    </div>
                                    <!-- <span class="label label-primary">1 of 2</span> -->
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
         </div>
       	</div>
     </div>
 </div>
@endsection

@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/additional-methods.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment-with-locales.js')}}"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#export_csv').on('click',function(){
            $('#submit_type').val('export_csv');
            $('#search_filter').submit();
         });
        $('#search_submit').on('click',function(){
            $('#submit_type').val('1');
            $('#search_submit').submit();
         });
        
       
    });     
</script>
@toastr_render
@endsection