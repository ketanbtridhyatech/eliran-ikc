@extends('admin.layout.final')
@section('title')
{{translate('User Management')}}
@endsection
@section('pageTitle')
{{translate('User Management')}}
@endsection
@section('content')
<style type="text/css">
 <?php if(currentLanguage()==1){?>
   .accordion-toggle:after {
    font-family: 'FontAwesome';
    content: "\f078";    
    float: left;
    font-size: 24px;
    margin-top: 14px;
} 
<?php }else{?> 
.accordion-toggle:after {
    font-family: 'FontAwesome';
    content: "\f078";    
    float: right;
    font-size: 24px;
    margin-top: 14px;
}
<?php }?>
</style>
@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('user')}}">{{translate('User Management')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Edit User')}}</li>
        </ol>
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            @if(!empty($errors->all()))
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $error }}</strong>
            </div>
            @endforeach
            @endif
            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">{{translate('Edit User')}}</h4>
            </div>
            <div class="card-body">

                <form action="{{backUrl('user/update')}}" name="userEditForm" id="userEditForm" method="post"
                    enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{{$data->id}}">
                    @csrf
                    <div class="form-body">
                        <h3 class="card-title">{{translate('Person Info')}}</h3>
                        <hr>
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('First Name')}} <span
                                            class="text-danger">*</span></label>
                                    <input type="text" id="first_name" name="first_name" class="form-control"
                                        placeholder=""
                                        value="{{($data->first_name)?$data->first_name:old('first_name')}}">
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Last Name')}} <span
                                            class="text-danger">*</span></label>
                                    <input type="text" id="last_name" name="last_name" class="form-control"
                                        placeholder="" value="{{($data->last_name)?$data->last_name:old('last_name')}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('First Name')}} (EN) <span
                                            class="text-danger">*</span></label>
                                    <input type="text" id="first_name_en" name="first_name_en" class="form-control"
                                        placeholder=""
                                        value="{{($data->first_name_en)?$data->first_name_en:old('first_name_en')}}">
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Last Name')}} (EN) <span
                                            class="text-danger">*</span></label>
                                    <input type="text" id="last_name_en" name="last_name_en" class="form-control"
                                        placeholder=""
                                        value="{{($data->last_name_en)?$data->last_name_en:old('last_name_en')}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Email')}} <span
                                            class="text-danger">*</span></label>
                                    <input type="text" id="email" name="email" class="form-control" placeholder=""
                                        value="{{($data->email)?$data->email:old('email')}}">
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Phone Number')}}</label>
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder=""
                                        value="{{($data->phone)?$data->phone:old('phone')}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">

                            <!--/span-->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Profile Photo')}}</label>
                                    <input type="file" id="profile_photo" name="profile_photo" class="form-control"
                                        placeholder="">
                                    @if(!empty($data->profile_photo))
                                    <img src="{{$data->profile_photo}}" width="50">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Social Id')}}</label>
                                    <input type="text" id="social_id_number" name="social_id_number"
                                        class="form-control" placeholder=""
                                        value="{{($data->social_id_number)?$data->social_id_number:old('social_id_number')}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <!--/row-->
                        <div class="row">

                            <!--/span-->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Date of Birth')}}
                                        ({{settingParam("date-format")}}) <span class="text-danger">*</span></label>
                                    <input type="text" id="birth_date" name="birth_date" class="form-control"
                                        placeholder=""
                                        value="{{($data->birth_date)?convertDate($data->birth_date):old('birth_date')}}"
                                        readonly="">
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <h3 class="box-title m-t-40">{{translate('Password')}}</h3>
                        <hr>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Password')}} <span class="text-danger">*</span></label>
                                    <input type="password" name="password" id="password" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Confirm Password')}} <span class="text-danger">*</span></label>
                                    <input type="password" name="confirmed" id="confirmed" class="form-control">
                                </div>
                            </div>
                        </div>

                        <h3 class="box-title m-t-40">{{translate('Address')}}</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('House Number')}} <span class="text-danger">*</span></label>
                                    <input type="text" name="address_street_number" id="address_street_number"
                                        class="form-control"
                                        value="{{($data->address_street_number)?$data->address_street_number:old('address_street_number')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Street')}} <span class="text-danger">*</span></label>
                                    <input type="text" name="address_street" id="address_street" class="form-control"
                                        value="{{($data->address_street)?$data->address_street:old('address_street')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('City')}} <span class="text-danger">*</span></label>

                                    <input type="text" name="address_city" id="address_city" class="form-control"
                                        value="{{($data->address_city)?$data->address_city:old('address_city')}}">
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('City')}} (EN) <span class="text-danger">*</span></label>
                                    <input type="text" name="address_city_en" id="address_city_en" class="form-control"
                                        value="{{($data->address_city_en)?$data->address_city_en:old('address_city_en')}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Country')}} <span class="text-danger">*</span></label>
                                    <select class="form-control custom-select" name="country_id" id="country_id"
                                        data-placeholder="Choose a Country">
                                        <option value="">Please Select</option>
                                        @if(isset($countryList) && !empty($countryList))
                                        @foreach($countryList as $countryListVal)
                                        <option value="{{$countryListVal->id}}"
                                            {{(isset($data->country_id) && ($data->country_id == $countryListVal->id))?"selected=selected":""}}>
                                            {{(currentLanguage()==1)?$countryListVal->country_name:$countryListVal->country_name_en}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Mobile Number')}} <span
                                            class="text-danger">*</span></label>
                                    <div class="input-group">
                                        @if(currentLanguage()==1)
                                        <input type="text" id="mobile_phone" name="mobile_phone" class="form-control"
                                            placeholder=""
                                            value="{{($data->mobile_phone)?$data->mobile_phone:old('mobile_phone')}}">
                                        <div class="input-group-append w-25">
                                            <input type="text" id="country_code" name="country_code"
                                                class="input-group-text"
                                                value="{{($data->country_code)?$data->country_code:old('country_code')}}"
                                                readonly="">
                                        </div>
                                        
                                    @else
                                    <div class="input-group-append w-25">
                                        <input type="text" id="country_code" name="country_code"
                                            class="input-group-text"
                                            value="{{($data->country_code)?$data->country_code:old('country_code')}}"
                                            readonly="">
                                    </div>
                                    <input type="text" id="mobile_phone" name="mobile_phone" class="form-control"
                                        placeholder=""
                                        value="{{($data->mobile_phone)?$data->mobile_phone:old('mobile_phone')}}">
                                    @endif
                                     </div>
                                </div>
                            </div>

                            <!--/span-->
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Zip Code')}} <span class="text-danger">*</span></label>
                                    <input type="text" name="address_zip" id="address_zip" class="form-control"
                                        value="{{($data->address_zip)?$data->address_zip:old('address_zip')}}">
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Fax')}}</label>
                                    <input type="text" name="fax" id="fax" class="form-control"
                                        value="{{($data->fax)?$data->fax:old('fax')}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Passport Id')}} </label>
                                    <input type="text" name="passport_id" id="passport_id" class="form-control"
                                        value="{{($data->passport_id)?$data->passport_id:old('passport_id')}}">
                                </div>
                            </div>

                        </div>

                        <h3 class="box-title m-t-40">{{translate('Skills')}}</h3>
                        <hr>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{translate('User Skills')}} </label>
                                    <select id="skill_id" name="skill_id[]" class="select2 form-control custom-select"
                                        style="width: 100%" multiple="multiple" data-placeholder="Choose">
                                        @if(isset($skills) && !empty($skills))
                                        @foreach($skills as $skillsVal)
                                        <option value="{{$skillsVal->id}}"
                                            {{isset($userSkills) && in_array($skillsVal->id,$userSkills)?"selected":""}}>
                                            {{(currentLanguage() == 1)?$skillsVal->skill_name:$skillsVal->skill_name_en}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                        </div>


                    </div>
                    <!-- <h3 class="box-title m-t-40">{{translate('Password')}}</h3>
                    <hr>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('Password')}} <span class="text-danger">*</span></label>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('Confirm Password')}} <span class="text-danger">*</span></label>
                                <input type="password" name="confirmed" id="confirmed" class="form-control">
                            </div>
                        </div>
                    </div> -->

                    <h3 class="box-title m-t-40">{{translate('Assign Role')}}</h3>
                        <hr>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('User Role')}} <span class="text-danger">*</span></label>
                                    <select class="form-control custom-select" name="role_id" id="role_id" data-placeholder="Choose a Role" >
                                        <option value="">Please Select</option>
                                        @if(isset($roles) && !empty($roles))
                                        @foreach($roles as $rolesVal)
                                        <option value="{{$rolesVal->id}}" {{(isset($data->role_id) && ($data->role_id == $rolesVal->id))?"selected=selected":""}}>{{(currentLanguage()==1)?$rolesVal->role_name:$rolesVal->role_name_en}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                        </div>

                    <h3 class="box-title m-t-40 accordion-toggle" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">{{translate('New Fields')}}</h3>
                    <hr>
                    <div class="collapse" id="collapseExample">
  <div class="card card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('Info Id')}} <span class="text-danger">*</span></label>
                                <input type="text" name="info_id" id="info_id" class="form-control"
                                    value="{{($data->info_id)?$data->info_id:old('info_ids')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('Owner Email')}} <span class="text-danger">*</span></label>
                                <input type="text" name="owner_email" id="owner_email" class="form-control"
                                    value="{{($data->owner_email)?$data->owner_email:old('owner_email')}}">
                            </div>
                        </div>
                    </div>
                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('Sagir Owner Id')}} <span class="text-danger">*</span></label>
                                <input type="text" name="sagir_owner_id" id="sagir_owner_id" class="form-control"
                                    value="{{($data->sagir_owner_id)?$data->sagir_owner_id:old('sagir_owner_id')}}">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('is_current_owner')}} <span class="text-danger">*</span></label>
                                <input type="text" name="is_current_owner" id="is_current_owner" class="form-control"
                                    value="{{($data->is_current_owner)?$data->is_current_owner:old('is_current_owner')}}">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <!--/row-->

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('Order Id')}} <span class="text-danger">*</span></label>
                                <input type="text" id="order_id" name="order_id" class="form-control"
                                    value="{{($data->order_id)?$data->order_id:old('order_id')}}">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">{{translate('New Sid')}} <span
                                        class="text-danger">*</span></label>
                                <input type="text" id="new_sid" name="new_sid" class="form-control"
                                    value="{{($data->new_sid)?$data->new_sid:old('new_sid')}}">
                            </div>
                        </div>

                        <!--/span-->
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('Record Type')}} <span class="text-danger">*</span></label>
                                <input type="text" name="record_type" id="record_types" class="form-control"
                                    value="{{($data->record_type)?$data->record_type:old('record_type')}}">
                            </div>
                        </div>
                        <!--/span-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('Data Id')}}</label>
                                <input type="text" name="data_id" id="data_id" class="form-control"
                                    value="{{($data->data_id)?$data->data_id:old('data_id')}}">
                            </div>
                        </div>
                        <!--/span-->
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{translate('Owner Code')}} </label>
                                <input type="text" name="owner_code" id="owner_code" class="form-control"
                                    value="{{($data->owner_code)?round($data->owner_code):old('owner_code')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('New Org Data Id')}} </label>
                                    <input type="text" name="new_org_data_id" id="new_org_data_id" class="form-control"
                                        value="{{($data->new_org_data_id)?round($data->new_org_data_id):old('owner_code')}}">
                                </div>
                            </div>


                        </div>
                        <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('New Fill Date')}} </label>
                                    <input type="text" name="new_fill_date" id="new_fill_date" class="form-control"
                                        value="{{($data->new_fill_date)?convertDate($data->new_fill_date):old('new_fill_date')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{translate('New Filler IP')}} </label>
                                        <input type="text" name="new_filler_ip" id="new_filler_ip" class="form-control"
                                            value="{{($data->new_filler_ip)?round($data->new_filler_ip):old('new_filler_ip')}}">
                                    </div>
                                </div>
    
                        </div>
                         <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Club Id')}} </label>
                                    <input type="text" name="club_id" id="club_id" class="form-control"
                                        value="{{($data->club_id)?round($data->club_id):old('club_id')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{translate('Owner Payment Sum')}} </label>
                                        <input type="text" name="owner_payment_sum" id="owner_payment_sum" class="form-control"
                                            value="{{($data->owner_payment_sum)?round($data->owner_payment_sum):old('owner_payment_sum')}}">
                                    </div>
                                </div>
    
                        </div>

                        <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Owner Payment Last4')}} </label>
                                    <input type="text" name="owner_payment_last4" id="owner_payment_last4" class="form-control"
                                        value="{{($data->owner_payment_last4)?round($data->owner_payment_last4):old('owner_payment_last4')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{translate('Member Status')}} </label>
                                        <input type="text" name="member_status" id="member_status" class="form-control"
                                            value="{{($data->member_status)?round($data->member_status):old('member_status')}}">
                                    </div>
                                </div>
    
                        </div>

                        <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Special Key')}} </label>
                                    <input type="text" name="special_key" id="special_key" class="form-control"
                                        value="{{($data->special_key)?round($data->special_key):old('special_key')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{translate('Expire Date')}} </label>
                                        <input type="text" name="expire_date" id="expire_date" class="form-control"
                                            value="{{($data->expire_date)?convertDate($data->expire_date):old('expire_date')}}">
                                    </div>
                                </div>
    
                        </div>

                        <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Owner Total Payment')}} </label>
                                    <input type="text" name="owner_total_payment" id="owner_total_payment" class="form-control"
                                        value="{{($data->owner_total_payment)?round($data->owner_total_payment):old('owner_total_payment')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{translate('Start Date')}} </label>
                                        <input type="text" name="start_date" id="start_date" class="form-control"
                                            value="{{($data->start_date)?convertDate($data->start_date):old('start_date')}}">
                                    </div>
                                </div>
    
                        </div>

                        <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Record Source')}} </label>
                                    <input type="text" name="record_source" id="record_source" class="form-control"
                                        value="{{($data->record_source)?round($data->record_source):old('record_source')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{translate('Is Judge')}} </label>
                                        <input type="text" name="is_judge" id="is_judge" class="form-control"
                                            value="{{($data->is_judge)?round($data->is_judge):old('is_judge')}}">
                                    </div>
                                </div>
    
                        </div>

                        <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('City')}} </label>
                                    <select class="form-control custom-select" name="city_id" id="city_id"
                                        data-placeholder="Choose a Country">
                                        <option value="">Please Select</option>
                                        @if(isset($countryList) && !empty($countryList))
                                        @foreach($HairsCity as $hc)
                                        <option value="{{$hc->Code_City}}"
                                            {{(isset($data->city_id) && ($data->city_id == $hc->Code_City))?"selected=selected":""}}>
                                            {{$hc->City_Name}}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>

                                   <!--  <input type="text" name="city_id" id="city_id" class="form-control"
                                        value="{{($data->city_id)?round($data->city_id):old('city_id')}}"> -->
                                </div>
                            </div>

                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{translate('Private Phone 1')}} </label>
                                        <input type="text" name="private_phone_1" id="private_phone_1" class="form-control"
                                            value="{{($data->private_phone_1)?round($data->private_phone_1):old('private_phone_1')}}">
                                    </div>
                                </div>
    
                        </div>
                             <div class="row">


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{translate('Private Phone 2')}} </label>
                                    <input type="text" name="private_phone_2" id="private_phone_2" class="form-control"
                                        value="{{($data->private_phone_2)?round($data->private_phone_2):old('private_phone_2')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{translate('Note')}} </label>
                                        <input type="text" name="note" id="note" class="form-control"
                                            value="{{($data->note)?round($data->note):old('note')}}">
                                    </div>
                                </div>
    
                        </div>


                        <div class="row">

                            <!--/span-->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Image')}}</label>
                                    <input type="file" id="image" name="image" class="form-control"
                                        placeholder="">
                                    @if(!empty($data->image))
                                    <img src="{{$data->image}}" width="50">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Invoice Id')}}</label>
                                    <input type="text" id="invoice_id" name="invoice_id"
                                        class="form-control" placeholder=""
                                        value="{{($data->invoice_id)?$data->invoice_id:old('invoice_id')}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row">

                            <!--/span-->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Breed Id')}}</label>
                                    <input type="text" id="breed_id" name="breed_id"
                                        class="form-control" placeholder=""
                                        value="{{($data->breed_id)?$data->breed_id:old('breed_id')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('User Key')}}</label>
                                    <input type="text" id="user_key" name="user_key"
                                        class="form-control" placeholder=""
                                        value="{{($data->user_key)?$data->user_key:old('user_key')}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>

                        <div class="row">

                            <!--/span-->

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Is Breed Manager')}}</label>
                                    <input type="text" id="is_breed_manager" name="is_breed_manager"
                                        class="form-control" placeholder=""
                                        value="{{($data->is_breed_manager)?$data->is_breed_manager:old('is_breed_manager')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Payment Status')}}</label>
                                    <input type="text" id="payment_status" name="payment_status"
                                        class="form-control" placeholder=""
                                        value="{{($data->payment_status)?$data->payment_status:old('payment_status')}}">
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                </div>
            </div>
                  

                    

                        
                       
                        

                       
                        @if(!empty($dogs_details))
                        <h3 class="box-title m-t-40">{{translate('Dogs Details')}}</h3>
                        <hr>
                        
                        <div class="table-responsive" id="table-responsive">
                            <table id="user_list_migration"
                                class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg"
                                data-paging="true" data-paging-size="7" style="">
                                <thead>
                                    <tr class="footable-header">
                                        
                                        <th>{{translate('SagirID')}}</th>
                                        <th>{{translate('Dog Name')}} (Hebrew)</th>
                                        <th>{{translate('Dog Name')}} (English)</th>
                                        <th>{{translate('RaceID')}}</th>
                                        <th>{{translate('Current Owner ID')}}</th>
                                        <th>{{translate('BirthDate')}}</th>
                                        <th>Action</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($dogs_details) && !empty($dogs_details))
                                    
                                    @foreach($dogs_details as $dog)
                                    <tr>
                                       
                                        <td>{{isset($dog->SagirID)?round($dog->SagirID):'N/A'}}
                                        </td>
                                        
                                        <td>{{isset($dog->Heb_Name)?$dog->Heb_Name:'N/A'}}
                                        </td>
                                        
                                        <td>{{isset($dog->Eng_Name)?$dog->Eng_Name:'N/A'}}
                                        </td>
                                    
                                        <td>{{isset($dog->RaceID)?round($dog->RaceID):'N/A'}}
                                        </td>
                                        <td>{{isset($dog->CurrentOwnerId)?round($dog->CurrentOwnerId):'N/A'}}
                                        </td>
                                        <td>{{isset($dog->BirthDate)?$dog->BirthDate:'N/A'}}
                                        </td>
                                        <td>
                                            <a href="javascript:" class="delete_record" user_id="{{$data->id}}" sagir_id="{{round($dog->SagirID)}}">
                                                <i class="fas fa-trash" data-toggle="tooltip"
                                                    data-original-title="Delete"></i>
                                            </a>
                                        </td>
                                        
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                              
                            </table>
                        </div>
                        @endif

                        <input type="hidden" value="{{translate('Are you sure you want to do this reclamation?')}}?"
                        id="popup_title">
                    <input type="hidden"
                        value="{{translate('New users will be created according to the settings you have selected')}}"
                        id="popup_message">
                    <input type="hidden" value="{{translate('Of course it is!')}}" id="popup_yes_button">
                    <input type="hidden" value="{{translate('I regretted ...')}}" id="popup_no_button">

                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>
                            {{translate('Update')}}</button>
                        <a href="{{backUrl('user')}}" class="btn btn-dark">{{translate('Cancel')}}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"
    rel="stylesheet">
<link href="{{ asset('assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/node_modules/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
@endsection

@section('jquery')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js">
</script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script src="{{ asset('assets/node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>
<script src="{{ asset('js/pages/mask.init.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('js/admin/user_add_edit.js')}}"></script>


{!! isset($jsValidator)?$jsValidator->selector('#userEditForm'):'' !!}

@toastr_render
@endsection

@section('javascript')
<script type="text/javascript">
    $(function () {
        $('#birth_date').datetimepicker({
            timepicker:false,
            maxDate: '0',
            format: '{{settingParam("date-format")}}',
        });
        $('#new_fill_date').datetimepicker({
            timepicker:false,
            maxDate: '0',
            format: '{{settingParam("date-format")}}',
        });
        $('#expire_date').datetimepicker({
            timepicker:false,
            maxDate: '0',
            format: '{{settingParam("date-format")}}',
        });
        $('#start_date').datetimepicker({
            timepicker:false,
            maxDate: '0',
            format: '{{settingParam("date-format")}}',
        });
    });
    $(".select2").select2();
</script>
@endsection