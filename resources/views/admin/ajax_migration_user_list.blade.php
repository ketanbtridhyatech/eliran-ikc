<table id="user_list_migration"
class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg"
data-paging="true" data-paging-size="7" style="">
<thead>
    <tr class="footable-header">
        <th><input type="checkbox" class="control-input" name="checkAllUsers"
                id="checkAllUsers" value="all"></th>
        <th>@sortablelink('id', translate('Internal record ID'))</th>
        <th>@sortablelink('owner_code', translate('Owner Code'))</th>
        <th>@sortablelink('first_name', translate('Name'))</th>
        <th>@sortablelink('email', translate('Email'))</th>
        <th>@sortablelink('record_type', translate('Record Type'))</th>
        <th>@sortablelink('phone', translate('Phone'))</th>
        <th>@sortablelink('sagir_owner_id', translate('Sagir'))</th>
    </tr>
</thead>
<tbody>
    @if(isset($listing) && !empty($listing))
    @php $i=(($listing->currentPage() - 1) * $listing->perPage()+1);
    
    @endphp
    @foreach($listing as $listingVal)
    <tr class="{{!empty($listingVal->migration_status)?'bg-gray':''}}">
        <td>
            <input type="checkbox" class="control-input usercheck"
                name="userCheck[]" id="userCheck{{$listingVal->id}}"
                value="{{$listingVal->id}}">
        </td>
        <!-- <td class="footable-first-visible">{{$i++}}</td>-->
        <td>{{$listingVal->id}}</td>
        <td>{{isset($listingVal->owner_code)?number_format($listingVal->owner_code):'N/A'}}
        </td>
        @if(currentLanguage()==1)
        <td>{{isset($listingVal->first_name)?$listingVal->first_name." ".$listingVal->last_name:'N/A'}}
        </td>
        @else
        <td>{{isset($listingVal->first_name_en)?$listingVal->first_name_en." ".$listingVal->last_name_en:'N/A'}}
        </td>
        @endif
        <td>{{isset($listingVal->email)?$listingVal->email:'N/A'}}</td>

        <td>{{isset($listingVal->record_type)?$listingVal->record_type:'N/A'}}
        </td>
        <td>{{isset($listingVal->phone)?$listingVal->phone:'N/A'}}
        </td>
        <td>{{isset($listingVal->sagir_owner_id)?number_format($listingVal->sagir_owner_id):'N/A'}}
        </td>

    </tr>
    @endforeach
    @endif
</tbody>
<tfoot>
    <tr class="footable-paging">
        <td colspan="8">
            <div class="footable-pagination-wrapper">
                <div class="text-right">
                  {!!$listing->appends(\Request::except('page'))->render() !!} 
                </div>
                <div class="divider">
                </div>
               
            </div>
        </td>
    </tr>
</tfoot>
</table>