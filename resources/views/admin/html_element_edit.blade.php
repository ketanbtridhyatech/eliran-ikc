@extends('admin.layout.final')
@section('title')
Html Element Management
@endsection
@section('pageTitle')
HTML Element Management
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('settings')}}">Html Element Management</a></li>
            <li class="breadcrumb-item active">Edit Html Element</li>
        </ol>
        
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">Edit Html Element</h4>
            </div>
            <form name="admin_html_element_update" id="admin_html_element_update" method="post" action="{{backUrl('admin_html_element/update')}}">
                <input type="hidden" name="id" value="{{$data->id}}">
                @csrf
                <!-- <div class="card-body">
                    <h4 class="card-title">Person Info</h4>
                </div>
                <hr> <--></-->
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Slug:</label>
                            <input type="text" class="form-control" id="slug" name="slug" value="{{$data->slug}}" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="name">Html Element:</label>
                            <select class="form-control" id="value" name="html_element">
                                <option value="">{{translate('Select')}}</option>
                                <option value="textbox" {{$data->html_element=="textbox"?'selected':''}}>{{translate('TextBox')}}</option>
                                <option value="checkbox" {{$data->html_element=="checkbox"?'selected':''}}>{{translate('Checkbox')}}</option>
                                <option value="select_list" {{$data->html_element=="select_list"?'selected':''}}>{{translate('SelectList')}}</option>
                                <option value="radio_button" {{$data->html_element=="radio_button"?'selected':''}}>{{translate('Radio Button')}}</option>
                                <option value="file_upload" {{$data->html_element=="file_upload"?'selected':''}}>{{translate('File Upload')}}</option>
                                <option value="textarea" {{$data->html_element=="textarea"?'selected':''}}>{{translate('TextArea')}}</option>
                            </select>
                            
                        </div>
                    </div>
                    <div class="card-body">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Update</button>
                        <a href="{{backUrl('admin_html_element')}}" class="btn btn-dark">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('css')
<link href="{{ asset('assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('jquery')
<script type="text/javascript" src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#admin_html_element_update'):'' !!}

@toastr_render
@endsection

@section('javascript')
@endsection