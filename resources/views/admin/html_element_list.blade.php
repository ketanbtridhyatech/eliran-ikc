@extends('admin.layout.final')
@section('title')
Html Element Management
@endsection
@section('pageTitle')
Html Element Management
@endsection
@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">Home</a></li>
            <li class="breadcrumb-item active">Html Element Management</li>
        </ol>
        <a href="{{backUrl('admin_html_element/add')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</a>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
         <div class="card-body">
            <div class="table-responsive">
                <table id="html_element" class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg" data-paging="true" data-paging-size="7" style="">
                    <thead>
                            <th class="footable-first-visible">No</th>
                            <th>@sortablelink('name', 'Name')</th>
                            <th>@sortablelink('slug', 'Slug')</th>
                            <th>@sortablelink('html_element', 'HTML Element')</th>
                            <th>Action</th>
                        
                    </thead>
                    <tbody>
                        @if(isset($listing) && !empty($listing))
                        @php $i=(($listing->currentPage() - 1) * $listing->perPage()+1);@endphp
                        @foreach($listing as $listingVal)
                        <tr>
                            <td class="footable-first-visible">{{$i++}}</td>
                            <td>{{isset($listingVal->name)?$listingVal->name:'N/A'}}</td>
                            <td>{{isset($listingVal->slug)?$listingVal->slug:'N/A'}}</td>
                            <td>{{isset($listingVal->html_element)?$listingVal->html_element:'N/A'}}</td>
                            <td>
                                <a href="{{backUrl('admin_html_element/edit/'.$listingVal->id)}}">
                                    <i class="fas fa-edit" data-toggle="tooltip" data-original-title="Edit"></i>
                                </a>
                                @if($listingVal->html_element == 'select_list' || $listingVal->html_element == 'radio_buutton' || $listingVal->html_element == 'checkbox')
                               <a href="{{backUrl('admin_html_element/admin_html_element_option/'.$listingVal->id)}}">
                                    <i class="fas fa-align-justify" data-toggle="tooltip" data-original-title="Html Element Options"></i>
                                </a>
                                @endif
                                <!-- <a onclick="return confirm('Are you sure want to delete this record?')" href="{{backUrl('settings/delete/'.$listingVal->id)}}">
                                    <i class="far fa-trash-alt" data-toggle="tooltip" data-original-title="Delete"></i>
                                </a> -->
                                
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr class="footable-paging">
                            <td colspan="8">
                                <div class="footable-pagination-wrapper">
                                    <div class="text-right">{!! $listing->appends(\Request::except('page'))->render() !!}</div>
                                    <div class="divider">
                                    </div>
                                    <!-- <span class="label label-primary">1 of 2</span> -->
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>


        </div>
    </div>
</div>
</div>
@endsection
@section('jquery')

@toastr_render
@endsection