@extends('admin.layout.login-register.final')
@section('title')
Login
@endsection

@section('content')
<ul class="navbar-nav my-lg-0">
    <li class="nav-item">
        <div class="language-picker">
            <select name="language_id" id="language_id_login">
                <option value="0" {{(currentLanguage() == 0)?"selected=selected":""}}>English</option>
                <option value="1" {{(currentLanguage() == 1)?"selected=selected":""}}>Hebrew</option>
            </select>
        </div>
    </li>
</ul>
<section id="wrapper">
    <div class="login-register" style="background-image:url({{asset('assets/images/background/login-register.jpg')}});">
        <div class="login-box card">
            <div class="card-body">
                @if(Session::get('message'))
                <div class="alert {{Session::get('msg-class')}} alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{{ Session::get('message') }}</strong>
                </div>
                @endif

                <form class="form-horizontal form-material" id="mobileForm" method="post" action="">
                    <h3 class="text-center m-b-20">{{translate('Sign In')}}</h3>
                    <!-- <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" name="mobile_phone" id="mobile_phone" type="text" placeholder="Mobile" value="" autocomplete="off"> 
                        </div>
                    </div> -->
                    <div class="form-group ">
                    <div class="input-group my-group"> 
                        <select id="country_code" id="country_code" class="selectpicker form-control" data-live-search="true" title="Please select Country Code">
                            @foreach($countries as $countriesVal)
                            <option value="{{$countriesVal->country_code}}">{{$countriesVal->country_code}}</option>
                            @endforeach
                        </select> 
                        <input type="text" class="form-control w-50" name="mobile_phone" id="mobile_phone" placeholder="{{translate('Mobile')}}" autocomplete="off"/>
                        
                    </div>
                    </div>
                    <div class="form-group text-center">
                        <div class="col-xs-12 p-b-20">
                            <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">{{translate('Generate OTP')}}</button>
                        </div>
                        <!-- <div class="ml-auto">
                            <a href="{{backUrl('login')}}" id="to-loginForm" class="text-muted"><i class="fas fa-lock m-r-5"></i> Log In With Email / Password</a> 
                        </div> -->

                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center" >
                            {{translate("Can't access? click here")}}
                             <a href="#" class="text-info m-l-5"  data-toggle="modal" data-target="#signUpModal"><b>{{translate('Sign Up')}}</b></a>
                        </div>
                    </div>
                </form>

                <form class="form-horizontal form-material" id="otpForm" method="post" action="">
                    <h3 class="text-center m-b-20">{{translate('Sign In')}}</h3>
                    <div class="form-group ">

                       <div class="form-group">
                           <div class="col-xs-12">
                               <input class="form-control" name="otp" id="otp" type="text"  placeholder="{{translate('otp')}}" value=""> </div>
                           </div>

                           <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">{{translate('Submit')}}</button>
                            </div>
                            <!-- <div class="ml-auto">
                                <a href="{{backUrl('login')}}" id="to-loginForm" class="text-muted"><i class="fas fa-lock m-r-5"></i> Log In With Email / Password</a> 
                            </div> -->

                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center" >
                                {{translate("Can't access? click here")}}
                                 <a href="#" class="text-info m-l-5"  data-toggle="modal" data-target="#signUpModal"><b>{{translate('Sign Up')}}</b></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @endsection
<div class="modal fade" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display:none">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">{{translate('Sign Up')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form  class="form-horizontal form-material" id="signup" method="post" action="">
                      <div class="modal-body">
                        
                        <div class="col-md-12 form-group">
                            <label>{{translate('First Name')}}</label><input type="text" name="first_name" class="form-control" id="first_name">
                        </div>
                        <div class="col-md-12 form-group">
                            <label>{{translate('Last Name')}}</label><input type="text" name="last_name" class="form-control" id="last_name">
                        </div>
                        <div class="col-md-12 form-group">
                            <label style="margin-bottom:3%;">{{translate('Mobile Phone')}}</label>
                            <div class="input-group my-group"> 

                                <select id="country_code_signup" name="country_code_signup" class="selectpicker form-control" data-live-search="true" title="Please select Country Code">
                                    @foreach($countries as $countriesVal)
                                    <option value="{{$countriesVal->country_code}}">{{$countriesVal->country_code}}</option>
                                    @endforeach
                                </select> 
                                <input type="text" class="form-control w-50" name="mobile_phone_signup" id="mobile_phone_signup" placeholder="Mobile" autocomplete="off"/>
                                
                            </div>
                            <label id="mobile_phone_signup-error" class="error" for="mobile_phone_signup"></label>
                        </div>
                        <div class="col-md-12 form-group">
                            <label>{{translate('Remarks')}} </label><br/><textarea id="remarks" name="remarks" class="form-control"></textarea>
                        </div>
                        
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{translate('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{translate('Submit')}}</button>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
                 <input type="hidden" id="Mobile_number_invalid" value="{{translate('Mobile number invalid!')}}">
                <input type="hidden" id="User_invalid" value="{{translate('User invalid!')}}">
                <input type="hidden" id="invalid_OTP" value="{{translate('invalid OTP!')}}">
                <input type="hidden" id="OTP_Successfully_sent" value="{{translate('OTP Successfully sent!')}}">
                <input type="hidden" id="Login_Successfully" value="{{translate('Login Successfully.')}}">

    @section('jquery')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script src="{{ asset('assets/node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js')}}"></script>
    <script src="{{ asset('js/pages/mask.init.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/admin/login.js')}}"></script>
    @endsection
