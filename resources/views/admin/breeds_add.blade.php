@extends('admin.layout.final')
@section('title')
{{translate('Breeds Management')}}
@endsection
@section('pageTitle')
{{translate('Breeds Management')}}
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('skills')}}">{{translate('Breeds Management')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Add Breeds')}}</li>
        </ol>
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Add Breed')}}</h4>
            </div>
            <form name="breedsAddForm" id="breedsAddForm" method="post" action="{{backUrl('breeds/store')}}">
                @csrf
                
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">{{translate('Breed Name')}}:</label>
                            <input type="text" class="form-control" id="breed_name" name="breed_name" value="{{old('breed_name')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Breed Name')}} (EN):</label>
                            <input type="text" class="form-control" id="breed_name_en" name="breed_name_en" value="{{old('breed_name_en')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Breed Code')}}:</label>
                            <input type="text" class="form-control" id="breed_code" name="breed_code" value="{{old('breed_code')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Desc')}}:</label>
                            <input type="text" class="form-control" id="breed_desc" name="breed_desc" value="{{old('breed_desc')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Group ID')}}:</label>
                            <input type="text" class="form-control" id="group_id" name="group_id" value="{{old('group_id')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('FCICODE')}}:</label>
                            <input type="text" class="form-control" id="FCICODE" name="FCICODE" value="{{old('FCICODE')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Fci Group')}}:</label>
                            <input type="text" class="form-control" id="fci_group" name="fci_group" value="{{old('fci_group')}}">
                        </div>

                        <div class="form-group">
                            <label for="name">{{translate('Status')}}:</label>
                            <select class="form-control custom-select" name="breed_status" id="breed_status" data-placeholder="Choose a Status" >
                                <option value="">{{translate('Please Select')}}</option>
                                <option value="For Use">{{translate('For Use')}}</option>
                                <option value="Not for use">{{translate('Not for use')}}</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="card-body">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{translate('Save')}}</button>
                        <a href="{{backUrl('breeds')}}" class="btn btn-dark">{{translate('Cancel')}}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#breedsAddForm'):'' !!}

@toastr_render
@endsection