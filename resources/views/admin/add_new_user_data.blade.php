 <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{translate('First Name Hebrew')}}</label>
                                    <input type="text" class="form-control first_name set_direction_rtl" id="first_name_{{$request->divid}}" name="first_name[]"
                                        data-id="0">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Last Name Hebrew')}}</label>
                                    <input type="text" class="form-control set_direction_rtl" id="last_name_{{$request->divid}}" name="last_name[]">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{translate('City Hebrew')}}</label>
                                    <input type="text" class="form-control set_direction_rtl" id="city_{{$request->divid}}" name="city[]">
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{translate('First Name English')}}</label>
                                    <input type="text" class="form-control first_name_en set_direction" id="first_name_en_{{$request->divid}}"
                                        name="first_name_en[]" data-id="0">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Last Name English')}}</label>
                                    <input type="text" class="form-control set_direction" id="last_name_en_{{$request->divid}}"
                                        name="last_name_en[]">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{translate('City English')}}</label>
                                    <input type="text" class="form-control set_direction" id="city_en_{{$request->divid}}" name="city_en[]">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Street name')}}</label>
                                    <input type="text" class="form-control" id="street_{{$request->divid}}" name="street[]">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('HousesNumber')}}</label>
                                    <input type="text" class="form-control" id="house_number_{{$request->divid}}" name="house_number[]">
                                </div>
                            </div>
                            
                        </div>
                       
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Email')}}</label>
                                    <input type="text" class="form-control set_direction" id="email_{{$request->divid}}" name="email[]">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{translate('SocialID')}}</label>
                                    <input type="text" class="form-control social_id_validation" id="social_id_{{$request->divid}}"
                                        name="social_id[]" data-id="0">
                                    <span class="error_social_id_0 error"></span>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    
                                </div>
                            </div>
                        </div>
                        
                        