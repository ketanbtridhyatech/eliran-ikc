@extends('admin.layout.final')
@section('title')
{{translate('Breedinghouses Management')}}
@endsection
@section('pageTitle')
{{translate('Breedinghouses Management')}}
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('breeds')}}">{{translate('Breedinghouses Management')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Edit Breedinghouses')}}</li>
        </ol>
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Edit Breedinghouses')}}</h4>
            </div>
           <form name="breedsAddForm" id="breedsAddForm" method="post" action="{{backUrl('breedinghouses/update')}}">
                 <input type="hidden" name="id" id="id" value="{{$data->id}}">
                @csrf
                
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">{{translate('English name')}}:</label>
                            <input type="text" class="form-control" id="breedinghouses_EngName" name="breedinghouses_EngName" value="{{($data->EngName)?$data->EngName:old('breedinghouses_EngName')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('hebrew name')}} (EN):</label>
                            <input type="text" class="form-control" id="breedinghouses_HebName" name="breedinghouses_HebName" value="{{($data->HebName)?$data->HebName:old('breedinghouses_HebName')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('GidulCode')}}:</label>
                            <input type="number" class="form-control" id="breedinghouses_gidulcode" name="breedinghouses_gidulcode" value="{{($data->GidulCode)?round($data->GidulCode):old('breedinghouses_gidulcode')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('MegadelCode')}}:</label>
                            <input type="number" class="form-control" id="breedinghouses_megadelCode" name="breedinghouses_megadelCode" value="{{($data->MegadelCode)?round($data->MegadelCode):old('breedinghouses_megadelCode')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Phone')}}:</label>
                            <input type="number" class="form-control" id="breedinghouses_phone" name="breedinghouses_phone" value="{{($data->Phone)?$data->Phone:old('breedinghouses_phone')}}">
                        </div>
                        
                    </div>
                    
                    <div class="card-body">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{translate('Save')}}</button>
                        <a href="{{backUrl('breedinghouses')}}" class="btn btn-dark">{{translate('Cancel')}}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#breedsAddForm'):'' !!}

@toastr_render
@endsection