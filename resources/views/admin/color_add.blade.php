@extends('admin.layout.final')
@section('title')
{{translate('Color Management')}}
@endsection
@section('pageTitle')
{{translate('Color Management')}}
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('colors')}}">{{translate('Color Management')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Add Color')}}</li>
        </ol>
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Add Color')}}</h4>
            </div>
            <form name="colorAddForm" id="colorAddForm" method="post" action="{{backUrl('colors/store')}}">
                @csrf
                
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">{{translate('English name')}}:</label>
                            <input type="text" class="form-control" id="colors_EngName" name="colors_EngName" value="{{old('colors_EngName')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('hebrew name')}} (EN):</label>
                            <input type="text" class="form-control" id="colors_HebName" name="colors_HebName" value="{{old('colors_HebName')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Old Code')}}:</label>
                            <input type="number" class="form-control" id="colors_OldCode" name="colors_OldCode" value="{{old('colors_OldCode')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Remark')}}:</label>
                            <input type="text" class="form-control" id="colors_remark" name="colors_remark" value="{{old('colors_remark')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Status')}}:</label>
                            <select class="form-control custom-select" name="colors_status" id="colors_status" data-placeholder="Choose a Status" >
                                <option value="">{{translate('Please Select')}}</option>
                                <option value="for use">{{translate('For Use')}}</option>
                                <option value="not for use">{{translate('Not for use')}}</option>
                            </select>
                        </div>
                        
                    </div>
                    
                    <div class="card-body">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{translate('Save')}}</button>
                        <a href="{{backUrl('colors')}}" class="btn btn-dark">{{translate('Cancel')}}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#colorAddForm'):'' !!}

@toastr_render
@endsection