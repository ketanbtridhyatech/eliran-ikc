@extends('admin.layout.final')
@section('title')
Html Element Management
@endsection
@section('pageTitle')
Html Element Management
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('settings')}}">Html Element Management</a></li>
            <li class="breadcrumb-item active">Add HTML Element</li>
        </ol>
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">Add Html Element</h4>
            </div>
            <form name="html_element_add" id="html_element_add" method="post" action="{{backUrl('admin_html_element/store')}}">
                @csrf
                <!-- <div class="card-body">
                    <h4 class="card-title">Person Info</h4>
                </div>
                <hr> <--></-->
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Slug:</label>
                            <input type="text" class="form-control" id="slug" name="slug" value="{{old('slug')}}" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="name">HtmlElement:</label>
                            <select class="form-control" id="value" name="html_element">
                                <option value="">{{translate('Select')}}</option>
                                <option value="textbox">{{translate('TextBox')}}</option>
                                <option value="checkbox">{{translate('Checkbox')}}</option>
                                <option value="select_list">{{translate('SelectList')}}</option>
                                <option value="radio_button">{{translate('Radio Button')}}</option>
                                <option value="file_upload">{{translate('File Upload')}}</option>
                                <option value="textarea">{{translate('TextArea')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-body">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                        <a href="{{backUrl('admin_html_element')}}" class="btn btn-dark">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('js/admin/html_element.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#html_element_add'):'' !!}

@toastr_render
@endsection