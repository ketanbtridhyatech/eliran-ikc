@if(! $dogs_details->isEmpty())
<div class="row">
    <div class="col-md-4">
        <a href="{{backUrl('data_compare/export_excel/'.$request->id)}}" target="_blank" id="export_excel" name="export_excel" class="btn btn-success mt-3">{{translate('Export to Excel')}}</a>
    </div>
</div>
@endif
<div class="table-responsive" id="table-responsive">

    <table id="user_list_migration"
        class="table table-bordered m-t-10 sticky-header table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg"
        data-paging="true" data-paging-size="7">
        <thead>
            <tr class="footable-header">
                <th>{{translate('No')}}</th>
                <th>{{translate('Sagir')}}</th>
                <th>{{translate('Dog Name English')}}</th>
                <th> {{translate('Dog Name Hebrew')}}</th>
                <th>{{translate('Breed Name')}}</th>
                <th>{{translate('Color Name')}}</th>
                <th>{{translate('ColorID')}}</th>
                <th>{{translate('RaceID')}}</th>
                <th>{{translate('Sex')}}</th>
                <th>{{translate('OwnerCode')}}</th>
                <th>{{translate('Data ID')}}</th>
                <th>{{translate('HairID')}}</th>
            </tr>
        </thead>
        <tbody>
            @if(! $dogs_details->isEmpty())
           
            @php
            $i=1;
            @endphp

            @foreach($dogs_details as $dog)
            <tr>
                <td>{{$i}}</td>
                <td>{{!empty($dog->SagirID)?round($dog->SagirID):''}}</td>
                <td>{{$dog->Eng_Name}}</td>
                <td>{{$dog->Heb_Name}}</td>
                <td>{{!empty($dog->breed_name)?$dog->breed_name->BreedName:''}}</td>
                <td>{{!empty($dog->color_name)?$dog->color_name->ColorNameHE:''}}</td>
                <td>{{round($dog->ColorID)}}</td>
                <td>{{round($dog->RaceID)}}</td>
                <td>{{$dog->Sex}}</td>
                <td>{{round($dog->CurrentOwnerId)}}</td>
                <td>{{!empty($dog->user_data_id)?$dog->user_data_id->data_id:''}}</td>
                <td>{{round($dog->HairID)}}</td>
            </tr>
            @php
            $i++;
            @endphp
            @endforeach
            @else
            <tr>
                <td colspan="12">No Records</td>
            </tr>
            @endif

        </tbody>
    </table>
</div>