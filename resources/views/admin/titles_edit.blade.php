@extends('admin.layout.final')
@section('title')
{{translate('Title Management')}}
@endsection
@section('pageTitle')
{{translate('Title Management')}}
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('titles')}}">{{translate('Title Management')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Edit Title')}}</li>
        </ol>
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Edit Title')}}</h4>
            </div>
           <form name="titleAddForm" id="titleAddForm" method="post" action="{{backUrl('titles/update')}}">
                 <input type="hidden" name="id" id="id" value="{{$data->id}}">
                @csrf
                
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">{{translate('Title Name')}}:</label>
                            <input type="text" class="form-control" id="titles_name" name="titles_name" value="{{($data->TitleName)?$data->TitleName:old('titles_name')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Title Code')}}:</label>
                            <input type="number" class="form-control" id="titles_code" name="titles_code" value="{{($data->TitleCode)?round($data->TitleCode):old('titles_code')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Remark')}}:</label>
                            <input type="text" class="form-control" id="titles_remark" name="titles_remark" value="{{($data->Remark)?$data->Remark:old('titles_remark')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Desc')}}:</label>
                            <input type="text" class="form-control" id="titles_desc" name="titles_desc" value="{{($data->TitleDesc)?$data->TitleDesc:old('titles_desc')}}">
                        </div>
                        
                    </div>
                    
                    <div class="card-body">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{translate('Save')}}</button>
                        <a href="{{backUrl('titles')}}" class="btn btn-dark">{{translate('Cancel')}}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#titleAddForm'):'' !!}

@toastr_render
@endsection