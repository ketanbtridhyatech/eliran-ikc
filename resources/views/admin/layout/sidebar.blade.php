<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                        aria-expanded="false">

                        @if(displayProfile() != '')
                        <img src="{{displayProfile()}}" alt="user-img" class="img-circle">
                        @endif
                        <span class="hide-menu"> {{displayName()}}</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="javascript:void(0)"><i class="ti-user"></i> {{translate('My Profile')}}</a></li>
                        <!-- <li><a href="javascript:void(0)"><i class="ti-wallet"></i> {{translate('My Balance')}}</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
                        <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li> -->
                        <li><a href="{{backUrl('logout')}}"><i class="fa fa-power-off"></i> {{translate('Logout')}}</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-small-cap">--- {{translate('PERSONAL')}}</li>
                <!-- <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-speedometer"></i><span class="hide-menu">Dashboard <span class="badge badge-pill badge-cyan ml-auto">4</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="index.html">Minimal </a></li>
                        <li><a href="index2.html">Analytical</a></li>
                        <li><a href="index3.html">Demographical</a></li>
                        <li><a href="index4.html">Modern</a></li>
                    </ul>
                </li> -->

                <li> <a class="waves-effect waves-dark" href="{{backUrl('user')}}" aria-expanded="false"><i
                            class="fas fa-users text-danger"></i><span
                            class="hide-menu">{{translate('User Management')}}</span></a></li>

                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"
                        style="white-space: nowrap;"><i class="fas fa-users text-success"></i><span
                            class="hide-menu">{{translate('People Management')}}</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="#">{{translate('Show All')}}</a></li>
                        <li><a href="#">{{translate('Add New')}}</a></li>
                        <li><a href="#">{{translate('Skills')}}</a></li>
                        <li><a href="#">{{translate('Search & Reports')}}</a></li>
                        <li><a href="#">{{translate('Logs')}}</a></li>

                    </ul>
                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="fas fa-users text-danger"></i><span
                            class="hide-menu">{{translate('Migration')}}</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="{{backUrl('migration')}}" aria-expanded="false">{{translate('Migration')}}</a>
                            <a href="{{backUrl('migration/data_compare')}}" aria-expanded="false">{{translate('Data Compare')}}</a>
                        </li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="fas fa-cog text-success"></i><span class="hide-menu">{{translate('Settings')}}
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{backUrl('module')}}">{{translate('Modules')}} </a></li>
                        <li><a href="{{backUrl('country')}}">{{translate('Countries')}} </a></li>
                        <li><a href="{{backUrl('roles')}}">{{translate('Roles')}} </a></li>
                        <li><a href="{{backUrl('skills')}}">{{translate('Skills')}} </a></li>
                        <li><a href="{{backUrl('translation')}}">{{translate('Translation')}}</a></li>
                        <li><a href="{{backUrl('settings')}}">{{translate('Site Settings')}}</a></li>
                        <li><a href="{{backUrl('admin_html_element')}}">{{translate('Dynamic Html Element')}}</a></li>
                         <li><a href="{{backUrl('template')}}">{{translate('template')}}</a></li>

                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="fas fa-cog text-success"></i><span class="hide-menu">{{translate('Table Management')}}
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{backUrl('get_dog_list')}}">{{translate('Dog List')}} </a></li>
                        <li><a href="{{backUrl('breeds')}}">{{translate('Breeds Management')}} </a></li>
                        <li><a href="{{backUrl('breedinghouses')}}">{{translate('Breedinghouses Management')}} </a></li>
                         <li><a href="{{backUrl('titles')}}">{{translate('Title Management')}} </a></li>
                          <li><a href="{{backUrl('colors')}}">{{translate('Color Management')}} </a></li>

                    </ul>
                </li>
               

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->