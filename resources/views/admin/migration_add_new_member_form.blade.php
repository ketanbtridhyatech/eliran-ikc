<div class="card" id="card_{{$request->count_div + 1}}">
    <div class="card-header" id="heading_{{$request->count_div + 1}}">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse_{{$request->count_div + 1}}"
            aria-expanded="true" aria-controls="collapse_{{$request->count_div + 1}}">
            <h5 class="mb-0" id="form_header_{{$request->count_div + 1}}">{{translate('Add Owner')}}</h5>
        </button>
        <a href="javascript:" class="{{(currentLanguage()==1)?'float-left':'float-right'}} delete_owner" data-id="{{$request->count_div + 1}}"><i
            class="fa fa-times"></i></a>
    </div>

    <div id="collapse_{{$request->count_div + 1}}" class="collapse show_validation"
        aria-labelledby="heading_{{$request->count_div + 1}}" data-parent="#accordian-3">
        <div class="card-body">

            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">&nbsp;</label>
                            <input type="text" class="form-control mobile_change" id="mobile_number_{{$request->count_div + 1}}"
                                name="mobile_number[]" dynamic_id = "{{$request->count_div + 1}}" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">{{translate('Mobile Number')}}</label>
                            <select class="form-control" name="m_prefix[]"
                                id="m_prefix_{{$request->count_div + 1}}">
                                <option value="">{{translate('Select')}}</option>
                                <option value="50">050</option>
                                <option value="51">051</option>
                                <option value="52">052</option>
                                <option value="53">053</option>
                                <option value="54">054</option>
                                <option value="55">055</option>
                                <option value="56">056</option>
                                <option value="58">058</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">{{translate('First Name Hebrew')}}</label>
                            <input type="text" class="form-control first_name" id="first_name_{{$request->count_div + 1}}"
                                name="first_name[]" data-id="{{$request->count_div + 1}}" >
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">{{translate('Last Name Hebrew')}}</label>
                            <input type="text" class="form-control" id="last_name_{{$request->count_div + 1}}"
                                name="last_name[]">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">{{translate('City Hebrew')}}</label>
                            <input type="text" class="form-control" id="city_{{$request->count_div + 1}}" name="city[]">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">{{translate('Street name')}}</label>
                            <input type="text" class="form-control" id="street_{{$request->count_div + 1}}"
                                name="street[]">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">{{translate('HouseNumber')}}</label>
                            <input type="text" class="form-control" id="house_number_{{$request->count_div + 1}}"
                                name="house_number[]">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">{{translate('Zipcode')}}</label>
                            <input type="text" class="form-control" id="zip_code_{{$request->count_div + 1}}"
                                name="zip_code[]">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">{{translate('First Name English')}}</label>
                            <input type="text" class="form-control first_name_en set_direction" id="first_name_en_{{$request->count_div + 1}}"
                                name="first_name_en[]" data-id="{{$request->count_div + 1}}">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">{{translate('Last Name English')}}</label>
                            <input type="text" class="form-control set_direction" id="last_name_en_{{$request->count_div + 1}}"
                                name="last_name_en[]">
                        </div>
                    </div>
                        
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">{{translate('City English')}}</label>
                            <input type="text" class="form-control set_direction" id="city_en_{{$request->count_div + 1}}"
                                name="city_en[]">
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">{{translate('Email')}}</label>
                            <input type="text" class="form-control set_direction" id="email_{{$request->count_div + 1}}"
                                name="email[]">
                        </div>
                    </div>
                
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">{{translate('SocialID')}}</label>
                            <input type="text" class="form-control social_id_validation" id="social_id_{{$request->count_div + 1}}"
                                name="social_id[]" data-id = "{{$request->count_div + 1}}">
                                <span class="error_social_id_{{$request->count_div + 1}} error"></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">{{translate('Birth Date')}}</label>
                            <div class="form-row form-group" id="show_date_fields">
                                <div class="col-md-4">
                                    <select class="col-12" id="year_{{$request->count_div + 1}}"
                                        name="year[]">
                                        <option value="">{{translate('Year')}}</option>
                                        @for ($a=2019;$a>=1919;$a--)
                                        <option value="{{$a}}">{{$a}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="col-12" id="month_{{$request->count_div + 1}}"
                                        name="month[]">
                                        <option value="">{{translate('Month')}}</option>
                                        @for($a=1;$a<=12;$a++) <option value="{{$a}}">{{$a}}
                                            </option>
                                            @endfor
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="col-12" id="day_{{$request->count_div + 1}}"
                                        name="day[]">
                                        <option value="">{{translate('Day')}}</option>
                                        @for($a=1;$a<=31;$a++) <option value="{{$a}}">{{$a}}
                                            </option>
                                            @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">{{translate('Country')}}</label>
                            <select class="form-control custom-select" id="country_{{$request->count_div + 1}}" name="country[]">
                                @if(!empty($countryList))
                                @foreach($countryList as $co)
                                @if(currentLanguage()==1)
                                <option value="{{$co->id}}">{{$co->country_name}}</option>
                                @else
                                <option value="{{$co->id}}">{{$co->country_name_en}}</option>
                                @endif
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">{{translate('Old Record Status:')}}</label>
                            <select class="form-control custom-select" name="old_record_status[]"
                                id="old_record_status_{{$request->count_div + 1}}s">
                                <option value="Partial Migration">Partial Migration</option>
                                <option value="Done">Done</option>
                            </select>
                        </div>
                    </div>-->

                </div>

            </div>

        </div>
    </div>
</div>