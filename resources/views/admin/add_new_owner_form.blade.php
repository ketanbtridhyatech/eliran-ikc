<div class="card show_validation" id="owner_{{$request->count_div + 1}}">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">{{translate('Owner')}} {{$request->count_div + 1}}</h4>

                </div>
                <div class="card-body">

                    @csrf
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="breeding_date">{{translate('Name')}}</label>
                                    <input type="text" class="form-control" id="dog_name_{{$request->count_div + 1}}" name="dog_name[]"
                                        value="{{(!empty($request->dog_name))?$request->dog_name:''}}"
                                        >
                                </div>
                            </div>
                            <div class="col-md-5">
                            	 <div class="row">
                                    <div class="col-md-10">
                                    	<div class="form-group">
                                    <label for="">{{translate('Mobile Number')}}</label>
                                    <div class="input-group my-group">
                                     @if (currentLanguage() == 1)
                                     <input type="text" data-id="{{$request->count_div + 1}}" class="form-control w-50 mobile_phone" onblur="checkMobileNumber({{$request->count_div + 1}})" name="mobile_number[]"
                                            id="mobile_phone_{{$request->count_div + 1}}" autocomplete="off" />
                                        <select id="country_code_{{$request->count_div + 1}}" name="country_code[]" class="selectpicker form-control"
                                            data-live-search="true" title="Please select Country Code">
                                            @foreach($countries as $countriesVal)
                                            <option value="{{$countriesVal->country_code}}">
                                                {{$countriesVal->country_code}}</option>
                                            @endforeach
                                        </select>
                                        
                                    @else
                                            <select id="country_code_{{$request->count_div + 1}}" name="country_code[]" class="selectpicker form-control"
                                            data-live-search="true" title="Please select Country Code">
                                            @foreach($countries as $countriesVal)
                                            <option value="{{$countriesVal->country_code}}">
                                                {{$countriesVal->country_code}}</option>
                                            @endforeach
                                        </select>
                                        <input type="text" data-id="{{$request->count_div + 1}}" class="form-control w-50 mobile_phone" onblur="checkMobileNumber({{$request->count_div + 1}})" name="mobile_number[]"
                                            id="mobile_phone_{{$request->count_div + 1}}" autocomplete="off" />
                                    @endif         
                                    
                                    </div>
                                </div>
                                    </div>
                                    <div class="col-md-2">
                                    	<label for="">&nbsp;</label>
                                    	<input type="button" class="btn btn-success border d-block" onclick="sendOtp({{$request->count_div + 1}})" name="otp" id="otp_{{$request->count_div + 1}}" value="{{translate('otp')}}" style="white-space: pre-wrap">
                                    </div>
                                 </div>
                                
                                
                            </div>
                           <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-8">
                                    	<div class="form-group otp_verify_{{$request->count_div + 1}}">
                                       <label for="">{{translate('Enter otp')}}</label>
                                        <input type="text" class="form-control" id="userOtp_{{$request->count_div + 1}}" name="userOtp[]">
                                         <input type="hidden" name="verifyotp" id="verifyotp_{{$request->count_div + 1}}">
                                         <label id="errorotp_{{$request->count_div + 1}}" style="color:red">Incorrect Otp</label>
                                </div>
                                   	</div>
                                   	
                                   	<div class="col-md-2">
                                        <div class="otp_verify_{{$request->count_div + 1}}">
                                        <label for="">&nbsp;</label>
                                         <input type="button" class="btn btn-success d-block" name="verify[]" value="{{translate('Verify')}}" id="verify_{{$request->count_div + 1}}" onclick="verifyOtp({{$request->count_div + 1}})" style="white-space: pre-wrap">
                                         <input type="hidden" name="verify_status_{{$request->count_div + 1}}" id="verify_status_{{$request->count_div + 1}}" value="0">
                                        </div>
                                    </div>
                                
                            </div>
                            
                        </div>
                        <div class="ownerDetails_{{$request->count_div + 1}}">
                       
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">{{translate('Remarks')}}</label>
                                <textarea class="form-control set_direction" id="remark_{{$request->count_div + 1}}" name="remark[]"></textarea>
                                
                            </div>
                        </div>

                    </div>

                </div>

            </div>