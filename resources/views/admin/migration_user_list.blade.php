@extends('admin.layout.final')
@section('title')
{{translate('User Management')}}
@endsection
@section('pageTitle')
{{translate('Migration')}}
@endsection
@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Migration')}}</li>
        </ol>
        <!--  <a href="{{backUrl('user/add')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> {{translate('Create New')}}</a>-->
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <input type="hidden" name="header_text" id="header_text" value="{{translate('Add Owner')}}">
    @if(empty($getCheckValue))
    <div class="col-lg-3 col-xlg-2 col-md- opacity-0 check_opacity">
        @else
        <div class="col-lg-3 col-xlg-2 col-md-  check_opacity">
            @endif
            <div class="stickyside">
                <form action="{{backUrl('migration/add_new_user')}}" method="post" role="search" name="migration_user"
                    id="migration_user">
                    @csrf
                    <input type="hidden" name="checked_id" id="checked_id" value="">
                    <div id="accordian-3">
                        <div class="card">
                            <a class="card-header" id="heading11" href="#">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapse1"
                                    aria-expanded="true" aria-controls="collapse1">
                                    <h5 class="mb-0" id="form_header">{{translate('Add Owner')}}</h5>
                                </button>
                            </a>
                            <div id="collapse1" class="collapse show_validation" aria-labelledby="heading11"
                                data-parent="#accordian-3">
                                <div class="card-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">&nbsp;</label>
                                                    <input type="text" class="form-control mobile_change"
                                                        id="mobile_number" name="mobile_number[]" dynamic_id="0">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('Mobile Number')}}</label>
                                                    <select class="form-control" name="m_prefix[]" id="m_prefix"
                                                        dynamic_id="0">
                                                        <option value="">{{translate('Select')}}</option>
                                                        <option value="50">050</option>
                                                        <option value="51">051</option>
                                                        <option value="52">052</option>
                                                        <option value="53">053</option>
                                                        <option value="54">054</option>
                                                        <option value="55">055</option>
                                                        <option value="56">056</option>
                                                        <option value="58">058</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label
                                                        class="control-label">{{translate('First Name Hebrew')}}</label>
                                                    <input type="text" class="form-control first_name" id="first_name"
                                                        name="first_name[]" data-id="0">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label
                                                        class="control-label">{{translate('Last Name Hebrew')}}</label>
                                                    <input type="text" class="form-control" id="last_name"
                                                        name="last_name[]">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('City Hebrew')}}</label>
                                                    <input type="text" class="form-control" id="city" name="city[]">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('Street name')}}</label>
                                                    <input type="text" class="form-control" id="street" name="street[]">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('HousesNumber')}}</label>
                                                    <input type="text" class="form-control" id="house_number"
                                                        name="house_number[]">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('Zipcode')}}</label>
                                                    <input type="text" class="form-control" id="zip_code"
                                                        name="zip_code[]">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label
                                                        class="control-label">{{translate('First Name English')}}</label>
                                                    <input type="text" class="form-control first_name_en set_direction"
                                                        id="first_name_en" name="first_name_en[]" data-id="0">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label
                                                        class="control-label">{{translate('Last Name English')}}</label>
                                                    <input type="text" class="form-control set_direction"
                                                        id="last_name_en" name="last_name_en[]">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('City English')}}</label>
                                                    <input type="text" class="form-control set_direction" id="city_en"
                                                        name="city_en[]">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('Email')}}</label>
                                                    <input type="text" class="form-control set_direction" id="email"
                                                        name="email[]">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('SocialID')}}</label>
                                                    <input type="text" class="form-control social_id_validation"
                                                        id="social_id" name="social_id[]" data-id="0">
                                                    <span class="error_social_id_0 error"></span>
                                                </div>

                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('Birth Date')}}</label>
                                                    <div class="form-row form-group" id="show_date_fields">
                                                        <div class="col-md-4">
                                                            <select class="col-12 custom_select" id="year"
                                                                name="year[]">
                                                                <option value="">{{translate('Year')}}</option>
                                                                @for ($a=2019;$a>=1919;$a--)
                                                                <option value="{{$a}}">{{$a}}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="col-12" id="month" name="month[]">
                                                                <option value="">{{translate('Month')}}</option>
                                                                @for($a=1;$a<=12;$a++) <option value="{{$a}}">{{$a}}
                                                                    </option>
                                                                    @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="col-12" id="day" name="day[]">
                                                                <option value="">{{translate('Day')}}</option>
                                                                @for($a=1;$a<=31;$a++) <option value="{{$a}}">{{$a}}
                                                                    </option>
                                                                    @endfor
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">{{translate('Country')}}</label>
                                                    <select class="form-control" id="country" name="country[]">
                                                        @if(!empty($countryList))
                                                        @foreach($countryList as $co)
                                                        @if(currentLanguage()==1)
                                                        <option value="{{$co->id}}">{{$co->country_name}}</option>
                                                        @else
                                                        <option value="{{$co->id}}">{{$co->country_name_en}}</option>
                                                        @endif
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>


                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <!--  <div class="card">
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">{{translate('Old Record Status')}}</label>
                                <select class="form-control" name="old_record_status"
                                    id="old_record_status">
                                    <option value="Partial Migration">{{translate('Partial Migration')}}</option>
                                    <option value="Done">{{translate('Done')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>-->
                </form>
                <input type="hidden" value="{{translate('Are you sure you want to do this reclamation?')}}?"
                    id="popup_title">
                <input type="hidden"
                    value="{{translate('New users will be created according to the settings you have selected')}}"
                    id="popup_message">
                <input type="hidden" value="{{translate('Of course it is!')}}" id="popup_yes_button">
                <input type="hidden" value="{{translate('I regretted ...')}}" id="popup_no_button">
                <div class="row">
                    <div class="col-md-5">
                        <button type="button" name="save_data" id="save_data" class="btn btn-success"> <i
                                class="fa fa-check"></i> {{translate('Submit')}}</button>
                    </div>
                    <div class="col-md-7">
                        <button type="button" name="add_owner" value="1" class="btn btn-success" id="add_owner"> <i
                                class="fa fa-check"></i>
                            {{translate('Add new owner')}}</button>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" value="" id="delete_popup_title">
        <input type="hidden" value="{{translate('Are you sure you want to delete it')}}?" id="delete_popup_message">
        <input type="hidden" value="{{translate('Yes, kick it off')}}" id="delete_popup_yes_button">
        <input type="hidden" value="{{translate("Don't delete !!!")}}" id="delete_popup_no_button">
        <div class="col-lg-9 col-xlg-10 col-md-7">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <!-- <h4 class="card-title">Add Module</h4> -->
                                    <form action="" method="GET" role="search" id="search_form">
                                        <div class="col-md-12">
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label
                                                                class="control-label">{{translate('Search By Name')}}</label>
                                                            <input type="text" class="form-control" id="search_by_name"
                                                                name="search_by_name"
                                                                value="{{isset($request->search_by_name)?$request->search_by_name:'' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label
                                                                class="control-label">{{translate('Search By Phone')}}</label>
                                                            <input type="text" class="form-control" id="search_by_phone"
                                                                name="search_by_phone"
                                                                value="{{isset($request->search_by_phone)?$request->search_by_phone:'' }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label
                                                                class="control-label">{{translate('Search By Owner Code')}}</label>
                                                            <input type="text" class="form-control"
                                                                id="search_by_owner_code" name="search_by_owner_code"
                                                                value="{{isset($request->search_by_owner_code)?$request->search_by_owner_code:'' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label
                                                                class="control-label">{{translate('Search By Email')}}</label>
                                                            <input type="text" class="form-control" id="search_by_email"
                                                                name="search_by_email"
                                                                value="{{isset($request->search_by_email)?$request->search_by_email:'' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label
                                                                class="control-label">{{translate('Search By Dog Sagir')}}</label>
                                                            <input type="text" class="form-control"
                                                                id="search_by_dog_sagir" name="search_by_dog_sagir"
                                                                value="{{isset($request->search_by_dog_sagir)?$request->search_by_dog_sagir:'' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label
                                                                class="control-label">{{translate('Show New Users')}}</label><br>
                                                            <input type="checkbox" class="control-input ml-5"
                                                                id="search_by_new_user" name="search_by_new_user"
                                                                value="1"
                                                                {{(isset($request->search_by_new_user) && $request->search_by_new_user==1) ?'checked':'' }}>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3 hide show_new_fields">
                                                        <div class="form-group">
                                                            <label
                                                                class="control-label">{{translate('First Name')}}</label>
                                                            <input type="text" class="form-control"
                                                                id="search_by_first_name" name="search_by_first_name"
                                                                value="{{isset($request->search_by_first_name)?$request->search_by_first_name:'' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 hide show_new_fields">
                                                        <div class="form-group">
                                                            <label
                                                                class="control-label">{{translate('Last Name')}}</label>
                                                            <input type="text" class="form-control"
                                                                id="search_by_last_name" name="search_by_last_name"
                                                                value="{{isset($request->search_by_last_name)?$request->search_by_last_name:'' }}">
                                                        </div>

                                                    </div>


                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" name="search_submit" value="1"
                                                            class="btn btn-success"> <i class="fa fa-check"></i>
                                                            {{translate('Search')}}</button>
                                                        <a href="#" class="btn btn-dark"
                                                            id="reset">{{translate('Reset')}}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h6 class="card-subtitle"></h6>


                                        <?php 
                                                        $pagination_menu = config('constant.show_records');
                                                        $per_page_record = empty($request->record_per_page_count) ? config('constant.recordPerPage') : $request->record_per_page_count;
                                                    ?>
                                        <select id="record_per_page" name="record_per_page" class="form-control mt-3">
                                            @foreach($pagination_menu as $pm)
                                            <option value="{{$pm}}" {{$pm == $record_per_page ?'selected' : ''}}>{{$pm}}
                                            </option>
                                            @endforeach
                                        </select>


                                    </form>
                                    <div class="table-responsive" id="table-responsive">

                                        <table id="user_list_migration"
                                            class="table table-bordered m-t-10 sticky-header table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg"
                                            data-paging="true" data-paging-size="7" style="">
                                            <thead>
                                                <tr class="footable-header">
                                                    <th><input type="checkbox" class="control-input usercheck"
                                                            name="checkAllUsers" id="checkAllUsers" value="all"></th>
                                                    <th>@sortablelink('id', translate('Internal record ID'))</th>
                                                    <th>@sortablelink('owner_code', translate('Owner Code'))</th>
                                                    <th>@sortablelink('first_name', translate('Name'))</th>
                                                    <th>@sortablelink('email', translate('Email'))</th>
                                                    <th>@sortablelink('record_type', translate('Record Type'))</th>
                                                    <th>@sortablelink('phone', translate('Phone'))</th>
                                                    <th>@sortablelink('sagir_owner_id', translate('Sagir'))</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(isset($listing) && !empty($listing))
                                                @php $i=(($listing->currentPage() - 1) * $listing->perPage()+1);

                                                @endphp
                                                @foreach($listing as $listingVal)
                                                <tr class="{{!empty($listingVal->migration_status)?'bg-gray':''}}">
                                                    <td>
                                                        <?php
                                                         $checked = "";
                                                        if(!empty($getCheckValue)){
                                                            if(in_array($listingVal->id, $getCheckValue)){
                                                                $checked = "checked";
                                                            }else{
                                                                $checked = "";
                                                            }
                                                        }
                                                        ?>
                                                        <input type="checkbox" class="control-input usercheck"
                                                            name="userCheck[]" id="userCheck{{$listingVal->id}}"
                                                            value="{{$listingVal->id}}" {{$checked}}>
                                                    </td>
                                                    <!-- <td class="footable-first-visible">{{$i++}}</td>-->
                                                    <td>{{$listingVal->id}}</td>
                                                    <td>{{isset($listingVal->owner_code)?round($listingVal->owner_code):'N/A'}}
                                                    </td>
                                                    @if(currentLanguage()==1)
                                                    <td>{{isset($listingVal->first_name)?$listingVal->first_name." ".$listingVal->last_name:'N/A'}}
                                                    </td>
                                                    @else
                                                    <td>{{isset($listingVal->first_name_en)?$listingVal->first_name_en." ".$listingVal->last_name_en:'N/A'}}
                                                    </td>
                                                    @endif
                                                    <td>{{isset($listingVal->email)?$listingVal->email:'N/A'}}</td>

                                                    <td>{{isset($listingVal->record_type)?$listingVal->record_type:'N/A'}}
                                                    </td>
                                                    <td>{{isset($listingVal->phone)?$listingVal->phone:'N/A'}}
                                                    </td>
                                                    <td>{{isset($listingVal->sagir_owner_id)?round($listingVal->sagir_owner_id):'N/A'}}
                                                    </td>
                                                    <td>
                                                        <a href="{{backUrl('user/edit/'.$listingVal->id)}}">
                                                            <i class="fas fa-edit" data-toggle="tooltip"
                                                                data-original-title="Edit"></i>
                                                        </a>
                                                        @if($listingVal->record_type == "Native")
                                                        <a href="#" class="delete_native"
                                                            record_id="{{$listingVal->id}}">
                                                            <i class="fas fa-trash" data-toggle="tooltip"
                                                                data-original-title="Delete"></i>
                                                        </a>
                                                        @endif
                                                    </td>

                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                            <tfoot>
                                                <tr class="footable-paging">
                                                    <td colspan="9">
                                                        <div class="footable-pagination-wrapper">
                                                            <div class="text-right">
                                                                {!!$listing->appends(\Request::except('page'))->render()
                                                                !!}
                                                            </div>
                                                            <div class="divider">
                                                            </div>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
@if(currentLanguage()==1)
<style>
    .floatThead-container {
        right: 0 !important;
    }
</style>
@endif
<link href="{{ asset('assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css"
    href="{{ asset('assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" type="text/css"
    href="{{ asset('assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css')}}">

<link href="{{ asset('assets/node_modules/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<link href="{{ asset('assets/node_modules/bootstrap-table/dist/bootstrap-table.min.css')}}" rel="stylesheet"
    type="text/css" />

@endsection

@section('jquery')
<script src="{{asset('assets/node_modules/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('assets/node_modules/jquery-validation/additional-methods.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('assets/node_modules/bootstrap-table/dist/bootstrap-table.min.js')}}"></script>
<script src="{{asset('assets/node_modules/bootstrap-table/dist/bootstrap-table-locale-all.min.js')}}"></script>
<script src="{{ asset('js/admin/migration.js?'.date('ymdmis'))}}"></script>
@toastr_render
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        // $(".sticky-header").floatThead({scrollingTop:68});
        $(".usercheck").change(function(){
            
            if($(this).val() == 'all'){
                var checkVal = [];
                 
                 $("input:checkbox:checked").each(function (index, vals) {

                    
    
                        checkVal.push(vals.value);
    
                    
                });
            }else{
                var checkVal = [];
                checkVal.push($(this).val());
            }
            if(this.checked) {
                jQuery.ajax({
                    url: backendUrl + "/migration/checkbox",
                    method: 'get',
                    data: {
                        checkVal: checkVal.join(),
                    },
                    success: function (data) {
                    }
                });
            }else{
                jQuery.ajax({
                    url: backendUrl + "/migration/checkbox/unChecked",
                    method: 'get',
                    data: {
                        unCheckVal: checkVal.join(),
                    },
                    success: function (data) {
                    }
                });
            }

        });
    });
</script>
@if(Session::has('success'))
<script>
    Swal.fire({
    position: 'center',
    type: 'success',
    title: 'New users created successfully',
    showConfirmButton: false,
    timer: 2000
})
</script>
@endif
@endsection