@extends('admin.layout.final')
@section('title')
Html Element Option Management
@endsection
@section('pageTitle')
HTML Element Option Management
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('settings')}}">Html Element Option Management</a></li>
            <li class="breadcrumb-item active">Edit Html Element Option</li>
        </ol>
        
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">Edit Html Element Option</h4>
            </div>
            <form name="html_element_value_edit" id="html_element_value_edit" method="post" action="{{backUrl('admin_html_element/admin_html_element_option/update')}}">
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="element_id" value="{{$element_id}}">
                @csrf
                <!-- <div class="card-body">
                    <h4 class="card-title">Person Info</h4>
                </div>
                <hr> <--></-->
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Option Text Hebrew:</label>
                            <input type="text" class="form-control set_direction_rtl" id="option_text_hebrew" name="option_text_hebrew" value="{{$data->option_text_hebrew}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Option Text English:</label>
                            <input type="text" class="form-control set_direction" id="option_text_english" name="option_text_english" value="{{$data->option_text_english}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Option Value:</label>
                            <input type="text" class="form-control" id="option_value" name="option_value" value="{{$data->option_value}}">
                        </div>
                    </div>
                    <div class="card-body">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Update</button>
                        <a href="{{backUrl('admin_html_element/admin_html_element_option/'.$element_id)}}" class="btn btn-dark">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('css')
<link href="{{ asset('assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('jquery')
<script type="text/javascript" src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#html_element_value_edit'):'' !!}

@toastr_render
@endsection

