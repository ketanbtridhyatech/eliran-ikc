@extends('admin.layout.final')
@section('title')
Template Management
@endsection
@section('pageTitle')
Template Management
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('settings')}}">Template Management</a></li>
            <li class="breadcrumb-item active">Add Field</li>
        </ol>
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">Add Field</h4>
            </div>
            <form name="tempatesAddForm" id="tempatesAddForm" method="post" action="{{backUrl('template/store')}}">
                @csrf
                <!-- <div class="card-body">
                    <h4 class="card-title">Person Info</h4>
                </div>
                <hr> <--></-->
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Field Name:</label>
                            <input type="text" class="form-control" id="field" name="field" value="{{old('field')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Slug:</label>
                            <input type="text" class="form-control" id="slug" name="slug" value="{{old('slug')}}" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="name">English Text:</label>
                            <textarea class="form-control" id="english_text" name="english_text">{{old('english_text')}}</textarea>
                          
                        </div>
                        <div class="form-group">
                            <label for="name">Translated Text:</label>
                            <textarea class="form-control" id="translated_text" name="translated_text">{{old('translated_text')}}</textarea>
                        </div>
                        
                    </div>
                    <div class="card-body">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                        <a href="{{backUrl('template')}}" class="btn btn-dark">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('js/admin/template.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#tempatesAddForm'):'' !!}

@toastr_render
@endsection