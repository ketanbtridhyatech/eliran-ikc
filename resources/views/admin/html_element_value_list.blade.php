@extends('admin.layout.final')
@section('title')
Html Element Option Management
@endsection
@section('pageTitle')
Html Element Option Management
@endsection
@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">Home</a></li>
            <li class="breadcrumb-item active">Html Element Option Management</li>
        </ol>
        <a href="{{backUrl('admin_html_element/admin_html_element_option/add/'.$id)}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</a>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
         <div class="card-body">
            <div class="table-responsive">
                <table id="html_element" class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg" data-paging="true" data-paging-size="7" style="">
                    <thead>
                            <th class="footable-first-visible">No</th>
                            <th>@sortablelink('option_text_hebrew', 'Option Text Hebrew')</th>
                            <th>@sortablelink('option_text_english', 'Option Text English')</th>
                            <th>@sortablelink('option_value', 'Option Value')</th>
                            <th>Action</th>
                        
                    </thead>
                    <tbody>
                        @if(isset($listing) && !empty($listing))
                        @php $i=(($listing->currentPage() - 1) * $listing->perPage()+1);@endphp
                        @foreach($listing as $listingVal)
                        <tr>
                            <td class="footable-first-visible">{{$i++}}</td>
                            <td>{{isset($listingVal->option_text_hebrew)?$listingVal->option_text_hebrew:'N/A'}}</td>
                            <td>{{isset($listingVal->option_text_english)?$listingVal->option_text_english:'N/A'}}</td>
                            <td>{{isset($listingVal->option_value)?$listingVal->option_value:'N/A'}}</td>
                            <td>
                                <a href="{{backUrl('admin_html_element/admin_html_element_option/edit/'.$id."/".$listingVal->id)}}">
                                    <i class="fas fa-edit" data-toggle="tooltip" data-original-title="Edit"></i>
                                </a>
                             
                                <a onclick="return confirm('Are you sure want to delete this record?')" href="{{backUrl('admin_html_element/admin_html_element_option/delete/'.$id."/".$listingVal->id)}}">
                                    <i class="far fa-trash-alt" data-toggle="tooltip" data-original-title="Delete"></i>
                                </a> 
                                
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr class="footable-paging">
                            <td colspan="8">
                                <div class="footable-pagination-wrapper">
                                    <div class="text-right">{!! $listing->appends(\Request::except('page'))->render() !!}</div>
                                    <div class="divider">
                                    </div>
                                    <!-- <span class="label label-primary">1 of 2</span> -->
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>


        </div>
    </div>
</div>
</div>
@endsection
@section('jquery')

@toastr_render
@endsection