@extends('admin.layout.final')
@section('title')
{{translate('Breeds Management')}}
@endsection
@section('pageTitle')
{{translate('Breeds Management')}}
@endsection
@section('content')

@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item"><a href="{{backUrl('breeds')}}">{{translate('Breeds Management')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Edit Breed')}}</li>
        </ol>
    </div>
</div>
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Edit Breed')}}</h4>
            </div>
            <form name="skillsEditForm" id="skillsEditForm" method="post" action="{{backUrl('breeds/update')}}">
                 <input type="hidden" name="id" id="id" value="{{$data->id}}">
                @csrf
                
                <div class="form-body">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">{{translate('Breed Name')}}:</label>
                            <input type="text" class="form-control" id="breed_name" name="breed_name" value="{{($data->BreedName)?$data->BreedName:old('breed_name')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Breed Name')}} (EN):</label>
                            <input type="text" class="form-control" id="breed_name_en" name="breed_name_en" value="{{($data->BreedNameEN)?$data->BreedNameEN:old('breed_name_en')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Breed Code')}}:</label>
                            <input type="text" class="form-control" id="breed_code" name="breed_code" value="{{($data->BreedCode)?$data->BreedCode:old('breed_code')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Desc')}}:</label>
                            <input type="text" class="form-control" id="breed_desc" name="breed_desc" value="{{($data->Desc)?$data->Desc:old('breed_desc')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Group ID')}}:</label>
                            <input type="text" class="form-control" id="group_id" name="group_id" value="{{($data->GroupID)?$data->GroupID:old('group_id')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('FCICODE')}}:</label>
                            <input type="text" class="form-control" id="FCICODE" name="FCICODE" value="{{($data->FCICODE)?$data->FCICODE:old('FCICODE')}}">
                        </div>
                        <div class="form-group">
                            <label for="name">{{translate('Fci Group')}}:</label>
                            <input type="text" class="form-control" id="fci_group" name="fci_group" value="{{($data->fci_group)?$data->fci_group:old('fci_group')}}{{old('fci_group')}}">
                        </div>

                        <div class="form-group">
                            <label for="name">{{translate('Status')}}:</label>
                            <select class="form-control custom-select" name="breed_status" id="breed_status" data-placeholder="Choose a Status" >
                                <option value="">{{translate('Please Select')}}</option>
                                <option value="For Use" {{($data->status && $data->status == 'For Use')?"selected":""}}>{{translate('For Use')}}</option>
                                <option value="Not for use" {{($data->status && $data->status == 'Not for use')?"selected":""}}>{{translate('Not for use')}}</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="card-body">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{translate('Save')}}</button>
                        <a href="{{backUrl('breeds')}}" class="btn btn-dark">{{translate('Cancel')}}</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#skillsEditForm'):'' !!}

@toastr_render
@endsection