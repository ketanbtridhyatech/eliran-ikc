@extends('admin.layout.final')
@section('title')
{{translate('User Management')}}
@endsection
@section('pageTitle')
{{translate('Data Compare')}}
@endsection
@section('breadcrumb')
<div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{backUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Data Compare')}}</li>
        </ol>
        <!--  <a href="{{backUrl('user/add')}}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> {{translate('Create New')}}</a>-->
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <input type="hidden" name="header_text" id="header_text" value="{{translate('Add Owner')}}">
        <div class="col-lg-12 col-xlg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <!-- <h4 class="card-title">Add Module</h4> -->
                                    <form action="#" method="post" role="search" id="search_form">
                                        <div class="col-md-12">
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label
                                                                class="control-label">{{translate('UserID')}}</label>
                                                            <input type="text" class="form-control" id="search_by_id"
                                                                name="search_by_id" value="{{isset($request->search_by_id)?$request->search_by_id:'' }}">
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="button" name="search_submit" value="1"
                                                            class="btn btn-success" id="search_submit"> <i class="fa fa-check"></i>
                                                            {{translate('Search')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h6 class="card-subtitle"></h6>
                                    </form>
                                    <div id="dogs_data"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
@if(currentLanguage()==1)
<style>
    .floatThead-container {
        right: 0 !important;
    }
</style>
@endif
<link href="{{ asset('assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css"
    href="{{ asset('assets/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" type="text/css"
    href="{{ asset('assets/node_modules/datatables.net-bs4/css/responsive.dataTables.min.css')}}">

<link href="{{ asset('assets/node_modules/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<link href="{{ asset('assets/node_modules/bootstrap-table/dist/bootstrap-table.min.css')}}" rel="stylesheet"
    type="text/css" />

@endsection

@section('jquery')
<script src="{{asset('assets/node_modules/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('assets/node_modules/jquery-validation/additional-methods.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('assets/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('assets/node_modules/bootstrap-table/dist/bootstrap-table.min.js')}}"></script>
<script src="{{asset('assets/node_modules/bootstrap-table/dist/bootstrap-table-locale-all.min.js')}}"></script>
@toastr_render
@endsection

@section('javascript')
<script src="{{ asset('js/admin/data_compare.js')}}"></script>
@endsection