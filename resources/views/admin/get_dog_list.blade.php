@extends('admin.layout.final')
@section('title')
{{translate('Dog Management')}}
@endsection
@section('pageTitle')
{{translate('Dog Management')}}
@endsection
@section('breadcrumb')
@endsection
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
         <div class="card-body">
         	 <div class="col-md-12">
                <form action="" method="GET" role="search">

                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-3">

                                    <?php
                                    
                                        $date_formate=str_replace(array('d','dd'),"DD",settingParam("date-format"));
                                        $date_formate=str_replace(array('m','mm'),"MM",$date_formate);
                                        $date_formate=str_replace(array('y','yyyy','yy','Y'),"YYYY",$date_formate);

                                        if($request->has('search_by_birth_date') && $request->search_by_birth_date != ''){
                                          $value = (explode("-",$request->search_by_birth_date));?>
                                            <input type="hidden" name="startDate" id="startDate" value="{{trim($value[0])}}">
                                            <input type="hidden" name="endDate" id="endDate" value="{{trim($value[1])}}">
                                        <?php }else{ ?>
                                            <input type="hidden" name="startDate" id="startDate" value="">
                                            <input type="hidden" name="endDate" id="endDate" value="">
                                        <?php }

                                    ?>
                                    <input type="hidden" id="date_format" name="date_format" value="{{$date_formate}}">
                                    <input type="hidden" id="current_language" name="current_language"
                                        value="{{(currentLanguage()==1)?'he':'en'}}">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By birth date')}} :</label>
                                    <input type="text" class="form-control" id="search_by_birth_date" name="search_by_birth_date" value="{{isset($request->search_by_birth_date)?convertDate($request->search_by_birth_date):'' }}" readonly>
                                </div>
                            </div>
                           
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Dog Name')}} :</label>
                                    <input type="text" class="form-control" id="search_by_dog_name" name="search_by_dog_name" value="{{isset($request->search_by_dog_name)?$request->search_by_dog_name:'' }}">
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Chip')}} :</label>
                                    <input type="text" class="form-control" id="search_by_chip" name="search_by_chip" value="{{isset($request->search_by_chip)?$request->search_by_chip:'' }}">
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Gender')}} :</label>

                                    <select class="form-control custom-select" name="search_by_gender" id="search_by_gender" data-placeholder="Choose a gender" >
                                        <option value="">{{translate('Please Select')}}</option>
                                        
                                         <option value="1"  {{(isset($request->search_by_gender) && (trim($request->search_by_gender) == 1))?"selected=selected":""}}>Male</option>
                                         <option value="2"  {{(isset($request->search_by_gender) && (trim($request->search_by_gender) == 2))?"selected=selected":""}}>Female</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Breeding House')}} :</label>

                                    <select class="form-control custom-select" name="search_by_breeding_house" id="search_by_breeding_house" data-placeholder="Choose a breeding hourse" >
                                        <option value="">{{translate('Please Select')}}</option>
                                        
                                    </select>
                                </div>
                            </div>
                          <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By SagirId')}} :</label>
                                    <input type="text" class="form-control" id="search_by_sagirId" name="search_by_sagirId" value="{{isset($request->search_by_sagirId)?$request->search_by_sagirId:'' }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Owner Name')}} :</label>
                                    <input type="text" class="form-control" id="search_by_owner_name" name="search_by_owner_name" value="{{isset($request->search_by_owner_name)?$request->search_by_owner_name:'' }}">
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By BreedName')}} :</label>

                                    <select class="form-control custom-select" name="search_by_breed_name" id="search_by_breed_name" data-placeholder="Choose a gender" >
                                        <option value="">{{translate('Please Select')}}</option>
                                        @if(!empty($breedsdb))
                                        @foreach($breedsdb as $bdb)
                                         <option value="{{$bdb->BreedCode}}"  {{(isset($request->search_by_breed_name) && (trim($request->search_by_breed_name) == $bdb->BreedCode))?"selected=selected":""}}>
                                         @if(currentLanguage()==1)
                                         {{$bdb->BreedName}}
                                         @else
                                          {{$bdb->BreedNameEN}}
                                         @endif
                                         </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                       

                        
                        <div class="card-body">
                            <button type="submit" name="search_submit" value="1" class="btn btn-success"> <i class="fa fa-check"></i> {{translate('Search')}}</button>
                            <a href="{{backUrl('get_dog_list')}}" class="btn btn-dark">{{translate('Reset')}}</a>
                        </div>
                    </div>
                </form>
            </div>
            <h6 class="card-subtitle"></h6>
            
            <div class="table-responsive">
                <!-- <div class="d-none1" id="action-user-btn">          
                    <a href="javascript:void(0)" id="delete-user-btn" class="btn btn-danger"><i class="far fa-trash-alt" data-toggle="tooltip" data-original-title="{{translate('Delete')}}"></i> </a>
                    <a href="javascript:void(0)" id="active-user-btn" class="btn btn-success">{{translate('Active')}}</a>
                    <a href="javascript:void(0)" id="inactive-user-btn" class="btn btn-danger">{{translate('Inactive')}}</a>

                    <a href="{{backUrl('exportUsers')}}" id="export-user-btn" class="btn btn-outline-secondary btn-rounded float-right">Excel <i class="mdi mdi-file-excel"></i></a>
                </div>
 -->
                <table id="demo-foo-addrow" class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg" data-paging="true" data-paging-size="7" style="">
                    <thead>
                        <tr class="footable-header">
                            <!-- <th><input type="checkbox" class="control-input" name="checkAllUsers" id="checkAllUsers" value="all"></th> -->
                            <th class="footable-first-visible">No.</th>
                            <th>@sortablelink('SagirID', translate('SagirID'))</th>
                            <?php if(currentLanguage()==1){?>
                            	<th>@sortablelink('Heb_Name', translate('Dog Name'))</th>
                            <?php }else{?>
                            	<th>@sortablelink('Eng_Name', translate('Dog Name'))</th>
                            <?php }?>
                            
                            <th>{{translate('Owner Name')}}</th>
                            
                            <th>{{translate('BreedHouse')}}</th>
                            <th>@sortablelink('BirthDate', translate('BirthDate'))</th>
                            <th>{{translate('Action')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($listing) && !empty($listing))
                        @php $i=(($listing->currentPage() - 1) * $listing->perPage()+1);@endphp
                        @foreach($listing as $listingVal)
                        <tr>
                           <!--  <td>
                                <input type="checkbox" class="control-input usercheck" name="userCheck[]" id="userCheck{{$listingVal->id}}" value="{{$listingVal->id}}">
                            </td> -->
                            <td class="footable-first-visible">{{$i++}}</td>
                           
                            <td>{{isset($listingVal->SagirID)?round($listingVal->SagirID):'N/A'}}</td>
                            <?php if(currentLanguage()==1){?>
                           			 <td>{{isset($listingVal->Heb_Name)?$listingVal->Heb_Name:'N/A'}}</td>
                    		<?php }else{?>
                           			 <td>{{isset($listingVal->Eng_Name)?$listingVal->Eng_Name:'N/A'}}</td>
                           <?php }?>
                           <?php 
                                $name = [];
                                if(!empty($listingVal->owner_name)){
                                if(currentLanguage()==1){
                                    foreach ($listingVal->owner_name as $key => $value) {
                                        if(!empty($value->first_name)){
                                            $name[] = $value->first_name;
                                        }
                                      
                                    }
                                }else{
                                    foreach ($listingVal->owner_name as $key => $value) {
                                        if(!empty($value->first_name_en)){
                                            $name[] = $value->first_name_en;  
                                        }
                                    }
                                }
                            }

                            ?>

                           <td>{{!empty($name)?implode(',',array_unique($name)):'N/A'}}</td>
                           <td></td>
                             <td>{{isset($listingVal->BirthDate)?convertDate($listingVal->BirthDate):'N/A'}}<br/>{{isset($listingVal->CreationDateTime)?convertDate($listingVal->CreationDateTime):'N/A'}}</td>
                             <td></td>
                            
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr class="footable-paging">
                            <td colspan="8">
                                <div class="footable-pagination-wrapper">
                                    <div class="text-right">{!! $listing->appends(\Request::except('page'))->render() !!}</div>
                                    <div class="divider">
                                    </div>
                                    <!-- <span class="label label-primary">1 of 2</span> -->
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
         </div>
       	</div>
     </div>
 </div>
@endsection

@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/additional-methods.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment-with-locales.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment-with-locales.js')}}"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
       // alert($('#date_format').val());
       moment.locale($('#current_language').val());
        if($('#startDate').val() != ''){
            $('#search_by_birth_date').daterangepicker({
                    startDate: $('#startDate').val(),
                    endDate: $('#endDate').val(),
                    locale: {
                      format: $('#date_format').val(),
                    },
                    language: $('#current_language').val(),

            });
            $('#search_by_birth_date').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format($('#date_format').val()) + '-' +     picker.endDate.format($('#date_format').val()));
            });

            $('#search_by_birth_date').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });

       }else{
             $('#search_by_birth_date').daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                      format: $('#date_format').val(),
                    },
                    language: $('#current_language').val(),
                    useCurrent: false
            });
             $('#search_by_birth_date').on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format($('#date_format').val()) + '-' +     picker.endDate.format($('#date_format').val()));
            });

            $('#search_by_birth_date').on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
           
       }
    });     
</script>
@toastr_render
@endsection