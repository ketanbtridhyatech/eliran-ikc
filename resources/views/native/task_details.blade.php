@extends('native.layout.final')
@section('title')
{{translate('Task Details')}}
@endsection
@section('pageTitle')
{{translate('Task Details')}}
@endsection
@section('content')
<style type="text/css">
    .setFontSize {
        font-size: 20px;
    }
</style>

@section('breadcrumb')
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Task Details')}}</li>
        </ol>
    </div>
</div> -->
@endsection






<input type="hidden" id="current_language" name="current_language" value="{{(currentLanguage()==1)?'he':'en'}}">
<div class="setFontSize">
    <form name="tasksform" id="tasksform" method="post" action="{{frontUrl('tasks/update')}}">
        <div class="row">
            <div class="col-lg-12">
            <?php if(empty($review_manager)){?>
                   @include('native.task_review_manage')
            <?php } ?>

            <?php if(((in_array(Auth::user()->id,$edit_permission) && !empty($review_manager) && $review_manager->user_id == Auth::user()->id ) || in_array($setting_details->value,$skill_details)) && count($pdf_details->toArray()) != 2)  { ?>
                    @include('native.task_chipping_details')
            <?php } ?>
            <?php if((in_array($setting_details->value,$skill_details) &&
                            in_array(Auth::user()->id,$edit_permission))){ ?>
                    @include('native.task_status')
            <?php }?>

            <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="mb-0 text-white">{{translate('Breeding Page')}}</h4>
                    </div>
                    <div class="card-body">

                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="">
                                        <label for="breeding_date">{{translate('Breeding Date')}}</label>&nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->BreddingDate)?convertDate($breeding_data->BreddingDate):''}}</label>
                                        <!-- <input type="text"
                                            class="form-control {{!empty($breeding_data)?"readonly_checkbox":''}}"
                                            id="breeding_date" name="breeding_date"
                                            value=""
                                            readonly> -->
                                    </div>
                                    <div class="">
                                        <label for="maleId">{{translate('Male ID')}}</label>
                                        &nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->MaleSagirId)?round($breeding_data->MaleSagirId):''}}</label>
                                        <!--  <input type="text" class="form-control" id="maleId" name="maleId"
                                            value=""
                                            {{!empty($breeding_data)?"readonly":''}}> -->
                                    </div>
                                    <div class="">
                                        <label for="foreignMale">
                                            {{translate('Male More Than 2')}}
                                        </label>&nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->Male_More_Than_2)?translate('Yes'):translate('No')}}</label>
                                        <!--  <input type="text" class="form-control"
                                            value="" readonly> -->

                                    </div>
                                    <div class="">
                                        <label for="maleId">{{translate('Male More Than 5')}}</label>&nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->Male_More_Than_5)?translate('Yes'):translate('No')}}</label>
                                        <!-- <input type="text" class="form-control"
                                            value="" readonly> -->
                                    </div>
                                    <div class="">
                                        <label for="maleId">{{translate('Male DNA')}}</label>
                                        &nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->Male_DNA )?translate('Yes'):translate('No')}}</label>
                                        <!--  <input type="text" class="form-control"
                                            value="" readonly> -->
                                    </div>
                                    <div class="">
                                        <label for="foreignMale">
                                            {{translate('Foreign Male')}}
                                        </label>
                                        &nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->Foreign_Male_Records)?translate('Yes'):translate('No')}}</label>
                                        <!--  <input type="text" class="form-control"
                                            value="{{!empty($breeding_data->Foreign_Male_Records)?"Yes":"No"}}" readonly
                                            name="foreign_male" id="foreign_mail">
 -->
                                    </div>
                                    <div class="">
                                        <label for="maleId">{{translate('Male Breeding Not Approved')}}</label>
                                        &nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->Male_Breeding_Not_Approved)?translate('Yes'):translate('No')}}</label>
                                        <!-- <input type="text" class="form-control"
                                            value="{{!empty($breeding_data->Male_Breeding_Not_Approved)?'Yes':'No'}}"
                                            readonly> -->
                                    </div>
                                    <div class="">
                                        <label for="foreignMale">
                                            {{translate('Male Rebreed 5')}}
                                        </label>&nbsp&nbsp :<br />
                                        <label>{{$breeding_data->male_rebreed_5 }}</label>
                                        <!--  <textarea class="form-control"
                                            readonly></textarea> -->

                                    </div>
                                    <div class="">
                                        <label for="maleId">{{translate('Male Rebreed 2')}}</label>&nbsp&nbsp :<br />
                                        <label>{{$breeding_data->male_rebreed_2 }}</label>
                                        <!--  <textarea class="form-control"
                                            readonly></textarea> -->
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="">
                                        <label for="femaleId">{{translate('Female ID')}}</label>&nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->SagirId)?round($breeding_data->SagirId):''}}</label>
                                        <!--  <input type="text" class="form-control" id="femaleId" name="femaleId"
                                            value=""
                                            readonly> -->
                                    </div>
                                    <div class="">
                                        <label for="foreignMale">
                                            {{translate('Breed Mismatch')}}
                                        </label>&nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->BreedMismatch)?translate('Yes'):translate('No')}}</label>
                                        <!-- <input type="text" class="form-control"
                                            value="{{!empty($breeding_data->BreedMismatch)?"Yes":"No"}}" readonly> -->

                                    </div>
                                    <div class="">
                                        <label for="rules_is_owner">{{translate('Rules IsOwner')}}</label>&nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->Rules_IsOwner)?translate('Yes'):translate('No')}}</label>
                                        <!--  <input type="text" class="form-control"
                                            value="" readonly> -->
                                    </div>
                                    <div class="">
                                        <label for="foreignMale">
                                            {{translate('Female DNA')}}
                                        </label>
                                        &nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->Female_DNA)?translate('Yes'):translate('No')}}</label>
                                        <!--  <input type="text" class="form-control"
                                            value="{{!empty($breeding_data->Female_DNA)?"Yes":"No"}}" readonly> -->

                                    </div>
                                    <div class="">
                                        <label for="foreignMale">
                                            {{translate('Female Breeding Not Approved')}}
                                        </label>
                                        &nbsp&nbsp :
                                        &nbsp&nbsp<label>{{!empty($breeding_data->Female_Breeding_Not_Approved)?translate('Yes'):translate('No')}}</label>
                                        <!--  <input type="text" class="form-control"
                                            value=""
                                            readonly name="foreign_male" id="foreign_mail"> -->

                                    </div>
                                    <div>

                                    </div>
                                    <div class="">
                                        <label for="maleId">{{translate('Female Rate')}}</label>&nbsp&nbsp :<br />
                                        <label>{{$breeding_data->female_rate}}</label>
                                        <!-- <textarea class="form-control"
                                            readonly></textarea> -->
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="">
                                        <label for="foreignMale">
                                            {{translate('Generation Notes')}}
                                        </label>&nbsp&nbsp :<br />
                                        <?php
                                        $convert_to_array=explode(',<br>',$breeding_data->generations_note);
                                        ?>
                                        <label>{{implode(',<br>',$convert_to_array)}}</label>
                                        <!--  <textarea class="form-control"
                                            readonly></textarea> -->

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">

                    <div class="card-header bg-info">
                        <h4 class="mb-0 text-white">{{translate('Birthing Page')}}</h4>
                    </div>
                    <div class="card-body">
                        @csrf
                        <div class="form-body">
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="breeding_date">{{translate('Birthing date')}}</label>
                                        <input type="text" class="form-control" id="birthing_date" name="birthing_date"
                                            value="{{!empty($breeding_data->birthing_date)?convertDate($breeding_data->birthing_date):''}}"
                                            readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label
                                            for="live_male_puppie">{{translate('Number of live male puppies')}}</label>
                                        <input type="text" class="form-control" id="live_male_puppie"
                                            name="live_male_puppie"
                                            value="{{!empty($breeding_data->live_male_puppie)?$breeding_data->live_male_puppie:0}}"
                                            min="0" readonly>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label
                                            for="live_male_puppie">{{translate('Number of live female puppies')}}</label>
                                        <input type="text" class="form-control" id="live_female_puppie"
                                            name="live_female_puppie"
                                            value="{{!empty($breeding_data->live_female_puppie)?$breeding_data->live_female_puppie:0}}"
                                            min="0" readonly>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label
                                            for="live_male_puppie">{{translate('Number of dead male puppies')}}</label>
                                        <input type="text" class="form-control" id="dead_male_puppie"
                                            name="dead_male_puppie" value="{{$breeding_data->dead_male_puppie}}" min="0"
                                            readonly>

                                    </div>
                                </div>
                            </div>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label
                                            for="live_female_puppie">{{translate('Number of dead female puppies')}}</label>
                                        <input type="text" class="form-control" id="dead_female_puppie"
                                            name="dead_female_puppie" value="{{$breeding_data->dead_female_puppie}}"
                                            min="0" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             <?php if(!empty($review_manager)){?>
                    @include('native.task_review_manage')
             <?php } ?>
             <?php if(((in_array(Auth::user()->id,$edit_permission) && !empty($review_manager) && $review_manager->user_id == Auth::user()->id ) || in_array($setting_details->value,$skill_details)) && count($pdf_details->toArray()) != 2) {}else{ ?>
                    @include('native.task_chipping_details')
             <?php } ?>
             <?php if((in_array($setting_details->value,$skill_details) &&
                            in_array(Auth::user()->id,$edit_permission))){}else{ ?>
                    @include('native.task_status')
             <?php } ?>
                <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="mb-0 text-white">{{translate('Task Details')}}</h4>
                    </div>
                    <div class="card-body">


                        @csrf
                        <input type="hidden" name="task_details_id" id="task_details_id" value="{{$id}}">
                        <div class="form-body">
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="task_name">{{translate('Task Name')}}</label>
                                        <input type="text" class="form-control" id="task_name" name="task_name"
                                            value="{{!empty($task_details)?translate(".$task_details->task_name")." ".round($breeding_data->SagirId):''}}"
                                            readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div
                                        class="form-group {{(in_array(Auth::user()->id,$edit_permission) && !empty($review_manager) && $review_manager->user_id == Auth::user()->id)?'':'readonly_checkbox'}}">
                                        <label for="full_details">{{translate('Full Description')}}</label>
                                        <textarea id="full_details" name="full_details"
                                            class="form-control  {{(in_array(Auth::user()->id,$edit_permission) && !empty($review_manager) && $review_manager->user_id == Auth::user()->id)?'':'readonly_bgcolor'}}">{{!empty($task_details)?$task_details->full_details:''}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label
                                            for="related_breeding">{{translate('Related Breeding Process Id')}}</label>
                                        <input type="text" class="form-control" id="related_breeding"
                                            name="related_breeding"
                                            value={{!empty($task_details)?$task_details->related_breeding_process_id :''}}
                                            readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="full_details">{{translate('Related To User Id')}}</label>
                                        <input type="text" class="form-control" id="related_user" name="related_user"
                                            value="{{(!empty($task_details) && !empty($task_details->related_to_user_id))?$task_details->user_details->first_name ." ".$task_details->user_details->last_name :''}}"
                                            readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="read_status">{{translate('Read Status')}}</label>
                                        <select class="form-control readonly_checkbox" id="status" name="status"
                                            readonly>
                                            <option value="">{{translate('Select Status')}}</option>
                                            <option value="0"
                                                {{(!empty($task_details) && $task_details->status==0)?'selected':''}}>
                                                {{translate('New')}}</option>
                                            <option value="1"
                                                {{(!empty($task_details) && $task_details->status==1)?'selected':''}}>
                                                {{translate('Was Read')}}</option>

                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="created_at">{{translate('Creation Date')}}</label>
                                        <input type="text" class="form-control" id="created_at" name="created_at"
                                            value={{!empty($task_details)?convertDate($task_details->created_at):''}}
                                            readonly>
                                        <input type="hidden" id="created_at_hidden"
                                            value="{{!empty($task_details)?date('Y-m-d',strtotime($task_details->created_at)):''}}">
                                    </div>
                                </div>
                                <?php
                                    $date_formate=str_replace(array('d','dd'),"DD",settingParam("date-format"));
                                    $date_formate=str_replace(array('m','mm'),"MM",$date_formate);
                                    $date_formate=str_replace(array('y','yyyy','yy','Y'),"YYYY",$date_formate);
                                ?>
                                <input type="hidden" id="date_format" value="{{$date_formate}}">
                                <div
                                    class="col-md-6 {{(in_array(Auth::user()->id,$edit_permission) && !empty($review_manager) && $review_manager->user_id == Auth::user()->id)?:'readonly_checkbox'}}">
                                    <div class="form-group">
                                        <label for="due_date">{{translate('Due Date')}}</label>
                                        <input type="text" class="form-control" id="due_date" name="due_date"
                                            value="{{!empty($task_details->due_date_time)?convertDate($task_details->due_date_time):''}}"
                                            readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="done_date">{{translate('Done Date')}}</label>
                                        <input type="text" class="form-control" id="done_date" name="done_date"
                                            value="{{(!empty($task_details) && !empty($task_details->done_date_time))?convertDate($task_details->done_date_time):''}}"
                                            readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="last_updated_at">{{translate('Last update date')}}</label>
                                        <input type="text" class="form-control" id="last_updated_at"
                                            name="last_updated_at"
                                            value="{{(!empty($task_details) && !empty($task_details->updated_at))?convertDate($task_details->updated_at):''}}"
                                            readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-success" name="submit_form" value="submit"> <i
                                    class="fa fa-check"></i> {{translate('Save')}}</button>
                            <a href="{{frontUrl('tasks')}}" class="btn btn-dark">{{translate('Cancel')}}</a>
                        </div>

                    </div>
                </div>
                <div class="card">

                    <!--   <div class="card-header bg-info">
                        <h4 class="mb-0 text-white">{{translate('Task History')}}</h4>
                    </div> -->
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <div class="table-responsive " id="table-responsive">
                                <table id="history_table"
                                    class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg"
                                    data-paging="true" data-paging-size="7" style="">
                                    <input type="hidden" value="{{$unupdated_dog}}" id="unupdated_dog">
                                    <thead>


                                        <th class="footable-first-visible">{{translate('No')}}</th>
                                        <th style="width:20%">{{translate('Date Time')}}</th>
                                        <th>{{translate('First Name')}}</th>
                                        <th>{{translate('Action Name')}}</th>

                                    </thead>
                                    <tbody>
                                        @php
                                        $i=1;
                                        @endphp
                                        @if(!empty($taskHistory))
                                        @foreach($taskHistory as $rd)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$rd->date_time}}</td>
                                            <td>
                                                @if(currentLanguage() == 1)
                                                {{$rd->first_name}}
                                                @else
                                                {{$rd->first_name_en}}
                                                @endif
                                            </td>
                                            <td>{{$rd->action_name}}</td>

                                        </tr>
                                        @php
                                        $i++;
                                        @endphp

                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="col-md-12">
    <div id="status_modal" class="modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title dog_popup_name"></h4>
                </div>
                <div class="modal-body" id="modal_data">

                </div>
                <div class="modal-footer">
                    <button type="button" id="change_status" class="btn btn-success"><i class="fa fa-check"></i>
                        {{translate('Submit')}}</button>
                    <button type="button" class="btn btn-dark" data-dismiss="modal">{{translate('Cancel')}}</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{translate('Are you sure you want to save and send this data?')}}" id="popup_message">
    <input type="hidden" value="{{translate('Yes sure!')}}" id="popup_yes_button">
    <input type="hidden" value="{{translate("No, bring me back to check again")}}" id="popup_no_button">
</div>

@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/additional-methods.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment-with-locales.js')}}"></script>
<script type="text/javascript"
    src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}">
</script>

<script type="text/javascript" src="{{ asset('js/native/task.js?'.time())}}"></script>
<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#tasksform'):'' !!}
@toastr_render
@endsection