@extends('native.layout.final')
@section('title')
{{translate('Payment')}}
@endsection
@section('pageTitle')
{{translate('Payment')}}
@endsection
@section('css')
<link href="{{ asset('css/pages/ecommerce.css?'.time()) }}" rel="stylesheet">
@endsection
@section('content')

@section('breadcrumb')
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Payment')}}</li>
        </ol>
    </div>
</div> -->
@endsection
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Payment')}}</h4>
            </div>
            <div class="card-body">
                <form name="payment" id="payment" method="post" action="https://direct.tranzila.com/ikc2/">
                    @csrf
                            <input type="hidden" name="currency" value="1">
                            <input type="hidden" name="pdesc" value="{{translate('Payment for litter transmission')}}">
                            <input type="hidden" name="lang" value="{{currentLanguage() == 1?'il':'us'}}">
                            <input type="hidden" name="contact" value="{{currentLanguage() == 1?Auth::user()->first_name." ".Auth::user()->last_name:Auth::user()->first_name_en." ".Auth::user()->last_name_en}}">
                            <input type="hidden" name="email" value="{{Auth::user()->email}}">
                            <input type="hidden" name="phone" value="{{Auth::user()->county_code.Auth::user()->mobile_phone}}">
                            <input type="hidden" name="cred_type" value="1">
                            <input type="hidden" name="terminal_name" value="ikctest">
                            <Input type="hidden"  name="supplier"  value="ikc2">

                        <div class="col-md-12 row p-t-20">
                        <div class="col-md-9 col-lg-9">
                            <div class="card">
                                <div class="card-header bg-info">
                                    <h5 class="m-b-0 text-white">{{translate('Your Cart')}}</h5>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table product-overview text-center">
                                            <thead>
                                                <tr>
                                                   
                                                    <th class="text-center">{{translate('Info')}}</th>
                                                    <th class="text-center">{{translate('Review Price')}}</th>
                                                    <th class="text-center">{{translate('Price Per Dog')}}</th>
                                                    <th class="text-center">{{translate('Quantity')}}</th>
                                                    <th class="text-center">{{translate('Total')}}<br>{{translate('(Review Price + (Price Per Dog * Quantity))')}}</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <h5 class="font-500">{{translate('Puppies')}}</h5>
                                                        <p>{{translate('Male and Female Puppies')}}</p>
                                                    </td>

                                                    <td><span>{{!empty($currency)?$currency->sign:''}}</span><span id="review_price">{{!empty($breeding_data->review_price)?round($breeding_data->review_price,2):0}}</span></td>
                                                    <td align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="price_per_puppie">{{!empty($breeding_data->price_per_dog )?round($breeding_data->price_per_dog,2):0}}</span></td>
                                                    <td width="70">
                                                        <input type="lable" id="quantity_1" class="form-control p-0 text-center" placeholder="0" readonly value="{{$breeding_data->live_female_puppie + $breeding_data->live_male_puppie}}">
                                                    </td>
                                                    <td width="150" align="center" class="font-500"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="total_price_1">{{round($breeding_data->review_price,2) + ($breeding_data->price_per_dog * ($breeding_data->live_female_puppie + $breeding_data->live_male_puppie))}}</span></td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td>
                                                        <h5 class="font-500"></h5>
                                                        <p>Certificate Charges</p>
                                                    </td>
                                                    <td  align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span class="review_price">0</span></td>
                                                    <td align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="certificate_price">{{!empty($breeding_data->certificate_price)?round($breeding_data->certificate_price,2):0}}</span></td>
                                                    <td>
                                                        <input type="lable" id="quantity_2" class="form-control p-0 text-center" placeholder="1" value="{{$breeding_data->live_female_puppie + $breeding_data->live_male_puppie}}" readonly>
                                                    </td>
                                                    <td class="font-500" align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="total_price_2">{{($breeding_data->certificate_price * ($breeding_data->live_female_puppie + $breeding_data->live_male_puppie))}}</span></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>
                                                         <h5 class="font-500">{{translate('Puppies')}}</h5>
                                                        <p>{{translate('Died Male and Female Puppies')}}</p>
                                                    </td>

                                                    <td><span>{{!empty($currency)?$currency->sign:''}}</span><span id="review_price1">0</span></td>
                                                    <td align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="price_per_puppie1">{{!empty($breeding_data->price_per_dog )?round($breeding_data->price_per_dog,2):0}}</span></td>
                                                    <td width="70">
                                                        <input type="lable" id="quantity_11" class="form-control p-0 text-center" placeholder="0" readonly value="{{$breeding_data->total_dead}}">
                                                    </td>
                                                    <td width="150" align="center" class="font-500"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="total_price_111">{{ ($breeding_data->price_per_dog * ($breeding_data->total_dead))}}</span></td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td>
                                                        <h5 class="font-500"></h5>
                                                        <p>Certificate Diduction</p>
                                                    </td>
                                                    <td  align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span class="review_price">0</span></td>
                                                    <td align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="certificate_price1">{{!empty($breeding_data->certificate_price)?round($breeding_data->certificate_price,2):0}}</span></td>
                                                    <td>
                                                        <input type="lable" id="quantity_22" class="form-control p-0 text-center" placeholder="1" value="{{$breeding_data->total_dead}}" readonly>
                                                    </td>
                                                    <td class="font-500" align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="total_price_222">{{($breeding_data->certificate_price * ($breeding_data->total_dead))}}</span></td>
                                                    
                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                        <div class="col-md-3 col-lg-3">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title margintop-10">{{translate('CART SUMMARY')}}</h5>
                                    <hr>
                                    <small>{{translate('Total Price')}}</small>
                                    <h2>{{!empty($currency)?$currency->sign:''}}<span id="total_cart_price">{{!empty($breeding_data->total_payment)?round($breeding_data->total_payment,2):0}}</span></h2>
                                    <input type="hidden" name="sum" value="{{!empty($breeding_data->total_payment)?round($breeding_data->total_payment,2):0}}" id="sum">
                                    <hr>
                                    
                                </div>
                            </div>
                            
                        </div>
                    
                    </div>
                        <div class="card-body">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>
                                {{translate('Pay now')}}</button>
                                <a href="{{ url()->previous() }}" class="btn btn-dark" id="cancel_button">{{translate('Go back')}}</a>

                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <!-- Column -->


@endsection
@section('jquery')
@toastr_render
@endsection