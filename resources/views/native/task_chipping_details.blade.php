 <div class="card">
                    <div class="card-header bg-info">
                        <h4 class="mb-0 text-white">{{translate('Chipping Page New Headline')}}</h4>
                        <h5 class="mb-0 text-white">{{translate('Chipping Page small Headline')}}</h5>
                    </div>
                    <div class="card-body">
                        @if($breeding_data->filled_step == 3)
                        <div class="form-body">
                            <div class="form-group">
                                <!-- <label for="status" class="control-label">{{translate('Male owner approve the breeding')}}:</label>-->
                                <h3 class="mb-10">
                                    <center>
                                        {{((in_array(Auth::user()->id,$edit_permission) && !empty($review_manager) && $review_manager->user_id == Auth::user()->id ) || in_array($setting_details->value,$skill_details))?'':translate('You cannot edit this part yet')}}
                                    </center>
                                </h3>
                                <div
                                    class="col-sm-5 {{(in_array(Auth::user()->id,$edit_permission) && !empty($review_manager) && $review_manager->user_id == Auth::user()->id && $pdf_details->isEmpty())?'':'readonly_checkbox'}}">
                                    <label>{{translate('Male Owner approval')}}</label>
                                    <select
                                        class="custom-select {{(in_array(Auth::user()->id,$edit_permission) && !empty($review_manager) && $review_manager->user_id == Auth::user()->id)?'':'readonly_select'}}"
                                        id="male_owner_agree" name="male_owner_agree">
                                        <option value="">{{translate('Select')}}</option>
                                        <option value="MaleApproved"
                                            {{(!empty($task_details) && $task_details->male_owner_agree=="MaleApproved")?"selected":""}}>
                                            {{translate('Male owner approve the breeding')}}</option>
                                        <option value="MaleDeny"
                                            {{(!empty($task_details) && $task_details->male_owner_agree=="MaleDeny")?"selected":""}}>
                                            {{translate('Male owner deny the breeding')}}</option>
                                        <option value="MaleFight"
                                            {{(!empty($task_details) && $task_details->male_owner_agree=="MaleFight")?"selected":""}}>
                                            {{translate('Male owner not approving the breeding due to fight with female owner')}}
                                        </option>

                                    </select>

                                </div>
                                @if(in_array($setting_details->value,$skill_details))
                                <div
                                    class="col-sm-5 m-t-10 {{in_array(Auth::user()->id,$edit_permission)?'':'readonly_checkbox'}}">
                                    <label for="pedigree-color"
                                        class="control-label">{{translate('Pedigree color')}}:</label>
                                    @if(in_array(Auth::user()->id,$edit_permission))
                                    {{ generate_html_element("pedigree-color","pedigree_color","pedigree_color","string",$task_details->pedigree_color)}}
                                    @else
                                    {{ generate_html_element("pedigree-color","pedigree_color","pedigree_color","string",$task_details->pedigree_color,array(),array('readonly'))}}
                                    @endif
                                </div>
                                @endif

                            </div>

                            <div class="table-responsive">
                                <div class="table-responsive" id="table-responsive">
                                    <table id="dog_list_status"
                                        class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg"
                                        data-paging="true" data-paging-size="7" style="">
                                        <input type="hidden" value="{{$unupdated_dog}}" id="unupdated_dog">
                                        <thead>


                                            <th class="footable-first-visible">{{translate('tax')}}</th>
                                            <th>{{translate('Tentative title')}}</th>
                                            <th>{{translate('Chip Number')}}</th>
                                            <th>{{translate('Color')}}</th>
                                            <th>{{translate('sex')}}</th>
                                            <th>{{translate('A routine review decision')}}</th>
                                            <th>{{translate('editing')}}</th>

                                        </thead>
                                        <tbody>
                                            @php
                                            $i=1;
                                            @endphp
                                            @if(!empty($related_dogs))
                                            @foreach($related_dogs as $rd)
                                            <tr>
                                                <td>{{$i}}</td>
                                                <td id="name_{{$rd->id}}">
                                                    {{!empty($rd->temparory_name)?$rd->temparory_name:'N/A'}}&nbsp;&nbsp;&nbsp;&nbsp;<span
                                                        class="label label-info" id="{{"dog_span_".$rd->id}}"
                                                        {{(empty($rd->approval_status) || !empty($task_details->male_owner_agree))?"style=display:none;":""}}>{{translate('processed')}}</span>
                                                </td>
                                                <td>{{(!empty($rd->chip_number) || $rd->chip_number == 0)?$rd->chip_number:'N/A'}}
                                                </td>
                                                <td>
                                                    <?php 
                                                    if(!empty($rd->dog_color)){
                                                        if(currentLanguage()==1){
                                                           echo $rd->dog_color->ColorNameHE; 
                                                        }else{
                                                           echo $rd->dog_color->ColorNameEN; 

                                                        }
                                                    }else{
                                                        if($rd->color == '0' || $rd->color == 0){
                                                        echo   translate('Color not fit to breed rules');
                                                        echo "\n";
                                                       echo  $rd->other_color;
                                                        }else{
                                                            echo 'N/A';
                                                        }
                                                    }

                                                ?>

                                                </td>
                                                <td>{{!empty($rd->gender)?translate($rd->gender):'N/A'}}</td>
                                                <td>
                                                    <?php
                                            //$status=!empty($rd->dog_status)?(currentLanguage()==1)?$rd->dog_status->option_text_hebrew:$rd->dog_status->option_text_english:'N/A';
                                            $status_id=!empty($rd->approval_status)?$rd->approval_status:'';
                                           ?>
                                                    @if(!empty($rd->approval_status) &&
                                                    $rd->approval_status=='Approved')
                                                    @php
                                                    $status=translate('Proper');
                                                    @endphp
                                                    @elseif((!empty($rd->approval_status) &&
                                                    $rd->approval_status=='NeedTest'))
                                                    @php
                                                    $status=translate('Duty Of MAG at age one - see note');
                                                    @endphp
                                                    @elseif((!empty($rd->approval_status) &&
                                                    $rd->approval_status=='NotApproved'))
                                                    @php
                                                    $status=translate('Not happy to grow - see note');
                                                    @endphp
                                                    @else
                                                    @php
                                                    $status="N/A";
                                                    @endphp
                                                    @endif
                                                    {{$status}}
                                                </td>
                                                <td
                                                    class="{{(in_array(Auth::user()->id,$edit_permission) && ((!empty($review_manager) && $review_manager->user_id == Auth::user()->id) || in_array($setting_details->value,$skill_details)))?'':'readonly_checkbox'}}">
                                                    @if($breeding_data->filled_step == 3 && $rd->is_dead != 1)
                                                    <a href="javascript:" class="change_status" dog_id="{{$rd->id}}"
                                                        current_status="{{$rd->approval_status}}"
                                                        dog_status="{{$status_id}}" title="Change Status"><i
                                                            class="fas fa-edit" data-toggle="tooltip"
                                                            data-original-title="Change Status"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @php
                                            $i++;
                                            @endphp

                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="edit_permission"
                            value="{{in_array(Auth::user()->id,$edit_permission)?'':'disabled'}}">
                        <input type="hidden"
                            value="{{translate('Note that the chip you filled out does not contain the correct amount of digits, are you sure you want to continue?')}}"
                            id="popup_message_chip">
                        <input type="hidden" value="{{translate('Yes sure!')}}" id="popup_yes_button">
                        <input type="hidden" value="{{translate('Cancel')}}" id="popup_cancel_button">
                        <input type="hidden" value="{{translate('puppy photo validation')}}"
                            id="puppy_photo_validation">
                        <input type="hidden"
                            value="{{translate('You did not report the reason for the status of the puppy')}}"
                            id="popup_note_validation">

                        <input type="hidden" value="{{translate('The chip number is less than 15 chars')}}"
                            id="popup_less_number_first">
                        <input type="hidden" value="{{translate('If you still want to proceed please click OK')}}"
                            id="popup_less_number_second">

                        <input type="hidden" value="{{translate("No, bring me back to check again")}}"
                            id="popup_no_button_chip">
                        <input type="hidden" id="exits_in_database"
                            value="{{translate('The chip already exist in the dogs database')}}">
                        <input type="hidden" id="exits_in_database_zero_enter"
                            value="{{translate('Please cancel and enter 0 in the chip field if you are not sure about the chip')}}">

                        <input type="hidden" id="female_sagirid_hidden"
                            value="{{!empty($breeding_data->SagirId)?round($breeding_data->SagirId):''}}">
                        <input type="hidden" id="Color_not_fit_to_breed_rules"
                            value="{{translate('Color not fit to breed rules')}}">
                        @if($pdf_details->isEmpty())
                        <button type="button"
                            class="btn btn-success {{(in_array(Auth::user()->id,$edit_permission) && !empty($review_manager) && $review_manager->user_id == Auth::user()->id)?'':'readonly_checkbox readonly_bgcolor'}}"
                            name="submit_2" id="submit_2" value="submit_2"> <i class="fa fa-check"></i>
                            {{translate('Final approval for routine review')}}</button>
                        @endif
                        @else
                        <center>
                            <h3> {{translate('Waiting for user action')}} </h3>
                        </center>
                        @endif
                        @if(!$pdf_details->isEmpty())
                        <table id="pdf_files" border="0" class="mt-3">
                            <tbody>
                                @foreach ($pdf_details as $pdf )
                                <tr>
                                    <td>{{convertDate($pdf->created_at)}}&nbsp;&nbsp;&nbsp;<a
                                            href="{{url('/download/'.$pdf->id)}}" target="_blank"><i
                                                class="fas fa-file-pdf"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>