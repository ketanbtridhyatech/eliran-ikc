@extends('native.layout.final')
@section('title')
{{translate('Active Breeding')}}
@endsection
@section('pageTitle')
{{translate('Active Breeding')}}
@endsection
@section('breadcrumb')
<style>

</style>
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Active Breeding')}}</li>
        </ol>
    </div>
</div> -->
@endsection

@section('content')
<div class="row">

    <div class="col-lg-12">

        <div class="card   ">
            <div class="card-body">
                <div class="">
                    <div class="table-responsive active_breeding_table" id="table-responsive">
                        <table id="user_list_migration" class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg active_breeding_table_tag"
                            data-paging="true" data-paging-size="7">

                            <thead>
                                <tr class="footable-header">
                                    <th class="footable-first-visible"><?php if(currentLanguage()==1){?>
                                    {{translate('dog no bredding list')}}
                                    <?php }else{ echo "#"; }?></th>
                                    <th>@sortablelink('BreddingDate',translate('Date of breeding'))</th>
                                    <th>@sortablelink('mother_detail.Heb_Name',translate('Female name'))</th>
                                    <th>@sortablelink('father_detail.Heb_Name',translate('Father name'))</th>
                                    <th>{{translate('Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(isset($dogs_details) && !empty($dogs_details))
                                @php $i=(($dogs_details->currentPage() - 1) * $dogs_details->perPage()+1);@endphp
                                @foreach($dogs_details as $dog)
                                
                                <tr>
                                    <td class="footable-first-visible">{{$i++}}</td>
                                    <td>{{isset($dog->BreddingDate)?convertDate($dog->BreddingDate):'N/A'}} </td>
                                    <td>
                                        <div>
                                            <?php 
                                            if(isset($dog->mother_detail->Heb_Name)){
                                                    echo $dog->mother_detail->Heb_Name;
                                            }else{
                                                echo 'N/A';
                                            }
                                        ?>
                                        </div>
                                        <div>
                                            <?php 
                                            if(isset($dog->mother_detail->Eng_Name)){
                                                    echo $dog->mother_detail->Eng_Name;
                                            }else{
                                                echo 'N/A';
                                            }

                                            echo '('.round($dog->SagirId).')';
                                        ?>

                                        </div>

                                    </td>
                                    <td>
                                        <div>
                                            <?php 
                                            if(isset($dog->father_detail->Heb_Name) && !empty($dog->father_detail->Heb_Name)){
                                               
                                        
                                                echo $dog->father_detail->Heb_Name;
                                              
                                               
                                              
                                            }else{
                                                 echo 'N/A';
                                            }
                                        ?>
                                        </div>
                                        <div>
                                            <?php 
                                            if(isset($dog->father_detail->Eng_Name) && !empty($dog->father_detail->Eng_Name)){
                                               
                                               
                                                echo $dog->father_detail->Eng_Name;
                                              
                                               
                                              
                                            }else{
                                                 echo 'N/A';
                                            }
                                            echo '('.round($dog->MaleSagirId).')';
                                        ?>
                                        </div>
                                    </td>

                                    <td>

                                        <div class="container step_dropdown">
                                            <div class="row">

                                                <div class="dropdown">
                                                    <button class="btn btn-secondary dropdown-toggle" type="button"
                                                        id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                        {{translate('Please Select')}}
                                                    </button>
                                                    <ul class="dropdown-menu multi-level ul_class_dropdown" role="menu"
                                                        aria-labelledby="dropdownMenu">

                                                        @if(isset($dog->mother_detail->GenderID) &&
                                                        round($dog->mother_detail->GenderID) == 2)

                                                        <?php 
                    if(isset($dog->filled_step) && $dog->filled_step == 1){
                         $first = "blueclassbackground";
                         $secord = "greenclassbackground" ;
                         $third = $four = $five = '';
                    }elseif(isset($dog->filled_step) && $dog->filled_step == 2){
                         $first = $secord= "blueclassbackground";
                         $third = "greenclassbackground";
                         $four = $five = '';
                    }elseif(isset($dog->filled_step) && $dog->filled_step == 3){
                         $first = $secord =  $third = "blueclassbackground";
                         $four = "greenclassbackground";
                         $five = "";
                    }elseif(isset($dog->filled_step) && $dog->filled_step == 4){
                         $first = $secord =  $third =  $four = "blueclassbackground";
                         $five = "greenclassbackground";
                    }else{
                        $first = $secord =  $third =  $four = $five = "blueclassbackground";
                    }
                ?>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item" tabindex="-1"
                                                                href="#">{{translate('Breeding process')}}</a>
                                                            <ul class="dropdown-menu ul_class_dropdown_sub"
                                                                style="top:-110px;">
                                                            <?php if($dog->status == 0){?>
                                                                <li class="dropdown-item {{$first}}"><a
                                                                        href="{{url('/breeding/add/'.round($dog->SagirId))}}">{{translate('Breeding Page')}}
                                                                    </a></li>
                                                                <li class="dropdown-item {{$secord}}"><a
                                                                        href="{{url('/birthing/'.round($dog->SagirId))}}">{{translate('Birthing Page')}}
                                                                        <?php if($secord == "greenclassbackground"){
                           echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                                                                <li class="dropdown-item {{$third}}"><a
                                                                        href="{{url('/chipping/'.round($dog->SagirId))}}">{{translate('Chipping Page')}}<?php if($third == "greenclassbackground"){
                            echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                                                                <li class="dropdown-item {{$four}}"><a
                                                                        href="{{url('/supervisor_checkup/'.round($dog->SagirId))}}">{{translate('Supervisor checkup')}}
                                                                        <?php if($four == "greenclassbackground"){
                            echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                                                                <li class="dropdown-item {{$five}}"><a
                                                                        href="{{url('/owner_transfer/'.$dog->mother_detail->DataID)}}">{{translate('Ownership transffer')}}
                                                                        <?php if($five == "greenclassbackground"){
                            echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                            <?php }else{?>
                                    <li class="dropdown-item {{$first}}"><a
                                                                        href="{{url('/breeding/breeding_done/'.round($dog->id))}}">{{translate('Breeding Page')}}
                                                                    </a></li>
                                                                <li class="dropdown-item {{$secord}}"><a
                                                                        href="{{url('/birthing_done/'.round($dog->id))}}">{{translate('Birthing Page')}}
                                                                        <?php if($secord == "greenclassbackground"){
                           echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                                                                <li class="dropdown-item {{$third}}"><a
                                                                        href="{{url('/chipping_done/'.round($dog->id))}}">{{translate('Chipping Page')}}<?php if($third == "greenclassbackground"){
                            echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                                                                <li class="dropdown-item {{$four}}"><a
                                                                        href="{{url('/supervisor_checkup_done/'.round($dog->id))}}">{{translate('Supervisor checkup')}}
                                                                        <?php if($four == "greenclassbackground"){
                            echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                                                                <li class="dropdown-item {{$five}}"><a
                                                                        href="{{url('/owner_transfer/'.$dog->mother_detail->DataID)}}">{{translate('Ownership transffer')}}
                                                                        <?php if($five == "greenclassbackground"){
                            echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                            <?php } ?>

                                                            </ul>
                                                        </li>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>


                                        <?php if(Auth::user()->id == '231475' || Auth::user()->id == '230910' || Auth::user()->id == '227053' || Auth::user()->id == '232154') {?>
                                        <button class="form-group btn btn-danger"
                                            onclick="delete_breding({{round($dog->id)}})">{{translate('Delete')}}</button>
                                        <?php } ?>
                                    </td>
                                </tr>
                           
                                @endforeach

                                @endif
                            </tbody>
                        </table>
                        <input type="hidden" value="" name="hidden_sagirId" id="hidden_sagirId">
                        <input type="hidden" value="{{translate('Are you sure you want to delete it')}}"
                            name="delete_alert_text" id="delete_alert_text">
                        <input type="hidden" value="{{translate('Yes sure!')}}" name="alert_text_yes"
                            id="alert_text_yes">
                        <input type="hidden" value="{{translate('No')}}" name="alert_text_no" id="alert_text_no">
                        {{ $dogs_details->appends($_GET)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link href="{{ asset('assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('jquery')
<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{ asset('js/native/dog.js?'.time())}}"></script>
@toastr_render
@endsection

@section('javascript')
<script type="text/javascript">
    $(".select2").select2();
   
   $(window).scroll(function(){
    if ($(window).scrollTop() >= 60) {
        $('#breeding_btn').addClass('fixed-header');
    }
    else {
        $('#breeding_btn').removeClass('fixed-header'); 
    }
});

function delete_breding(id){
             Swal.fire({
                    title: $('#delete_alert_text').val(),
                    text: '',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: $('#alert_text_yes').val(),
                    cancelButtonText: $('#alert_text_no').val()
                }).then((result) => {
                    if (result.value) {
                        
                
                          $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            //$('#ajaxLoader').addClass('d-block');
                            jQuery.ajax({
                                url: getsiteurl() + "/active_breeding/delete_dog",
                                method: 'post',
                                data: {
                                    id: id,
                                },
                                success: function (data) {
                                    location.reload();
                                }
                            });
                }
                });


    
}
</script>

@endsection