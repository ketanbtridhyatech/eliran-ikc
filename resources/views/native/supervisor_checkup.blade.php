@extends('native.layout.final')
@section('title')
{{translate('Supervisor checkup')}}
@endsection
@section('pageTitle')
{{translate('Supervisor checkup')}}
@endsection
@section('content')

@section('breadcrumb')
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Supervisor checkup')}}</li>
        </ol>
    </div>
</div> -->
@endsection

<div class="breadcrumb_new_responsive">
    <?php if(empty($breeding_data)){
        $third = "greenclass";
        $first = $secord = $third = $four = $five ="greyclass";
    }else{
        if(isset($breeding_data->filled_step) && $breeding_data->filled_step == 3){
             $first = $secord = $third = "blueclass";
             $four = "greenclass" ;
             $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 4){
             $first = $secord =  $third =  $four = "blueclass";
             $five = "greenclass";
        }else{
            $first = $secord =  $third =  $four = $five = "blueclass";
        }
    }?>

    <div class="breadcrumb_new flat mb-2">

         <?php if(isset($breeding_data->status) && $breeding_data->status == 3){?>

    <a href="{{url('/breeding/breeding_done/'.$breeding_data->id)}}" class="{{$first}}">{{translate('Breeding Page')}}</a>
    <a href="{{url('/birthing_done/'.$breeding_data->id)}}" class="{{$secord}}">{{translate('Birthing Page')}}</a>
    <a href="{{url('/chipping_done/'. $breeding_data->id)}}" class="{{$third}}">{{translate('Chipping Page')}}</a>
    <a href="#" class="{{$four}}">{{translate('Supervisor checkup')}}</a>
    <a href="#" class="{{$five}}">{{translate('Ownership transffer')}}</a>
    <?php }else{?>

        <a href="{{url('/birthing/'.$sagirId)}}" class="{{$first}}">{{translate('Breeding Page')}}</a>
        <a href="{{url('/birthing/'.$sagirId)}}" class="{{$secord}}">{{translate('Birthing Page')}}</a>
        <a href="{{url('/chipping/'.$sagirId)}}" class="{{$third}}">{{translate('Chipping Page')}}</a>
        <a href="#" class="{{$four}}">{{translate('Supervisor checkup')}}</a>
        <a href="#" class="{{$five}}">{{translate('Ownership transffer')}}</a>

    <?php } ?>
    </div>
</div>


<div class="row">

    <div class="col-lg-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Supervisor checkup')}}</h4>
            </div>
            <div class="card-body">
                @csrf
                <div class="form-body">
                    @if(!empty($related_task) && !empty($related_manager) && !empty($related_task->review_place)
                    && !empty($related_task->review_date))
                    <div class="row p-t-20">
                        <div class="col-md-12">
                            <label for="review_manager">{{translate('Name of manager')}} :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                @if(currentLanguage() == 1)
                                {{!empty($related_manager->user_details)?$related_manager->user_details->first_name." ".$related_manager->user_details->last_name:''}}
                                @else
                                {{!empty($related_manager->user_details)?$related_manager->user_details->first_name_en." ".$related_manager->user_details->last_name_en:''}}
                                @endif
                            </label>
                        </div>
                    </div>
                    <div class="row p-t-20">
                        <div class="col-md-12">
                            <label for="review_place">{{translate('Place of review')}} :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                {{!empty($related_task->review_place)?$related_task->review_place:''}}
                            </label>
                        </div>
                    </div>

                    <div class="row p-t-20">
                        <div class="col-md-12">
                            <label for="review_place">{{translate('Date of review')}} :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                {{!empty($related_task->review_date)?convertDate($related_task->review_date):''}}
                            </label>

                        </div>
                    </div>
                    @if(!$pdf_details->isEmpty())
                    <div class="row p-t-20">
                        <div class="col-md-12">
                            <label for="review_place">{{translate('Reports')}} :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                
                                <table id="pdf_files" border="0" class="mt-3">
                                    <tbody>
                                        @foreach ($pdf_details as $pdf )
                                        <tr>
                                            <td>{{convertDate($pdf->created_at)}}&nbsp;&nbsp;&nbsp;<a
                                                    href="{{url('/download/'.$pdf->id)}}" target="_blank"><i
                                                        class="fas fa-file-pdf"></i></a></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                               
                              
                            </label>

                        </div>
                    </div>
                    @endif
                    @else

                    <center>
                        <h3>{{translate('Waiting for update')}}</h3>
                    </center>

                    @endif

                </div>

            </div>
        </div>
    </div>
</div>

@endsection