<div class="card-header bg-info">

    <h4 class="mb-0 text-white">{{translate('Dog')}} <span id="dog_count_{{$request->count_div + 1}}">{{$request->count_div + 1}}</span></h4>

</div>
<div class="card-body">
    <div class="form-body">
        <div class="row p-t-20">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="temparory_name_{{$request->count_div + 1}}">{{translate('Temporary Name')}}</label>
                    <input type="text"
                        class="form-control" id="temparory_name_{{$request->count_div + 1}}" name="temparory_name[]">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="chip_number_{{$request->count_div + 1}}">{{translate('Chip Number')}}</label>
                    <input type="text" class="form-control" id="chip_number_{{$request->count_div + 1}}" name="chip_number[]">
                </div>
            </div>
        </div>
        <div class="row p-t-20">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="color_{{$request->count_div + 1}}">{{translate('Color')}}</label>
                    <select class="form-control select2" id="color_{{$request->count_div + 1}}" name="color[]">
                        
                            <option value="">Select</option>
                            @if(!empty($colors))
                            @foreach($colors as $co)
                            <option value="{{$co->id}}">{{(currentLanguage()==1)?$co->ColorNameHE:$co->ColorNameEN}}</option>
                            @endforeach
                            @endif
                        
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="hair_type_{{$request->count_div + 1}}">{{translate('Hair Type')}}</label>
                    <select class="form-control select2" id="hair_type_{{$request->count_div + 1}}" name="hair_type[]">
                        
                            <option value="">Select</option>
                            @if(!empty($hairs))
                            @foreach($hairs as $ha)
                            <option value="{{$ha->id}}">{{(currentLanguage()==1)?$ha->HairNameHE:$ha->HairNameEN}}</option>
                            @endforeach
                            @endif
                        
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>