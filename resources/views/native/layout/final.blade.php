@include('native.layout.header')
<body class="skin-blue-dark fixed-layout">

    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <div class="preloader d-none " id="ajaxLoader" style="opacity: 0.6;">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    @include('sweetalert::alert')
    <div id="main-wrapper">
        @include('native.layout.topbar')
        @include('native.layout.sidebar')
        @include('native.layout.page-wrapper')
    </div>
    
    @include('native.layout.footer')        
</body>
</html> 