<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
<!--                <li class="user-pro"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)"
                        aria-expanded="false">

                        @if(displayProfile() != '')
                        <img src="{{displayProfile()}}" alt="user-img" class="img-circle">
                        @endif
                        <span class="hide-menu"> {{displayName()}}</span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="javascript:void(0)"><i class="ti-user"></i> {{translate('My Profile')}}</a></li>
                            <li><a href="{{frontUrl('logout')}}"><i class="fa fa-power-off"></i> {{translate('Logout')}}</a>
                            </li>
                        </ul>
                </li>-->
                <li> <a class="waves-effect waves-dark" href="{{frontUrl('dog')}}" aria-expanded="false"><i
                    class="fas fa-users text-danger"></i><span
                    class="hide-menu">{{translate('My Dogs')}}</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{frontUrl('active_breeding')}}" aria-expanded="false"><i
                    class="fas fa-users text-danger"></i><span
                    class="hide-menu">{{translate('Active Breeding')}}</span></a>
                </li>
                @php
                $related_task = related_task();
                @endphp
                @if($related_task > 0)
               <li> <a class="waves-effect waves-dark" href="{{url('/tasks')}}" aria-expanded="false"><i
                    class="fas fa-tasks text-danger"></i><span
                    class="hide-menu">{{translate('Tasks')}}</span></a>
                </li>
                @endif
                @if(user_related_skill())
                <li>
                <a class="waves-effect waves-dark" href="{{url('/breed_related_dog')}}" aria-expanded="false"><i
                    class="fas fa-tasks text-danger"></i><span
                    class="hide-menu">{{translate('All Breedings report')}}</span></a></li>
                @endif

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->