<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.png') }}">
    

    <title>@yield('title')</title>
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="{{ asset('assets/node_modules/morrisjs/morris.css') }}" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="{{ asset('assets/node_modules/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    @if(currentLanguage() == 1)
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Assistant">
    <link href="{{ asset('css/style_rtl.min.css?'.time()) }}" rel="stylesheet">
    <link href="{{ asset('css/rtl.css?'.time()) }}" rel="stylesheet">
    @else
    <link href="{{ asset('css/style_ltr.min.css?'.time()) }}" rel="stylesheet">
    <link href="{{ asset('css/ltr.css?'.time()) }}" rel="stylesheet">
    @endif
    <link href="{{ asset('css/pages/footable-page.css?'.time()) }}" rel="stylesheet">
        
    
    

    <!-- Dashboard 1 Page CSS -->
    <link href="{{ asset('css/pages/dashboard1.css?'.time()) }}" rel="stylesheet">
    <link href="{{ asset('css/select2.min.css?'.time()) }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/fontawesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/node_modules/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}">
<![endif]-->
@toastr_css
@yield('css')
<link href="{{ asset('css/custom.css?'.time()) }}" rel="stylesheet">
<link href="{{ asset('assets/node_modules/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
<style type="text/css">
body{
    font-size:18px;
}
ul li span{
    font-size:18px !important;
}
ol li {
    font-size:18px !important;
}
</style>
</head>