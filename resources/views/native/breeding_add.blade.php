@extends('native.layout.final')
@section('title')
{{translate('Breeding')}}
@endsection
@section('pageTitle')
{{translate('Breeding')}}
@endsection
@section('content')
<style>
    /*.swal2-popup{
        max-height: 700px;
    }
    .swal2-popup .swal2-content{
        overflow-y: auto;
        max-height: 700px;
    }
    .swal2-container{
       width:450px !important; 
       padding-right:0px !important;
      left: 50%;
    transform: translate(-50%);
    top:5% !important;
    }
    #status_modal .modal-dialog{
        margin:0px !important;
    }
    #status_modal.modal{
        height:90% !important;
    }*/
</style>
<!-- <link rel="stylesheet" href="{{ asset('css/jquery-ui.css?'.time())}}"> -->
@section('breadcrumb')
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Breeding')}}</li>
        </ol>
    </div>
</div> -->
@endsection

<div class="breadcrumb_new_responsive">

<div class="breadcrumb_new flat mb-2">
    <?php if(empty($breeding_data)){
        $first = "greenclass";
        $secord = $third = $four = $five ="greyclass";
    }else{
        if(isset($breeding_data->filled_step) && $breeding_data->filled_step == 1){
             $first = "blueclass";
             $secord = "greenclass";
             $third = $four = $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 2){
             $first = $secord= "blueclass";
             $third = "greenclass";
             $four = $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 3){
             $first = $secord =  $third = "blueclass";
             $four = "greenclass";
             $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 4){
             $first = $secord =  $third =  $four = "blueclass";
             $five = "greenclass";
        }else{
            $first = $secord =  $third =  $four = $five = "blueclass";
        }
    }?>
    <?php if(isset($breeding_data->status) && $breeding_data->status == 3){?>

    <a href="#step1" class="{{$first}}">{{translate('Breeding Page')}}</a>
    <a href="{{url('/birthing_done/'.$breeding_data->id)}}" class="{{$secord}}">{{translate('Birthing Page')}}</a>
    <a href="{{url('/chipping_done/'.$breeding_data->id)}}" class="{{$third}}">{{translate('Chipping Page')}}</a>
    <a href="{{url('/supervisor_checkup_done/'.$breeding_data->id)}}" class="{{$four}}">{{translate('Supervisor checkup')}}</a>

    <?php }else{?>

	<a href="#step1" class="{{$first}}">{{translate('Breeding Page')}}</a>
	<a href="{{url('/birthing/'.$sagirId)}}" class="{{$secord}}">{{translate('Birthing Page')}}</a>
    <a href="{{url('/chipping/'.$sagirId)}}" class="{{$third}}">{{translate('Chipping Page')}}</a>
    <a href="{{url('/supervisor_checkup/'.$sagirId)}}" class="{{$four}}">{{translate('Supervisor checkup')}}</a>

    <?php } ?>
    <a href="#" class="{{$five}}">{{translate('Ownership transffer')}}</a>
</div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Add Breeding')}}
                <?php if(isset($dogName) && !empty($dogName)){ 
                     echo "(";
                        if(!empty($dogName->Heb_Name)){
                            echo $dogName->Heb_Name;
                        }
                        if(!empty($dogName->Eng_Name)){
                            echo " , ".$dogName->Eng_Name;
                        }
                        if(!empty($dogName->SagirID)){
                            echo " , ".round($dogName->SagirID);
                        }
                         echo ")";
                    
                 }?>
                </h4>
            </div>
            <div class="card-body">
            <form name="breedingAddForm" id="breedingAddForm" method="post" action="{{frontUrl('breeding/store')}}">
                @csrf
                <div class="form-body">
                    <div class="row p-t-20">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="breeding_date">{{translate('Breeding Date')}}</label>
                                @if(!empty($breeding_data->BreddingDate))
                                @php
                                $breeding_date=convertDate($breeding_data->BreddingDate);
                                @endphp
                                @elseif(!empty(old('breeding_date')))
                                @php
                                $breeding_date=old('breeding_date');
                                @endphp
                                @else
                                @php
                                $breeding_date = '';
                                @endphp
                                @endif

                               
                                <input type="text" class="form-control {{!empty($breeding_data)?"readonly_checkbox":''}}"  id="breeding_date" name="breeding_date" value="{{(!empty($breeding_date))?$breeding_date:''}}" {{!empty($breeding_data)?"readonly":''}}>
                                <?php
                                    $date_formate=str_replace(array('d','dd'),"DD",settingParam("date-format"));
                                    $date_formate=str_replace(array('m','mm'),"MM",$date_formate);
                                    $date_formate=str_replace(array('y','yyyy','yy','Y'),"YYYY",$date_formate);
                                    $date = date('Y-m-d');
                                ?>
                                <input type="hidden" id="date_format" value="{{$date_formate}}">
                                <input type="hidden" id="current_language" name="current_language" value="{{(currentLanguage()==1)?'he':'en'}}">
                                <input type="hidden" id="current_date" value='{{$date}}'>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" >
                                <label for="femaleId">{{translate('Female ID')}}</label>
                                <input type="text" class="form-control" id="femaleId" name="femaleId" value="{{$sagirId}}" disabled>
                                <input type="hidden" class="form-control" id="hidden_femaleId" name="hidden_femaleId" value="{{$sagirId}}">
                            </div>
                        </div>
                    </div>
                    <div class="row p-t-20">
                        <div class="col-md-6">
                            <div class="form-group">
                                @if(!empty($breeding_data->MaleSagirId))
                                @php
                                $male_id=round($breeding_data->MaleSagirId);
                                @endphp
                                @elseif(!empty(old('maleId')))
                                @php
                                $male_id=old('maleId');
                                @endphp
                                @else
                                @php
                                $male_id = '';
                                @endphp
                                @endif
                                <label for="maleId">{{translate('Male ID')}}</label>
                                <input type="text" class="form-control" id="maleId" name="maleId" value="{{!empty($male_id)?$male_id:""}}" {{!empty($breeding_data)?"readonly":''}}>
                                <label id="sagirExits"></label>
                            </div>
                        </div>
                        <div class="col-md-6 {{!empty($breeding_data)?"readonly_checkbox":''}}">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <label></label>
                                <input class="form-check-input" type="checkbox" id="foreignMale" value="1" name="foreignMale" {{!empty($breeding_data->Foreign_Male_Records)?'checked':!empty(old('foreignMale'))?'checked':''}} >
                                <label class="form-check-label" for="foreignMale">
                                   {{translate('Foreign Male')}}
                                </label>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="row p-t-20">
                    <?php if(!empty(old('maleId')) || !empty($breeding_data)){
                        if(!empty($breeding_data)){?>
                     <div class=" col-md-6 custom-control custom-checkbox mr-sm-2  {{($breeding_data->status == 1 || $breeding_data->status == 2)?'readonly_checkbox':''}} {{(isset($breeding_data->status) && $breeding_data->status == 3)?'readonly_checkbox':''}}">
                                <label></label>
                                <input class="form-check-input pregnancy" type="checkbox" id="pregnancy" value="1" name="pregnancy" {{($breeding_data->status == 1)?'checked':''}} >
                                <label class="form-check-label" for="pregnancy">
                                   {{translate('the pregnancy failed')}}
                                </label>
                            </div> 
                    <?php } } ?>
                </div>

                <div class="card-body">
                    

                     <input type="hidden" value="{{!empty($breeding_data)?$breeding_data->id:''}}" id="breeding_id">
                     <input type="hidden" value="{{translate('Yes sure!')}}" id="popup_yes_button">
                     <input type="hidden" value="{{translate('Are you sure')}}" id="are_you_sure">
                <input type="hidden" value="{{translate("No")}}" id="popup_no_button">
                    <button type="submit" class="btn btn-danger {{!empty($breeding_data)?"readonly_bgcolor":""}}" name="verify" value="verify" {{!empty($breeding_data)?"disabled":""}}> <i class="fa fa-check"></i> {{translate('Verify')}}</button>
                    <input type="hidden" id="loading_text" value="{{translate('Sending data ....')}}">
                    <input type="button" class="btn btn-dark {{!empty($breeding_data)?"readonly_bgcolor":""}}" id="cancel_button" value="{{translate('Cancel')}}" onclick="location.href='{{frontUrl('dog')}}'" {{!empty($breeding_data)?"disabled":""}}>
                    <?php if(!empty(old('maleId')) || !empty($breeding_data)){?>
                    <button type="submit" class='btn btn-success {{!empty($breeding_data)?"readonly_bgcolor":""}}'style="margin-left:24%" name="submit" value="submit" {{!empty($breeding_data)?"disabled":""}}> <i class="fa fa-check"></i> {{translate('Save')}}</button>
                    <?php }?>
                    <?php if(empty(old('maleId')) && empty($breeding_data)){ ?>
                        <br/><label style="color:red">*{{translate('Verify needed')}}</label>
                   <?php } ?>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@if(session()->has('breedingWarning'))
<?php
$warnings = session()->pull('breedingWarning')[0];
?>
<div id="breeding_warning_1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><strong>Warning!</strong></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
          @if(count($warnings)>0)
            <ul class="alert alert-warning">
                @foreach($warnings as $val)
                  <li>{{$val}}</li>
                @endforeach
            </ul>
          @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
@section('jquery')
<!--  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $( function() {
    $( ".swal2-container" ).dialog();
  } );
    
</script> -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment.js')}}"></script>
<script type="text/javascript"  src="{{ asset('assets/node_modules/moment/moment-with-locales.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/native/breeding_form.js?'.time())}}"></script>



{!! isset($jsValidator)?$jsValidator->selector('#breedingAddForm'):'' !!}
@toastr_render
@endsection