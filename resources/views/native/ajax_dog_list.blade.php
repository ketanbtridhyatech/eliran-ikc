<input type="hidden" value="{{$unupdated_dog}}" id="unupdated_dog">
<thead>
    <th class="footable-first-visible">{{translate('No')}}</th>
    <th>{{translate('Temporary Name')}}</th>
    <th>{{translate('Chip Number')}}</th>
    <th>{{translate('Color')}}</th>
    <th>{{translate('Gender')}}</th>
    <th>{{translate('Status')}}</th>
    <th>{{translate('Action')}}</th>

</thead>
<tbody>
    @php
    $i=1;
    @endphp
    @if(!empty($related_dogs))
    @foreach($related_dogs as $rd)
    <tr>
        <td>{{$i}}</td>
        <td>{{!empty($rd->temparory_name)?$rd->temparory_name:'N/A'}}&nbsp;&nbsp;&nbsp;&nbsp;<span
                class="label label-info" id="{{"dog_span_".$rd->id}}"
                {{empty($rd->approval_status)?"style=display:none;":""}}>{{translate('processed')}}</span></td>
        <td>{{(!empty($rd->chip_number) || $rd->chip_number == 0)?$rd->chip_number:'N/A'}}</td>
        <td>
            <?php 
if(!empty($rd->dog_color)){
    if(currentLanguage()==1){
       echo $rd->dog_color->ColorNameHE; 
    }else{
       echo $rd->dog_color->ColorNameEN; 

    }
}else{
    if($rd->color == '0' || $rd->color == 0){
    echo   translate('Color not fit to breed rules');
    echo "\n";
   echo  $rd->other_color;
    }else{
        echo 'N/A';
    }
}

?>
        </td>
        <td>{{!empty($rd->gender)?translate($rd->gender):'N/A'}}</td>
        <td>
            <?php
            $status_id=!empty($rd->approval_status)?$rd->approval_status:'';
            ?>
            @if(!empty($rd->approval_status) && $rd->approval_status=='Approved')
            @php
            $status=translate('Proper');
            @endphp
            @elseif((!empty($rd->approval_status) &&
            $rd->approval_status=='NeedTest'))
            @php
            $status=translate('Duty Of MAG at age one - see note');
            @endphp
            @elseif((!empty($rd->approval_status) &&
            $rd->approval_status=='NotApproved'))
            @php
            $status=translate('Not happy to grow - see note');
            @endphp
            @else
            @php
            $status="N/A";
            @endphp
            @endif
            {{$status}}
        </td>
        <td>
            @if($rd->is_dead != 1)
            <a href="javascript:" class="change_status" dog_id="{{$rd->id}}" current_status="{{$rd->approval_status}}"
                title="Change Status"><i class="fas fa-edit" data-toggle="tooltip"
                    data-original-title="Change Status"></i></a>
            @endif
        </td>
    </tr>
    @php
    $i++;
    @endphp

    @endforeach
    @endif
</tbody>