@extends('native.layout.final')
@section('title')
{{translate('Chipping')}}
@endsection
@section('pageTitle')
{{translate('Chipping')}}
@endsection
@section('content')

@section('breadcrumb')
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Chipping')}}</li>
        </ol>
    </div>
</div> -->
@endsection

<div class="breadcrumb_new_responsive">
    <?php if(empty($breeding_data)){
        $third = "greenclass";
        $first = $secord = $third = $four = $five ="greyclass";
    }else{
        if(isset($breeding_data->filled_step) && $breeding_data->filled_step == 1){
             $first = "blueclass";
             $third = "greenclass" ;
             $secord = $four = $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 2){
             $first = $secord= "blueclass";
             $third = "greenclass";
             $four = $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 3){
             $first = $secord =  $third = "blueclass";
             $four = "greenclass";
             $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 4){
             $first = $secord =  $third =  $four = "blueclass";
             $five = "greenclass";
        }else{
            $first = $secord =  $third =  $four = $five = "blueclass";
        }
    }?>
    <div class="breadcrumb_new flat mb-2">

     <?php if(isset($breeding_data->status) && $breeding_data->status == 3){?>

    <a href="{{url('/breeding/breeding_done/'.$breeding_data->id)}}" class="{{$first}}">{{translate('Breeding Page')}}</a>
    <a href="{{url('/birthing_done/'.$breeding_data->id)}}" class="{{$secord}}">{{translate('Birthing Page')}}</a>
    <a href="#" class="{{$third}}">{{translate('Chipping Page')}}</a>
    <a href="{{url('/supervisor_checkup_done/'.$breeding_data->id)}}" class="{{$four}}">{{translate('Supervisor checkup')}}</a>
    <a href="#" class="{{$five}}">{{translate('Ownership transffer')}}</a>
    <?php }else{?>

        <a href="{{url('/breeding/add/'.$sagirId)}}" class="{{$first}}">{{translate('Breeding Page')}}</a>
        <a href="{{url('/birthing/'.$sagirId)}}" class="{{$secord}}">{{translate('Birthing Page')}}</a>
        <a href="#" class="{{$third}}">{{translate('Chipping Page')}}</a>
        <a href="{{url('/supervisor_checkup/'.$sagirId)}}" class="{{$four}}">{{translate('Supervisor checkup')}}</a>
        <a href="#" class="{{$five}}">{{translate('Ownership transffer')}}</a>

    <?php } ?>
    </div>
</div>

<div class="row {{(isset($breeding_data->status) && $breeding_data->status == 3)?'readonly_checkbox':''}}">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Add Chipping')}}
                <?php if(isset($dogs_data) && !empty($dogs_data)){ 
                        echo "(";
                        if(!empty($dogs_data->Heb_Name)){
                            echo $dogs_data->Heb_Name;
                        }
                        if(!empty($dogs_data->Eng_Name)){
                            echo " , ".$dogs_data->Eng_Name;
                        }
                        if(!empty($dogs_data->SagirID)){
                            echo " , ".round($dogs_data->SagirID);
                        }
                        echo ")";
                 }?>
                </h4>
            </div>
            <div class="card-body">
                <form name="chippingform" id="chippingform" method="post" action="{{frontUrl('chipping/store')}}">
                    @csrf
                    <input type="hidden" name="SagirId" value="{{$id}}">
                    <input type="hidden" id="total_dogs" name="total_dogs" value="{{$total_dogs}}">
                    <input type="hidden" id="total_male_dogs" name="total_male_dogs" value="{{$breeding_data-> live_male_puppie}}">
                    <input type="hidden" id="total_female_dogs" name="total_female_dogs" value="{{$breeding_data-> live_female_puppie}}">
                    <input type="hidden" name="alert_text" id="alert_text" value="{{translate('Maximum alert text')}}">
                    <?php 
                        $male = $female = 0;
                    ?>
                    <div id="add_new_dogs">
                        <div class="table-responsive">
                            <div class="table-responsive" id="table-responsive">
                                <table id="chipping_dog_list" class="table table-bordered">
                                    <thead>
                                        <th class="footable-first-visible">
                                        <?php if(currentLanguage()==1){?>
                                        {{translate('Chip sequence number')}}
                                        <?php }else{ echo "#"; }?>
                                        </th>
                                        <th>{{translate('Dead')}}</th>
                                        <th>{{translate('Temporary Name')}}</th>
                                        <th>{{translate('Chip Number')}}</th>
                                        <th>{{translate('Color')}}</th>
                                        <th>{{translate('Gender')}}</th>


                                    </thead>
                                    <tbody>
                                        @if(!empty($total_dogs))
                                        @for($i=0;$i<$total_dogs;$i++) 
                                        <tr id="row_{{$i}}"  record_id="{{!empty($chipping_data[$i])?$chipping_data[$i]->id:''}}">
                                            <td>{{$i+1}}</td>
                                            
                                            <td
                                                class="{{(!$chipping_data->isEmpty() && $breeding_data->payment_type == 'phone_payment')?'readonly_checkbox':''}}">
                                                <input type="checkbox" id="dead_{{$i}}" dog_no={{$i}} name="dead[]"
                                                    value="1_{{$i}}"
                                                    {{(!empty($chipping_data[$i]->is_dead) && $chipping_data[$i]->is_dead == 1)?'checked':''}}
                                                    class="dead_checkbox">
                                            </td>
                                            <td><input type="text" class="form-control" id="temparory_name_{{$i}}"
                                                    name="temparory_name[]"
                                                    value="{{!empty($chipping_data[$i]->temparory_name)?$chipping_data[$i]->temparory_name:''}}"
                                                    {{(!$chipping_data->isEmpty() && $breeding_data->payment_type == 'phone_payment')?'readonly':''}} dog_no={{$i}}>
                                            </td>
                                            <td>
                                                
                                                <input type="text" class="form-control only_number" id="chip_number_{{$i}}"
                                                    name="chip_number[]"
                                                    <?php 
                                                    if(isset($chipping_data[$i]->chip_number) && $chipping_data[$i]->chip_number == 0){
                                                        $chipvalue = $chipping_data[$i]->chip_number;
                                                    }elseif(!empty($chipping_data[$i]->chip_number))
                                                    {
                                                        $chipvalue = $chipping_data[$i]->chip_number;
                                                    }else{
                                                        $chipvalue = '';
                                                    }

                                                    ?>
                                                    value="{{$chipvalue}}"
                                                    {{(!$chipping_data->isEmpty() && $breeding_data->payment_type == 'phone_payment')?'readonly':''}} dog_no={{$i}}>
                                            </td>
                                            <td
                                                class="{{(!$chipping_data->isEmpty() && $breeding_data->payment_type == 'phone_payment')?'readonly_checkbox':''}}">
                                                <div class="select_readonly_{{$i}}">
                                                    <input type="hidden" class="selecttext" value="{{translate('Select')}}">
                                                    <select class="form-control select2 color_dropdown" id="color_{{$i}}"
                                                        name="color[]" dog_no={{$i}}>
                                                        <option value="">{{translate('Select')}}</option>
                                                        
                                                        @if(!empty($colors))
                                                        @foreach($colors as $co)
                                                        <option value="{{$co->id}}"
                                                            {{(!empty($chipping_data[$i]->color) && $chipping_data[$i]->color == $co->id)?'selected':''}}>
                                                            {{(currentLanguage()==1)?$co->ColorNameHE:$co->ColorNameEN}}
                                                        </option>
                                                        
                                                        @endforeach
                                                        @endif
                                                        <option value="0" {{(isset($chipping_data[$i]->color) && $chipping_data[$i]->color == '0')?'selected':''}}>
                                                            {{translate('Other')}}
                                                        </option>

                                                    </select>
                                                    <div>    
                                                    <input class="form-control mt-2" type="text" name="other_{{$i}}" id="other_{{$i}}" dog_no="{{$i}}" value="{{(!empty($chipping_data[$i]->other_color))?$chipping_data[$i]->other_color:''}}">
                                                </div>
                                                     
                                                </div>
                                                <span id="color_{{$i}}-error" class="invalid-feedback"></span>

                                            </td>
                                            <td
                                                class="{{(!$chipping_data->isEmpty() && $breeding_data->payment_type == 'phone_payment')?'readonly_checkbox':''}}">

                                                <div class="custom-control select_readonly_{{$i}}">
                                                    <input class="form-check-input required_gender gender_test" type="radio"
                                                        id="male_{{$i}}" dog_no={{$i}} value="male" name="gender_{{$i}}[]"
                                                        <?php 
                                                        if(!empty($chipping_data[$i]->gender) && $chipping_data[$i]->gender=="male"){
                                                               echo "checked";
                                                               $male++; 
                                                        }
                                                        ?>
                                                        >
                                                    <label class="form-check-label {{(currentLanguage()==1)?'':'mr-4'}}"
                                                        for="male_{{$i}}">{{translate('male')}}
                                                    </label>
                                                    <input class="form-check-input required_gender gender_test" type="radio"
                                                        id="female_{{$i}}" dog_no={{$i}} value="female" name="gender_{{$i}}[]"
                                                        <?php 
                                                            if(!empty($chipping_data[$i]->gender) && $chipping_data[$i]->gender=="female"){
                                                                 echo "checked";
                                                               $female++; 
                                                            }
                                                        ?>
                                                       >
                                                    <label class="form-check-label" for="female_{{$i}}">
                                                        {{translate('female')}}
                                                    </label>
                                                    <label for="gender_{{$i}}[]" class="error"></label>
                                                </div>
                                            </td>
                                            </tr>
                                            @endfor
                                            @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                         <input type="hidden" id="current_male_dogs" name="current_male_dogd" value="{{$male}}">
                          <input type="hidden" id="current_female_dogs" name="current_female_dogd" value="{{$female}}">
                    </div>
                  
                    <div class="row p-t-20">
                            <div class="col-md-12 {{(!empty($breeding_data) && $breeding_data->payment_type=="phone_payment")?"readonly_checkbox":""}}" >
                                <div class="form-group">
                                    <label>{{translate('I am interested in birthing review done by')}}</label>
                                    <div class="custom-control custom-checkbox {{currentLanguage() == 1?'p-0':''}}">
                                        <input class="form-check-input" type="radio" id="breeding_promoter" value="breeding_promoter"
                                            name="review_type" {{(!empty($breeding_data->review_type) && $breeding_data->review_type=='breeding_promoter')?'checked':''}}>
                                        <label class="form-check-label {{(currentLanguage()==1)?'ml-4':'mr-4'}}" for="breeding_promoter">
                                            {{translate('Breed promoter')}} ({{!empty($currency)?$currency->sign:''}}{{!empty($club_details)?round($club_details->GeneralReviewFee,2):0}}&nbsp;{{translate("for review")}} + {{!empty($currency)?$currency->sign:''}}{{!empty($club_details)?round($club_details->DogReviewFee,2):0}}&nbsp;{{translate("per dog")}} )
                                            <?php 
                                            if(!empty($udBreedPromoter))
                                            {
                                                echo "(".$udBreedPromoter->first_name.")( ".$udBreedPromoter->country_code.$udBreedPromoter->mobile_phone.")";
                                            }
                                            ?>
                                        </label>
                                        <input class="form-check-input" type="radio" id="breeding_group" value="breeding_group"
                                            name="review_type" {{(!empty($breeding_data->review_type) && $breeding_data->review_type=='breeding_group')?'checked':''}} >
                                        <label class="form-check-label" for="breeding_group">
                                            {{translate('Breeding group')}} ({{!empty($currency)?$currency->sign:''}}{{!empty($review_price_settings)?round($review_price_settings->price,2):0}}&nbsp;{{translate("for review")}} + {{!empty($currency)?$currency->sign:''}}{{!empty($price_per_dog_setting)?round($price_per_dog_setting->price,2):0}}&nbsp;{{translate("per dog")}})
                                            <?php 
                                            if(!empty($userData))
                                            {
                                                echo "(".$userData->first_name.")( ".$userData->country_code.$userData->mobile_phone.")";
                                            }
                                            ?>
                                            
                                        </label>
                                        <div><label for="review_type" class="error"></label></div>
                                    </div>
                                    <label>{{translate('please check price')}}</label>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-12 row p-t-20">
                        <div class="col-md-9 col-lg-9">
                            <div class="card">
                                <div class="card-header bg-info">
                                    <h5 class="m-b-0 text-white">{{translate('Your Cart')}}</h5>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table product-overview text-center">
                                            <thead>
                                                <tr>
                                                   
                                                    <th class="text-center">{{translate('Info')}}</th>
                                                    <th class="text-center">{{translate('Review Price')}}</th>
                                                    <th class="text-center">{{translate('Price Per Dog')}}</th>
                                                    <th class="text-center">{{translate('Quantity')}}</th>
                                                    <th class="text-center">{{translate('Total')}}<br>{{translate('(Review Price + (Price Per Dog * Quantity))')}}</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <h5 class="font-500">{{translate('Puppies')}}</h5>
                                                        <p>{{translate('Male and Female Puppies')}}</p>
                                                    </td>
                                                   <input type="hidden" name="club_review_price" id="club_review_price" value="{{!empty($club_details)?round($club_details->GeneralReviewFee,2):0}}">
                                                   <input type="hidden" name="setting_review_price" id="setting_review_price" value="{{!empty($review_price_settings)?round($review_price_settings->price,2):0.00}}">
                                                   <input type="hidden" name="setting_per_dog_price" id="setting_per_dog_price" value="{{!empty($price_per_dog_setting)?round($price_per_dog_setting->price,2):0.00}}">
                                                   <input type="hidden" name="club_price_per_dog" id="club_price_per_dog" value="{{!empty($club_details)?round($club_details->DogReviewFee,2):0.00}}">

                                                    <td><span>{{!empty($currency)?$currency->sign:''}}</span><span id="review_price">0</span></td>
                                                    <td align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="price_per_puppie">0</span></td>
                                                    <input type="hidden" name="store_price_per_puppie" id="store_price_per_puppie" value="0">
                                                    <input type="hidden" name="store_review_price" id="store_review_price" value="0">
                                                    <td width="70">
                                                        <?php 
                                                        $quantity_1 = $breeding_data->live_male_puppie + $breeding_data->live_female_puppie;
                                                        ?>
                                                        <input type="lable" id="quantity_1" class="form-control p-0 text-center" placeholder="0" value="{{$quantity_1}}" readonly>
                                                    </td>
                                                    <td width="150" align="center" class="font-500"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="total_price_1">0</span></td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td>
                                                        <h5 class="font-500"></h5>
                                                        <p>{{translate('Certificate Charges')}}</p>
                                                    </td>
                                                    <td  align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span class="review_price">0</span></td>
                                                    <td align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="certificate_price">{{!empty($pricing_data)?round($pricing_data->price,2):0}}</span></td>
                                                    <input type="hidden" name="store_certificate_price" id="store_certificate_price" value="{{!empty($pricing_data)?round($pricing_data->price,2):0}}">
                                                    <td>
                                                         <?php 
                                                        $quantity_2 = $breeding_data->live_male_puppie + $breeding_data->live_female_puppie;
                                                        ?>
                                                        <input type="lable" id="quantity_2" class="form-control p-0 text-center" placeholder="1" value="{{$quantity_2}}" readonly>
                                                    </td>
                                                    <td class="font-500" align="center"><span>{{!empty($currency)?$currency->sign:''}}</span><span id="total_price_2">0</span></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <h5 class="font-500">{{translate('Puppies')}}</h5>
                                                        <p>{{translate('Died Male and Female Puppies')}}</p>
                                                    </td>

                                                    <td align="center">{{!empty($currency)?$currency->sign:''}}<span
                                                            id="review_price1">{{!empty($breeding_data)?round($breeding_data->review_price,2):0}}</span></td>
                                                    <td align="center">{{!empty($currency)?$currency->sign:''}}<span
                                                            id="price_per_puppie1">{{!empty($club_details)?round($club_details->DogReviewFee,2):0}}</span>
                                                    </td>
                                                    <td width="70">
                                                        <input type="text" id="quantity_11" class="form-control p-0 text-center"
                                                            placeholder="0" readonly>
                                                    </td>
                                                    <td width="150" align="center" class="font-500">
                                                        -{{!empty($currency)?$currency->sign:''}}<span
                                                            id="total_price_11">0</span></td>
                                                </tr>
                                                <tr>

                                                    <td>
                                                        <h5 class="font-500"></h5>
                                                        <p>{{translate('Certificate Diduction')}}</p>
                                                    </td>
                                                    <td align="center">{{!empty($currency)?$currency->sign:''}}<span
                                                            class="review_price">0</span></td>
                                                    <td align="center">{{!empty($currency)?$currency->sign:''}}<span
                                                            id="certificate_price1">{{!empty($pricing_data)?round($pricing_data->price,2):0}}</span>
                                                    </td>
                                                    <td>
                                                        <input type="text" id="quantity_22" name="quantity_22" class="form-control p-0 text-center"
                                                            placeholder="0" value="0" readonly>
                                                    </td>
                                                    <td class="font-500" align="center">
                                                        -{{!empty($currency)?$currency->sign:''}}<span
                                                            id="total_price_22">0</span></td>

                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                        <div class="col-md-3 col-lg-3">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title margintop-10">{{translate('CART SUMMARY')}}</h5>
                                    <hr>
                                    <small>{{translate('Total Price')}}</small>
                                    <h2>{{!empty($currency)?$currency->sign:''}}<span id="total_cart_price">0</span></h2>
                                    <input type="hidden" name="total_cart_price" value="0" id="total_cart_price_text">
                                    <hr>
                                    
                                </div>
                            </div>
                            
                        </div>
                    
                       <div class="col-md-4 {{(!empty($breeding_data) && $breeding_data->payment_type == "phone_payment")?"readonly_checkbox":""}}">
                            <div class="form-group">
                                <label>{{translate('Payment type')}}</label>
                                <div class="custom-control custom-checkbox {{currentLanguage() == 1?'p-0':''}}">
                                    <?php
                                        //$review_type=!empty($breeding_data->review_type)?explode(',',$breeding_data->review_type):array();
                                      
                                    ?>
                                    <input class="form-check-input" type="radio" id="credit_card" value="credit_card"
                                        name="payment_type" {{(!empty($breeding_data) && $breeding_data->payment_type == "credit_card")?"checked":""}}>
                                    <label class="form-check-label {{(currentLanguage()==1)?'ml-4':'mr-4'}}" for="credit_card">
                                        {{translate('Credit card')}}
                                    </label>
                                    <input class="form-check-input" type="radio" id="phone_payment" value="phone_payment"
                                        name="payment_type" {{(!empty($breeding_data) && $breeding_data->payment_type == "phone_payment")?"checked":""}}>
                                    <label class="form-check-label {{(currentLanguage()==1)?'ml-4':'mr-4'}}" for="phone_payment">
                                        {{translate('Phone payment')}}
                                    </label>
                                    <div>
                                        <label for="payment_type" class="error"></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="total_refund" id="total_refund" value="0">
                    </div>
                </form>

                <div class="card-body">
                    <button type="button" class="btn btn-success {{(!$chipping_data->isEmpty() && $breeding_data->payment_type == 'phone_payment')?'readonly_bgcolor':''}}" name="submit" value="submit" id="submit_btn"
                        {{(!$chipping_data->isEmpty() && $breeding_data->payment_type == 'phone_payment')?'disabled':''}}> <i class="fa fa-check"></i>
                        {{translate('Save')}}</button>
                    <input type="hidden" id="loading_text" value="{{translate('Sending data ....')}}">
                    <input type="button" class="btn btn-dark {{(!$chipping_data->isEmpty() && $breeding_data->payment_type == 'phone_payment')?'readonly_bgcolor':''}}" id="cancel_button" value="{{translate('Cancel')}}"
                        onclick="location.href='{{frontUrl('dog')}}'" {{(!$chipping_data->isEmpty() && $breeding_data->payment_type == 'phone_payment')?'disabled':''}}><br/>
                     <label style="font-size:14px;">{{translate('Payment for review')}}</label>
                </div>


                <input type="hidden" value="{{translate('Are you sure you want to save and send this data?')}}"
                    id="popup_message">
                <input type="hidden"
                    value="{{translate('Note that the chip you filled out does not contain the correct amount of digits, are you sure you want to continue?')}}"
                    id="popup_message_chip">

                <input type="hidden" id="exits_in_database" value="{{translate('The chip already exist in the dogs database')}}">
                <input type="hidden" id="exits_in_database_zero_enter" value="{{translate('Please cancel and enter 0 in the chip field if you are not sure about the chip')}}">
                <input type="hidden" value="{{translate('Yes sure!')}}" id="popup_yes_button">
                <input type="hidden" value="{{translate('Cancel')}}" id="popup_cancel_button">
                <input type="hidden" value="{{translate('The chip number is less than 15 chars')}}" id="popup_less_number_first">
                <input type="hidden" value="{{translate('If you still want to proceed please click OK')}}" id="popup_less_number_second">
                <input type="hidden" value="{{translate("No, don't ship yet")}}" id="popup_no_button">
                <input type="hidden" value="{{translate("No, bring me back to check again")}}"
                    id="popup_no_button_chip">


            </div>
        </div>
    </div>
</div>
@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/additional-methods.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment.js')}}"></script>
<script type="text/javascript"
    src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}">
</script>

<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/native/chipping.js?'.time()) }}"></script>
{!! isset($jsValidator)?$jsValidator->selector('#chippingform'):'' !!}
@toastr_render
@endsection