@extends('native.layout.final')
@section('title')
{{translate('Birthing')}}
@endsection
@section('pageTitle')
{{translate('Birthing')}}
@endsection
@section('css')
<link href="{{ asset('css/pages/ecommerce.css?'.time()) }}" rel="stylesheet">
@endsection
@section('content')

@section('breadcrumb')
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Birthing')}}</li>
        </ol>
    </div>
</div> -->
@endsection

<div class="breadcrumb_new_responsive">
    <?php if(empty($breeding_data)){
        $secord = "greenclass";
        $first = $third = $four = $five ="greyclass";
    }else{
        if(isset($breeding_data->filled_step) && $breeding_data->filled_step == 1){
             $first = "blueclass";
             $secord = "greenclass" ;
             $third = $four = $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 2){
             $first = $secord= "blueclass";
             $third = "greenclass";
             $four = $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 3){
             $first = $secord =  $third = "blueclass";
             $four = "greenclass";
             $five ="greyclass";
        }elseif(isset($breeding_data->filled_step) && $breeding_data->filled_step == 4){
             $first = $secord =  $third =  $four = "blueclass";
             $five = "greenclass";
        }else{
            $first = $secord =  $third =  $four = $five = "blueclass";
        }
    }?>

    <div class="breadcrumb_new flat mb-2">
        <?php if(isset($breeding_data->status) && $breeding_data->status == 3){?>

    <a href="{{url('/breeding/breeding_done/'.$breeding_data->id)}}" class="{{$first}}">{{translate('Breeding Page')}}</a>
    <a href="#" class="{{$secord}}">{{translate('Birthing Page')}}</a>
    <a href="{{url('/chipping_done/'.$breeding_data->id)}}" class="{{$third}}">{{translate('Chipping Page')}}</a>
    <a href="{{url('/supervisor_checkup_done/'.$breeding_data->id)}}" class="{{$four}}">{{translate('Supervisor checkup')}}</a>
    <a href="#" class="{{$five}}">{{translate('Ownership transffer')}}</a>
    <?php }else{?>

        <a href="{{url('/breeding/add/'.$sagirId)}}" class="{{$first}}">{{translate('Breeding Page')}}</a>
        <a href="#" class="{{$secord}}">{{translate('Birthing Page')}}</a>
        <a href="{{url('/chipping/'.$sagirId)}}" class="{{$third}}">{{translate('Chipping Page')}}</a>
        <a href="{{url('/supervisor_checkup/'.$sagirId)}}" class="{{$four}}">{{translate('Supervisor checkup')}}</a>
        <a href="#" class="{{$five}}">{{translate('Ownership transffer')}}</a>
    <?php } ?>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header bg-info">
                <h4 class="mb-0 text-white">{{translate('Add Birthing')}}
                    <?php if(isset($maleDogData) && !empty($maleDogData)){ 
                         echo "(";
                        if(!empty($maleDogData->Heb_Name)){
                            echo $maleDogData->Heb_Name;
                        }
                        if(!empty($maleDogData->Eng_Name)){
                            echo " , ".$maleDogData->Eng_Name;
                        }
                        if(!empty($maleDogData->SagirID)){
                            echo " , ".round($maleDogData->SagirID);
                        }
                        echo ")";
                        
                 }?>
                    <?php if(isset($breeding_data->mother_detail) && !empty($breeding_data->mother_detail)){ 
                     echo "(";
                        if(!empty($breeding_data->mother_detail->Heb_Name)){
                            echo $breeding_data->mother_detail->Heb_Name;
                        }
                        if(!empty($breeding_data->mother_detail->Eng_Name)){
                            echo " , ".$breeding_data->mother_detail->Eng_Name;
                        }
                        if(!empty($breeding_data->mother_detail->SagirID)){
                            echo " , ".round($breeding_data->mother_detail->SagirID);
                        }
                        echo ")";
                    
                 }?>
                </h4>
            </div>
            <div class="card-body">
                <form name="birthingAddForm" id="birthingAddForm" method="post" action="{{frontUrl('birthing/store')}}">
                    @csrf
                    <div class="form-body">
                        <div class="row p-t-20 {{currentLanguage() == 1?'p-0':'set_lable_height'}}">
                            <div class="col-md-12">
                                <label>
                                    {{translate('If you need assistance please contact the Race Coordinator')}} : 
                                    <?php 
                                    if(!empty($udBreedPromoter))
                                    {

                                        echo "(".$udBreedPromoter->first_name.") (".$udBreedPromoter->last_name.") - ( ".$udBreedPromoter->country_code.$udBreedPromoter->mobile_phone.")";
                                    }
                                    ?>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    
                                    <?php
                                    $date_formate=str_replace(array('d','dd'),"DD",settingParam("date-format"));
                                    $date_formate=str_replace(array('m','mm'),"MM",$date_formate);
                                    $date_formate=str_replace(array('y','yyyy','yy','Y'),"YYYY",$date_formate);
                                ?>
                                    <input type="hidden" name="bread_date" id="bread_date"
                                        value="{{!empty($breeding_data)?date('Y-m-d',strtotime($breeding_data->BreddingDate)):''}}">
                                    <?php
                                 $dateToday = date('Y-m-d');
                                ?>
                                    <input type="hidden" name="current_date_get" id="current_date_get"
                                        value="{{$dateToday}}">
                                    <input type="hidden" id="date_format" value="{{$date_formate}}">
                                    <input type="hidden" id="current_language" name="current_language"
                                        value="{{(currentLanguage()==1)?'he':'en'}}">
                                    <label for="breeding_date">{{translate('Birthing date')}}</label>
                                    <input type="text" class="form-control"
                                        id="{{(!empty($breeding_data) && $breeding_data->filled_step >= "2")?'birthing_date_readonly':'birthing_date'}}"
                                        name="birthing_date"
                                        value="{{!empty($breeding_data->birthing_date)?convertDate($breeding_data->birthing_date):''}}"
                                        readonly>
                                </div>
                            </div>


                        </div>
                        <div class="table-responsive">
                            <div class="table-responsive" id="table-responsive">
                                <table id="user_list_migration"
                                    class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg"
                                    data-paging="true" data-paging-size="7" style="width:60%">
                                    <thead>
                                        <tr class="footable-header">
                                            <th style="text-align:center;">{{translate('male')}}</th>
                                            <th style="text-align:center;">{{translate('female')}}</th>
                                            <th style="text-align:center;">{{translate('A number of puppies')}}</th>
                                            <tr />
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div style="text-align:center;">
                                                    <div class="form-group">

                                                        <input type="number" class="form-control" id="live_male_puppie"
                                                            name="live_male_puppie"
                                                            value="{{!empty($breeding_data->live_male_puppie)?$breeding_data->live_male_puppie:''}}"
                                                            min="1"
                                                            {{(!empty($breeding_data) && $breeding_data->filled_step>="2")?"readonly":""}}>
                                                        <input type="hidden" class="form-control" id="hidden_femaleId"
                                                            name="hidden_femaleId" value="{{$sagirId}}">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="text-align:center;">
                                                    <div class="form-group">

                                                        <input type="number" class="form-control"
                                                            id="live_female_puppie" name="live_female_puppie"
                                                            value="{{!empty($breeding_data->live_female_puppie)?$breeding_data->live_female_puppie:''}}"
                                                            min="1"
                                                            {{(!empty($breeding_data) && $breeding_data->filled_step>="2")?"readonly":""}}>

                                                    </div>
                                                </div>
                                            </td>
                                            <td style="text-align:center;">{{translate('Living')}}</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="text-align:center;">
                                                    <div class="form-group">

                                                        <input type="number" class="form-control" id="dead_male_puppie"
                                                            name="dead_male_puppie"
                                                            value="{{$breeding_data->dead_male_puppie}}" min="0"
                                                            {{(!empty($breeding_data) && $breeding_data->filled_step>="2")?"readonly":""}}>

                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="text-align:center;">
                                                    <div class="form-group">

                                                        <input type="number" class="form-control"
                                                            id="dead_female_puppie" name="dead_female_puppie"
                                                            value="{{$breeding_data->dead_female_puppie}}" min="0"
                                                            {{(!empty($breeding_data) && $breeding_data->filled_step>="2")?"readonly":""}}>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="text-align:center;">
                                                {{translate('Dead')}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <div class="row p-t-20">
                            <div
                                class="col-md-12 {{(!empty($breeding_data) && $breeding_data->filled_step>="2")?"readonly_checkbox":""}}">
                                <div class="form-group">

                                    <div class="custom-control custom-checkbox {{currentLanguage() == 1?'p-0':''}}">

                                        <input class="form-check-input" type="checkbox" id="publish_data" value="1"
                                            name="publish_data" {{!empty($breeding_data->publish_data)?'checked':''}}>
                                        <label class="form-check-label mr-4" for="publish_data">
                                            {{translate('I want the association to publish the litter')}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row p-t-20">
                            <div
                                class="col-md-12 {{(!empty($breeding_data) && $breeding_data->filled_step>="2")?"readonly_checkbox":""}}">
                                <div class="form-group">

                                    <div class="custom-control custom-checkbox {{currentLanguage() == 1?'p-0':''}}">

                                        <input class="form-check-input" type="checkbox" id="share_data" value="1"
                                            name="share_data" {{!empty($breeding_data->share_data)?'checked':''}}>
                                        <label class="form-check-label mr-4" for="share_data">
                                            {{translate('I want the association to pass on the details to a food vendor so I can get food for the puppy bitch and gift packages for puppy buyers')}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <input type="hidden" id="loading_text" value="{{translate('Sending data ....')}}">
                        <?php if(!empty($breeding_data) && $breeding_data->filled_step >= "2"){?>
                        <div class=" col-md-6 custom-control custom-checkbox mr-sm-2 {{($breeding_data->status == 2)?'readonly_checkbox':''}} {{(isset($breeding_data->status) && $breeding_data->status == 3)?'readonly_checkbox':''}}"
                            style="margin-left:-2%;">
                            <label></label>
                            <input class="form-check-input pregnancy" type="checkbox" id="pregnancy" value="1"
                                name="pregnancy" {{($breeding_data->status == 2)?'checked':''}}>
                            <label class="form-check-label" for="pregnancy">
                                {{translate('All the puppies passed away')}}
                            </label>
                        </div>
                        <?php } ?>
                        <button type="submit"
                            class='btn btn-success {{(!empty($breeding_data) && $breeding_data->filled_step >= "2")?"readonly_bgcolor":""}}'
                            {{(!empty($breeding_data) && $breeding_data->filled_step >= "2")?"disabled":""}}> <i
                                class="fa fa-check"></i>
                            {{translate('Save')}}</button>
                        <input type="button"
                            class="btn btn-dark {{(!empty($breeding_data) && $breeding_data->filled_step >= "2")?"readonly_bgcolor":""}}"
                            id="cancel_button" value="{{translate('Cancel')}}"
                            onclick="location.href='{{frontUrl('dog')}}'"
                            {{(!empty($breeding_data) && $breeding_data->filled_step >= "2")?"disabled":""}}>

                    </div>
                    <input type="hidden" value="{{!empty($breeding_data)?$breeding_data->id:''}}" id="breeding_id">
                    <input type="hidden" value="{{translate('Yes sure!')}}" id="popup_yes_button">
                    <input type="hidden" value="{{translate('Are you sure')}}" id="are_you_sure">
                    <input type="hidden" value="{{translate("No")}}" id="popup_no_button">
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Column -->


@endsection
@section('jquery')
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/additional-methods.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment-with-locales.js')}}"></script>
<script type="text/javascript"
    src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}">
</script>
<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/native/birthing_form.js?'.time())}}"></script>
<!-- {!! isset($jsValidator)?$jsValidator->selector('#birthingAddForm'):'' !!} -->
@toastr_render
@endsection