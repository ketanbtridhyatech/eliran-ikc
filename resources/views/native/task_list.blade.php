@extends('native.layout.final')
@section('title')
{{translate('Tasks Management')}}
@endsection
@section('pageTitle')
{{translate('Tasks Management')}}
@endsection
@section('breadcrumb')
<!-- <link href="{{ asset('css/pages/style.min.css') }}" rel="stylesheet"> -->
<link href="{{ asset('css/pages/inbox.css?'.time()) }}" rel="stylesheet">
<style>

</style>
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Tasks Management')}}</li>
        </ol>
    </div>
</div> -->
@endsection

@section('content')
<div class="row">

    <div class="col-lg-12">
        <!--  <div class="d-none1 d-none card" id="breeding_btn">
            <a href="{{frontUrl('breeding/add/')}}" class="btn btn-danger d-lg-block m-l-15 "
                id='breedingBtn'>{{translate('Breeding')}}</a>
        </div>-->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <div class="card-body inbox-panel">
                                <ul class="list-group list-group-full">
                                    <li class="list-group-item active"> <a href="javascript:void(0)"><i
                                                class="mdi mdi-gmail"></i> {{translate('Inbox')}} </a><span
                                            class="badge badge-success ml-auto">{{count($tasks)}}</span></li>
                                    <li class="list-group-item">
                                        <a href="javascript:void(0)"> <i class="mdi mdi-star"></i>
                                            {{translate('Starred')}} </a>
                                    </li>
                                </ul>
                                <h3 class="card-title m-t-40">{{translate('Labels')}}</h3>
                                <div class="list-group b-0 mail-list">
                                    <a href="javascript:void(0)" class="list-group-item"><span
                                            class="fa fa-circle text-success m-r-10"></span>{{translate('read only')}}</a>
                                    <a href="javascript:void(0)" class="list-group-item"><span
                                            class="fa fa-circle text-info m-r-10"></span>{{translate('action needed')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-8 bg-light border-left">
                            <div class="card-body">
                                <div class="btn-group m-b-10 m-r-10" role="group"
                                    aria-label="Button group with nested dropdown">
                                    <!-- <button type="button" class="btn btn-secondary font-18"><i class="mdi mdi-inbox-arrow-down"></i></button> -->
                                    <button type="button" class="btn btn-secondary font-18"><i
                                            class="mdi mdi-alert-octagon"></i></button>
                                    <button type="button" class="btn btn-secondary font-18"><i
                                            class="mdi mdi-delete"></i></button>
                                </div>
                                <div class="btn-group m-b-10 m-r-10" role="group"
                                    aria-label="Button group with nested dropdown">
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button"
                                            class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false"> <i
                                                class="mdi mdi-folder font-18 "></i> </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1"></div>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button"
                                            class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false"> <i
                                                class="mdi mdi-label font-18"></i> </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1"> </div>
                                    </div>
                                </div>
                                <button type="button " class="btn btn-secondary m-r-10 m-b-10"><i
                                        class="mdi mdi-reload font-18"></i></button>
                                <div class="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button"
                                        class="btn m-b-10 btn-secondary font-18 dropdown-toggle" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">{{translate('more')}}</button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1"></div>
                                </div>
                            </div>
                            <div class="card-body p-t-0">
                                <div class="card b-all shadow-none">
                                    <div class="inbox-center table-responsive">
                                        <table class="table table-hover no-wrap">
                                            <tbody>
                                                @if(isset($tasks) && !$tasks->isEmpty())
                                                @php $i=(($tasks->currentPage() - 1) * $tasks->perPage()+1);@endphp
                                                @foreach($tasks as $t)
                                                <tr>
                                                    <td>
                                                        <div class="custom-control custom-checkbox mr-sm-2">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="checkbox3" value="check">
                                                            <label class="custom-control-label" for="checkbox3"></label>
                                                        </div>
                                                    </td>
                                                    <td class="hidden-xs-down"><i class="fa fa-star-o"></i></td>
                                                    <td class="hidden-xs-down">

                                                        @if(currentLanguage() == 1)


                                                        {{isset($t->related_to_user_id)?$t->user_details['first_name']." ".$t->user_details['last_name']:'N/A'}}


                                                        @else

                                                        {{isset($t->related_to_user_id)?$t->user_details['first_name_en']." ".$t->user_details['last_name_en'] :'N/A'}}

                                                        @endif
                                                    </td>
                                                    <td class="max-texts">
                                                        {{--  @if($t->is_editable == 1) --}}
                                                        <a href="{{url('/tasks/task_details/'.$t->id)}}">
                                                            {{-- @else
                                                            <a href="#">
                                                            @endif --}}

                                                            @if($t->related_user_edit_permisstion == 0)
                                                            <span
                                                                class="label label-success">{{translate('read only')}}</span>
                                                            @else
                                                            <span
                                                                class="label label-info">{{translate('action needed')}}</span>
                                                            @endif

                                                            @php
                                                            $breed_id = '';
                                                            @endphp
                                                            @if($t->task_name == 'A new birth form for')
                                                            @php
                                                            $breed_id =
                                                            !empty($t->breed_details)?round($t->breed_details->SagirId):'';
                                                            @endphp
                                                            @endif

                                                            {{isset($t->task_name)?translate($t->task_name)." ".$breed_id:'N/A'}}</a><br>
                                                        <h6>
                                                            {{translate('Task current manager')}} :
                                                            {{$t->editable_users}} </h6>

                                                    </td>
                                                    <td class="hidden-xs-down"><i class="fa fa-paperclip"></i></td>
                                                    <td class="text-right">
                                                        {{isset($t->created_at)?convertDate($t->created_at):'N/A'}}
                                                    </td>
                                                </tr>
                                                @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link href="{{ asset('assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('jquery')
<script type="text/javascript" src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{ asset('assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{ asset('js/native/dog.js?'.time())}}"></script>
@toastr_render
@endsection

@section('javascript')
<script type="text/javascript">
    $(".select2").select2();
   
   $(window).scroll(function(){
    if ($(window).scrollTop() >= 60) {
        $('#breeding_btn').addClass('fixed-header');
    }
    else {
        $('#breeding_btn').removeClass('fixed-header'); 
    }
});
</script>

@endsection