@extends('native.layout.final')
@section('title')
{{translate('Transfer Owner')}}
@endsection
@section('pageTitle')
{{translate('Ownership transfer for')}}
{{(!empty($dog_details))?(currentLanguage()==1)?$dog_details->Heb_Name:$dog_details->Eng_Name:''}}
{{(!empty($dog_details))?round($dog_details->SagirID):''}}
@endsection
@section('content')

@section('breadcrumb')
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Tranfer Owner')}}</li>
        </ol>
    </div>
</div> -->
@endsection
<?php 
if($usercount == 1){ ?>
<div class="row">
    <div class="col-md-12">
        <div class="card owner_transfer_message">
                <div class="card-body">
                    <div class="form-body">
                        <div class="row p-t-20" style="margin-top:20%">
                          <label>
                        {{translate('This dog have more than one owner, please contact the office for more details')}}
                           </label>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="row" style="{{$usercount == 1 ? 'display:none':''}}">
    <div class="col-md-12">
    <button class="btn btn-primary" style="float:right;margin-bottom:10px;" id="add_owner">{{translate('add another owner')}}</button>
    </div>
    <div class="col-lg-12 multiple_owner">

        <form name="transfer_owner" id="transfer_owner" method="post" action="{{frontUrl('owner_transfer/store')}}">
        <input type="hidden" name="sagir_id" value="{{round($dog_details->SagirID)}}">
        <input type="hidden" name="dog_id" value="{{round($dog_details->DataID)}}">
        <div id="ownerDiv" class="show_validation">
            <div class="card" id="owner_1">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">{{translate('Owner')}} 1</h4>

                </div>
                <input type="hidden" name="total_div" id="total_div" value='1'>
                <div class="card-body">

                    @csrf
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="breeding_date">{{translate('Name')}}</label>
                                    <input type="text" class="form-control" id="dog_name_1" name="dog_name[]"
                                        value="{{(!empty($dog_details))?(currentLanguage()==1)?$dog_details->Heb_Name:$dog_details->Eng_Name:''}}"
                                    >
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                    <label for="">{{translate('Mobile Number')}}</label>

                                    <div class="input-group my-group">
                                        @if (currentLanguage() == 1)
                                         <input type="text" data-id="1"  onblur="checkMobileNumber(1)" class="form-control w-50 mobile_phone" name="mobile_number[]"
                                            id="mobile_phone_1"  autocomplete="off" />
                                        <select id="country_code_1" name="country_code[]" class="selectpicker form-control"
                                            data-live-search="true" title="Please select Country Code">
                                            @foreach($countries as $countriesVal)
                                            <option value="{{$countriesVal->country_code}}">
                                                {{$countriesVal->country_code}}</option>
                                            @endforeach
                                        </select>
                                       
                                        @else
                                            <select id="country_code_1" name="country_code[]" class="selectpicker form-control"
                                            data-live-search="true" title="Please select Country Code">
                                            @foreach($countries as $countriesVal)
                                            <option value="{{$countriesVal->country_code}}">
                                                {{$countriesVal->country_code}}</option>
                                            @endforeach
                                            </select>
                                            <input type="text" data-id="1"  onblur="checkMobileNumber(1)" class="form-control w-50 mobile_phone" name="mobile_number[]"
                                                id="mobile_phone_1"  autocomplete="off" />
                                        @endif   
                                    
                                    </div>
                                </div>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">&nbsp;</label>
                                        <input type="button" class="btn btn-success d-block"  onclick="sendOtp(1)" name="otp" id="otp_1"value="{{translate('otp')}}" style="white-space: pre-wrap">
                                    </div>
                                </div>
                                
                                
                            </div>
                            
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-8">
                                         <div class="form-group otp_verify_1">
                                               <label for="">{{translate('Enter otp')}}</label>
                                                <input type="text" class="form-control" id="userOtp_1"name="userOtp[]">
                                                <input type="hidden" name="verifyotp" id="verifyotp_1">
                                                <label id="errorotp_1" style="color:red">Incorrect Otp</label>
                                                
                                        </div>
                                    </div>
                                        
                                    <div class="col-md-2">
                                        <div class="otp_verify_1">
                                        <label for="">&nbsp;</label>
                                         <input type="button" class="btn btn-success d-block" name="verify[]" value="{{translate('Verify')}}" id="verify_1" onclick="verifyOtp(1)" style="white-space: pre-wrap">
                                         <input type="hidden" name="verify_status_1" id="verify_status_1" value="0">
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                            
                        
                        <div class="ownerDetails_1">
                        
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">{{translate('Remarks')}}</label>
                                <textarea class="form-control set_direction" id="remark_1" name="remark[]"></textarea>
                                
                            </div>
                        </div>

                    </div>

                </div>

            </div>
         </div>
        </form>
    </div>
</div>

<!-- <div class="row"> -->
    <div class="col-md-12" style="{{$usercount == 1 ? 'display:none':''}}">
        <div class="card">
            <div class="card-body">
                <button type="button" class="btn btn-success" name="save_owner_data" id="save_owner_data" value="submit"> <i class="fa fa-check"></i>
                    {{translate('Save')}}</button>
                <input type="button" class="btn btn-dark {{!empty($breeding_data)?"readonly_bgcolor":""}}"
                    id="cancel_button" value="{{translate('Cancel')}}" onclick="location.href='{{frontUrl('dog')}}'">
            </div>
        </div>
    </div>
<!-- </div> -->


@endsection
@section('jquery')
<script src="{{asset('assets/node_modules/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('assets/node_modules/jquery-validation/additional-methods.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/moment/moment-with-locales.js')}}"></script>
<script type="text/javascript"
    src="{{ asset('assets/node_modules/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}">
</script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{{ asset('js/native/owner_transfer.js?'.time())}}"></script>



{!! isset($jsValidator)?$jsValidator->selector('#breedingAddForm'):'' !!}
@toastr_render
@endsection