<div class="card">

                    <div class="card-header bg-info">
                        <h4 class="mb-0 text-white">{{translate('Review Booking')}}</h4>
                    </div>
                    <div class="card-body">
                        @csrf
                        @if($breeding_data->filled_step == 3)

                        <div class="form-body {{!empty($review_manager)?"readonly_checkbox":""}}">
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="review_manager">{{translate('Name of manager')}}</label>
                                        <select id="review_manaer" name="review_manager"
                                            class="form-control {{!empty($review_manager)?'readonly_select':''}}">
                                            <option value="">{{translate('Please choose a user')}}</option>
                                            @if($related_user->isEmpty())
                                            <option value="0"
                                                {{(!empty($review_manager) && $review_manager->user_id == 0)?"selected":""}}>
                                                {{translate('No one found')}}</option>
                                            @else
                                            @foreach($related_user as $u)
                                            <option value="{{$u->id}}"
                                                {{(!empty($review_manager) && $review_manager->user_id == $u->id)?"selected":""}}>
                                                {{(currentLanguage()==1)?$u->first_name." ".$u->last_name:$u->first_name_en." ".$u->last_name_en}}
                                            </option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <!-- <input type="text" class="form-control" id="review_manager" name="review_manager" value="{{!empty($task_details->manager)?$task_details->manager->first_name." ".$task_details->manager->last_name:''}}" readonly> -->


                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="live_male_puppie">{{translate('Place of review')}}</label>
                                        <input type="text" class="form-control" id="review_place" name="review_place"
                                            value="{{!empty($task_details->review_place)?$task_details->review_place:''}}"
                                            {{!empty($review_manager)?'readonly':''}}>

                                    </div>
                                </div>
                            </div>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="breeding_date">{{translate('Date of review')}}</label>
                                        <input type="text" class="form-control" id="review_date" name="review_date"
                                            value="{{!empty($task_details->review_date)?convertDate($task_details->review_date):''}}"
                                            {{!empty($review_manager)?'readonly':''}}>
                                        <label>
                                            <?php 
                                                  $date42 = strtotime($breeding_data->birthing_date);
                                                  $after42 = strtotime("+42 day", $date42);
                                                  $after56 = strtotime("+56 day", $date42);
                                                  //echo "<pre>";print_r($after42);exit;
                                            ?>{{translate('selected date must be min 6 weeks after the breeding date')}} {{convertDate(date('Y-m-d',$after42))}} - {{convertDate(date('Y-m-d',$after56))}}</label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <input type="hidden" id="review_manager_validation"
                            value="{{translate('Please select user to do review')}}">
                        <input type="hidden" id="review_date_validation"
                            value="{{translate('Please select date for review')}}">
                        <input type="hidden" id="review_place_validation"
                            value="{{translate('please select review location')}}">
                        <input type="hidden" name="submit_form_review" id="submit_form_review_val" value="">
                        <?php
                        if(!empty($breeding_data->BreddingDate)){
                          /*  $date42 = strtotime($breeding_data->birthing_date);
                            $after42 = strtotime("+42 day", $date42);
                            echo "<pre>";print_r($after42);exit;*/
                            $after42date =  date('Y-m-d', strtotime($breeding_data->BreddingDate));
                        }else{
                            $after42date = date('Y-m-d');
                        } 
                        
                        ?>
                         <input type="hidden" class="form-control" id="birthing_date_hidden"
                                            value="{{$after42date}}"
                                            readonly>
                        <div class="card-body {{!empty($review_manager)?"readonly_checkbox":""}}">
                            <button type="button"
                                class="btn btn-success {{!empty($review_manager)?'readonly_bgcolor':''}}"
                                name="submit_form_review_btn" id="submit_form_review" value="1"> <i
                                    class="fa fa-check"></i> {{translate('Save')}}</button>
                        </div>
                        @else
                        <center>
                            <h3>{{translate('Waiting for user action')}}</h3>
                        </center>
                        @endif

                    </div>
                </div>