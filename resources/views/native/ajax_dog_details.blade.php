
<style type="text/css">
    
    #edit_puppie_details .select2-hidden-accessible,
    #edit_puppie_details .select2-container{
        width: 100% !important;
    }
   /* .lb-data .lb-close {
        position: absolute;
        top: -34px;
        float:right;
        margin-left: 42%;
    }*/
</style>
<link href="{{ asset('css/lightbox.css?'.time()) }}" rel="stylesheet">
<form id="edit_puppie_details" name="edit_puppie_details" enctype="multipart/form-data">
    <input type="hidden" name="task_id" value="{{$task_id}}">
    <input type="hidden" id="dog_popup_name" value="{{$dog_details->temparory_name}}">
    <input type="hidden" id="dog_id" name="dog_id" value="{{$dog_details->id}}">
    <div class="form-group">
        <label for="note" class="control-label">{{translate('Chip Number')}}:</label>
        <input type="text" class="form-control" id="chip_number" name="chip_number" value="{{(!empty($dog_details->chip_number) || $dog_details->chip_number == 0)?$dog_details->chip_number:""}}" {{in_array($setting_details->value,$skill_details)?"readonly":""}}>
    </div>
    <div class='form-group {{in_array($setting_details->value,$skill_details)?"readonly_checkbox":""}}'>
        <label for="note" class="control-label w-100">{{translate('Color')}}:</label>
        <select class="form-control select2 color_dropdown" id="color" name="color">
        <option value="">Select</option>
        @if(!empty($color))
         @foreach($color as $co)
            <option value="{{$co->id}}" {{(!empty($dog_details->color) && $dog_details->color == $co->id)?'selected':''}}>
            {{(currentLanguage()==1)?$co->ColorNameHE:$co->ColorNameEN}}
            </option>

         @endforeach
        @endif
        <option value="0" {{(isset($dog_details->color) && $dog_details->color == '0')?'selected':''}}>
        {{translate('Color not fit to breed rules')}}
    </option>
        </select>
        <label class="mt-2" id="other_text">{{translate('Problem with color selection')}}</label>
         <input class="form-control mt-2" type="text" name="other" id="other" value="{{(!empty($dog_details->other_color))?$dog_details->other_color:''}}">
     
    </div>
    <div class="form-group status_radio">
        <!-- <label for="status" class="control-label">{{translate('Status')}}:</label>-->
        <div class="col-sm-12">
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="approved"
                    name="approval_status" value="Approved" {{(!empty($dog_details->approval_status) && $dog_details->approval_status=='Approved' )?"checked":""}}>
                <label class="custom-control-label"
                    for="approved">{{translate('Proper')}}</label>
            </div>
        </div>
    </div>
    <div class="form-group status_radio">
        <!-- <label for="status" class="control-label">{{translate('Duty Of MAG at age one - see note')}}:</label>-->
        <div class="col-sm-12">
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="need_test"
                    name="approval_status" value="NeedTest" {{(!empty($dog_details->approval_status) && $dog_details->approval_status=='NeedTest' )?"checked":""}}>
                <label class="custom-control-label"
                    for="need_test">{{translate('Duty Of MAG at age one - see note')}}</label>
            </div>
        </div>
    </div>
    <div class="form-group status_radio">
        <!-- <label for="status" class="control-label">{{translate('Not happy to grow - see note')}}:</label>-->
        <div class="col-sm-12">
            <div class="custom-control custom-radio">
                <input type="radio" class="custom-control-input" id="not_approved"
                    name="approval_status" value="NotApproved" {{(!empty($dog_details->approval_status) && $dog_details->approval_status=='NotApproved')?"checked":""}}>
                <label class="custom-control-label"
                    for="not_approved">{{translate('Not happy to grow - see note')}}</label>
            </div>
        </div>
    </div>
    <div class="form-group {{in_array($setting_details->value,$skill_details)?"readonly_checkbox":""}}">
        <label for="note" class="control-label">{{translate('Note')}}:</label>
        <textarea class="form-control" id="note" name="note" {{in_array($setting_details->value,$skill_details)?"readonly":""}}>{{!empty($dog_details->note)?$dog_details->note:""}}</textarea>
    </div>
    @if(in_array($setting_details->value,$skill_details))
    <div class="form-group">
        <label for="note" class="control-label">{{translate('Puppies supervisor remarks')}}:</label>
        <textarea class="form-control" id="supervisor_notes" name="supervisor_notes">{{!empty($dog_details->supervisor_notes)?$dog_details->supervisor_notes:""}}</textarea>
    </div>
    <div class="form-group">
        <label for="image" class="control-label">{{translate('Puppie Image')}}:</label>
        <div class="col-md-2 mt-10">
            @if(!empty($dog_details->image))
             <a href="{{$dog_details->image}}" data-lightbox="image-1"><img src="{{$dog_details->image}}" width="100" height="100" class="mt-10"></a>
            
            @endif
        </div>
    </div>

<div class="form-group">
        <label for="image" class="control-label">{{translate('Document')}}:</label>
        <div class="col-md-12">
    <?php 
    if(isset($dog_details->document_url) && !empty($dog_details->document_url)){
$imgExts = array("jpg", "jpeg", "png", "JPEG", "PNG");
$url = $dog_details->document_url;
$urlExt = pathinfo($url, PATHINFO_EXTENSION);
$urlarray = explode('?',$urlExt);
//echo $urlExt ;exit;
if (isset($urlarray[0]) && in_array($urlarray[0], $imgExts)) {  ?>
@if(!empty($dog_details->document))
 <a href="{{$dog_details->document_url}}" data-lightbox="image-1"><img src="{{$dog_details->document_url}}" width="100" height="100" class="mt-10"></a>
@endif
<?php }else{ ?>
    @if(!empty($dog_details->document))
    <ul>
    <li><a href="{{$dog_details->document_url}}" target="_blank">{{$dog_details->document}}</a></li>
    </ul>
    @endif
<?php } }?>
</div>
    </div>
    @else
    <div class="form-group {{in_array($setting_details->value,$skill_details)?"readonly_checkbox":""}}">
        <label for="image" class="control-label">{{translate('Puppie Image')}}:</label>
        <input type="file" name="image" id="image" class="form-control" accept="image/x-png,image/gif,image/jpeg">
        <input type="hidden" id="image_file" value="{{$dog_details->image}}">
        <div class="col-md-2 mt-10">
            @if(!empty($dog_details->image))
             <a href="{{$dog_details->image}}" data-lightbox="image-1"><img src="{{$dog_details->image}}" width="100" height="100" class="mt-10"></a>
            
            @endif
        </div>
    </div>
    <div class="form-group {{in_array($setting_details->value,$skill_details)?"readonly_checkbox":""}}">
        <label for="image" class="control-label">{{translate('Document')}}:</label>
        <input type="file" name="document" id="document" class="form-control">
        <div class="col-md-12">
             <?php 
if(isset($dog_details->document_url) && !empty($dog_details->document_url)){


$imgExts = array("jpg", "jpeg", "png", "JPEG", "PNG");
$url = $dog_details->document_url;
$urlExt = pathinfo($url, PATHINFO_EXTENSION);
$urlarray = explode('?',$urlExt);
//echo $urlExt ;exit;
if (isset($urlarray[0]) && in_array($urlarray[0], $imgExts)) {  ?>
@if(!empty($dog_details->document))
 <a href="{{$dog_details->document_url}}" data-lightbox="image-1"><img src="{{$dog_details->document_url}}" width="100" height="100" class="mt-10"></a>
@endif
<?php }else{ ?>
    @if(!empty($dog_details->document))
    <ul>
    <li><a href="{{$dog_details->document_url}}" target="_blank">{{$dog_details->document}}</a></li>
    </ul>
    @endif
<?php } }?>
        </div>
    </div>
    @endif
    
</form>
<script type="text/javascript" src="{{ asset('js/native/lightbox.js?'.time())}}"></script>
<script type="text/javascript">
    $(document).ready(function () {

      $('#color').trigger('change');
    $('.dog_popup_name').html($('#dog_popup_name').val());
});
    $('#color').change(function () {
     if($(this).val() == '0'){
        //alert("change");
              $('#other_text').show();
                $('#other').show();
                $('#other').rules( "add", { required: true });
            }else{
                $('#other_text').hide();
                $('#other').rules("remove");
                $('#other').hide();
            }
    });
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    });
    $('.lb-details').html('');
</script>
