@extends('native.layout.final')
@section('title')
{{translate('All Breedings report')}}
@endsection
@section('pageTitle')
{{translate('All Breedings report')}}
@endsection
@section('breadcrumb')
<style>

</style>
<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Active Breeding')}}</li>
        </ol>
    </div>
</div> -->
@endsection

@section('content')
<div class="row">

    <div class="col-lg-12">

        <div class="card   ">
            <div class="card-body">
                <div class="">
                    <div class="table-responsive active_breeding_table" id="table-responsive">
                        <table id="user_list_migration" class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg active_breeding_table_tag"
                            data-paging="true" data-paging-size="7">

                            <thead>
                                <tr class="footable-header">
                                    <th class="footable-first-visible">{{translate('No')}}</th>
                                    <th>@sortablelink('BreddingDate',translate('Date of breeding'))</th>
                                    <th>@sortablelink('mother_detail.Heb_Name',translate('Female name'))</th>
                                    <th>@sortablelink('father_detail.Heb_Name',translate('Male name'))</th>
                                   {{--  <th>{{translate('Action')}}</th> --}}
                                </tr>
                            </thead>
                            <tbody>

                                @if(isset($dogs_details) && !empty($dogs_details))
                                @php $i=(($dogs_details->currentPage() - 1) * $dogs_details->perPage()+1);@endphp
                                @foreach($dogs_details as $dog)
                                
                                <tr>
                                    <td class="footable-first-visible">{{$i++}}</td>
                                    <td>{{isset($dog->BreddingDate)?convertDate($dog->BreddingDate):'N/A'}} </td>
                                    <td>
                                        <div>
                                            <?php 
                                            if(isset($dog->mother_detail->Heb_Name)){
                                                    echo $dog->mother_detail->Heb_Name;
                                            }else{
                                                echo 'N/A';
                                            }
                                        ?>
                                        </div>
                                        <div>
                                            <?php 
                                            if(isset($dog->mother_detail->Eng_Name)){
                                                    echo $dog->mother_detail->Eng_Name;
                                            }else{
                                                echo 'N/A';
                                            }

                                            echo '('.round($dog->SagirId).')';
                                        ?>

                                        </div>

                                    </td>
                                    <td>
                                        <div>
                                            <?php 
                                            if(isset($dog->father_detail->Heb_Name) && !empty($dog->father_detail->Heb_Name)){
                                               
                                        
                                                echo $dog->father_detail->Heb_Name;
                                              
                                               
                                              
                                            }else{
                                                 echo 'N/A';
                                            }
                                        ?>
                                        </div>
                                        <div>
                                            <?php 
                                            if(isset($dog->father_detail->Eng_Name) && !empty($dog->father_detail->Eng_Name)){
                                               
                                               
                                                echo $dog->father_detail->Eng_Name;
                                              
                                               
                                              
                                            }else{
                                                 echo 'N/A';
                                            }
                                            echo '('.round($dog->MaleSagirId).')';
                                        ?>
                                        </div>
                                    </td>
                                </tr>
                           
                                @endforeach

                                @endif
                            </tbody>
                        </table>
                        <input type="hidden" value="" name="hidden_sagirId" id="hidden_sagirId">
                        <input type="hidden" value="{{translate('Are you sure you want to delete it')}}"
                            name="delete_alert_text" id="delete_alert_text">
                        <input type="hidden" value="{{translate('Yes sure!')}}" name="alert_text_yes"
                            id="alert_text_yes">
                        <input type="hidden" value="{{translate('No')}}" name="alert_text_no" id="alert_text_no">
                        {{ $dogs_details->appends($_GET)->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link href="{{ asset('assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('jquery')
<script src="{{asset('assets/node_modules/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{ asset('js/native/dog.js?'.time())}}"></script>
@toastr_render
@endsection

@section('javascript')
<script type="text/javascript">
    $(".select2").select2();
   
   $(window).scroll(function(){
    if ($(window).scrollTop() >= 60) {
        $('#breeding_btn').addClass('fixed-header');
    }
    else {
        $('#breeding_btn').removeClass('fixed-header'); 
    }
});

function delete_breding(id){
             Swal.fire({
                    title: $('#delete_alert_text').val(),
                    text: '',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: $('#alert_text_yes').val(),
                    cancelButtonText: $('#alert_text_no').val()
                }).then((result) => {
                    if (result.value) {
                        
                
                          $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            //$('#ajaxLoader').addClass('d-block');
                            jQuery.ajax({
                                url: getsiteurl() + "/active_breeding/delete_dog",
                                method: 'post',
                                data: {
                                    id: id,
                                },
                                success: function (data) {
                                    location.reload();
                                }
                            });
                }
                });


    
}
</script>

@endsection