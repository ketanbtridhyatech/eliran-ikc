@extends('native.layout.final')
@section('title')
{{translate('Dog Management')}}
@endsection
@section('pageTitle')
{{translate('Dog Management')}}
@endsection
@section('breadcrumb')

<!-- <div class="col-md-7 align-self-center text-right">
    <div class="d-flex justify-content-end align-items-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{frontUrl('/')}}">{{translate('Home')}}</a></li>
            <li class="breadcrumb-item active">{{translate('Dog Management')}}</li>
        </ol>
    </div>
</div> -->
@endsection

@section('content')
<div class="row">

    <div class="col-lg-12">
        <div class="d-none1 d-none card" id="breeding_btn">
            <a href="{{frontUrl('breeding/add/')}}" class="btn btn-danger d-lg-block m-l-15 "
                id='breedingBtn'>{{translate('Breeding')}}</a>
        </div>
        <div class="card   ">
            <div class="card-body">
                 <div class="col-md-12">
                <form action="" method="GET" role="search">
 
     

                    <div>
                      <h3> <b>{{translate('Search By')}} : </b></h3><br/>
                    </div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Sagir Id')}} :</label>
                                    <input type="text" class="form-control" id="search_by_sagir_id" name="search_by_sagir_id" value="{{isset($request->search_by_sagir_id)?$request->search_by_sagir_id:'' }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Dog Name')}} :</label>
                                    <input type="text" class="form-control" id="search_by_dog_name" name="search_by_dog_name" value="{{isset($request->search_by_dog_name)?$request->search_by_dog_name:'' }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{translate('Search By Breed Name')}} :</label>

                                    <select class="form-control custom-select" name="search_by_breed_name" id="search_by_breed_name" data-placeholder="Choose a Status" >
                                        <option value="">{{translate('Please Select')}}</option>
                                         @if(isset($breeddog) && !empty($breeddog))
                                         @foreach($breeddog as $bd)
                                         <option value="{{round($bd->RaceID)}}"  {{(isset($request->search_by_breed_name) && (trim($request->search_by_breed_name) == trim(round($bd->RaceID))))?"selected=selected":""}}>{{$bd->BreedName}}</option>
                                         @endforeach
                                         @endif
                                    </select>
                                </div>
                            </div>
                            
                        </div>

                        
                        <div class="card-body">
                            <button type="submit" name="search_submit" value="1" class="btn btn-success"> <i class="fa fa-check"></i> {{translate('Search')}}</button>
                            <a href="{{frontUrl('dog')}}" class="btn btn-dark">{{translate('Reset')}}</a>
                        </div>
                    </div>
                </form>
            </div>
                <div class="table-responsive">
                    <div class="table-responsive" id="table-responsive">
                        <table id="user_list_migration"
                            class="table table-bordered m-t-30 table-hover contact-list footable footable-5 footable-paging footable-paging-center breakpoint-lg"
                            data-paging="true" data-paging-size="7" style="">
                            <thead>
                                <tr class="footable-header">
                                    <th></th>
                                    <th class="footable-first-visible">
                                    <?php if(currentLanguage()==1){?>
                                    {{translate('dog no')}}
                                    <?php }else{ echo "#"; }?></th>
                                    <th>@sortablelink('Heb_Name',translate('Dog Name'))</th>
                                    <th>@sortablelink('breed_name.BreedName',translate('Dog breed'))</th>
                                    <th>@sortablelink('GenderID',translate('Gender'))</th>
                                    <th>@sortablelink('BirthDate',translate('BirthDate'))</th>
                                    <th>@sortablelink('SagirId',translate('SagirID'))</th>
                                    <th>{{translate('Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dogs_details) && !empty($dogs_details))
                                @php $i=(($dogs_details->currentPage() - 1) * $dogs_details->perPage()+1);@endphp
                                @foreach($dogs_details as $dog)
                                <tr>
                                    <td>
                                        <input type="checkbox" class="control-input dogcheck" name="dogcheck[]"
                                            id="dogcheck{{$dog->DataID}}" value="{{$dog->SagirID}}"
                                            data-gender="{{ round($dog->GenderID)}}">
                                    </td>
                                    <td class="footable-first-visible">{{$i++}}</td>
                                    <td>
                                        <div>{{isset($dog->Heb_Name)?$dog->Heb_Name :'N/A'}}</div>
                                        <div>{{isset($dog->Eng_Name)?$dog->Eng_Name :'N/A'}}</div>
                                    </td>
                                    <td>{{isset($dog->breed_name->BreedName)?$dog->breed_name->BreedName:'N/A'}}</td>
                                    <td>{{(isset($dog->GenderID) && !empty($dog->GenderID))?((round($dog->GenderID) == 2 ? translate('female'):translate('male'))):'N/A'}}
                                    </td>
                                    <td>{{isset($dog->BirthDate)?convertDate($dog->BirthDate):'N/A'}} </td>
                                    <td>{{isset($dog->SagirID)?round($dog->SagirID):'N/A'}}
                                    </td>
                                    
                                    <td>
                                       
                                          <div class="container step_dropdown">
    <div class="row">
        <h2></h2>
        <hr>
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{translate('Please Select')}}
            </button>
            <ul class="dropdown-menu multi-level ul_class_dropdown" role="menu" aria-labelledby="dropdownMenu">
               <?php 
               $first = 'greenclassbackground';
               $secord =  $third =  $four = $five = '';
                    if(isset($dog->breeding_name->filled_step) && $dog->breeding_name->filled_step == 1){
                         $first = "blueclassbackground";
                         $secord = "greenclassbackground" ;
                         $third = $four = $five = '';
                    }elseif(isset($dog->breeding_name->filled_step) && $dog->breeding_name->filled_step == 2){
                         $first = $secord= "blueclassbackground";
                         $third = "greenclassbackground";
                         $four = $five = '';
                    }elseif(isset($dog->breeding_name->filled_step) && $dog->breeding_name->filled_step == 3){
                         $first = $secord =  $third = "blueclassbackground";
                         $four = "greenclassbackground";
                         $five = "";
                    }elseif(isset($dog->breeding_name->filled_step) && $dog->breeding_name->filled_step == 4){
                         $first = $secord =  $third =  $four = "blueclassbackground";
                         $five = "greenclassbackground";
                    }elseif(isset($dog->breeding_name->filled_step) && $dog->breeding_name->filled_step == 5){
                        $first = $secord =  $third =  $four = $five = "blueclassbackground";
                    }
                ?>

                 @if(isset($dog->GenderID) && round($dog->GenderID) == 2)
                <li class="dropdown-submenu">
                  <a  class="dropdown-item" tabindex="-1" href="#">{{translate('Breeding process')}}</a>
                  <ul class="dropdown-menu ul_class_dropdown_sub">
                    
                    <li class="dropdown-item {{$first}}"><a href="{{url('/breeding/add/'.round($dog->SagirID))}}">{{translate('Breeding Page')}}<?php if($first == "greenclassbackground"){
                           echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                    <li class="dropdown-item {{$secord}}"><a href="{{url('/birthing/'.round($dog->SagirID))}}">{{translate('Birthing Page')}}<?php if($secord == "greenclassbackground"){
                           echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                    <li class="dropdown-item {{$third}}"><a href="{{url('/chipping/'.round($dog->SagirID))}}">{{translate('Chipping Page')}}<?php if($third == "greenclassbackground"){
                           echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                    <li class="dropdown-item {{$four}}"><a href="{{url('/supervisor_checkup/'.round($dog->SagirID))}}">{{translate('Supervisor checkup')}} <?php if($four == "greenclassbackground"){
                           echo "( " .translate('waiting for submit') . " )";
                            }?></a></li>
                   
                  </ul>
                </li>
                <li class="dropdown-divider"></li>
                 @endif
                <li class="dropdown-item"><a href="{{url('/owner_transfer/'.$dog->DataID)}}">{{translate('Transfer Owner')}}</a></li>
              </ul>
        </div>
    </div>
</div>
                                    </td>
                                </tr>
                                @endforeach

                                @endif

                            </tbody>
                        </table>
                        {{ $dogs_details->appends($_GET)->links() }}
                        <input type="hidden" value="" name="hidden_sagirId" id="hidden_sagirId">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link href="{{ asset('assets/node_modules/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('jquery')
<script type="text/javascript" src="{{ asset('assets/node_modules/multiselect/js/jquery.multi-select.js')}}"></script>
<script src="{{ asset('js/native/dog.js?'.time())}}"></script>
@toastr_render
@endsection

@section('javascript')
<script type="text/javascript">
    $(".select2").select2();
   
   $(window).scroll(function(){
    if ($(window).scrollTop() >= 60) {
        $('#breeding_btn').addClass('fixed-header');
    }
    else {
        $('#breeding_btn').removeClass('fixed-header'); 
    }
});
</script>

@endsection