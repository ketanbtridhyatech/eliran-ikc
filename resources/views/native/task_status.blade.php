<div class="card">

                    <div class="card-header bg-info">
                        <h4 class="mb-0 text-white">{{translate('Task status')}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="col-md-6 {{(in_array($setting_details->value,$skill_details) &&
                            in_array(Auth::user()->id,$edit_permission))?'':'readonly_checkbox'}}">
                            <div class="form-group">
                                <label for="status">{{translate('Status')}}</label>
                                @if(in_array($setting_details->value,$skill_details) &&
                                in_array(Auth::user()->id,$edit_permission))
                                {{ generate_html_element("task-status","task_status","task_status","string",$task_details->status)}}
                                @else
                                {{ generate_html_element("task-status","task_status","task_status","string",$task_details->status,array(),array("readonly"))}}
                                @endif
                            </div>

                        </div>
                        <div class="col-md-3">
                            @if(in_array($setting_details->value,$skill_details) &&
                            in_array(Auth::user()->id,$edit_permission))
                            <button type="submit" class="btn btn-success" name="update_status" id="update_status"
                                value="update_status"> <i class="fa fa-check"></i>
                                {{translate('Submit')}}</button>
                            @else
                            <button type="submit" class="btn btn-success readonly_bgcolor" name="update_status"
                                id="update_status" value="update_status" disabled> <i class="fa fa-check"></i>
                                {{translate('Submit')}}</button>
                            @endif
                        </div>
                    </div>
                </div>