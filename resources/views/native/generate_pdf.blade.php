<html>

<head>
    <title>{{translate('Puppy Card')}}</title>
    <link href="{{ asset('css/pdf_css.css?'.time()) }}" rel="stylesheet">
</head>

<body>
    @foreach($dog_data as $dog)
    <div>
        <h1>
            <center class="heading-text">{{translate('תעודת גור')}}</center>
        </h1>
        <img src="{{(!empty($dog->puppie_details))?$dog->puppie_details->image:''}}" alt="img" class="float-left">
    </div>
    <div class="puppie_name">
        <h2>{{$dog->Heb_Name}}</h2>
    </div>
    <div class="top-mragin">
        <table>
            <tr>
                <td>
                    <h3>גזע : {{!empty($dog->breed_name)?$dog->breed_name->BreedName:''}} </h3>
                </td>
                <td>
                    <h3>צבע : {{!empty($dog->color_name_new)?$dog->color_name_new->ColorNameHE:''}} </h3>
                </td>
                <td>
                    <h3>מין : {{round($dog->GenderID == 1) ?"Male":"Female"}} </h3>
                </td>
            </tr>
        </table>
    </div>
    <div class="top-margin">
        <table>
            <tr>
                <td>
                    <h3>מספר סגיר : {{round($dog->SagirID).$dog->sagir_prefix}} </h3>
                </td>
                <td>
                    <h3>מספר שבב : {{$dog->Chip}} </h3>
                </td>

            </tr>
        </table>
    </div>
    <div class="top-margin">
        <table>
            <tr>
                <td>
                    <h3>המוכר : 
                <?php 
                    if(!empty($dog->user_mother_details) && !empty($dog->user_mother_details->user_detail)){
                        echo $dog->user_mother_details->user_detail->first_name . " , " . $dog->user_mother_details->user_detail->last_name . " , 0" . $dog->user_mother_details->user_detail->mobile_phone;
                    }
                ?>
                    </h3>
                </td>

            </tr>
        </table>
    </div>
    <div class="top-margin">
        <table>
            <tr>
                <td>
                    <h3>תוצאת ביקורת שגר: 
                    <?php
                    if(!empty($dog->puppie_details)){
                        if($dog->puppie_details->approval_status == 'Approved'){ ?>
                            מאושר
                       <?php }
                        if($dog->puppie_details->approval_status == 'NeedTest'){ ?>
                             בגיל שנה מהסיבה
                        <?php }
                        if($dog->puppie_details->approval_status == 'NotApproved'){?>
                            לא מתאים לגידול מהסיבה
                       <?php }
                    }
                    ?>
                    {{(!empty($dog->puppie_details))?$dog->puppie_details->note:''}} </h3>
                     </h3>
                </td>

            </tr>
        </table>
    </div>
    
    @if(!empty($dog->puppie_details) && !empty($dog->puppie_details->supervisor_notes))
    <div class="top-margin">
        <table>
            <tr>
                <td>
                    <h3>הערות מנהל ספר הגידול :{{(!empty($dog->puppie_details))?$dog->puppie_details->supervisor_notes:''}} </h3>
                </td>

            </tr>
        </table>
    </div>
    @endif

    @if(!empty($dog->pedigree_color))
    <div class="top-margin">
        <table>
            <tr>
                <td>
                    <h3>הגור {}לתעודה בצבע {{$dog->pedigree_color}} מטעם ההתאחדות הישראלית</h3>
                </td>

            </tr>
        </table>
    </div>
    @endif
    <div class="top-margin">
        <table>
            <tr>
                <td>
                    <h3>פרטי הורי הגור כפי שדווחו להתאחדות</h3>
                </td>

            </tr>
        </table>
    </div>
    <div class="top-margin page-break">
        <table class="border">
            <tr>
                <td>
                    <h3></h3>
                </td>
                <td>
                    <h3>נקבה</h3>
                </td>
                <td>
                    <h3>זכר </h3>
                </td>


            </tr>
            <tr>
                <td>
                    שם
                </td>
                <td>
                    <?php 
                    if(!empty($dog->mother_details)){
                        echo $dog->mother_details->Heb_Name;
                        echo "<br>";
                        echo $dog->mother_details->Eng_Name;
                    }

                    ?>
                </td>
                <td>
                    <?php 
                    if(!empty($dog->father_details)){
                        echo $dog->father_details->Heb_Name;
                        echo "<br>";
                        echo $dog->father_details->Eng_Name;
                    }

                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    מס' סגיר
                </td>
                <td>
                    {{!empty($dog->mother_details)?round($dog->mother_details->SagirID):""}}
                </td>
                <td>
                    {{!empty($dog->father_details)?round($dog->father_details->SagirID):""}}
                </td>
            </tr>
            
            <tr>
                <td>
                    תארים
                </td>
                <td>
                   
                </td>
                <td>
                   
                </td>
            </tr>
            <tr>
                <td>
                    פרופיל דנ"א
                </td>

                <td>
                    <?php 
                     $yes = "כן";
                     $no = "לא";   
                    ?>
                     {{(empty($breeding_data->Female_DNA) && $breeding_data->Female_DNA == 0)?$no:$yes}}
                </td>
                <td>
                    <?php 
                     $yes = "כן";
                     $no = "לא";   
                    ?>
                   {{(empty($breeding_data->Male_DNA) && $breeding_data->Male_DNA == 0)?$no:$yes}}
                </td>
            </tr>
           
        </table>
    </div>
    @endforeach
</body>

</html>
