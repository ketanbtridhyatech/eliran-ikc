 <meta charset="UTF-8">
    <table width="100%" align="center">
        <thead>
            <tr class="footable-header">
                <th>{{translate('No')}}</th>
                <th>{{translate('English name')}}</th>
                <th>{{translate('hebrew name')}}</th>
                <th> {{translate('Old Code')}}</th>
            </tr>
        </thead>
        <tbody>
            @if(! $color_details->isEmpty())
           
            @php
            $i=1;
            @endphp

            @foreach($color_details as $color)
            <tr>
                <td>{{$i}}</td>
                <td>{{$color->ColorNameEN}}</td>
                <td>{{$color->ColorNameHE}}</td>
                <td>{{$color->OldCode}}</td>
            </tr>
            @php
            $i++;
            @endphp
            @endforeach
            @else
            <tr>
                <td colspan="12">No Records</td>
            </tr>
            @endif

        </tbody>
    </table>
