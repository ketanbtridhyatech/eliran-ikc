 <meta charset="UTF-8">
    <table width="100%" align="center">
        <thead>
            <tr class="footable-header">
                <th>{{translate('No')}}</th>
                <th>{{translate('Title Name')}}</th>
                <th>{{translate('Title Code')}}</th>
                <th>{{translate('Remark')}}</th>
                <th>{{translate('Desc')}}</th>
            </tr>
        </thead>
        <tbody>
            @if(! $titles_details->isEmpty())
           
            @php
            $i=1;
            @endphp

            @foreach($titles_details as $title)
            <tr>
                <td>{{$i}}</td>
                <td>{{$title->TitleName}}</td>
                <td>{{$title->TitleCode}}</td>
                <td>{{$title->Remark}}</td>
                <td>{{$title->TitleDesc}}</td>
            </tr>
            @php
            $i++;
            @endphp
            @endforeach
            @else
            <tr>
                <td colspan="12">No Records</td>
            </tr>
            @endif

        </tbody>
    </table>
