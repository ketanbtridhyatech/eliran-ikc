
    <table width="100%" align="center">
        <thead>
            <tr class="footable-header">
                <th>{{translate('No')}}</th>
                <th>{{translate('English Name')}}</th>
                <th>{{translate('Hebrew Name')}}</th>
                <th> {{translate('ID')}}</th>
                <th>{{translate('BreedCode')}}</th>
            </tr>
        </thead>
        <tbody>
            @if(! $breeds_details->isEmpty())
           
            @php
            $i=1;
            @endphp

            @foreach($breeds_details as $breed)
            <tr>
                <td>{{$i}}</td>
                <td>{{$breed->BreedNameEN}}</td>
                <td>{{$breed->BreedName}}</td>
                <td>{{$breed->id}}</td>
                <td>{{$breed->BreedCode}}</td>
            </tr>
            @php
            $i++;
            @endphp
            @endforeach
            @else
            <tr>
                <td colspan="12">No Records</td>
            </tr>
            @endif

        </tbody>
    </table>
