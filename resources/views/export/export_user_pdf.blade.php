<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style type="text/css">
      td,th {
        padding: 5px;
      }
  </style>
</head>
<body>

<div class="container">           
  <table width='100%' style='border-collapse: separate;font-size: 10px;margin-right: 10px'>
    <thead>
        <tr>
            <th width="4%">{{translate('First Name')}}</th>
            <th width="4%">{{translate('Last Name')}}</th>
            <th width="4%">{{translate('Email')}}</th>
            <th width="4%">{{translate('Country Code')}}</th>
            <th width="4%">{{translate('Mobile')}}</th>
            <th width="4%">{{translate('Phone Number')}}</th>
            <th width="4%">{{translate('Date of Birth')}}</th>
            <th width="4%">{{translate('Social Id')}}</th>
            <th width="4%">{{translate('House Number')}}</th>
            <th width="4%">{{translate('Street')}}</th>
            <th width="4%">{{translate('City')}}</th>
            <th width="4%">{{translate('Country')}}</th>
            <th width="4%">{{translate('Zip Code')}}</th>
            <th width="4%">{{translate('Fax')}}</th>
            <th width="4%">{{translate('User Skills')}}</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($listing) && !empty($listing))
        @foreach($listing as $listingVal)
        <tr>
            @if(currentLanguage()==1)
            <td>{{isset($listingVal->first_name)?$listingVal->first_name:'N/A'}}</td>
            <td>{{isset($listingVal->last_name)?$listingVal->last_name:'N/A'}}</td>
            @else
            <td>{{isset($listingVal->first_name_en)?$listingVal->first_name_en:'N/A'}}</td>
            <td>{{isset($listingVal->last_name_en)?$listingVal->last_name_en:'N/A'}}</td>
            @endif
            <td>{{isset($listingVal->email)?$listingVal->email:'N/A'}}</td>
            <td>{{isset($listingVal->country->country_code)?$listingVal->country->country_code:'N/A'}}</td>
            <td>{{isset($listingVal->mobile_phone)?$listingVal->mobile_phone:'N/A'}}</td>
            <td>{{isset($listingVal->phone)?$listingVal->phone:'N/A'}}</td>
            <td>{{isset($listingVal->birth_date)?convertDate($listingVal->birth_date):'N/A'}}</td>
            <td>{{isset($listingVal->social_id_number)?$listingVal->social_id_number:'N/A'}}</td>
            <td>{{isset($listingVal->address_street_number)?$listingVal->address_street_number:'N/A'}}</td>
            <td>{{isset($listingVal->address_street)?$listingVal->address_street:'N/A'}}</td>
            <td>{{isset($listingVal->address_city_en)?$listingVal->address_city_en:'N/A'}}</td>
            <td>{{isset($listingVal->country->country_name_en)?$listingVal->country->country_name_en:'N/A'}}</td>
            <td>{{isset($listingVal->address_zip)?$listingVal->address_zip:'N/A'}}</td>
            <td>{{isset($listingVal->fax)?$listingVal->fax:'N/A'}}</td>
            <td>
                @if($listingVal->skills && !empty($listingVal->skills))
                @foreach($listingVal->skills as $skillsVals)
                @if(currentLanguage() == 1)
                {{$skillsVals->skill_name}},
                @else
                {{$skillsVals->skill_name_en}},
                @endif
                @endforeach
                @endif
            </td>
        </tr>
        @endforeach
        @endif

    </tbody>
</table>
</div>

</body>
</html>




