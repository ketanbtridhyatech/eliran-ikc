 <meta charset="UTF-8">
    <table width="100%" align="center">
        <thead>
            <tr class="footable-header">
                <th>{{translate('No')}}</th>
                <th>{{translate('English Name')}}</th>
                <th>{{translate('Hebrew Name')}}</th>
                <th> {{translate('GidulCode')}}</th>
                <th>{{translate('MegadelCode')}}</th>
            </tr>
        </thead>
        <tbody>
            @if(! $breedinghouses_details->isEmpty())
           
            @php
            $i=1;
            @endphp

            @foreach($breedinghouses_details as $breedinghouses)
            <tr>
                <td>{{$i}}</td>
                <td>{{$breedinghouses->EngName}}</td>
                <td>{{$breedinghouses->HebName}}</td>
                <td>{{$breedinghouses->GidulCode}}</td>
                <td>{{$breedinghouses->MegadelCode}}</td>
            </tr>
            @php
            $i++;
            @endphp
            @endforeach
            @else
            <tr>
                <td colspan="12">No Records</td>
            </tr>
            @endif

        </tbody>
    </table>
