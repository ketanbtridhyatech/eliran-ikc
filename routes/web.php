<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//app()->setLocale('iw');
//echo Config::get('app.locale');
Route::view('/', 'welcome');




Route::group(['middleware' => ['auth', 'checkmodule'],'prefix' => config('constant.backend')], function () {

	Route::get('/', 'AdminHomeController@index');
	Route::post('/changeLanguage', 'AdminUserController@changeLanguage');
	Route::get('logout', 'Auth\LoginController@logOut')->name('logout');

	Route::group(['prefix' => 'translation'], function () {
		Route::get('/', 'AdminTranslationController@index');
		Route::get('add', 'AdminTranslationController@add');
		Route::post('store', 'AdminTranslationController@store');
		Route::get('edit/{id}', 'AdminTranslationController@edit');
		Route::post('update', 'AdminTranslationController@update');
		Route::get('delete/{id}', 'AdminTranslationController@delete');
		

		Route::get('/module/{module_id}', 'AdminTranslationController@molduleIndex');
		Route::get('module/add/{module_id}', 'AdminTranslationController@moduleAdd');
		Route::post('module/store', 'AdminTranslationController@moduleStore');
		Route::get('module/edit/{module_id}/{id}', 'AdminTranslationController@moduleEdit');
		Route::post('module/update', 'AdminTranslationController@moduleUpdate');
		Route::get('module/delete/{module_id}/{id}', 'AdminTranslationController@moduleDelete');

		
	});

	
	Route::group(['prefix' => 'module'], function () {
		Route::get('/', 'AdminModulesController@index');
		Route::get('add', 'AdminModulesController@add');
		Route::post('store', 'AdminModulesController@store');
		Route::get('edit/{id}', 'AdminModulesController@edit');
		Route::post('update', 'AdminModulesController@update');
		Route::get('delete/{id}', 'AdminModulesController@delete');
	});

	Route::group(['prefix' => 'user'], function () {
		Route::get('/', 'AdminUserController@index');
		Route::get('add', 'AdminUserController@add');
		Route::post('store', 'AdminUserController@store');
		Route::get('edit/{id}', 'AdminUserController@edit');
		Route::post('update', 'AdminUserController@update');
		Route::get('delete/{id}', 'AdminUserController@delete');
		Route::get('detail/{id}', 'AdminUserController@detail');
		Route::post('multiDelete', 'AdminUserController@multiDelete');
		Route::post('multiActive', 'AdminUserController@multiActive');
		Route::post('multiInActive', 'AdminUserController@multiInActive');
		Route::post('getCountryCode', 'AdminUserController@getCountryCode');
		Route::delete('deleteRelatedDog', 'AdminUserController@deleteRelatedDog');
	
	});

	Route::group(['prefix' => 'settings'], function () {
		Route::get('/', 'AdminSettingsController@index');
		Route::post('checkSlug', 'AdminSettingsController@checkSlug');
		Route::get('add', 'AdminSettingsController@add');
		Route::post('store', 'AdminSettingsController@store');
		Route::get('edit/{id}', 'AdminSettingsController@edit');
		Route::post('update', 'AdminSettingsController@update');
		Route::get('delete/{id}', 'AdminSettingsController@delete');
	});

	Route::group(['prefix' => 'template'], function () {
			Route::get('/', 'AdminTemplateController@index');
			Route::get('add', 'AdminTemplateController@add');
			Route::post('checkSlug', 'AdminTemplateController@checkSlug');
			
			Route::post('store', 'AdminTemplateController@store');
			Route::get('edit/{id}', 'AdminTemplateController@edit');
			Route::post('update', 'AdminTemplateController@update');
			Route::get('delete/{id}', 'AdminTemplateController@delete');
	});
	Route::group(['prefix' => 'admin_html_element'], function () {
		Route::get('/', 'AdminHtmlElementController@index');
		Route::post('checkSlug', 'AdminHtmlElementController@checkSlug');
		Route::get('add', 'AdminHtmlElementController@add');
		Route::post('store', 'AdminHtmlElementController@store');
		Route::get('edit/{id}', 'AdminHtmlElementController@edit');
		Route::post('update', 'AdminHtmlElementController@update');
		Route::get('delete/{id}', 'AdminHtmlElementController@delete');

		Route::get('/admin_html_element_option/{id}', 'AdminHtmlElementController@get_html_element_value');
		Route::get('/admin_html_element_option/add/{element_id}', 'AdminHtmlElementController@html_element_value_add');
		Route::post('/admin_html_element_option/store', 'AdminHtmlElementController@html_element_value_store');
		Route::get('/admin_html_element_option/edit/{element_id}/{id}', 'AdminHtmlElementController@html_element_value_edit');
		Route::post('/admin_html_element_option/update', 'AdminHtmlElementController@html_element_value_update');
		Route::get('/admin_html_element_option/delete/{element_id}/{id}','AdminHtmlElementController@html_element_value_delete');


		
	});

	Route::group(['prefix' => 'country'], function () {
		Route::get('/', 'AdminCountryController@index');
		Route::get('add', 'AdminCountryController@add');
		Route::post('store', 'AdminCountryController@store');
		Route::get('edit/{id}', 'AdminCountryController@edit');
		Route::post('update', 'AdminCountryController@update');
		Route::get('delete/{id}', 'AdminCountryController@delete');
	});

	Route::group(['prefix' => 'skills'], function () {
		Route::get('/', 'AdminSkillsController@index');
		Route::get('add', 'AdminSkillsController@add');
		Route::post('store', 'AdminSkillsController@store');
		Route::get('edit/{id}', 'AdminSkillsController@edit');
		Route::post('update', 'AdminSkillsController@update');
		Route::get('delete/{id}', 'AdminSkillsController@delete');
	});

	Route::group(['prefix' => 'users-skills'], function () {
		Route::get('/', 'AdminUsersSkillsController@index');
		Route::get('assign/{id}', 'AdminUsersSkillsController@assign');
		Route::post('store', 'AdminUsersSkillsController@store');
		
	});

	Route::group(['prefix' => 'roles'], function () {
		Route::get('/', 'AdminRolesController@index');
		Route::get('add', 'AdminRolesController@add');
		Route::post('store', 'AdminRolesController@store');
		Route::get('edit/{id}', 'AdminRolesController@edit');
		Route::post('update', 'AdminRolesController@update');
		Route::get('delete/{id}', 'AdminRolesController@delete');
	});
	Route::get('exportUsers', 'AdminUserController@exportUsers');
	

});

Route::group(['before' => 'auth','prefix' => 'admin'], function(){
	//Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
	//Route::post('login', 'Auth\LoginController@doLogin');

	//Route::get('login/otp', 'Auth\LoginOtpController@showOtpLoginForm');	
	Route::get('login', 'Auth\LoginOtpController@showOtpLoginForm')->name('login');
	Route::post('sendOTP', 'Auth\LoginOtpController@sendOTP');
	Route::post('otpLogin', 'Auth\LoginOtpController@otpLogin');
	Route::get('migration', 'AdminUserController@migration');
	Route::post('migration/add_new_user','AdminUserController@migration_add_new_user');
	Route::post('migration/add_new_member_form','AdminUserController@migration_add_new_member_form');
	Route::get("migration/checkbox","AdminUserController@migrationCheckbox");
	Route::get("migration/checkbox/unChecked","AdminUserController@migrationCheckboxunChecked");
	Route::post('migration/checking_user','AdminUserController@checking_user');
	Route::get('migration/data_compare','AdminUserController@data_compare');
	Route::post('migration/search_related_dogs','AdminUserController@search_related_dogs');
	Route::get('data_compare/export_excel/{id}','AdminUserController@dogs_excel');
	Route::post('migration/delete_native_user/{id}','AdminUserController@delete_native_user');
Route::post('sendSignUpMail', 'Native\MailController@sendSignUpMail');
Route::get('get_dog_list', 'BackendDogController@get_dog_list');
/*Route::get('get_breeds_list', 'BackendBreedController@get_breeds_list');*/

	Route::group(['prefix' => 'breeds'], function () {
		Route::get('/', 'BackendBreedController@get_breeds_list');
		Route::get('add', 'BackendBreedController@add');
		Route::post('store', 'BackendBreedController@store');
		Route::get('edit/{id}', 'BackendBreedController@edit');
		Route::post('update', 'BackendBreedController@update');
		Route::get('delete/{id}', 'BackendBreedController@delete');
	});
	Route::group(['prefix' => 'breedinghouses'], function () {
		Route::get('/', 'AdminBreedingHousesController@get_breedinghouses_list');
		Route::get('add', 'AdminBreedingHousesController@add');
		Route::post('store', 'AdminBreedingHousesController@store');
		Route::get('edit/{id}', 'AdminBreedingHousesController@edit');
		Route::post('update', 'AdminBreedingHousesController@update');
		Route::get('delete/{id}', 'AdminBreedingHousesController@delete');
	});
	Route::group(['prefix' => 'titles'], function () {
		Route::get('/', 'AdminTitleController@get_title_list');
		Route::get('add', 'AdminTitleController@add');
		Route::post('store', 'AdminTitleController@store');
		Route::get('edit/{id}', 'AdminTitleController@edit');
		Route::post('update', 'AdminTitleController@update');
		Route::get('delete/{id}', 'AdminTitleController@delete');
	});
	Route::group(['prefix' => 'colors'], function () {
		Route::get('/', 'AdminColorController@get_color_list');
		Route::get('add', 'AdminColorController@add');
		Route::post('store', 'AdminColorController@store');
		Route::get('edit/{id}', 'AdminColorController@edit');
		Route::post('update', 'AdminColorController@update');
		Route::get('delete/{id}', 'AdminColorController@delete');
	});
});
	Route::post('sendSignUpMail', 'Native\MailController@sendSignUpMail');
//For Frontend user
Route::group(['before' => 'auth'], function(){
    Route::get('login', 'Native\FrontLoginOtpController@showOtpLoginForm')->name('login');
    Route::post('sendOTP', 'Native\FrontLoginOtpController@sendOTP');
    Route::post('otpLogin', 'Native\FrontLoginOtpController@otpLogin');
    Route::post('checkUser', 'Native\FrontLoginOtpController@checkUser');
});
Route::post('/changeLanguage', 'Native\FrontHomeController@changeLanguage');
Route::get('logout', 'Native\FrontLoginController@logOut')->name('logout');

Route::group(['middleware' => ['checkmodule']], function () {
    Route::group(['prefix' => 'dog'], function () {
        Route::get('/', 'Native\FrontDogController@getDogList');
    });
    Route::group(['prefix' => 'breeding'], function () {
        Route::get('add/{sagirId}', 'Native\BreedingController@add')->name('add');
        Route::get('breeding_done/{sagirId}', 'Native\BreedingController@add')->name('breeding_done');
        Route::post('store', 'Native\BreedingController@store');
        Route::post('check_sagir_exits', 'Native\BreedingController@check_sagir_exits');
        Route::post('breeding_status_change', 'Native\BreedingController@breeding_status_change');
        
	});
	Route::group(['prefix' => 'birthing'], function () {
        Route::get('{sagirId}', 'Native\BreedingController@add_birthing')->name('add_birthing');
		Route::post('store', 'Native\BreedingController@store_birthing');
		Route::get('payment/{id}', 'Native\BreedingController@payment');
        Route::post('birthing_status_change', 'Native\BreedingController@birthing_status_change');
	});
    Route::get('birthing_done/{sagirId}', 'Native\BreedingController@add_birthing')->name('birthing_done');

	Route::group(['prefix' => 'tasks'], function () {
		Route::get('/', 'Native\FrontTaskController@getTaskList');
		Route::get('/task_details/{id}', 'Native\FrontTaskController@getTaskDetails');
		Route::post('/update', 'Native\FrontTaskController@update_task');
		Route::post('/task_details/update_dog_status', 'Native\FrontTaskController@updateDogStatus');
		Route::post('/task_details/get_dog_details', 'Native\FrontTaskController@get_dog_details');
		Route::post('/check_breed_color','Native\FrontTaskController@check_breed_color');
	});
	Route::group(['prefix' => 'chipping'], function () {
		Route::get('/{id}', 'Native\BreedingController@add_chipping')->name('add_chipping');
		Route::post('/add_new_dog_form','Native\BreedingController@add_new_dog_form');
		Route::post('/store','Native\BreedingController@store_chipping');
		Route::post('/ajax_add_dog','Native\BreedingController@ajax_store_dog');
		Route::post('/chcked_dogsDB','Native\BreedingController@chcked_dogsDB');
		Route::post('/chcked_dogsDB_single','Native\BreedingController@chcked_dogsDB_single');
		Route::post('/chcked_dogsDB_validation','Native\BreedingController@chcked_dogsDB_validation');
	});
	Route::get('chipping_done/{id}', 'Native\BreedingController@add_chipping')->name('chipping_done');

	Route::group(['prefix' => 'supervisor_checkup'], function () {
		Route::get('{id}', 'Native\BreedingController@supervisor_checkup')->name('supervisor_checkup');
		
	});
	Route::get('supervisor_checkup_done/{id}', 'Native\BreedingController@supervisor_checkup')->name('supervisor_checkup_done');
	
	Route::group(['prefix' => 'owner_transfer'], function () {
		Route::get('{id}', 'Native\BreedingController@owner_transfer');
		Route::post('/add_new_owner_form','Native\BreedingController@add_new_owner_form');
		Route::post('/owner_exits','Native\BreedingController@owner_exits');
		Route::post('/otp_send','Native\BreedingController@otp_send');
		Route::post('/append_userdata','Native\BreedingController@append_userdata');
		Route::post('/store','Native\BreedingController@owner_transfer_store');
		
	});
	
	Route::group(['prefix' => 'active_breeding'], function () {
		Route::get('/', 'Native\BreedingController@active_breeding');
		Route::post('/delete_dog', 'Native\BreedingController@delete_dog');
		
	});
	Route::group(['prefix' => 'breed_related_dog'], function () {
		Route::get('/', 'Native\BreedingController@breed_related_dog');
		
		
	});
	//Route::get('/generate_pdf/{id}','Native\BreedingController@generate_pdf');
	Route::get('/download/{id}','Native\FrontTaskController@download');
	
	
	
});

Route::group(['prefix' => 'payment'], function () {
		Route::get('notify', 'Native\PaymentController@notify');
		Route::get('return_url', 'Native\PaymentController@return_url');
	});

Route::get('/error', function () {  return view('error_page'); })->name('error_page');