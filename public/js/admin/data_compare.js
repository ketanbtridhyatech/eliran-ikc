$('#search_submit').on('click',function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#ajaxLoader').addClass('d-block');
    jQuery.ajax({
        url: backendUrl + "/migration/search_related_dogs",
        method: 'post',
        data: {
            id: $('#search_by_id').val(),
        },
        success: function (data) {
           $('#dogs_data').html(data);
           $('#ajaxLoader').removeClass('d-block');
        }
    });
})
$(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });