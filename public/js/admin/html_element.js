$(document).ready(function () {
    $("#name").change(function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
       // $('#ajaxLoader').addClass('d-block');
        jQuery.ajax({
            url: backendUrl + "/admin_html_element/checkSlug",
            method: 'post',
            data: {
                fieldTitle: $(this).val(),
            },
            success: function (data) {
                data = jQuery.parseJSON(JSON.stringify(data));
                //$('#ajaxLoader').removeClass('d-block');
               $('#slug').val(data.slug);


            }});

    });
});