$(document).ready(function () {
    $("#language_id_login").change(function(){

        var  language_id =  $(this).val();
       // alert(language_id);
        //$.cookie("language_id", null , {path: "/"});
        $.cookie("language_id", language_id , {path: "/"});
        location.reload();
    });
    $('#mobile_phone').focus();

    $("#mobileForm").validate({
        rules: {
            mobile_phone: {
                required: true
            },
            country_code: {
                required: true
            },
        },
        messages: {
            mobile_phone: 'Please enter your registered mobile number.',
        },
        submitHandler: function (form) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        /*$("#mobileForm").slideUp();
       $("#otpForm").fadeIn();
      $('#otp').focus();
        return false;*/

            $('#ajaxLoader').addClass('d-block');
            jQuery.ajax({
                url: backendUrl + "/sendOTP",
                method: 'post',
                data: {
                    mobile_phone: $('#mobile_phone').val(),
                    country_code: $('#country_code').val(),
                    Mobile_number_invalid : $('#Mobile_number_invalid').val(),
                    User_invalid : $('#User_invalid').val(),
                    OTP_Successfully_sent : $('#OTP_Successfully_sent').val(),
                },
                success: function (data) {
                    data = jQuery.parseJSON(JSON.stringify(data));
                    $('#ajaxLoader').removeClass('d-block');
                    if (data.success == true) {
                        toastr.success(data.msg);
                        $("#mobileForm").slideUp();
                        $("#otpForm").fadeIn();
                        $('#otp').focus();
                    } else {
                        //$("#mobileForm").slideUp();
                        // $("#otpForm").fadeIn();
                        toastr.error(data.msg);
                    }


                }
            });
        }
    });

    $("#otpForm").validate({
        rules: {
            otp: {
                required: true
            },

        },
        messages: {
            otp: 'Please enter recieved OTP',
        },
        submitHandler: function (form) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#ajaxLoader').addClass('d-block');
            jQuery.ajax({
                url: backendUrl + "/otpLogin",
                method: 'post',
                data: {
                    mobile_phone: $('#mobile_phone').val(),
                    otp: $('#otp').val(),
                    Login_Successfully : $('#Login_Successfully').val(),
                    invalid_OTP : $('#invalid_OTP').val(),
                },
                success: function (data) {
                    data = jQuery.parseJSON(JSON.stringify(data));
                    if (data.success == true) {
                        toastr.success(data.msg);
                            window.location.reload(true);
                    } else {
                        $('#ajaxLoader').removeClass('d-block');
                        toastr.error(data.msg);
                    }


                }
            });
        }
    });
    $("#signup").validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            mobile_phone_signup: {
                required: true
            },
            /*remarks: {
                required: true
            },*/

        },
        messages: {
            first_name: 'Please enter first name.',
            last_name: 'Please enter last name.',
            mobile_phone_signup: 'Please enter your registered mobile number.',
            //remarks: 'Please enter remarks.',
        },
        submitHandler: function (form) {
           $('#ajaxLoader').addClass('d-block');
           $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
           });
           
                   
                       $('#ajaxLoader').addClass('d-block');
                        jQuery.ajax({
                            url: backendUrl + "/sendSignUpMail",
                            method: 'post',
                            data: {
                                mobile_phone_signup: $('#mobile_phone_signup').val(),
                                country_code_signup: $('#country_code_signup').val(),
                                first_name: $('#first_name').val(),
                                last_name: $('#last_name').val(),
                                remarks: $('#remarks').val(),

                            },
                            success: function (data) {
                                data = jQuery.parseJSON(JSON.stringify(data));
                                $('#ajaxLoader').removeClass('d-block');
                                if (data.success == true) {
                                    toastr.success(data.msg);
                                    $("#signup").trigger("reset");
                                    $('#signUpModal').modal('hide');
                                     return false;
                                } else {
                                    //$("#mobileForm").slideUp();
                                    // $("#otpForm").fadeIn();
                                    toastr.error(data.msg);
                                }
            
            
                            }
                        });
                   
                
        }
    });

});
