$(document).ready(function () {
    $("#birthingAddForm").validate({

        rules: {
            birthing_date: {
                required: true
            },
            live_male_puppie: {
                required: function (element) {
                    if ($("#live_female_puppie").val() == "" && $("#live_male_puppie").val() == "") {
                        return true
                    } else {
                        return false
                    }
                }
            },
        },
        messages: {
            live_male_puppie: {
                required:"Add at least one live puppie"
            },
            
        },
    });

    var current_date_get = $('#current_date_get').val().split("-");
    var bread_created_date = $('#bread_date').val().split("-");

    $('#birthing_date').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false,
        minDate: new Date(bread_created_date[0], bread_created_date[1] - 1, bread_created_date[2]),
        maxDate: new Date(current_date_get[0], current_date_get[1] - 1, current_date_get[2]),
        format: $('#date_format').val(),
        lang: $('#current_language').val()
    }).on('change', function (e, date) {

        $("#birthingAddForm").valid()
    });




    $('#breeding_warning').modal('show');

    $('input[name="pregnancy"]').click(function () {
        //alert("in");
        if ($(this).prop('checked') == true) {
            Swal.fire({
                title: '',
                text: $('#are_you_sure').val(),
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: $('#popup_yes_button').val(),
                cancelButtonText: $('#popup_no_button').val()
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    //$('#ajaxLoader').addClass('d-block');
                    jQuery.ajax({
                        url: getsiteurl() + "/birthing/birthing_status_change",
                        method: 'post',
                        data: {
                            status: '1',
                            breeding_id: $('#breeding_id').val()
                        },
                        success: function (data) {
                            window.location.href = getsiteurl() + '/dog';
                        }
                    });
                }
            })
        }
    });

});

$("#live_male_puppie").keypress(function (evt) {
    evt.preventDefault();
});
$("#dead_male_puppie").keypress(function (evt) {
    evt.preventDefault();
});
$("#live_female_puppie").keypress(function (evt) {
    evt.preventDefault();
});
$("#dead_female_puppie").keypress(function (evt) {
    evt.preventDefault();
});
$("form button[type=submit]").click(function () {
    if ($('#birthingAddForm').valid()) {
        $(this).html($('#loading_text').val())
        $('button[type=submit]').not(this).prop('disabled', true);
        $('#cancel_button').prop('disabled', true);
    }

})
$('#live_male_puppie').on('change', function () {
    $("#birthingAddForm").valid();
})
$('#live_female_puppie').on('change', function () {
    $("#birthingAddForm").valid();
})
