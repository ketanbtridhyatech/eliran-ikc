$(document).ready(function () {

    $('#country_id').on("change", function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#ajaxLoader').addClass('d-block');
        jQuery.ajax({
            url: backendUrl + "/user/getCountryCode",
            method: 'post',
            data: {
                country_id: $('#country_id').val(),
            },
            success: function (data) {
                data = jQuery.parseJSON(JSON.stringify(data));
                $("#ajaxLoader").removeClass("d-block");
                if (data.status === true) {
                    $("#country_code").val(data.country_code)
                } else {
                    toastr.error(data.msg);
                }

            }
        });

    });
    $('.delete_record').on('click', function () {
            Swal.fire({
                title: $('#popup_title').val(),
                text: $('#popup_message').val(),
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: $('#popup_yes_button').val(),
                cancelButtonText: $('#popup_no_button').val()
            }).then((result) => {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    jQuery.ajax({
                        url: backendUrl + "/user/deleteRelatedDog",
                        method: 'delete',
                        data: {
                            sagir_id: $(this).attr('sagir_id'),
                            user_id: $(this).attr('user_id')
                        },
                        success: function (data) {

                            Swal.fire({
                                
                                text: "Dogs Deleted Successfully !",
                                showCancelButton: false,
                            }).then((result) => {
                                location.reload();
                            });
                             


                        }
                    })
                }

            })
        })

        ! function ($) {
            "use strict";

            var SweetAlert = function () {};

            //examples 
            SweetAlert.prototype.init = function () {



                },

                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
        }(window.jQuery),

        //initializing 
        function ($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);


});
