$(document).ready(function () {
	var bread_created_date = $('#current_date').val().split("-");
    $('#breeding_date').bootstrapMaterialDatePicker({ 
    weekStart: 0, time: false ,format:$('#date_format').val(),
    maxDate: new Date(bread_created_date[0], bread_created_date[1] - 1, bread_created_date[2]),
    lang : $('#current_language').val()});
$('input[name="pregnancy"]').click(function(){
    //alert("in");
    if($(this).prop('checked') == true){
        Swal.fire({
            title: '',
            text: $('#are_you_sure').val(),
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: $('#popup_yes_button').val(),
            cancelButtonText: $('#popup_no_button').val()
        }).then((result) => {
            if (result.value) {
                         $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        //$('#ajaxLoader').addClass('d-block');
                        jQuery.ajax({
                            url: getsiteurl()+"/breeding/breeding_status_change",
                            method: 'post',
                            data: {
                                status:'1',
                                breeding_id:$('#breeding_id').val()
                            },
                            success: function (data) {
                               window.location.href = getsiteurl()+'/dog';
                            }
                        });
            }
        })
    }       
});
});
    $('#breeding_warning').modal('show');
$("form button[type=submit]").click(function() {
    if($('#breedingAddForm').valid()){
    $(this).html($('#loading_text').val())
    $('button[type=submit]').not(this).prop('disabled',true);
    $('#cancel_button').prop('disabled',true);
    }
});

$('#maleId').change(function() {
    //alert("in");
        var value = $(this).val();
      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        //$('#ajaxLoader').addClass('d-block');
        jQuery.ajax({
            url: getsiteurl()+"/breeding/check_sagir_exits",
            method: 'post',
            data: {
                id:value
            },
            success: function (data) {
                data = jQuery.parseJSON(JSON.stringify(data));
                if(data.status == 'match'){
                    var nameText = data.Heb_Name + " , " + data.Eng_Name;
                    $('#sagirExits').html(nameText);
                }else{
                    $('#sagirExits').html('');
                    
                }
            }
        });
});