$(document).ready(function () {


    if ($('#edit_permission').val() == '' && $('#unupdated_dog').val() == 0) {
        $('#submit_2').attr('disabled', false);
    } else {
        $('#submit_2').attr('disabled', true);
    }
    var task_created_date = $('#created_at_hidden').val().split("-");
    $('#due_date').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false,
        format: $('#date_format').val(),
        lang: $('#current_language').val(),
        minDate: new Date(task_created_date[0], task_created_date[1] - 1, task_created_date[2])
    });
    var after_brithing_date = $('#birthing_date_hidden').val().split("-");
    $('#review_date').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false,
        format: $('#date_format').val(),
        minDate: new Date(after_brithing_date[0], after_brithing_date[1] - 1, after_brithing_date[2]),
        lang : $('#current_language').val()
    });

    $(document).on('keypress', '#chip_number', function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    })
});
$(document).on('click', '.change_status', function () {
    if ($('#male_owner_agree').val() != "") {
        $('#dog_span_' + $(this).attr('dog_id')).show();
    }
    //$('#dog_id').val($(this).attr('dog_id'));
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#ajaxLoader').addClass('d-block');
    jQuery.ajax({
        url: getsiteurl() + "/tasks/task_details/get_dog_details",
        method: 'post',
        data: {
            dog_id: $(this).attr('dog_id'),
            task_id: $('#task_details_id').val()
        },
        success: function (data) {
            //$('#color').trigger('change');
            $('#ajaxLoader').removeClass('d-block');
            $('#modal_data').html(data);
            $('.select2').select2({
                placeholder: 'select',
                dropdownParent: $('#status_modal')
            });

            /*$('.color_dropdown').select2({
                placeholder: 'select'
            });*/

            //$('#color').trigger('change');
            $("#edit_puppie_details").validate({
                // Specify validation rules
                rules: {
                    note: {
                        required: function (element) {
                            if ($("#need_test").is(":checked") || $("#not_approved").is(":checked")) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    },
                    image: {
                        required: function (element) {
                            if (image_available == 0) {
                                return true;
                            } else {
                                return false;
                            }
                        },
                        accept: "jpg|jpeg|png|JPEG|PNG"
                    },
                    document: {
                        accept: "pdf|jpg|jpeg|png|JPEG|PNG"
                    },

                    chip_number: {
                        required: true,
                        digits: true,

                    },
                },
                // Specify validation error messages
                messages: {
                    note: $('#popup_note_validation').val(),
                    image: {
                        required: $('#puppy_photo_validation').val(),
                        accept: "Please select only image"
                    },
                    document: {
                        accept: "Please select only document"
                    }
                },

            });
            $('#color').trigger('change');
            $("#color").change(function () {
                if ($(this).val() == '0') {
                    // alert("change");
                    $('#other_text').show();
                    $('#other').show();
                    $('#other').rules("add", {
                        required: true
                    });
                    $('#note').val($('#Color_not_fit_to_breed_rules').val());
                    //$(".status_radio").removeClass("readonly_checkbox");
                    $("#not_approved").prop("checked", true);
                    $(".status_radio").addClass("readonly_checkbox");
                } else {
                    $('#other_text').hide();
                    $('#other').rules("remove");
                    $('#other').hide();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    //$('#ajaxLoader').addClass('d-block');
                    jQuery.ajax({
                        url: getsiteurl() + "/tasks/check_breed_color",
                        method: 'post',
                        data: {
                            color: $(this).val(),
                            female_sagirid_hidden: $('#female_sagirid_hidden').val(),
                        },
                        success: function (data) {
                            if (data == 0 || data == '0') {
                                //alert("0");
                                $('#note').val($('#Color_not_fit_to_breed_rules').val());
                                $("#not_approved").prop("checked", true);
                                $(".status_radio").addClass("readonly_checkbox");

                            } else {
                                // alert("1");
                                //$('#note').val("");
                                $(".status_radio").removeClass("readonly_checkbox");

                            }
                        }
                    });
                }

            });
            /*if($('#color').val() == '0'){
                $('#other').show();
                $('#other').rules( "add", { required: true });
            }else{
                $('#other').rules("remove");
                $('#other').hide();
            }*/
            $('#status_modal').modal();
            if ($("#image_file").val() == '') {
                var image_available = 0;
            } else {
                var image_available = 1;
            }




        }
    });
})

$(document).on('click', '#change_status', function () {

    if ($('#edit_puppie_details').valid()) {
        var not_valid_chip = 0;
        if ($('#chip_number').val() != 0) {
            if ($('#chip_number').val().length < 15) {
                not_valid_chip = 1;
            }
            if ($('#chip_number').val().length >= 16) {
                not_valid_chip = 1;
            }
        } else {
            not_valid_chip = 1;
        }

        if (not_valid_chip == 1) {
            Swal.fire({
                title: 'פרטי השבב אינם תקינים',
                text: '',
                type: 'warning',
                //showCancelButton: true,
                confirmButtonColor: '#d33',
                //cancelButtonColor: '#d33',
                confirmButtonText: $('#popup_cancel_button').val(),
            }).then((result) => {
                return false;
            });
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            //$('#ajaxLoader').addClass('d-block');
            jQuery.ajax({
                url: getsiteurl() + "/chipping/chcked_dogsDB_single",
                method: 'post',
                data: {
                    chip_number: $('#chip_number').val(),
                    id: $('#dog_id').val(),
                    task_details_id: $('#task_details_id').val(),
                },
                success: function (data1) {

                    if (data1 == '1') {
                        // alert($('#chip_number').val());
                        var form = $('#edit_puppie_details');
                        var formdata = false;
                        if (window.FormData) {
                            formdata = new FormData(form[0]);
                        }

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $('#ajaxLoader').addClass('d-block');
                        jQuery.ajax({
                            url: getsiteurl() + "/tasks/task_details/update_dog_status",
                            method: 'post',
                            data: new FormData($('#edit_puppie_details')[0]),
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function (data) {
                                $('#ajaxLoader').removeClass('d-block');

                                $('#dog_list_status').html(data);
                                // $('#dog_span_'+$('#dog_id').val()).show();
                                if ($('#edit_permission').val() == '' && $('#unupdated_dog').val() == 0) {
                                    $('#submit_2').attr('disabled', false);
                                } else {
                                    $('#submit_2').attr('disabled', true);
                                }
                                $('#status_modal').modal('toggle');

                            }
                        });
                    } else {
                        Swal.fire({
                            title: $('#exits_in_database').val(),
                            text: '',
                            type: 'warning',
                            confirmButtonColor: '#d33',
                            confirmButtonText: $('#popup_cancel_button').val()
                        }).then((result) => {
                            if (result.value) {
                                return false;
                            }
                        });
                    }
                }
            });
        }



    }
})

$('#submit_form_review').on('click', function (e) {
    $("#tasksform").validate({
        // Specify validation rules
        rules: {
            review_manager: {
                required: true
            },
            review_date: {
                required: true
            },
            review_place: {
                required: true
            }
        },
        messages: {
            review_manager: $('#review_manager_validation').val(),
            review_date: $('#review_date_validation').val(),
            review_place: $('#review_place_validation').val()
        },
    });
    if ($('#tasksform').valid()) {
        $('#submit_form_review_val').val(1);
        $('#tasksform').submit();
    }
});
$('#submit_2').on('click', function (e) {
    $("#tasksform").validate({
        // Specify validation rules
        rules: {
            male_owner_agree: {
                required: true
            }
        },
        // Specify validation error messages


    });
    if ($('#tasksform').valid()) {
        Swal.fire({
            title: '',
            text: $('#popup_message').val(),
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: $('#popup_yes_button').val(),
            cancelButtonText: $('#popup_no_button').val()
        }).then((result) => {
            if (result.value) {

                $('#tasksform').submit();

            }
        })
    }


})
