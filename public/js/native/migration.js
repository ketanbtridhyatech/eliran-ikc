$(document).ready(function () {
    jQuery.validator.addMethod("exactlength", function (value, element, param) {
        return this.optional(element) || value.length == param;
    }, $.validator.format("Please enter exactly {0} characters."));
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
    }, "Please Enter Only Characters");
    jQuery.validator.addMethod("validemail", function (value, element) {
        return this.optional(element) || /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/i.test(value);
    }, "Please Enter Valid Email");
    jQuery.validator.addMethod("hebrew_validation", function (value, element) {
        return this.optional(element) || /^[\u0590-\u05FF ,.'-]+$/i.test(value);
    }, "Please Enter Only Hebrew");
    jQuery.validator.addMethod("english_validation", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/i.test(value);
    }, "Please Enter Only English");

    $(".stickyside").stick_in_parent({
        offset_top: 100
    });
    // @preserve jQuery.floatThead 1.2.9 - http://mkoryak.github.io/floatThead/ - Copyright (c) 2012 - 2014 Misha Koryak
    // @license MIT
    ! function (a) {
        function b(a, b, c) {
            if (8 == g) {
                var d = j.width(),
                    e = f.debounce(function () {
                        var a = j.width();
                        d != a && (d = a, c())
                    }, a);
                j.on(b, e)
            } else j.on(b, f.debounce(c, a))
        }

        function c(a) {
            window.console && window.console && window.console.log && window.console.log(a)
        }

        function d() {
            var b = a('<div style="width:50px;height:50px;overflow-y:scroll;position:absolute;top:-200px;left:-200px;"><div style="height:100px;width:100%"></div>');
            a("body").append(b);
            var c = b.innerWidth(),
                d = a("div", b).innerWidth();
            return b.remove(), c - d
        }

        function e(a) {
            if (a.dataTableSettings)
                for (var b = 0; b < a.dataTableSettings.length; b++) {
                    var c = a.dataTableSettings[b].nTable;
                    if (a[0] == c) return !0
                }
            return !1
        }
        a.floatThead = a.floatThead || {}, a.floatThead.defaults = {
            cellTag: null,
            headerCellSelector: "tr:first>th:visible",
            zIndex: 1001,
            debounceResizeMs: 10,
            useAbsolutePositioning: !0,
            scrollingTop: 0,
            scrollingBottom: 0,
            scrollContainer: function () {
                return a([])
            },
            getSizingRow: function (a) {
                return a.find("tbody tr:visible:first>*")
            },
            floatTableClass: "floatThead-table",
            floatWrapperClass: "floatThead-wrapper",
            floatContainerClass: "floatThead-container",
            copyTableClass: !0,
            debug: !1
        };
        var f = window._,
            g = function () {
                for (var a = 3, b = document.createElement("b"), c = b.all || []; a = 1 + a, b.innerHTML = "<!--[if gt IE " + a + "]><i><![endif]-->", c[0];);
                return a > 4 ? a : document.documentMode
            }(),
            h = null,
            i = function () {
                if (g) return !1;
                var b = a("<table><colgroup><col></colgroup><tbody><tr><td style='width:10px'></td></tbody></table>");
                a("body").append(b);
                var c = b.find("col").width();
                return b.remove(), 0 == c
            },
            j = a(window),
            k = 0;
        a.fn.floatThead = function (l) {
            if (l = l || {}, !f && (f = window._ || a.floatThead._, !f)) throw new Error("jquery.floatThead-slim.js requires underscore. You should use the non-lite version since you do not have underscore.");
            if (8 > g) return this;
            if (null == h && (h = i(), h && (document.createElement("fthtr"), document.createElement("fthtd"), document.createElement("fthfoot"))), f.isString(l)) {
                var m = l,
                    n = this;
                return this.filter("table").each(function () {
                    var b = a(this).data("floatThead-attached");
                    if (b && f.isFunction(b[m])) {
                        var c = b[m]();
                        "undefined" != typeof c && (n = c)
                    }
                }), n
            }
            var o = a.extend({}, a.floatThead.defaults || {}, l);
            return a.each(l, function (b) {
                b in a.floatThead.defaults || !o.debug || c("jQuery.floatThead: used [" + b + "] key to init plugin, but that param is not an option for the plugin. Valid options are: " + f.keys(a.floatThead.defaults).join(", "))
            }), this.filter(":not(." + o.floatTableClass + ")").each(function () {
                function c(a) {
                    return a + ".fth-" + y + ".floatTHead"
                }

                function i() {
                    var b = 0;
                    A.find("tr:visible").each(function () {
                        b += a(this).outerHeight(!0)
                    }), Z.outerHeight(b), $.outerHeight(b)
                }

                function l() {
                    var a = z.outerWidth(),
                        b = I.width() || a;
                    if (X.width(b - F.vertical), O) {
                        var c = 100 * a / (b - F.vertical);
                        S.css("width", c + "%")
                    } else S.outerWidth(a)
                }

                function m() {
                    C = (f.isFunction(o.scrollingTop) ? o.scrollingTop(z) : o.scrollingTop) || 0, D = (f.isFunction(o.scrollingBottom) ? o.scrollingBottom(z) : o.scrollingBottom) || 0
                }

                function n() {
                    var b, c;
                    if (V) b = U.find("col").length;
                    else {
                        var d;
                        d = null == o.cellTag && o.headerCellSelector ? o.headerCellSelector : "tr:first>" + o.cellTag, c = A.find(d), b = 0, c.each(function () {
                            b += parseInt(a(this).attr("colspan") || 1, 10)
                        })
                    }
                    if (b != H) {
                        H = b;
                        for (var e = [], f = [], g = [], i = 0; b > i; i++) e.push('<th class="floatThead-col"/>'), f.push("<col/>"), g.push("<fthtd style='display:table-cell;height:0;width:auto;'/>");
                        f = f.join(""), e = e.join(""), h && (g = g.join(""), W.html(g), bb = W.find("fthtd")), Z.html(e), $ = Z.find("th"), V || U.html(f), _ = U.find("col"), T.html(f), ab = T.find("col")
                    }
                    return b
                }

                function p() {
                    if (!E) {
                        if (E = !0, J) {
                            var a = z.width(),
                                b = Q.width();
                            a > b && z.css("minWidth", a)
                        }
                        z.css(db), S.css(db), S.append(A), B.before(Y), i()
                    }
                }

                function q() {
                    E && (E = !1, J && z.width(fb), Y.detach(), z.prepend(A), z.css(eb), S.css(eb))
                }

                function r(a) {
                    J != a && (J = a, X.css({
                        position: J ? "absolute" : "fixed"
                    }))
                }

                function s(a, b, c, d) {
                    return h ? c : d ? o.getSizingRow(a, b, c) : b
                }

                function t() {
                    var a, b = n();
                    return function () {
                        var c = s(z, _, bb, g);
                        if (c.length == b && b > 0) {
                            if (!V)
                                for (a = 0; b > a; a++) _.eq(a).css("width", "");
                            q();
                            var d = [];
                            for (a = 0; b > a; a++) d[a] = c.get(a).offsetWidth;
                            for (a = 0; b > a; a++) ab.eq(a).width(d[a]), _.eq(a).width(d[a]);
                            p()
                        } else S.append(A), z.css(eb), S.css(eb), i()
                    }
                }

                function u(a) {
                    var b = I.css("border-" + a + "-width"),
                        c = 0;
                    return b && ~b.indexOf("px") && (c = parseInt(b, 10)), c
                }

                function v() {
                    var a, b = I.scrollTop(),
                        c = 0,
                        d = L ? K.outerHeight(!0) : 0,
                        e = M ? d : -d,
                        f = X.height(),
                        g = z.offset(),
                        i = 0;
                    if (O) {
                        var k = I.offset();
                        c = g.top - k.top + b, L && M && (c += d), c -= u("top"), i = u("left")
                    } else a = g.top - C - f + D + F.horizontal;
                    var l = j.scrollTop(),
                        m = j.scrollLeft(),
                        n = I.scrollLeft();
                    return b = I.scrollTop(),
                        function (k) {
                            if ("windowScroll" == k ? (l = j.scrollTop(), m = j.scrollLeft()) : "containerScroll" == k ? (b = I.scrollTop(), n = I.scrollLeft()) : "init" != k && (l = j.scrollTop(), m = j.scrollLeft(), b = I.scrollTop(), n = I.scrollLeft()), !h || !(0 > l || 0 > m)) {
                                if (R) r("windowScrollDone" == k ? !0 : !1);
                                else if ("windowScrollDone" == k) return null;
                                g = z.offset(), L && M && (g.top += d);
                                var o, s, t = z.outerHeight();
                                if (O && J) {
                                    if (c >= b) {
                                        var u = c - b;
                                        o = u > 0 ? u : 0
                                    } else o = P ? 0 : b;
                                    s = i
                                } else !O && J ? (l > a + t + e ? o = t - f + e : g.top > l + C ? (o = 0, q()) : (o = C + l - g.top + c + (M ? d : 0), p()), s = 0) : O && !J ? (c > b || b - c > t ? (o = g.top - l, q()) : (o = g.top + b - l - c, p()), s = g.left + n - m) : O || J || (l > a + t + e ? o = t + C - l + a + e : g.top > l + C ? (o = g.top - l, p()) : o = C, s = g.left - m);
                                return {
                                    top: o,
                                    left: s
                                }
                            }
                        }
                }

                function w() {
                    var a = null,
                        b = null,
                        c = null;
                    return function (d, e, f) {
                        null == d || a == d.top && b == d.left || (X.css({
                            top: d.top,
                            left: d.left
                        }), a = d.top, b = d.left), e && l(), f && i();
                        var g = I.scrollLeft();
                        J && c == g || (X.scrollLeft(g), c = g)
                    }
                }

                function x() {
                    if (I.length) {
                        var a = I.width(),
                            b = I.height(),
                            c = z.height(),
                            d = z.width(),
                            e = d > a ? G : 0,
                            f = c > b ? G : 0;
                        F.horizontal = d > a - f ? G : 0, F.vertical = c > b - e ? G : 0
                    }
                }
                var y = k,
                    z = a(this);
                if (z.data("floatThead-attached")) return !0;
                if (!z.is("table")) throw new Error('jQuery.floatThead must be run on a table element. ex: $("table").floatThead();');
                var A = z.find("thead:first"),
                    B = z.find("tbody:first");
                if (0 == A.length) throw new Error("jQuery.floatThead must be run on a table that contains a <thead> element");
                var C, D, E = !1,
                    F = {
                        vertical: 0,
                        horizontal: 0
                    },
                    G = d(),
                    H = 0,
                    I = o.scrollContainer(z) || a([]),
                    J = o.useAbsolutePositioning;
                null == J && (J = o.scrollContainer(z).length);
                var K = z.find("caption"),
                    L = 1 == K.length;
                if (L) var M = "top" === (K.css("caption-side") || K.attr("align") || "top");
                var N = a('<fthfoot style="display:table-footer-group;"/>'),
                    O = I.length > 0,
                    P = !1,
                    Q = a([]),
                    R = 9 >= g && !O && J,
                    S = a("<table/>"),
                    T = a("<colgroup/>"),
                    U = z.find("colgroup:first"),
                    V = !0;
                0 == U.length && (U = a("<colgroup/>"), V = !1);
                var W = a('<fthrow style="display:table-row;height:0;"/>'),
                    X = a('<div style="overflow: hidden;"></div>'),
                    Y = a("<thead/>"),
                    Z = a('<tr class="size-row"/>'),
                    $ = a([]),
                    _ = a([]),
                    ab = a([]),
                    bb = a([]);
                if (Y.append(Z), z.prepend(U), h && (N.append(W), z.append(N)), S.append(T), X.append(S), o.copyTableClass && S.attr("class", z.attr("class")), S.attr({
                        cellpadding: z.attr("cellpadding"),
                        cellspacing: z.attr("cellspacing"),
                        border: z.attr("border")
                    }), S.css({
                        borderCollapse: z.css("borderCollapse"),
                        border: z.css("border")
                    }), S.addClass(o.floatTableClass).css("margin", 0), J) {
                    var cb = function (a, b) {
                        var c = a.css("position"),
                            d = "relative" == c || "absolute" == c;
                        if (!d || b) {
                            var e = {
                                paddingLeft: a.css("paddingLeft"),
                                paddingRight: a.css("paddingRight")
                            };
                            X.css(e), a = a.wrap("<div class='" + o.floatWrapperClass + "' style='position: relative; clear:both;'></div>").parent(), P = !0
                        }
                        return a
                    };
                    O ? (Q = cb(I, !0), Q.append(X)) : (Q = cb(z), z.after(X))
                } else z.after(X);
                X.css({
                    position: J ? "absolute" : "fixed",
                    marginTop: 0,
                    top: J ? 0 : "auto",
                    zIndex: o.zIndex
                }), X.addClass(o.floatContainerClass), m();
                var db = {
                        "table-layout": "fixed"
                    },
                    eb = {
                        "table-layout": z.css("tableLayout") || "auto"
                    },
                    fb = z[0].style.width || "";
                x();
                var gb, hb = function () {
                    (gb = t())()
                };
                hb();
                var ib = v(),
                    jb = w();
                jb(ib("init"), !0);
                var kb = f.debounce(function () {
                        jb(ib("windowScrollDone"), !1)
                    }, 300),
                    lb = function () {
                        jb(ib("windowScroll"), !1), kb()
                    },
                    mb = function () {
                        jb(ib("containerScroll"), !1)
                    },
                    nb = function () {
                        m(), x(), hb(), ib = v(), (jb = w())(ib("resize"), !0, !0)
                    },
                    ob = f.debounce(function () {
                        x(), m(), hb(), ib = v(), jb(ib("reflow"), !0)
                    }, 1);
                O ? J ? I.on(c("scroll"), mb) : (I.on(c("scroll"), mb), j.on(c("scroll"), lb)) : j.on(c("scroll"), lb), j.on(c("load"), ob), b(o.debounceResizeMs, c("resize"), nb), z.on("reflow", ob), e(z) && z.on("filter", ob).on("sort", ob).on("page", ob), z.data("floatThead-attached", {
                    destroy: function () {
                        var a = ".fth-" + y;
                        q(), z.css(eb), U.remove(), h && N.remove(), Y.parent().length && Y.replaceWith(A), z.off("reflow"), I.off(a), P && (I.length ? I.unwrap() : z.unwrap()), J && z.css("minWidth", ""), X.remove(), z.data("floatThead-attached", !1), j.off(a)
                    },
                    reflow: function () {
                        ob()
                    },
                    setHeaderHeight: function () {
                        i()
                    },
                    getFloatContainer: function () {
                        return X
                    },
                    getRowGroups: function () {
                        return E ? X.find("thead").add(z.find("tbody,tfoot")) : z.find("thead,tbody,tfoot")
                    }
                }), k++
            }), this
        }
    }(jQuery),
    function (a) {
        a.floatThead = a.floatThead || {}, a.floatThead._ = window._ || function () {
            var b = {},
                c = Object.prototype.hasOwnProperty,
                d = ["Arguments", "Function", "String", "Number", "Date", "RegExp"];
            return b.has = function (a, b) {
                return c.call(a, b)
            }, b.keys = function (a) {
                if (a !== Object(a)) throw new TypeError("Invalid object");
                var c = [];
                for (var d in a) b.has(a, d) && c.push(d);
                return c
            }, a.each(d, function () {
                var a = this;
                b["is" + a] = function (b) {
                    return Object.prototype.toString.call(b) == "[object " + a + "]"
                }
            }), b.debounce = function (a, b, c) {
                var d, e, f, g, h;
                return function () {
                    f = this, e = arguments, g = new Date;
                    var i = function () {
                            var j = new Date - g;
                            b > j ? d = setTimeout(i, b - j) : (d = null, c || (h = a.apply(f, e)))
                        },
                        j = c && !d;
                    return d || (d = setTimeout(i, b)), j && (h = a.apply(f, e)), h
                }
            }, b
        }()
    }(jQuery);




    $(".sticky-header").floatThead({
        scrollingTop: 68
    });

    $("#migration_user").validate({

        ignore: [],
        rules: {
            'first_name[]': {
                required: true,
                //lettersonly:true,
                hebrew_validation: true
            },
            'last_name[]': {
                required: true,
                hebrew_validation: true
            },
            'last_name_en[]': {
                //required: true,
                english_validation: true
            },
            'first_name_en[]': {
                //required: true,
                lettersonly: true,
                english_validation: true

            },
            'mobile_number[]': {
                required: true,
                digits: true,
                exactlength: 7
            },
            'email[]': {
                validemail: true,
            },
            'zip_code[]': {
                number: true
            },
           /* 'house_number[]': {
                number: true
            },
            'street[]': {
                hebrew_validation: true
            },*/
            'city[]': {
                hebrew_validation: true
            },
            'city_en[]': {
                english_validation: true
            },
            'm_prefix[]':{
               required:true 
            }


        },

        messages: {
            'first_name[]': {
                required: 'Please Enter First Name',
                //lettersonly:'Please Enter Characters',
                hebrew_validation: "Please Enter Only Hebrew"
            },
            'last_name[]': {
                required: 'Please Enter Last Names',
                hebrew_validation: "Please Enter Only Hebrew"
            },
            'last_name_en[]': {
                //required: 'Please Enter Last Name English',
                english_validation: "Please Enter Only English"
            },
            'first_name_en[]': {
               // required: 'Please Enter First Name English',
                lettersonly: 'Please Enter Characters',
                english_validation: "Please Enter Only English"
            },
            'mobile_number[]': {
                digits: "Please Enter Only Number",
                exactlength: "Please Enter Valid Mobile Number",
                required: "Please Enter Mobile Number"
            },
            'email[]': {
                validemail: "Please Enter Valid Email"
            },
            'zip_code[]': {
                number: "Please Enter Only Numeric Value"
            },
           /* 'house_number[]': {
                number: "Please Enter Only Numeric Value"

            },*/
            'city[]': {
                hebrew_validation: "Please Enter Only Hebrew"
            },
            'city_en[]': {
                english_validation: "Please Enter Only english"
            },
            /*'street[]': {
                hebrew_validation: "Please Enter Only Hebrew"
            },*/
            'm_prefix[]':{
                required:"Please select Prefix" 
             }


        },
        submitHandler: function (form) {
            var validate = 1;
            // do other things for a valid form
            $(".social_id_validation").each(function () {

                if ($(this).val() != '') {
                    if (ValidateID($(this).val(), $(this).attr('data-id'))) {
                        validate = 1
                    } else {
                        validate = 0
                    }
                }

            });
            if (validate == 1) {

                Swal.fire({
                    title: $('#popup_title').val(),
                    text: $('#popup_message').val(),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: $('#popup_yes_button').val(),
                    cancelButtonText: $('#popup_no_button').val()
                }).then((result) => {
                    if (result.value) {
                        form.submit();
                        $('#ajaxLoader').addClass('d-block');
                    }
                })



            }

        }


    });
    ! function ($) {
        "use strict";

        var SweetAlert = function () {};

        //examples 
        SweetAlert.prototype.init = function () {

            },

            //init
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    }(window.jQuery),

    //initializing 
    function ($) {
        "use strict";
        $.SweetAlert.init()
    }(window.jQuery);
    $("#checkAllUsers").click(function () {
        $('.usercheck').not(this).prop('checked', this.checked);
    });
    $(".usercheck").click(function () {
        if (!$(this).is(':checked')) {
            $('#checkAllUsers').prop('checked', false);
        }
    });

    $("input:checkbox").click(function () {
        if ($(this).prop('name') != 'search_by_new_user') {
            if ($('input:checkbox:checked').length > 0) {
                $('.check_opacity').removeClass('opacity-0');
                $('#action-user-btn').removeClass('d-none');
            } else {
                $('.check_opacity').addClass('opacity-0');
                $('#action-user-btn').addClass('d-none');
            }
            var checked = [];
            $("input:checkbox:checked").each(function (index, vals) {

                if (this.checked) {
                    if (vals.value != 'all') {
                        checked.push(vals.value);
                    }
                }
            });
            //$('#checked_id').val(checked.join(','));
        }

    });



    $("#delete-user-btn").click(function () {

        if (confirm("Do you want to delete this record?")) {
            var checked = [];
            $("input:checkbox:checked").each(function (index, vals) {
                if (this.checked) {
                    checked.push(vals.value);
                }
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#ajaxLoader').addClass('d-block');
            jQuery.ajax({
                url: backendUrl + "/user/multiDelete",
                method: 'post',
                data: {
                    user_ids: checked,
                },
                success: function (data) {
                    data = jQuery.parseJSON(JSON.stringify(data));
                    $("#ajaxLoader").removeClass("d-block");
                    if (data.status === true) {
                        toastr.success(data.msg);
                        window.location.reload(true);
                    } else {
                        toastr.error(data.msg);
                    }

                }
            });
        }

    });



    $("#active-user-btn").click(function () {

        if (confirm("Do you want to active this record?")) {
            var checked = [];
            $("input:checkbox:checked").each(function (index, vals) {
                if (this.checked) {
                    checked.push(vals.value);
                }
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#ajaxLoader').addClass('d-block');
            jQuery.ajax({
                url: backendUrl + "/user/multiActive",
                method: 'post',
                data: {
                    user_ids: checked,
                },
                success: function (data) {
                    data = jQuery.parseJSON(JSON.stringify(data));
                    $("#ajaxLoader").removeClass("d-block");
                    if (data.status === true) {
                        toastr.success(data.msg);
                        window.location.reload(true);
                    } else {
                        toastr.error(data.msg);
                    }

                }
            });
        }

    });


    $("#inactive-user-btn").click(function () {

        if (confirm("Do you want to inactive this record?")) {
            var checked = [];
            $("input:checkbox:checked").each(function (index, vals) {
                if (this.checked) {
                    checked.push(vals.value);
                }
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#ajaxLoader').addClass('d-block');
            jQuery.ajax({
                url: backendUrl + "/user/multiInActive",
                method: 'post',
                data: {
                    user_ids: checked,
                },
                success: function (data) {
                    data = jQuery.parseJSON(JSON.stringify(data));
                    $("#ajaxLoader").removeClass("d-block");
                    if (data.status === true) {
                        toastr.success(data.msg);
                        window.location.reload(true);
                    } else {
                        toastr.error(data.msg);
                    }

                }
            });
        }

    });

    // $("#sticky").sticky({topSpacing:100});
    $('#search_by_new_user').trigger('change');

});

$('#save_data').on('click', function () {
    if ($('#migration_user').valid()) {
        $('#migration_user').submit();

    } else {
        $('.show_validation').addClass('show');
        $(document.body).trigger("sticky_kit:recalc");

        return false;
    }

})


$('#add_owner').on('click', function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var count_div = $('#accordian-3 .card').length
    jQuery.ajax({
        url: backendUrl + "/migration/add_new_member_form",
        method: 'post',
        data: {
            count_div: count_div,
        },
        success: function (data) {
            $('#accordian-3').append(data);
            $('form a').click(function (event) {
                event.preventDefault();
                //$(this).closest('form').attr('action', this.href ).submit();
            });
            //$('#migration_user').valid();
        }
    });
})

$('form a').click(function (event) {
    event.preventDefault();
    //$(this).closest('form').attr('action', this.href ).submit();
});

function delay(callback, ms) {
    var timer = 0;
    return function () {
        var context = this,
            args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}
$(document).on('keyup', '.mobile_change', delay(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    if ($(this).attr('dynamic_id') == '0' || $(this).attr('dynamic_id') == 0) {
        var dynamic_id = $(this).attr('dynamic_id');
        var phone_number = $('#mobile_number').val();
        var m_prefix = $('#m_prefix').val();
    } else {
        var dynamic_id = $(this).attr('dynamic_id');
        var phone_number = $('#mobile_number_' + $(this).attr('dynamic_id')).val();
        var m_prefix = $('#m_prefix_' + $(this).attr('dynamic_id')).val();
    }

    jQuery.ajax({
        url: backendUrl + "/migration/checking_user",
        method: 'post',
        type: JSON,
        data: {
            phone_number: phone_number,
            m_prefix: m_prefix

        },
        success: function (data) {
            if (data != '') {

                var data = jQuery.parseJSON(data);
                if (data.birth_date) {
                    var bdate = data.birth_date.split('-');
                } else {
                    var bdate = '';
                }

                //console.log(bdate);
                if (dynamic_id == '0' || dynamic_id == 0) {
                    var prefix = data.mobile_phone.substring(0, 2);
                    var phone_number = data.mobile_phone.substring(2);
                    $('#m_prefix').val(prefix);
                    $('#mobile_number').val(phone_number);
                    $('#first_name').val(data.first_name);
                    $('#first_name_en').val(data.first_name_en);
                    $('#last_name').val(data.last_name);
                    $('#last_name_en').val(data.last_name_en);
                    $('#email').val(data.email);
                    $('#city').val(data.address_city);
                    $('#city_en').val(data.address_city_en);
                    $('#social_id').val(data.social_id_number);
                    if (data.birth_date) {
                        $('#year').val(bdate[0]);
                        $('#month').val(bdate[1]);
                        $('#day').val(bdate[2]);
                    } else {
                        $('#year').val('');
                        $('#month').val('');
                        $('#day').val('');
                    }
                    $('#street').val(data.address_street);
                    $('#house_number').val(data.house_number);
                    $('#zip_code').val(data.address_zip);
                    $('#country').val(data.country_id);
                    $('#form_header').html(data.first_name +"  "+data.first_name_en);

                } else {
                    var prefix = data.mobile_phone.substring(0, 2);
                    var phone_number = data.mobile_phone.substring(2);
                    $('#m_prefix_' + dynamic_id).val(prefix);
                    $('#mobile_number_' + dynamic_id).val(phone_number);
                    $('#first_name_' + dynamic_id).val(data.first_name);
                    $('#first_name_en_' + dynamic_id).val(data.first_name_en);
                    $('#last_name_' + dynamic_id).val(data.last_name);
                    $('#last_name_en_' + dynamic_id).val(data.last_name_en);
                    $('#email_' + dynamic_id).val(data.email);
                    $('#city_' + dynamic_id).val(data.address_city);
                    $('#city_en_' + dynamic_id).val(data.address_city_en);
                    $('#social_id_' + dynamic_id).val(data.social_id_number);
                    if (data.birth_date) {
                        $('#year_' + dynamic_id).val(bdate[0]);
                        $('#month_' + dynamic_id).val(bdate[1]);
                        $('#day_' + dynamic_id).val(bdate[2]);
                    } else {
                        $('#year_' + dynamic_id).val('');
                        $('#month_' + dynamic_id).val('');
                        $('#day_' + dynamic_id).val('');
                    }

                    $('#street_' + dynamic_id).val(data.address_street);
                    $('#house_number_' + dynamic_id).val(data.house_number);
                    $('#zip_code_' + dynamic_id).val(data.address_zip);
                    $('#country_' + dynamic_id).val(data.country_id);
                    $('#form_header_' + dynamic_id).html(data.first_name +"  "+data.first_name_en);

                }

            } else {
                if (dynamic_id == '0' || dynamic_id == 0) {
                    //$('#m_prefix').val('050');
                    //$('#mobile_number_' + dynamic_id).val('');
                    $('#first_name').val('');
                    $('#first_name_en').val('');
                    $('#last_name').val('');
                    $('#last_name_en').val('');
                    $('#email').val('');
                    $('#city').val('');
                    $('#city_en').val('');
                    $('#social_id').val('');
                    $('#year').val('');
                    $('#month').val('');
                    $('#day').val('');
                    $('#street').val('');
                    $('#house_number').val('');
                    $('#zip_code').val('');
                    $('#country').val('1');
                    $('#form_header').html($('#header_text').val());
                } else {
                    //$('#m_prefix_' + dynamic_id).val('050');
                    //$('#mobile_number_' + dynamic_id).val('');
                    $('#first_name_' + dynamic_id).val('');
                    $('#first_name_en_' + dynamic_id).val('');
                    $('#last_name_' + dynamic_id).val('');
                    $('#last_name_en_' + dynamic_id).val('');
                    $('#email_' + dynamic_id).val('');
                    $('#city_' + dynamic_id).val('');
                    $('#city_en_' + dynamic_id).val('');
                    $('#social_id_' + dynamic_id).val('');
                    $('#year_' + dynamic_id).val('');
                    $('#month_' + dynamic_id).val('');
                    $('#day_' + dynamic_id).val('');
                    $('#street_' + dynamic_id).val('');
                    $('#house_number_' + dynamic_id).val('');
                    $('#zip_code_' + dynamic_id).val('');
                    $('#country_' + dynamic_id).val('1');
                    $('#form_header_' + dynamic_id).html($('#header_text').val());
                }
            }
        }
    });
}, 500));

function ValidateID(str, data_id) {
    //INPUT VALIDATION

    // Just in case -> convert to string
    var IDnum = str.toString()

    // Validate correct input
    if ((IDnum.length > 9) || (IDnum.length < 5)) {
        $('.error_social_id_' + data_id).html("Enter Valide Social Id")
        return false;
    }
    if (isNaN(str)) {
        $('.error_social_id_' + data_id).html("Enter Valide Social Id")
        return false;
    }

    // The number is too short - add leading 0000
    if (IDnum.length < 9) {
        while (IDnum.length < 9) {
            IDnum = '0' + IDnum;
        }
    }

    // CHECK THE ID NUMBER
    var mone = 0,
        incNum;
    for (var i = 0; i < 9; i++) {
        incNum = Number(IDnum.charAt(i));
        incNum *= (i % 2) + 1;
        if (incNum > 9)
            incNum -= 9;
        mone += incNum;
    }
    if (mone % 10 == 0) {
        $('.error_social_id_' + data_id).html('')
        return true;
    } else {
        $('.error_social_id_' + data_id).html("Enter Valide Social Id")
        return false;
    }
}
$('#reset').on('click', function () {
    location.href = backendUrl + '/migration'
})
$('#search_by_new_user').on('change', function () {
    if ($(this).prop("checked") == true) {
        $('.show_new_fields').removeClass('hide');
    } else {
        $('.show_new_fields').addClass('hide');
    }
})
$(document).on('keyup', '.first_name_en', function () {
    if ($(this).attr('data-id') == 0) {
        if ($('#first_name').val() == '') {
            $('#form_header').html('');
        }
        if ($('#form_header').html() == '') {
            $('#form_header').html($(this).val());
        } else {
            $('#form_header').html($('#first_name').val() + "  " + $(this).val());
        }
        if($('#first_name').val() == '' && $('#first_name_en').val() == ''){
            $('#form_header').html($('#header_text').val());
        }

    } else {
        if ($('#first_name_' + $(this).attr('data-id')).val() == '') {
            $('#form_header_' + $(this).attr('data-id')).html('');
        }
        if ($('#form_header_' + $(this).attr('data-id')).html() == '') {
            $('#form_header_' + $(this).attr('data-id')).html($(this).val());
        } else {
            $('#form_header_' + $(this).attr('data-id')).html($('#first_name_' + $(this).attr('data-id')).val() + "   " + $(this).val());
        }
        if($('#first_name_' + $(this).attr('data-id')).val() == '' && $('#first_name_en_' + $(this).attr('data-id')).val() == ''){
            $('#form_header_' + $(this).attr('data-id')).html($('#header_text').val());
        }

    }

})
$(document).on('keyup', '.first_name', function () {
    if ($(this).attr('data-id') == 0) {
        if ($('#first_name_en').val() == '') {
            $('#form_header').html('');
        }
        if ($('#form_header').html() == '') {
            $('#form_header').html($(this).val());
        } else {
            $('#form_header').html($('#first_name_en').val() + $(this).val());
        }
        if($('#first_name').val() == '' && $('#first_name_en').val() == ''){
            $('#form_header').html($('#header_text').val());
        }

    } else {
        if ($('#first_name_en_' + $(this).attr('data-id')).val() == '') {
            $('#form_header_' + $(this).attr('data-id')).html('');
        }
        if ($('#form_header_' + $(this).attr('data-id')).html() == '') {
            $('#form_header_' + $(this).attr('data-id')).html($(this).val());
        } else {
            $('#form_header').html($('#first_name_en' + $(this).attr('data-id')).val() + $(this).val());
        }
        if($('#first_name_' + $(this).attr('data-id')).val() == '' && $('#first_name_en_' + $(this).attr('data-id')).val() == ''){
            $('#form_header_' + $(this).attr('data-id')).html($('#header_text').val());
        }

    }
})
$('#record_per_page').on('change',function(){
    $('#search_form').submit();
})
$(document).on('click','.delete_owner',function(){
    $('#card_'+$(this).attr('data-id')).remove();
})