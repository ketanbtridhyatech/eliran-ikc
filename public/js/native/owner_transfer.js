
$(document).ready(function () {
    jQuery.validator.addMethod("exactlength", function (value, element, param) {
        return this.optional(element) || value.length == param;
    }, $.validator.format("Please enter exactly {0} characters."));
    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
    }, "Please Enter Only Characters");
    jQuery.validator.addMethod("validemail", function (value, element) {
        return this.optional(element) || /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/i.test(value);
    }, "Please Enter Valid Email");
    jQuery.validator.addMethod("hebrew_validation", function (value, element) {
        return this.optional(element) || /^[\u0590-\u05FF ,.'-]+$/i.test(value);
    }, "Please Enter Only Hebrew");
    jQuery.validator.addMethod("english_validation", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9$@$!%*?&#^-_. +]+$/i.test(value);
    }, "Please Enter Only English");
  //  $('#ownerDetails').css('display','none');
    $(".ownerDetails_1").css("display", "none");
    $(".otp_verify_1").css("display", "none");
    $('#errorotp_1').css("display", "none");
});
$('#add_owner').on('click', function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var dog_name  = $("#dog_name_1").val();
    //alert(dog_name);
    var count_div = $('#ownerDiv .card').length;
    jQuery.ajax({
        url: getsiteurl() + "/owner_transfer/add_new_owner_form",
        method: 'post',
        data: {
            count_div: count_div,
            dog_name: dog_name,
        },
        success: function (response) {
           // alert(response);
            $('#ownerDiv').append(response);
            $(".otp_verify_"+(count_div + 1)).css("display", "none");
            $(".ownerDetails_"+(count_div + 1)).css("display", "none");
            $('#errorotp_'+(count_div + 1)).css("display", "none");
            $('form a').click(function (event) {
                event.preventDefault();
                //$(this).closest('form').attr('action', this.href ).submit();
            });
            //var number = $('#total_div').val();
            $('#total_div').val((count_div + 1));
            //$('#migration_user').valid();
        }
    });
});
function checkMobileNumber(id){
    var divid = id;
            
}
function sendOtp(id){
    var check = 0;
    var divid = id;
    var country_code = $('#country_code_'+divid).val();
    var number = $('#mobile_phone_'+divid).val();
    //alert(number);
   // alert($('#mobile_phone_'+divid).val().length);
    if($('#mobile_phone_'+divid).val().length === 0){
        return false;
    }
    var current_value = country_code + number ;
    var total_div =  $('#total_div').val();
    for (i = 1; i <= total_div ; i++) {
        if(divid != i){
               var cc = $('#country_code_'+i).val();
               var n = $('#mobile_phone_'+i).val(); 
               var xvalue = cc + n;
               if(current_value == xvalue){
                    check = 1;
               }
        }
    }
    
    if($.trim( $('#mobile_phone_'+divid+'-error').html() ).length)
    {
        return false;
    }
   // alert(check);
    if(check == '1'){
      //  alert(check);
       // alert('#mobile_phone_'+divid+'-error');
           // $('#mobile_phone_'+divid+'-error').text('Already Used this mobile number');
            $('#mobile_phone_'+divid+'-error').html('Already Used this mobile number');
            $('#mobile_phone_'+divid+'-error').css("display", "block");
            return false;
    }else{
        $('#mobile_phone_'+divid+'-error').css("display", "none");
    }
    
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    jQuery.ajax({
        url: getsiteurl() + "/owner_transfer/otp_send",
        method: 'post',
        data: {
            country_code: country_code,
            number: number,
        },
        success: function (response) {
           // console.log(response);
            if(response.code == '0' || response.code == 0){
                swal({
                    title: "",
                    text: "please Enter valid Number",
                    icon: "error",
                });
                return false;
            }
            $('#verifyotp_'+divid).val(response.code);
            $('.otp_verify_'+divid).css("display", "block");
           /* if(response == '1'){
                $("#country_code_"+divid).css("border-color", "green");
                $("#mobile_phone_"+divid).css("border-color", "green");
                $('#otp_'+divid).addClass('readonly_bgcolor');
                $('#otp_'+divid).addClass('readonly_checkbox');
            }else{
                 $("#country_code_"+divid).css("border-color", "red");
                $("#mobile_phone_"+divid).css("border-color", "red");
                $('#otp_'+divid).removeClass('readonly_bgcolor');
                 $('#otp_'+divid).removeClass('readonly_checkbox');
            }*/
           // alert(response);
            /*$('#ownerDiv').append(response);
            $(".otp_"+(count_div + 1)).css("display", "none");
            $(".ownerDetails_"+(count_div + 1)).css("display", "none");
            $('form a').click(function (event) {
                event.preventDefault();
                //$(this).closest('form').attr('action', this.href ).submit();
            });*/
            //$('#migration_user').valid();
        }
    });
}
function verifyOtp(divid){

    var exits = 0;
     var originotp = $('#verifyotp_'+divid).val();
     var inputotp = $('#userOtp_'+divid).val();
     var country_code = $('#country_code_'+divid).val();
        var number = $('#mobile_phone_'+divid).val();
if(originotp == inputotp){
                     $('#country_code_'+divid).addClass('readonly_bgcolor readonly_checkbox');
                     $('#mobile_phone_'+divid).addClass('readonly_checkbox readonly_bgcolor');
                     $('#userOtp_'+divid).addClass('readonly_bgcolor readonly_checkbox');
                     $('#otp_'+divid).addClass('readonly_checkbox readonly_bgcolor');
                     $('#verify_'+divid).addClass('readonly_bgcolor readonly_checkbox');
                     $('#errorotp_'+divid).css("display", "none");
                     $("#verify_status_"+divid).val('1');
                     
                     /*if(exits != '1'){
                        alert("in" + exits);
                            
                     }*/
                       
                    
                
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: getsiteurl() + "/owner_transfer/owner_exits",
            method: 'post',
            data: {
                country_code: country_code,
                number: number,
            },
            success: function (response) {
                
                if(response == '1'){
                  // alert("true "+response);
                    //exits = '1';
                    $('.ownerDetails_'+divid).html('');
                    $("#country_code_"+divid).css("border-color", "green");
                    $("#mobile_phone_"+divid).css("border-color", "green");
                    $('#otp_'+divid).addClass('readonly_bgcolor');
                    $('#otp_'+divid).addClass('readonly_checkbox');
                    $(".ownerDetails_"+divid).css("display", "none");
                    
                }else{
                    //alert("false "+response);
                     $("#country_code_"+divid).css("border-color", "none");
                    $("#mobile_phone_"+divid).css("border-color", "none");
                    $(".ownerDetails_"+divid).css("display", "block");
                     if(!$.trim( $('.ownerDetails_'+divid).html() ).length){
                       // alert("true");
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        jQuery.ajax({
                            url: getsiteurl() + "/owner_transfer/append_userdata",
                            method: 'post',
                            data: {
                                divid: divid,
                            },
                            success: function (response) {
                                $('.ownerDetails_'+divid).append(response);
                            }
                        });
                    } 
                    /*$('#otp_'+divid).removeClass('readonly_bgcolor');
                     $('#otp_'+divid).removeClass('readonly_checkbox');*/
                   
                }
                
              
            }
        });
     }else{
        $('#errorotp_'+divid).css("display", "block");
     } 
}
$('#save_owner_data').on('click', function () {
   // alert("123");
    if ($('#transfer_owner').valid()) {
        $('#transfer_owner').submit();

    } else {
     //   alert("false");
        $('.show_validation').addClass('show');
        $(document.body).trigger("sticky_kit:recalc");

        return false;
    }

});

$("#transfer_owner").validate({

        ignore: [],
        rules: {
            'first_name[]': {
                required: true,
                //lettersonly:true,
                hebrew_validation: true
            },
            'last_name[]': {
                required: true,
                hebrew_validation: true
            },
            'last_name_en[]': {
                required: true,
                english_validation: true
            },
            'first_name_en[]': {
                required: true,
                lettersonly: true,
                english_validation: true

            },
            'mobile_number[]': {
                required: true,
                digits: true,
                maxlength: 10
            },
            'email[]': {
                required: true,
                validemail: true,
            },
            'house_number[]': {
                required: true,
                number: true
            },
            'social_id[]': {
                required: true,
            },
            'street[]': {
                required: true,
            },
            'dog_name[]': {
                required: true,
            },
            'city[]': {
                required: true,
                hebrew_validation: true
            },
            'city_en[]': {
                required: true,
                english_validation: true
            }


        },

        messages: {
            'first_name[]': {
                required: 'Please Enter First Name',
                //lettersonly:'Please Enter Characters',
                hebrew_validation: "Please Enter Only Hebrew"
            },
            'last_name[]': {
                required: 'Please Enter Last Names',
                hebrew_validation: "Please Enter Only Hebrew"
            },
            'last_name_en[]': {
                required: 'Please Enter Last Name English',
                english_validation: "Please Enter Only English"
            },
            'first_name_en[]': {
               required: 'Please Enter First Name English',
                lettersonly: 'Please Enter Characters',
                english_validation: "Please Enter Only English"
            },
            'mobile_number[]': {
                digits: "Please Enter Only Number",
                exactlength: "Please Enter Valid Mobile Number",
                required: "Please Enter Mobile Number"
            },
            'email[]': {
               required: 'Please Enter Email',
                validemail: "Please Enter Valid Email"
            },
            'house_number[]': {
               required: 'Please Enter House Number',
                number: "Please Enter Only Numeric Value"

            },
            'city[]': {
               required: 'Please Enter City Name',
                hebrew_validation: "Please Enter Only Hebrew"
            },
            'city_en[]': {
               required: 'Please Enter City Name English',
                english_validation: "Please Enter Only english"
            },
            'social_id[]': {
                required: "Please Enter SocialId"
            },
            'street[]': {
                required: "Please Enter Street Name"
            },
            'dog_name[]': {
                required: "Please Enter Name"
            },
            
            'country_code[]':{
                required:"Please select Prefix" 
             }


        },
        submitHandler: function (form) {

           // alert("true");
        var wrong = 0;
           var total_div =  $('#total_div').val();
        for (i = 1; i <= total_div ; i++) {
            var value = $('#verify_status_'+i).val();
            if(value != 1){
                wrong = 1;
            }
        }
        if(wrong == '1'){
            swal({
                    title: "",
                    text: "please verify all otps",
                    icon: "error",
            });
            //alert("please verify all otp");
            exit();
        }
            var validate = 1;
            // do other things for a valid form
            $(".social_id_validation").each(function () {

                if ($(this).val() != '') {
                    if (ValidateID($(this).val(), $(this).attr('data-id'))) {
                        validate = 1
                    } else {
                        validate = 0
                    }
                }

            });
            if (validate == 1) {

                /*Swal.fire({
                    title: $('#popup_title').val(),
                    text: $('#popup_message').val(),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: $('#popup_yes_button').val(),
                    cancelButtonText: $('#popup_no_button').val()
                }).then((result) => {*/
                    //if (result.value) {
                        form.submit();
                        //$('#ajaxLoader').addClass('d-block');
                   // }
             /*   })*/



            }

        }


    });

function ValidateID(str, data_id) {
    //INPUT VALIDATION

    // Just in case -> convert to string
    var IDnum = str.toString()

    // Validate correct input
    if ((IDnum.length > 9) || (IDnum.length < 5)) {
        $('.error_social_id_' + data_id).html("Enter Valide Social Id")
        return false;
    }
    if (isNaN(str)) {
        $('.error_social_id_' + data_id).html("Enter Valide Social Id")
        return false;
    }

    // The number is too short - add leading 0000
    if (IDnum.length < 9) {
        while (IDnum.length < 9) {
            IDnum = '0' + IDnum;
        }
    }

    // CHECK THE ID NUMBER
    var mone = 0,
        incNum;
    for (var i = 0; i < 9; i++) {
        incNum = Number(IDnum.charAt(i));
        incNum *= (i % 2) + 1;
        if (incNum > 9)
            incNum -= 9;
        mone += incNum;
    }
    if (mone % 10 == 0) {
        $('.error_social_id_' + data_id).html('')
        return true;
    } else {
        $('.error_social_id_' + data_id).html("Enter Valide Social Id")
        return false;
    }
}