$(document).ready(function () {
    $.each($(".color_dropdown"), function () {
        // alert("test");
        $(this).trigger('change');
    });
    jQuery.validator.addMethod("exactlength", function (value, element, param) {
        return this.optional(element) || value.length <= param;
    }, $.validator.format("The chip id must be 15 numbers"));
    var selecttext = $('.selecttext').val();
    $('.select2').select2({
        placeholder: selecttext
    })
    $.each($(".dead_checkbox:checked"), function () {

        $(this).trigger('change')

    });
    $("input[name='review_type']").change(function () {
        // alert("in");
        if ($("input[name='review_type']:checked").val() == 'breeding_group') {
            $('#review_price').html($('#setting_review_price').val());
            $('#price_per_puppie').html($('#setting_per_dog_price').val());
            
            var dead_puppies = $(".dead_checkbox:checked").length;
             if (dead_puppies > 0) {
                 $('#review_price1').html('0');
                 $('#price_per_puppie1').html($('#setting_per_dog_price').val());
             } else {
                 $('#review_price1').html('0');
                 $('#price_per_puppie1').html(0);
             }
            
            $('#store_review_price').val($('#setting_review_price').val());
            $('#store_price_per_puppie').val($('#setting_per_dog_price').val());
           
        } else if ($("input[name='review_type']:checked").val() == "breeding_promoter") {
            $('#review_price').html($('#club_review_price').val());
            $('#price_per_puppie').html($('#club_price_per_dog').val());
            
            var dead_puppies = $(".dead_checkbox:checked").length;
            if (dead_puppies > 0) {
                $('#review_price1').html('0');
                $('#price_per_puppie1').html($('#club_price_per_dog').val());
            } else {
                $('#review_price1').html('0');
                $('#price_per_puppie1').html(0);
            }
            $('#store_review_price').val($('#club_review_price').val());
            $('#store_price_per_puppie').val($('#club_price_per_dog').val());
            
        }

        var total = parseFloat($('#review_price1').html(), 2) + (parseFloat($('#quantity_11').val() * $('#price_per_puppie1').html()));
        var total_certification_price = (parseFloat($('#quantity_22').val() * $('#certificate_price1').html()));
        $('#total_price_11').html(total);
        $('#total_price_22').html(total_certification_price);
        set_main_price();
    })
    $("input[name='review_type']").trigger('change');
   // dead_male_female_price();
    set_main_price();
    /* $('#quantity_1').val(parseInt($("#live_male_puppie").val()) + parseInt($("#live_female_puppie").val()));
     $('#quantity_2').val(parseInt($("#live_male_puppie").val()) + parseInt($("#live_female_puppie").val()));*/

});
$('.only_number').on('keypress', function (evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
})
$(document).on('change', 'input[type=text]', function () {
    // alert($(this).prop('name'));
    store_data($(this).val(), $('#row_' + $(this).attr('dog_no')).attr('record_id'), $(this).prop('name'), $(this).attr('dog_no'));
});
/*$('input[type=text]').on('change',function(){
    
});*/
$('select').on('change', function () {
    store_data($(this).val(), $('#row_' + $(this).attr('dog_no')).attr('record_id'), $(this).prop('name'), $(this).attr('dog_no'));
});

$('input[type=radio]').on('click', function () {
    var name_check = $(this).attr('id');
    var count = 0;
    if (name_check == 'breeding_promoter' || name_check == 'breeding_group' || name_check == 'credit_card' || name_check == 'phone_payment') {
        count = 1;
    }
    if (count == 0) {


        var m = 0;
        var f = 0;
        //alert($(this).val());
        var currentId = $(this).attr('id');
        //alert(currentId);

        $('input[type=radio]:checked').each(function () {
            var loopId = $(this).attr('id');
            // console.log(loopId);
            if (currentId != loopId) {
                if ($(this).val() == "male") {
                    m = m + 1;
                }
                if ($(this).val() == "female") {
                    f = f + 1;
                    // alert(f+1);
                }
            }

            //alert($(this).val());

            // store_data($(this).val(),$('#row_'+$(this).attr('dog_no')).attr('record_id'),'gender',$(this).attr('dog_no'));
        });

        var totalmale = $('#total_male_dogs').val();
        var totalfemale = $('#total_female_dogs').val();
        var selectedVal = $(this).val();
        var alert_text = $('#alert_text').val();
        // alert(selectedVal);
        if (selectedVal == 'male') {
            if (m < totalmale) {
                store_data($(this).val(), $('#row_' + $(this).attr('dog_no')).attr('record_id'), 'gender', $(this).attr('dog_no'));
            } else {
                /*swal({
                    title: "",
                    text:  ,
                    icon: "error",
                });*/
                Swal.fire({
                    title: '',
                    text: alert_text,
                    type: 'error',
                }).then((result) => {})
                return false;
            }
        } else {
            if (f < totalfemale) {
                store_data($(this).val(), $('#row_' + $(this).attr('dog_no')).attr('record_id'), 'gender', $(this).attr('dog_no'));
            } else {
                /* swal({
                     title: "",
                     text: "Maximum " + totalfemale + " female select allow" ,
                     icon: "error",
                 });*/
                Swal.fire({
                    title: '',
                    text: alert_text,
                    type: 'error',
                }).then((result) => {})
                // alert();
                return false;
            }
        }
    }
    /*console.log("male"+ m);
    console.log("female"+ f);*/
    // store_data($(this).val(),$('#row_'+$(this).attr('dog_no')).attr('record_id'),'gender',$(this).attr('dog_no'));
});
$('#submit_btn').on('click', function () {

    if ($('#chippingform').valid()) {
        //var arrremoveborder = new Array();
        for (var i = 0; i < $('#total_dogs').val(); i++) {
            $('#chip_number_' + i).css("border-color", "#e9ecef");
        }

        var not_valid_chip = 0;
        for (var i = 0; i < $('#total_dogs').val(); i++) {
            //  alert($('#chip_number_'+i).val().length);
            if ($('#chip_number_' + i).val() != 0) {
                //alert($('#chip_number_'+i).val().length);
                if ($('#chip_number_' + i).val().length < 15) {
                    $('#chip_number_' + i).css("border-color", "red");
                    not_valid_chip = 1;
                }
                if ($('#chip_number_' + i).val().length >= 16) {
                    $('#chip_number_' + i).css("border-color", "red");
                    not_valid_chip = 1;
                }
            }
        }
        var arr = new Array();
        for (var i = 0; i < $('#total_dogs').val(); i++) {
            if ($('#chip_number_' + i).val() != 0) {
                arr.push($('#chip_number_' + i).val());
            }
        }
        var keyarr = new Array();
        for (var i = 0; i < $('#total_dogs').val(); i++) {
            if ($('#chip_number_' + i).val() != 0) {
                keyarr.push('#chip_number_' + i);
            }
        }
       /* console.log(arr);
        console.log(keyarr);*/
        var same = 0;
        if (arr.length > 0) {
            for (i = 0; i < arr.length; i++) {
                for (x = 0; x < arr.length; x++) {
                    if (arr[i] == arr[x] && i != x) {
                        $('#chip_number_' + x).css("border-color", "red");
                        var same = 1;
                    }
                }
            }
        }

        if (same == 1) {
            // alert("in");
            Swal.fire({
                title: $('#exits_in_database').val(),
                text: $('#exits_in_database_zero_enter').val(),
                type: 'warning',
                //showCancelButton: true,
                //cancelButtonColor: '#d33',
                confirmButtonColor: '#d33',
                confirmButtonText: $('#popup_cancel_button').val()
            }).then((result) => {
                if (result.value) {
                    return false;
                }
            });
        } else {
            if (not_valid_chip == 1) {


                Swal.fire({
                    title: 'פרטי השבב אינם תקינים',
                    text: '',
                    type: 'warning',

                    confirmButtonColor: '#d33',
                    confirmButtonText: $('#popup_cancel_button').val()
                    //cancelButtonText: $('#popup_no_button_chip').val()
                }).then((result) => {
                    if (result.value) {
                        return false;
                    }
                })
            } else {
                Swal.fire({
                    title: '',
                    text: $('#popup_message').val(),
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: $('#popup_yes_button').val(),
                    cancelButtonText: $('#popup_no_button').val()
                }).then((result) => {
                    if (result.value) {


                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        //$('#ajaxLoader').addClass('d-block');
                        jQuery.ajax({
                            url: getsiteurl() + "/chipping/chcked_dogsDB",
                            method: 'post',
                            data: {
                                chip_number: arr,
                                chip_id : keyarr

                            },
                            success: function (data) {
                               /* console.log(data.count);
                                return false;*/
                                if (data.count == '1') {

                                    if ($('#chippingform').valid()) {
                                        $(this).html($('#loading_text').val())
                                        $('#cancel_button').prop('disabled', true);
                                    }
                                    $('#chippingform').submit();
                                    $('#ajaxLoader').addClass('d-block');

                                } else {
                                    if (data.key_return.length > 0) {
                                        for (i = 0; i < data.key_return.length; i++) {
                                           $(data.key_return[i]).css("border-color", "red");
                                        }
                                    }
                                    Swal.fire({
                                        title: $('#exits_in_database').val(),
                                        text: $('#exits_in_database_zero_enter').val(),
                                        type: 'warning',
                                        confirmButtonColor: '#d33',
                                        confirmButtonText: $('#popup_cancel_button').val()
                                    }).then((result) => {
                                        if (result.value) {
                                            return false;
                                        }
                                    });
                                }
                            }
                        });


                    }
                })
            }
        }
        // console.log(same);
        // return false;
        //alert(not_valid_chip);
        // return false;



    }

})
$.validator.addMethod('required_gender', function (value, elem) {
    return $(elem).parent().find('.required_gender:checked').length > 0;
}, 'This field is required');
$('#chippingform').validate({
    ignore: ":disabled",
    rules: {
        'color[]': "required",
        'temparory_name[]': "required",
        //'other[]':"required",
        'chip_number[]': {
            required: true,
            digits: true,

        },
        'review_type': "required",
        "payment_type": "required",
    },
    messages: {
        'color[]': "This field is required",
        'temparory_name[]': "This field is required",
        'chip_numbers[]': {
            required: "This field is required",
            remote: 'The chip is already in use!'
        },
    },
    errorPlacement: function (error, element) {
        if (element.hasClass('select2') && element.next('.select2-container').length) {
            error.insertAfter(element.next('.select2-container'));
        } else {
            error.insertAfter(element);
        }
    }
})
$('.dead_checkbox').change(function () {
    if ($(this).prop('checked') == true) {
        var dog_no = $(this).attr('dog_no');
        $('this').prop('name', 'dead[' + dog_no + ']');
        $('#row_' + dog_no).addClass('set_disabled');
        $('#temparory_name_' + dog_no).attr('readonly', true);
        $('#temparory_name_' + dog_no).prop('name', 'temparory_name[' + dog_no + ']');
        $('#chip_number_' + dog_no).attr('readonly', true);
        $('#chip_number_' + dog_no).prop('name', 'chip_number[' + dog_no + ']');
        $('#color_' + dog_no).prop('name', 'color[' + dog_no + ']');
        $('#male_' + dog_no).prop('name', 'gender_' + dog_no + '[' + dog_no + ']');
        $('#female_' + dog_no).prop('name', 'gender_' + dog_no + '[' + dog_no + ']');
        $('#male_' + dog_no).removeClass('required_gender');
        $('#female_' + dog_no).removeClass('required_gender');
        $('.select_readonly_' + dog_no).addClass('set_disabled');
        $('.select_readonly_' + dog_no).addClass('readonly_checkbox');
        store_data(1, $('#row_' + $(this).attr('dog_no')).attr('record_id'), $(this).prop('name'), $(this).attr('dog_no'));

    } else {
        var dog_no = $(this).attr('dog_no');
        $('this').prop('name', 'dead[]');
        $('#row_' + dog_no).removeClass('set_disabled');
        $('#temparory_name_' + dog_no).attr('readonly', false);
        $('#temparory_name_' + dog_no).prop('name', 'temparory_name[]');
        $('#chip_number_' + dog_no).attr('readonly', false);
        $('#chip_number_' + dog_no).prop('name', 'chip_number[]');
        $('#color_' + dog_no).prop('name', 'color[]');
        $('#male_' + dog_no).prop('name', 'gender_' + dog_no + '[]');
        $('#female_' + dog_no).prop('name', 'gender_' + dog_no + '[]');
        $('#hair_type_' + dog_no).prop('name', 'hair_type[]');
        $('.select_readonly_' + dog_no).removeClass('set_disabled');
        $('.select_readonly_' + dog_no).removeClass('readonly_checkbox');
        $('#male_' + dog_no).addClass('required_gender');
        $('#female_' + dog_no).addClass('required_gender');
        store_data('', $('#row_' + $(this).attr('dog_no')).attr('record_id'), $(this).prop('name'), $(this).attr('dog_no'));


    }
    var dead_puppies = $(".dead_checkbox:checked").length;
    if (dead_puppies > 0) {

        $('#refund_calculation').show()
    } else {
        $('#refund_calculation').hide()
    }
    $('#quantity_11').val(parseInt(dead_puppies));
    $('#quantity_22').val(parseInt(dead_puppies));
    
    $("input[name='review_type']").trigger('change');
    //set_main_price();
    // $('#total_cart_price').html(parseFloat(total,2)+parseFloat($('#total_price_2').html(),2));
    //$('#total_cart_price_text').val(parseFloat(total,2)+parseFloat($('#total_price_2').html(),2));


})

function set_main_price() {
    // alert(parseFloat($('#total_price_22').html()));

   // dead_male_female_price();
    var total = parseFloat($('#review_price').html(), 2) + (parseFloat($('#quantity_1').val() * $('#price_per_puppie').html()));
    var total_certification_price = (parseFloat($('#quantity_2').val() * $('#certificate_price').html()));
    var total_dead = parseFloat($('#review_price1').html(), 2) + (parseFloat($('#quantity_11').val() * $('#price_per_puppie1').html()));

    $('#total_price_11').html(total_dead);
    var malfemaletotal = parseFloat($('#total_price_11').html());
    var cerDiductiontotal = parseFloat($('#total_price_22').html());
    var plus = parseFloat(total + total_certification_price);
    var minuse = parseFloat(malfemaletotal + cerDiductiontotal);
    $('#total_refund').val(minuse);
    /*console.log("plus  " +plus);
    console.log("minuse  " +minuse);*/
    $('#total_price_1').html(total);
    $('#total_price_2').html(total_certification_price);
    $('#total_cart_price').html((plus - minuse));
    $('#total_cart_price_text').val((plus - minuse));
}



function store_data(value, record_id, field, dog_id) {
    var dead_puppies = $(".dead_checkbox:checked").length;
    if (dead_puppies > 0) {

        var cart_price = $('#total_cart_price_text').val();
    } else {
        var cart_price = '';
    }
    if (field == 'other_' + dog_id) {
        field = 'other[]';
    }
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //$('#ajaxLoader').addClass('d-block');
    jQuery.ajax({
        url: getsiteurl() + "/chipping/ajax_add_dog",
        method: 'post',
        data: {
            SagirId: $("input[name='SagirId']").val(),
            dog_id: record_id,
            field: field,
            value: value,
            cart_price: cart_price

        },
        success: function (data) {

            $('#row_' + dog_id).attr('record_id', data);
        }
    });

}
$(".color_dropdown").change(function () {
    // alert('trigger fire');
    //alert($(this).attr('dog_no'));
    var full_id = $(this).attr('dog_no');

    if ($(this).val() == '0') {
        //alert("in");
        /* $('#other_div_'+full_id[1]).css('visibility','visible');
         $('#other_'+full_id[1]).attr('disabled',false);*/
        //alert(full_id);
        $('#other_' + full_id).show();
        $('#other_' + full_id).rules("add", {
            required: true
        });

        // $('#other_div_'+full_id[1]).empty();
        //$('#other_div_'+full_id[1]).append('');

        //$("#other_"+full_id[1]).css("visibility", "block");

    } else {
        $('#other_' + full_id).val('');
        $('#other_' + full_id).rules("remove");
        $('#other_' + full_id).hide();

        //$('#other_'+full_id[1]).attr('disabled',true);

        // $('#other_div_'+full_id[1]).empty();
    }
});
