<?php

return [

    'backend' => 'admin',
   'language' => [
        0 => 'en',
        1 => 'iw'
    ],
    'adminPerPage' => 10,
    'show_records' => [
        '50'=>'50',
        '100'=>'100',
        '150'=>'150',
        '200'=>'200',
        '250'=>'250'
    ]
    
];
