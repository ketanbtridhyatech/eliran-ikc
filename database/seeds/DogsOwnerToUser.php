<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;
use App\NewUser;

class DogsOwnerToUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dogs_owner = DB::table('DogsOwners')->get();
        foreach ($dogs_owner as $do) {
            $user = new User();
            $user->data_id = $do->DataID;
            $user->owner_code = $do->OwnerCode;
            $user->first_name = $do->OwnerName;
            $user->address_street = $do->Street;
            $user->address_city = $do->CityName;
            $user->phone = $do->Phone;
            $user->email = $do->Email;
            $user->info_id = $do->InfoID;
            $user->sagir_owner_id = $do->SagirOwnerID;
            $user->is_current_owner = $do->IsCurrentOwner;
            $user->order_id = $do->OrderID;
            $user->first_name_en = $do->OwnerNameEN;
            $user->address_street_en = $do->StreetEN;
            $user->address_city_en = $do->CityNameEn;
            $user->mobile_phone = $do->New_Mobile;
            $user->new_sid = $do->New_SID;
            $user->new_org_data_id = $do->New_Org_DataID;
            $user->new_fill_date = $do->New_Fill_Date;
            $user->new_filler_ip = $do->New_FillerIP;
            $user->address_street_number = $do->New_StreetNumber;
            $user->is_superadmin = 0;
            $user->language_id = 0; 
            $user->status = 1; 
            $user->record_type = 'Owners';
            $user->created_at = $do->CreationDateTime;
            $user->updated_at = $do->ModificationDateTime;
            $user->save();
        }
    }
}
