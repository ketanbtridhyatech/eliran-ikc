<?php

use Illuminate\Database\Seeder;
use App\ColorsDB;
use App\Imports\BreedsImport;
use Maatwebsite\Excel\Facades\Excel;
class MergeNewColorTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path="Colour_2020.xlsx";
        $array = Excel::toArray(new BreedsImport, $path);
        if(!empty($array)){
            for($i=0;$i<count($array[0])-1;$i++){
                $color = ColorsDB::where('DataID',$array[0][$i+1][0])->first();
                if(empty($color)){
                    $color = new ColorsDB();
                }
                $color->DataID=$array[0][$i+1][0];
                $color->ColorNameHE = $array[0][$i+1][1];
                $color->ColorNameEN = $array[0][$i+1][2];
                $color->Remark = $array[0][$i+1][3];
                $color->OldCode = $array[0][$i+1][4];
                $color->status = $array[0][$i+1][5];
                $color->save();
                
            }
        }
    }
}
