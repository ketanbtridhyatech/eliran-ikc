<?php

use Illuminate\Database\Seeder;
use App\BreedClub;
use App\Imports\BreedsImport;
use App\BreedsDB;
use App\Clubs;
use Maatwebsite\Excel\Facades\Excel;

class MergeNewBreedClub extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path="Breeds_Clubs_2020.xlsx";
        $array = Excel::toArray(new BreedsImport, $path);
        if(!empty($array)){
            for($i=0;$i<count($array[0])-1;$i++){
                $breed_club = new BreedClub();
                $breed = BreedsDB::where('DataID',$array[0][$i+1][0])->first();
                $club = Clubs::where('DataID',$array[0][$i+1][1])->first();
                $breed_club->breed_id = $breed->id;
                $breed_club->club_id = $club->id;
                $breed_club->save();
                
            }
        }
    }
}
