<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\NewUser;

class MembersToUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = DB::table('Members')->get();
        foreach ($members as $me) {
            $user = new User();
            $user->data_id = $me->DataID;
            $user->club_id =  $me->ClubID;
            $user->first_name = $me->Owner_FirstName;
            $user->last_name = $me->Owner_LastName;
            $user->phone =  $me-> Owner_Phone;
            $user->mobile_phone =  $me->Owner_Mobile;
            $user->owner_email = $me->Owner_Email;
            $user->address_city = $me->Owner_City;
            $user->address_street = $me->Owner_Street;
            $user->address_street_number = $me->Owner_Number;
            $user->owner_payment_sum = $me->Owner_PaymentSum;
            $user->owner_payment_last4 = $me->Owner_PaymentLast4;
            $user->member_status = $me->MemberStatus;
            $user->special_key = $me->SpecialKey;
            $user->expire_date = $me->ExpireDate;
            $user->owner_total_payment = $me->Owner_TotalPayment;
            $user->fax = $me->Owner_Fax;
            $user->start_date = $me->StartDate;
            $user->address_zip  = $me->Owner_Zip;
            $user->record_source = $me->RecordSource;
            $user->is_judge = $me->IsJudge;
            $user->city_id = $me->CityID;
            $user->private_phone_1 =  $me->PrivatePhone_1;
            $user->private_phone_2 = $me->PrivatePhone_2;
            $user->birth_date = $me->BirthDate;
            $user->note = $me->Note;
            $user->image = $me->Image;
            $user->invoice_id =  $me->InvoiceID;
            $user->breed_id = $me->BreedID;
            $user->email = $me->UserName;
            $user->password = Hash::make($me->Password);
            $user->user_key = $me->UserKey;
            $user->is_breed_manager =  $me->IsBreedManager;
            $user->payment_status = $me->PaymentStatus;
            $user->is_superadmin = 0;
            $user->language_id = 0; 
            $user->status = 1; 
            $user->record_type = 'Members';
            $user->created_at = $me->CreationDateTime;
            $user->updated_at = $me->ModificationDateTime;
            $user->save();
        }
    
    }
}
