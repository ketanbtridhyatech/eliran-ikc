<?php

use Illuminate\Database\Seeder;
use App\User;
class migrate_user_phone_number extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users=User::where('country_code','!=','+972')->where('record_type','Native')->get();
        if(!empty($users)){
            foreach($users as $user){
                $user_record = User::find($user->id);
                if(!empty($user_record)){
                    $user_record->mobile_phone=ltrim($user_record->country_code, '0').$user_record->mobile_phone;
                    $user_record->country_code='+972';
                    $user_record->save();
                }
            }
        }
    }
}
