<?php

use Illuminate\Database\Seeder;
use App\Clubs;
use App\Imports\BreedsImport;
use Maatwebsite\Excel\Facades\Excel;

class UpdateClubPrice extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path="Club_Prise_2020.xlsx";
        $array = Excel::toArray(new BreedsImport, $path);
        if(!empty($array)){
            for($i=0;$i<count($array[0])-1;$i++){
                $club = Clubs::where('DataID',$array[0][$i+1][0])->first();
                $club->RegistrationPrice = $array[0][$i+1][5];
                $club->GeneralReviewFee = $array[0][$i+1][3];
                $club->DogReviewFee = $array[0][$i+1][1];
                $club->Breed_NonReg_Price = $array[0][$i+1][4];
                $club->PerDog_NonReg_Price = $array[0][$i+1][2];
                $club->save();
            }
    }
}
}
