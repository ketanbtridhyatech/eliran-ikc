<?php

use Illuminate\Database\Seeder;
use App\Breed2Color;
use App\BreedsDB;
use App\ColorsDB;
use App\Imports\BreedsImport;
use Maatwebsite\Excel\Facades\Excel;

class Breed2ColorTableData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path="Breeds_Colours_new_2020.xlsx";
        $array = Excel::toArray(new BreedsImport, $path);
        if(!empty($array)){
            for($i=0;$i<count($array[0])-1;$i++){
                $color = ColorsDB::where('DataID',$array[0][$i+1][1])->first();
                $breed = BreedsDB::where('DataID',$array[0][$i+1][0])->first();
                $breed2color = new Breed2Color();
                $breed2color->breed_id = $breed->id;
                $breed2color->color_id = $color->id;
                $breed2color->save();
              
                
            }
        }
    }
}
