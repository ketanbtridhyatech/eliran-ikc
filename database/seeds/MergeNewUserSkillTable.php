<?php

use Illuminate\Database\Seeder;
use App\UsersSkills;
use App\Skills;
use App\Clubs;
use App\BreedsDB;
use App\Imports\BreedsImport;
use Maatwebsite\Excel\Facades\Excel;

class MergeNewUserSkillTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = "ID_Skill_Club_Breed_2020_ver1.1.xlsx";
        $array = Excel::toArray(new BreedsImport, $path);
        if (!empty($array)) {
            for ($i = 0; $i < count($array[0]) - 1; $i++) {
                $user_skill = new UsersSkills();
                $user_skill->user_id = $array[0][$i + 1][0];
                $skills = Skills::where('DataID', $array[0][$i + 1][1])->first();
                $user_skill->skill_id = $skills->id;
                if (!empty($array[0][$i + 1][2])) {
                    $club = Clubs::where('DataID', $array[0][$i + 1][2])->first();
                    $user_skill->club_id = $club->id;
                }
               if(!empty($array[0][$i + 1][3])){
                $breeds=BreedsDB::whereIn('DataID',explode(",",$array[0][$i+1][3]))->pluck('id')->toArray();
                $user_skill->breed_id = implode(',',$breeds);   
               }
                
                $user_skill->save();
            }
        }
    }
}
