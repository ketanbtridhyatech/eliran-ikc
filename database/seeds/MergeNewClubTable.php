<?php

use Illuminate\Database\Seeder;
use App\Clubs;
use App\Imports\BreedsImport;
use Maatwebsite\Excel\Facades\Excel;
class MergeNewClubTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path="Clubs_2020.xlsx";
        $array = Excel::toArray(new BreedsImport, $path);
        if(!empty($array)){
            for($i=0;$i<count($array[0])-1;$i++){
                $club = Clubs::where('DataID',$array[0][$i+1][0])->first();
                if(empty($club)){
                    $club = new Clubs();
                }
                $club->DataID=$array[0][$i+1][0];
                $club->Name = $array[0][$i+1][2];
                $club->status = $array[0][$i+1][11];
                $club->RegistrationPrice = $array[0][$i+1][12];
                $club->save();
                
            }
        }
    }
}
