<?php

use App\Dogs2Users;
use App\User;
use Illuminate\Database\Seeder;


class RemoveDuplicateUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::select('mobile_phone',DB::raw("COUNT(id) as duplicate_count"))->where('record_type',"Native")->havingRaw('duplicate_count > 1')->groupBy('mobile_phone')->get();
        if(!empty($users)){
            foreach($users as $u){
                if(!empty($u->mobile_phone)){
                $o_entries = User::where('mobile_phone',$u->mobile_phone)->where('record_type',"Native")->first();
                $d_entries = User::where('mobile_phone',$u->mobile_phone)->where('record_type',"Native")->where('id','!=',$o_entries->id)->get();
                  foreach($d_entries as $d){
                      Dogs2Users::where('user_id',$d->id)->update(array('user_id'=>$o_entries->id));
                      //User::where('id',$d->id)->delete();
                      DB::table('users')->delete($d->id);
                  } 
                  
                }
            }
        }
    }
}
