<?php

use Illuminate\Database\Seeder;
use App\Skills;
use App\Imports\BreedsImport;
use Maatwebsite\Excel\Facades\Excel;

class MergeNewSkillTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path="Skill_2020.xlsx";
        $array = Excel::toArray(new BreedsImport, $path);
        if(!empty($array)){
            for($i=0;$i<count($array[0])-1;$i++){
                $skill = Skills::where('skill_name',$array[0][$i+1][1])->first();
                if(empty($skill)){
                    $skill = new Skills();
                }
                $skill->DataID = $array[0][$i+1][0];
                $skill->skill_name= $array[0][$i+1][1];
                $skill->save();
                
            }
        }
    }
}
