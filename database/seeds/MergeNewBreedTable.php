<?php

use Illuminate\Database\Seeder;
use App\BreedsDB;
use App\Imports\BreedsImport;
use Maatwebsite\Excel\Facades\Excel;
class MergeNewBreedTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path="Breeds_2020.xlsx";
        $array = Excel::toArray(new BreedsImport, $path);
        if(!empty($array)){
            for($i=0;$i<count($array[0])-1;$i++){
                $breed = BreedsDB::where('DataID',$array[0][$i+1][0])->first();
                if(empty($breed)){
                    $breed = new BreedsDB();
                }
                $breed->DataID=$array[0][$i+1][0];
                $breed->BreedName = $array[0][$i+1][1];
                $breed->BreedNameEN = $array[0][$i+1][2];
                $breed->FCICODE = $array[0][$i+1][3];
                $breed->fci_group = $array[0][$i+1][4];
                $breed->status = $array[0][$i+1][5];
                $breed->save();
                
            }
        }
    }
}
