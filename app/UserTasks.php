<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserTasks extends Authenticatable
{

    public $table = "users_tasks";
    //public $timestamps = false;
    use Notifiable;
    use Sortable;
    use SoftDeletes;
    public $sortable = [
        'created_at',
        'task_name',
        'related_to_user_id',
        'due_date_time',
        'status',
    ];
    public function user_details(){
        return $this->hasOne('App\User', 'id','related_to_user_id');
    }
   /* public function manager(){
        return $this->hasOne('App\User', 'id','manager_user_id');
    }*/
    public function breed_details(){
        return $this->hasOne('App\Breeding', 'id','related_breeding_process_id');
    }
    public function breed_details_many(){
        return $this->hasMany('App\BreedingRelatedDog', 'breeding_id','related_breeding_process_id');
    }
}
