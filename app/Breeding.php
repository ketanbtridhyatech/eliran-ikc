<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Breeding extends Authenticatable
{

    public $table = "breedings";
    public $timestamps = false;
    use Notifiable;
    use Sortable;
    use SoftDeletes;
    
public $sortable = [
    'BreddingDate',
    'MaleSagirId',
    'SagirId',
    'mother_detail.Heb_Name',
    'father_detail.Heb_Name',
];
public function father_detail() {
    return $this->hasOne('App\DogsDB', 'SagirID', 'MaleSagirId');
}
   
public function mother_detail() {
    return $this->hasOne('App\DogsDB', 'SagirID', 'SagirId');
}

}
