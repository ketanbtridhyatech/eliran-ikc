<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class DogsDB extends Authenticatable
{

    public $table = "DogsDB";
    use Notifiable;
    use Sortable;
    protected $primaryKey = 'DataID';
   use SoftDeletes;
   protected $casts = [
    'SagirId' => 'int',
    'ColorID'=>'int',
    'SagirID' => 'int',
    //'RaceID'=>'int',
    'CurrentOwnerId'=>'int',
    'HairID'=>'int',
];
public $sortable = [
    'SagirId',
    'DogsDB.SagirId',
    'ColorID',
    'RaceID',
    'CurrentOwnerId',
    'HairID',
    'Eng_Name',
    'BirthDate',
    'Heb_Name',
    'breed_name.BreedName',
    'color_name.ColorNameHE',
    'breeding_name.MaleSagirId',
    'breeding_name.BreddingDate',
    'owner_name.first_name_en',
    'owner_name.first_name',
    'Sex',
    'GenderID',
    'SagirID',
];

public function breed_name() {
    return $this->hasOne('App\BreedsDB', 'BreedCode', 'RaceID');
}
public function color_name() {
    return $this->hasOne('App\ColorsDB', 'OldCode', 'ColorID');
}
public function color_name_old() {
    return $this->hasOne('App\ColorsDB', 'DataID', 'ColorID');
}
public function color_name_new() {
    return $this->hasOne('App\ColorsDB', 'id', 'ColorID');
}
public function user_data_id() {
    return $this->hasOne('App\User', 'owner_code', 'CurrentOwnerId');
}
public function breeding_name(){
    return $this->hasOne('App\Breeding', 'SagirId', 'SagirID');
}
public function owner_name(){
    return $this->hasMany('App\Dogs2Users', 'sagir_id', 'SagirID')->join('users','users.id','user_id')->where('dogs2users.status','current');
}
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    
    

}
