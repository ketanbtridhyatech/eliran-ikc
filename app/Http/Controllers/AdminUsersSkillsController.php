<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skills;
use App\UsersSkills;
use App\User;
use Auth;
use App\Traits\log;

class AdminUsersSkillsController extends Controller
{
	use log;
	public $moduleName = "users-skills";

	/* Add View */
	public function assign(Request $request)
	{
	    if(Auth::check() && Auth::user()->is_superadmin == 1){
		$skills = skills::all();
		$userId = $request->id;
		$userSkills = UsersSkills::select('skill_id')->where('user_id',$userId)->pluck('skill_id')->toArray();
		$userDetail = User::select("email")->find($userId);

		return view('admin.users_skills_add',compact('skills','userId','userSkills','userDetail'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Store Function for Save values */
	public function store(Request $request){

		if(Auth::check() && Auth::user()->is_superadmin == 1){
		$skills = [];
		$skills = $request->skill_id;
		if(isset($skills) && !empty($skills))
		{
			$olddata = UsersSkills::where('user_id',$user_id)->get();
			$this->delete($request->userId);

			foreach($skills as $skillsId){
				$users_skills = new UsersSkills;
				$users_skills->user_id = $request->userId;
				$users_skills->skill_id = $skillsId;
				$users_skills->save();
			}
			$newdata = UsersSkills::where('user_id',$user_id)->get();
			$desc = "the user id ".Auth::user()->id." that was updated skills";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'User user skills',$desc,'user_admin','',json_encode($olddata),json_encode($newdata));
			toastr()->success('Skill assigned successfully!');
			return redirect(backUrl('user'));
			
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName.'/assign/'.$request->userId))->withInput();
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }

	}

	public function delete($user_id){
            
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = UsersSkills::where('user_id',$user_id)->delete();

		return true;
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
}
