<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Translation;
use App\Modules;
use JsValidator;
use Session;
use Validator;
use Illuminate\Support\Facades\Redis;
use Auth;
use Carbon\Carbon;
use App\Traits\log;

class AdminTranslationController extends Controller
{
	use log;
	public $moduleName = 'translation';
    //
	public function __construct()
	{
        //$this->middleware('auth');
	}


	

	

	/* Listing Page */
	public function index(Request $request)
	{
		
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$dataQuery = Translation::with('modules');

		if ($request->has('search_by_module') && $request->search_by_module != '') {

			$dataQuery->where('module_id',$request->search_by_module);
		}
		if ($request->has('search_by_english_text') && $request->search_by_english_text != '') {

			$dataQuery->where('english_text','like','%'.$request->search_by_english_text.'%');
		}
		if ($request->has('search_by_translated_text') && $request->search_by_translated_text != '') {

			$dataQuery->where('translated_text','like','%'.$request->search_by_translated_text.'%');
		}
		

		$listing = $dataQuery->sortable()->paginate(config('constant.adminPerPage'));


		$modules = $this->getModules();


		return view('admin.translation_list',compact('listing','modules','request'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Add Page */
	public function add()
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = [
			'module_id' => 'required',
			'code_lable' => 'required|unique:code_lable',
			'english_text' => 'required|unique:translation',
			'translated_text' => 'required',
		];

		$jsValidator = JsValidator::make($validator);

		$modules = $this->getModules();

		return view('admin.translation_add',compact('jsValidator','modules'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Insertion */
	public function store(Request $request){

            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'module_id' => 'required',
			'code_lable' => 'required|unique:translation',
			'english_text' => 'required',
			'translated_text' => 'required',
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{

			$translation = new Translation;
			$translation->lang_id = 1;
			$translation->code_lable = $request->code_lable;
			$translation->module_id = $request->module_id;
			$translation->english_text = $request->english_text;
			$translation->translated_text = $request->translated_text;
			$translation->created_at = Carbon::now();

			if($translation->save()){
				$desc = "the user id ".Auth::user()->id." that was created translation";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created translation',$desc,'translation_admin','',json_encode($request->all()),'');


				$this->languageDataRedis();
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}


            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Edit Page */
	public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){

		$data = Translation::with('modules')->find($id);
		$validator = [
			'module_id' => 'required',
			'code_lable' => 'required|unique:translation,code_lable,'.$id,
			'english_text' => 'required',
			'translated_text' => 'required',
		];

		$jsValidator = JsValidator::make($validator);

		$modules = $this->getModules();

		return view('admin.translation_edit',compact('jsValidator','data','modules'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Update */
	public function update(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'module_id' => 'required',
			'code_lable' => 'required|unique:translation,code_lable,'.$request->id,
			'english_text' => 'required',
			'translated_text' => 'required',
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$translation = Translation::find($request->id);
			$oldData = json_encode($translation);
			$translation->lang_id = 1;
			$translation->module_id = $request->module_id;
			$translation->code_lable = $request->code_lable;
			$translation->english_text = $request->english_text;
			$translation->translated_text = $request->translated_text;
			$translation->updated_at = Carbon::now();

			if($translation->save()){
				$desc = "the user id ".Auth::user()->id." that was updated translation";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated translation',$desc,'translation_admin','',$oldData,json_encode($request->all()));

				$this->languageDataRedis();
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}

            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }

	}

	/* Delete */
	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Translation::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			$desc = "the user id ".Auth::user()->id." that was deleted translation";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted translation',$desc,'translation_admin','',$oldData,json_encode($id));

			$this->languageDataRedis();
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Get Modules Data */
	public function getModules(){
            
		return Modules::all();
	}



	public function molduleIndex(Request $request)
	{

            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$dataQuery = Translation::with('modules');

		/*if ($request->has('search_by_module') && $request->search_by_module != '') {

			$dataQuery->where('module_id',$request->search_by_module);
		}*/

		$dataQuery->where('module_id',$request->module_id);
		$listing = $dataQuery->sortable()->paginate(config('constant.adminPerPage'));
		

		$modules = $this->getModules();

		$moduleName = $this->getModuleName($request->module_id);


		return view('admin.translation_module_list',compact('listing','modules','request'))->with('moduleName',$moduleName);
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Add Page */
	public function moduleAdd(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = [
			'module_id' => 'required',
			'english_text' => 'required|unique:translation',
			'translated_text' => 'required',
		];

		$jsValidator = JsValidator::make($validator);

		$moduleName = $this->getModuleName($request->module_id);

		return view('admin.translation_module_add',compact('jsValidator','request'))->with('moduleName',$moduleName);
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Insertion */
	public function moduleStore(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'module_id' => 'required',
			'english_text' => 'required|unique:translation',
			'translated_text' => 'required',
		]);

		if ($validator->fails()) {
			return redirect(backUrl('translation/module/add/'.$request->module_id))
			->withErrors($validator)
			->withInput();
		}else{

			$translation = new Translation;
			$translation->lang_id = 1;
			$translation->module_id = $request->module_id;
			$translation->english_text = $request->english_text;
			$translation->translated_text = $request->translated_text;
			$translation->created_at = Carbon::now();
			
			if($translation->save()){

				$desc = "the user id ".Auth::user()->id." that was created translation module";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created translation module',$desc,'translation_admin','',json_encode($request->all()),'');

				$this->languageDataRedis();
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl('translation/module/'.$request->module_id));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl('translation/module/add/'.$request->module_id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }

	}

	/* Edit Page */
	public function moduleEdit(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
                $id = $request->id;

                    $data = Translation::with('modules')->find($id);
                    $validator = [
                            'module_id' => 'required',
                            'english_text' => 'required|unique:translation,english_text,'.$request->id,
                            'translated_text' => 'required',
                    ];

                    $jsValidator = JsValidator::make($validator);

                    $moduleName = $this->getModuleName($request->module_id);


                    return view('admin.translation_module_edit',compact('jsValidator','data','request'))->with('moduleName',$moduleName);
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Update */
	public function moduleUpdate(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'module_id' => 'required',
			'english_text' => 'required|unique:translation,english_text,'.$request->id,
			'translated_text' => 'required',
		]);

		if ($validator->fails()) {
			return redirect(backUrl('translation/edit'.$request->module_id.'/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$translation = Translation::find($request->id);
			$oldData = json_encode($translation);
			$translation->lang_id = 1;
			$translation->module_id = $request->module_id;
			$translation->english_text = $request->english_text;
			$translation->translated_text = $request->translated_text;
			$translation->updated_at = Carbon::now();
			
			if($translation->save()){
				$desc = "the user id ".Auth::user()->id." that was updated translation module";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated translation module',$desc,'translation_admin','',$oldData,json_encode($request->all()));

				$this->languageDataRedis();
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl('translation/module/'.$request->module_id));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl('translation/module/edit'.$request->module_id.'/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Delete */
	public function moduleDelete(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$id = $request->id;
		$data = Translation::find($id);
		$oldData = json_encode($data);
		if($data->delete()){

			$desc = "the user id ".Auth::user()->id." that was deleted translation module";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted translation module',$desc,'translation_admin','',$oldData,json_encode($request->all()));

			$this->languageDataRedis();
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl('translation/module/'.$request->module_id));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl('translation/module/'.$request->module_id));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function getModuleName($id){
		$data = Modules::find($id);
		return $data->name;
		
	}


	/* Store Data in Redis */

	public function languageDataRedis(){

		$data = Translation::with('modules')->get()->toArray();
		if(!empty($data)){
			foreach($data as $dataval){
				if($dataval['modules']['name']!='General'){
					$lang[$dataval['module_id']][$dataval['lang_id']][$dataval['code_lable']] = $dataval['english_text']."###".$dataval['translated_text'];
				}else{
					$lang['General'][$dataval['lang_id']][$dataval['code_lable']] = $dataval['english_text']."###".$dataval['translated_text'];
				}
			}
			Redis::set('translate', json_encode($lang));
		}
		return true;
		
		
	}
	

}
