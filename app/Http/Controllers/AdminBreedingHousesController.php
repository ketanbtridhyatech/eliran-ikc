<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Country;
use JsValidator;
use Validator;
use Auth;
use App\DogsDB;
use App\Dogs2Users;
use App\BreedsDB;
use App\UsersSkills;
use App\Breedinghouses;
use App\User;
use Session;
use Carbon\Carbon;
use App\Traits\log;
use App\Exports\BreedinghousesExport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;
class AdminBreedingHousesController extends Controller
{

  use log;
  public $moduleName = "breedinghouses";

  public function __construct()
  {
        $this->middleware('auth');
  }
  public function get_breedinghouses_list(Request $request){

  // echo "here";

  	$dataQuery = new  Breedinghouses();
  	if ($request->has('search_by_breedinghouses_name') && $request->search_by_breedinghouses_name != '') {

     $dataQuery = $dataQuery->where(function ($q)  use ($request) {
            $q->where('HebName', 'like', '%' . $request->search_by_breedinghouses_name . '%')
                ->orWhere('EngName', 'like', '%' . $request->search_by_breedinghouses_name . '%');
        });
    }
    if ($request->has('search_by_breedinghouses_code') && $request->search_by_breedinghouses_code != '') {

      $dataQuery = $dataQuery->where('GidulCode', $request->search_by_breedinghouses_code);
    }


   	if ($request->submit_type == 'export_csv') {
                return Excel::download(new BreedinghousesExport($dataQuery->sortable()->get()), 'breedinghouses.csv');
              /*  return (new BreedinghousesExport($dataQuery->sortable()->get()))->download('breedinghouses.csv');*/
    }
    $listing = $dataQuery->sortable()->paginate();
   //	echo "<pre>";print_r($request->all());exit;
 	$userSkill = UsersSkills::where('user_id',Auth::user()->id)->where('skill_id','30')->first();
    return view('admin.breedinghouses_list',compact('listing','request','userSkill'));
   
  }
  function validationRules(){
		$validator = [
			'breedinghouses_gidulcode' => 'required',
			'breedinghouses_HebName' => 'required',
			'breedinghouses_EngName' => 'required',
			'breedinghouses_megadelCode' => 'required',
			/*'breedinghouses_dataid' => 'required',*/

		];

		return $validator;
	}
  public function add(){
  	if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = $this->validationRules();
		$jsValidator = JsValidator::make($validator);
		return view('admin.breedinghouses_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
  }

  public function store(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{
		//	echo "<pre>";print_r($request->all());exit;
			$Breedinghouses = new Breedinghouses;
			$Breedinghouses->GidulCode = $request->breedinghouses_gidulcode;
			$Breedinghouses->HebName = $request->breedinghouses_HebName;
			$Breedinghouses->EngName = $request->breedinghouses_EngName;
			$Breedinghouses->MegadelCode = $request->breedinghouses_megadelCode;
			$Breedinghouses->Phone = $request->breedinghouses_phone;
			/*$breedsdb->Email = $request->breedinghouses_email;*/
			/*$breedsdb->DataID = $request->breedinghouses_dataid;*/

			
			$Breedinghouses->created_at = Carbon::now();
			
			if($Breedinghouses->save()){
				$desc = "the user id ".Auth::user()->id." that was created breedinghouses";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created breedinghouses',$desc,'breedinghouses_admin','',json_encode($request->all()),'');
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
	}else{
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
    }
  }

  public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Breedinghouses::find($id);

		$validator = $this->validationRules();

		$jsValidator = JsValidator::make($validator);
		
		return view('admin.breedinghouses_edit',compact('jsValidator','data'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

 public function update(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$Breedinghouses = Breedinghouses::find($request->id);
			$oldData = json_encode($Breedinghouses);
			$Breedinghouses->GidulCode = $request->breedinghouses_gidulcode;
			$Breedinghouses->HebName = $request->breedinghouses_HebName;
			$Breedinghouses->EngName = $request->breedinghouses_EngName;
			$Breedinghouses->MegadelCode = $request->breedinghouses_megadelCode;
			$Breedinghouses->Phone = $request->breedinghouses_phone;
			$Breedinghouses->updated_at = Carbon::now();
			
			if($Breedinghouses->save()){

				$desc = "the user id ".Auth::user()->id." that was updated breedinghouses";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated breedinghouses',$desc,'breedinghouses_admin','',$oldData,json_encode($request->all()));

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	} 

	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Breedinghouses::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			
			$desc = "the user id ".Auth::user()->id." that was deleted breedinghouses";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted breedinghouses',$desc,'breedinghouses_admin','',$oldData,json_encode($id));
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	
  

  
}
