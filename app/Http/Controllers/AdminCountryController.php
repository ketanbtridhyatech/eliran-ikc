<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use JsValidator;
use Validator;
use Auth;
use Session;
use Carbon\Carbon;
use App\Traits\log;

class AdminCountryController extends Controller
{

	use log;
	public $moduleName = "country";

	public function __construct()
	{
        //$this->middleware('auth');
	}


	/* Listing View */
	public function index(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$dataQuery = Country::sortable();

		if ($request->has('search_by_country_name') && $request->search_by_country_name != '') {
			
			$dataQuery->where('country_name','like','%'.$request->search_by_country_name.'%');
			$dataQuery->orWhere('country_name_en','like','%'.$request->search_by_country_name.'%');
			
		}
		if ($request->has('search_by_country_code') && $request->search_by_country_code != '') {
			
			$dataQuery->where('country_code',$request->search_by_country_code);
			
		}


		$listing = $dataQuery->paginate(settingParam('record-per-page'));
		return view('admin.country_list',compact('listing','request'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Common Validation Rules */
	function validationRules(){
		$validator = [
			'country_name' => 'required',
			'country_name_en' => 'required',
			'country_code' => 'required',
		];

		return $validator;
	}

	/* Add View */
	public function add()
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		
		$validator = $this->validationRules();
		$jsValidator = JsValidator::make($validator);
		return view('admin.country_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}


	/* Store Function for Save values */
	public function store(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){

		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{

			$country = new Country;
			$country->country_name = $request->country_name;
			$country->country_name_en = $request->country_name_en;
			$country->country_code = $request->country_code;
			$country->created_at = Carbon::now();
			
			if($country->save()){
				$desc = "the user id ".Auth::user()->id." that was created country";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created country',$desc,'country_admin','',json_encode($request->all()),'');

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Edit View */
	public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Country::find($id);

		$validator = $this->validationRules();

		$jsValidator = JsValidator::make($validator);
		return view('admin.country_edit',compact('jsValidator','data'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}


	/* Update Function for Update values */
	public function update(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$country = Country::find($request->id);
			$oldData = json_encode($country);
			$country->country_name = $request->country_name;
			$country->country_name_en = $request->country_name_en;
			$country->country_code = $request->country_code;
			$country->updated_at = Carbon::now();
			
			if($country->save()){
				$desc = "the user id ".Auth::user()->id." that was updated country";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated country',$desc,'country_admin','',$oldData,json_encode($request->all()));

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }

	}

	/* Soft Delete */
	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Country::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			$desc = "the user id ".Auth::user()->id." that was deleted country";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted country',$desc,'country_admin','',$oldData,json_encode($id));

			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
}
