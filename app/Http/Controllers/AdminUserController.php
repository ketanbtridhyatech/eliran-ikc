<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Country;
use App\Skills;
use App\UsersSkills;
use Auth;
use App;
use App\Dogs2Users;
use App\DogsDB;
use App\Exports\DogsExport;
use App\Exports\UsersExport;
use JsValidator;
use Validator;
use Hash;
use Image;
use File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\NewUser;
use App\Roles;
use App\RoleUser;
use App\Settings;
use App\HairsCity;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Traits\log;
ini_set('max_execution_time', '0');

class AdminUserController extends Controller
{
	use log;
	public $moduleName = 'user';
	//
	public function __construct()
	{
		$key = Settings::select('value')->where('slug', 'aws-access-key-id')->first();
        $secretKey = Settings::select('value')->where('slug', 'aws-secret-access-key')->first();
        $region = Settings::select('value')->where('slug', 'aws-default-region')->first();
        $bucket = Settings::select('value')->where('slug', 'aws-bucket')->first();
        $url = Settings::select('value')->where('slug', 'aws-url')->first();
        config(['filesystems.disks.s3.key' => $key['value'], 'filesystems.disks.s3.secret' => $secretKey['value'], 'filesystems.disks.s3.region' => $region['value'], 'filesystems.disks.s3.bucket' => $bucket['value'], 'filesystems.disks.s3.url' => $url['value']]);
		//$this->middleware('auth');
	}


	/* Listing View */
	public function index(Request $request)
	{
           if(Auth::check() && Auth::user()->is_superadmin == 1){
		$skills = Skills::all();
		$dataQuery = User::with('country', 'userSkills');

		if ($request->has('search_by_first') && $request->search_by_first != '') {
			if (currentLanguage() == 1) {
				$dataQuery->where('first_name', 'like', '%' . $request->search_by_first . '%');
			} else {
				$dataQuery->where('first_name_en', 'like', '%' . $request->search_by_first . '%');
			}
		}

		if ($request->has('search_by_last') && $request->search_by_last != '') {
			if (currentLanguage() == 1) {
				$dataQuery->where('last_name', 'like', '%' . $request->search_by_last . '%');
			} else {
				$dataQuery->where('last_name_en', 'like', '%' . $request->search_by_last . '%');
			}
		}

		if ($request->has('search_by_email') && $request->search_by_email != '') {

			$dataQuery->where('email', 'like', '%' . $request->search_by_email . '%');
		}

		if ($request->has('search_by_mobile') && $request->search_by_mobile != '') {

			$dataQuery->where('mobile_phone', 'like', '%' . $request->search_by_mobile . '%');
		}

		if ($request->has('search_by_city') && $request->search_by_city != '') {
			if (currentLanguage() == 1) {
				$dataQuery->where('address_city', 'like', '%' . $request->search_by_city . '%');
			} else {
				$dataQuery->where('address_city_en', 'like', '%' . $request->search_by_city . '%');
			}
		}

		if ($request->has('search_by_skills') && $request->search_by_skills != '') {

			$search_by_skills = $request->search_by_skills;
			$dataQuery->orWhereHas('skills', function ($skills) use ($search_by_skills) {
				$skills->whereIn('skill_id', $search_by_skills);
			});
		}

		if ($request->has('search_by_status') && $request->search_by_status != '') {

			$dataQuery->where('status', $request->search_by_status);
		}


		$listing = $dataQuery->sortable()->paginate(settingParam('record-per-page'));
		//echo "<pre>"; print_r($listing);exit;
		//echo $listing->currentPage();exit;


		return view('admin.user_list', compact('listing', 'skills', 'request'));
           }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}


	/* Add View */
	public function add()
	{

            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$countryList = [];
		$validator = [
			'first_name' => 'required',
			'last_name' => 'required',
			'first_name_en' => 'required',
			'last_name_en' => 'required',
			'email' => 'required|email|unique:users',
			'mobile_phone' => 'required|unique:users',
			'password' => 'required|min:6',
			'confirmed' => 'required|same:password',
			'address_street_number' => 'required',
			'address_street' => 'required',
			'address_city' => 'required',
			'address_city_en' => 'required',
			'address_zip' => 'required',
			'birth_date' => 'required',
			'profile_photo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
			'country_id' => 'required',

		];

		$customMessages = [
			'required' => 'The :attribute field can not be blank1111.'
		];

		$jsValidator = JsValidator::make($validator);

		$countryList = $this->getCountry();

		$skills = Skills::all();

		$roles = Roles::get();
		$HairsCity = HairsCity::get();
		//echo "<pre>";print_r(storage_path());exit;
		return view('admin.user_add', compact('jsValidator', 'countryList', 'skills', 'roles','HairsCity'));
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}


	/* Store Function for Save values */
	public function store(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validationArrayPassport = $skills = [];
		$validationArray = [
			'first_name' => 'required',
			'last_name' => 'required',
			'first_name_en' => 'required',
			'last_name_en' => 'required',
			'email' => 'required|email|unique:users',
			'mobile_phone' => 'required|unique:users',
			'password' => 'required|min:6',
			'confirmed' => 'required|same:password',
			'address_street_number' => 'required',
			'address_street' => 'required',
			'address_city' => 'required',
			'address_city_en' => 'required',
			'address_zip' => 'required',
			'birth_date' => 'required',
			'profile_photo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
			'country_id' => 'required',
			'country_code' => 'required',
		];

		/*if ($request->country_id && $request->country_id == 1) {
			$validationArrayPassport = [
				'passport_id' => 'required',
			];
		}*/

		$validationArrayMerge = array_merge($validationArray, $validationArrayPassport);


		$validator = Validator::make($request->all(), $validationArrayMerge);
		$validator->getTranslator()->setLocale('ro');
		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName . '/add'))
				->withErrors($validator)
				->withInput();
		} else {

			$skills = (isset($request->skill_id) && !empty($request->skill_id)) ? $request->skill_id : '';

			$user = new User;
			$user->first_name = $request->first_name;
			$user->last_name = $request->last_name;
			$user->first_name_en = $request->first_name_en;
			$user->last_name_en = $request->last_name_en;
			$user->email = $request->email;
			$user->mobile_phone = $request->mobile_phone;
			$user->phone = $request->phone;
			$user->password = Hash::make($request->password);
			$user->address_city = $request->address_city;
			$user->address_city_en = $request->address_city_en;
			$user->address_street_number = $request->address_street_number;
			$user->address_street = $request->address_street;
			$user->address_zip = $request->address_zip;
			$user->fax = $request->fax;
			$user->social_id_number = $request->social_id_number;
			//$user->birth_date = convertDateInYMD($request->birth_date);
			$user->birth_date =  date('Y-m-d', strtotime(str_replace('/', '-', $request->birth_date)));
			
			$user->country_id = $request->country_id;
			$user->country_code = $request->country_code;
			$user->passport_id = $request->passport_id;
			$user->created_at = Carbon::now();
			$user->role_id = $request->role_id;

			$user->info_id = $request->info_id;
			$user->owner_email = $request->owner_email;
			$user->sagir_owner_id = $request->sagir_owner_id;
			$user->is_current_owner = $request->is_current_owner;
			$user->order_id = $request->order_id;
			$user->new_sid = $request->new_sid;
			$user->record_type = $request->record_type;
			$user->data_id = $request->data_id;
			$user->owner_code = $request->owner_code;
			$user->new_org_data_id = $request->new_org_data_id;
			$user->new_fill_date = $request->new_fill_date;
			$user->new_filler_ip = $request->new_filler_ip;
			$user->club_id = $request->club_id;
			$user->owner_payment_sum = $request->owner_payment_sum;
			$user->owner_payment_last4 = $request->owner_payment_last4;
			$user->member_status = $request->member_status;
			$user->special_key = $request->special_key;
			$user->expire_date = $request->expire_date;
			$user->owner_total_payment = $request->owner_total_payment;
			$user->start_date = $request->start_date;
			$user->record_source = $request->record_source;
			$user->is_judge = $request->is_judge;
			$user->city_id = $request->city_id;
			$user->private_phone_1 = $request->private_phone_1;
			$user->private_phone_2 = $request->private_phone_2;
			$user->note = $request->note;
			$user->image = $request->image;
			$user->invoice_id = $request->invoice_id;
			$user->breed_id = $request->breed_id;
			$user->user_key = $request->user_key;
			$user->is_breed_manager = $request->is_breed_manager;
			$user->payment_status = $request->payment_status;
			
			if ($request->role_id == 1 || $request->role_id == '1') {
				$user->is_superadmin = 1;
			}else{
				$user->is_superadmin = 0;
			}

			/*if ($request->profile_photo && !empty($request->profile_photo)) {

				$profileImage = $request->profile_photo;
				$imageName = time() . '.' . $profileImage->getClientOriginalExtension();
				//$destinationPath = storage_path('users');
				$destinationPath = storage_path() . '/app/public/users';
				if (!File::exists($destinationPath)) {
					File::makeDirectory($destinationPath, $mode = 0777, true, true);
				}
				$img = Image::make($profileImage->getRealPath());

				$img->fit(100, 100, function ($constraint) {
					$constraint->aspectRatio();
				})->save($destinationPath . '/thumb_' . $imageName);

				$profileImage->move($destinationPath, $imageName);

				$user->profile_photo = $imageName;
			}*/
			if (isset($request->profile_photo)) {
	            $file = $request->profile_photo;

	            $destinationPath = 'user_photos';
	            $imageName = time() . '.' . $file->getClientOriginalName();
	            $file->store('user_photos', 's3');
	            Storage::disk('s3')->put($imageName, file_get_contents($file->getRealPath()));
	            //$file->move($destinationPath, $file->getClientOriginalName());
	            $user->profile_photo = $imageName;
	        }
			if ($user->save()) {

				/* Assing users skills*/
				$this->assignSkills($skills, $user->id);
					$desc = "the user id ".Auth::user()->id." that was created";
        			$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'create user',$desc,'users_admin','',json_encode($request->all()),'');
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			} else {
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName . '/add'))->withInput();
			}
		}
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}




	/* Edit View */
	public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = User::find($id);

		$validator = [
			//'first_name' => 'required',
			//'last_name' => 'required',
			//'first_name_en' => 'required',
			//'last_name_en' => 'required',
			'email' => 'email',
			//'mobile_phone' => 'required',
			//'address_street_number' => 'required',
			//'address_street' => 'required',
			//'address_city' => 'required',
			//'address_city_en' => 'required',
			//'address_zip' => 'required',
			//'birth_date' => 'required',
			'profile_photo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
			//'country_id' => 'required',
		];

		$jsValidator = JsValidator::make($validator);

		$countryList = $this->getCountry();

		$skills = Skills::all();

		$roles = Roles::get();

		$related_dogs = Dogs2Users::where('user_id', $id)->pluck('sagir_id');
		//echo "<pre>";print_r($data);exit;
		$dogs_details = DogsDB::whereIn("SagirID", $related_dogs)->get();
		if(!empty($data)){
			if(!empty($data->profile_photo)){
				$data->profile_photo = $this->getStorageURL($data->profile_photo);
			}
			if(!empty($data->image)){
				$data->image = $this->getStorageURL($data->image);
			}
		}
		$userSkills = UsersSkills::select('skill_id')->where('user_id', $id)->pluck('skill_id')->toArray();


		$HairsCity = HairsCity::get();
		return view('admin.user_edit', compact('jsValidator', 'data', 'countryList', 'skills', 'userSkills', 'roles', 'dogs_details','HairsCity'));
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}


	/* Update Function for Update values */
	public function update(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validationArrayPass = $validationArrayPassport = $skills = [];
		$validationArray = [
			//'first_name' => 'required',
			//'last_name' => 'required',
			//'first_name_en' => 'required',
			//'last_name_en' => 'required',
			//'email' => 'email',
			//'mobile_phone' => 'unique:users,mobile_phone,' . $request->id,
			//'address_street_number' => 'required',
			//'address_street' => 'required',
			//'address_city' => 'required',
			//'address_city_en' => 'required',
			//'address_zip' => 'required',
			//'birth_date' => 'required',
			'profile_photo' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
			//'country_id' => 'required',
			//'country_code' => 'required',
		];

		if ($request->password && $request->password != '') {
			$validationArrayPass = [
				'password' => 'required|min:6',
				'confirmed' => 'required|same:password',
			];
		}

		/*if ($request->country_id && $request->country_id == 1) {
			$validationArrayPassport = [
				'passport_id' => 'required',
			];
		}*/

		$validationArrayMerge = array_merge($validationArray, $validationArrayPass, $validationArrayPassport);

		$validator = Validator::make($request->all(), $validationArrayMerge);


		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName . '/edit/' . $request->id))
				->withErrors($validator)
				->withInput();
		} else {

			$skills = (isset($request->skill_id) && !empty($request->skill_id)) ? $request->skill_id : '';

			$user = User::find($request->id);
			$olddata = json_encode($user);
			$user->first_name = $request->first_name;
			$user->last_name = $request->last_name;
			$user->first_name_en = $request->first_name_en;
			$user->last_name_en = $request->last_name_en;
			$user->email = $request->email;
			$user->mobile_phone = $request->mobile_phone;
			$user->phone = $request->phone;
			$user->address_city = $request->address_city;
			$user->address_city_en = $request->address_city_en;
			$user->address_street_number = $request->address_street_number;
			$user->address_street = $request->address_street;
			$user->address_zip = $request->address_zip;
			$user->fax = $request->fax;
			$user->social_id_number = $request->social_id_number;
			//$user->birth_date = convertDateInYMD($request->birth_date);
			$user->birth_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->birth_date)));
			$user->country_id = $request->country_id;
			$user->country_code = $request->country_code;
			$user->passport_id = $request->passport_id;
			$user->updated_at = Carbon::now();
			$user->role_id = $request->role_id;
			$user->info_id = $request->info_id;
			$user->owner_email = $request->owner_email;
			$user->sagir_owner_id = $request->sagir_owner_id;
			$user->is_current_owner = $request->is_current_owner;
			$user->order_id = $request->order_id;
			$user->new_sid = $request->new_sid;
			$user->record_type = $request->record_type;
			$user->data_id = $request->data_id;
			$user->owner_code = $request->owner_code;
			$user->new_org_data_id = $request->new_org_data_id;
			//$user->new_fill_date = convertDateInYMD($request->new_fill_date);
			$user->new_fill_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->new_fill_date)));
			$user->new_filler_ip = $request->new_filler_ip;
			$user->club_id = $request->club_id;
			$user->owner_payment_sum = $request->owner_payment_sum;
			$user->owner_payment_last4 = $request->owner_payment_last4;
			$user->member_status = $request->member_status;
			$user->special_key = $request->special_key;
			//$user->expire_date = convertDateInYMD($request->expire_date);
			$user->expire_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->expire_date)));
			$user->owner_total_payment = $request->owner_total_payment;
			//$user->start_date = convertDateInYMD($request->start_date);
			$user->start_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->start_date)));
			$user->record_source = $request->record_source;
			$user->is_judge = $request->is_judge;
			$user->city_id = $request->city_id;
			$user->private_phone_1 = $request->private_phone_1;
			$user->private_phone_2 = $request->private_phone_2;
			$user->note = $request->note;
			//$user->image = $request->image;

			/*if ($request->image && !empty($request->image)) {

				$profileImage = $request->image;
				$imageName = time() . '.' . $profileImage->getClientOriginalExtension();
				//$destinationPath = storage_path('users');
				$destinationPath = storage_path() . '/app/public/users';
				if (!File::exists($destinationPath)) {
					File::makeDirectory($destinationPath, $mode = 0777, true, true);
				}
				$img = Image::make($profileImage->getRealPath());

				$img->fit(100, 100, function ($constraint) {
					$constraint->aspectRatio();
				})->save($destinationPath . '/thumb_' . $imageName);

				$profileImage->move($destinationPath, $imageName);

				$user->image = $imageName;
			}*/
			if (isset($request->image)) {
		            $file = $request->image;

		            $destinationPath = 'user_photos';
		            $imageName = time() . '.' . $file->getClientOriginalName();
		            $file->store('user_photos', 's3');
		            Storage::disk('s3')->put($imageName, file_get_contents($file->getRealPath()));
		            //$file->move($destinationPath, $file->getClientOriginalName());
		            $user->image = $imageName;
		        }
			$user->invoice_id = $request->invoice_id;
			$user->breed_id = $request->breed_id;
			$user->user_key = $request->user_key;
			$user->is_breed_manager = $request->is_breed_manager;
			$user->payment_status = $request->payment_status;

			if(isset($request->role_id)){
				if ($request->role_id == 1 || $request->role_id == '1') {
					$user->is_superadmin = 1;
				}else{
					$user->is_superadmin = 0;
				}
			}
			//echo "<pre>";print_r($user);exit;

			if ($request->password && $request->password != '') {
				$user->password = Hash::make($request->password);
			}

			/*if ($request->profile_photo && !empty($request->profile_photo)) {

				$profileImage = $request->profile_photo;
				$imageName = time() . '.' . $profileImage->getClientOriginalExtension();
				//$destinationPath = storage_path('users');
				$destinationPath = storage_path() . '/app/public/users';
				if (!File::exists($destinationPath)) {
					File::makeDirectory($destinationPath, $mode = 0777, true, true);
				}
				$img = Image::make($profileImage->getRealPath());

				$img->fit(100, 100, function ($constraint) {
					$constraint->aspectRatio();
				})->save($destinationPath . '/thumb_' . $imageName);

				$profileImage->move($destinationPath, $imageName);

				$user->profile_photo = $imageName;
			}*/
			if (isset($request->profile_photo)) {
	            $file = $request->profile_photo;

	            $destinationPath = 'user_photos';
	            $imageName = time() . '.' . $file->getClientOriginalName();
	            $file->store('user_photos', 's3');
	            Storage::disk('s3')->put($imageName, file_get_contents($file->getRealPath()));
	            //$file->move($destinationPath, $file->getClientOriginalName());
	            $user->profile_photo = $imageName;
	        }

			if ($user->save()) {
//echo "<pre>";print_r($user);exit;
				$desc = "the user id ".Auth::user()->id." that was updated";
        			$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'update user',$desc,'users_admin','',$olddata,json_encode($request->all()));

				/* Assing users skills*/
		
				$this->assignSkills($skills, $request->id);

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			} else {
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName . '/add'))->withInput();
			}
		}
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}


	/* Soft Delete */
	public function delete($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = User::find($id);
		$olddata = json_encode($data);
		if ($data->delete()) {
			$desc = "the user id ".Auth::user()->id." that was deleted";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'delete user',$desc,'users_admin','',$olddata,'');
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		} else {
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}

	/* Detail View */
	public function detail($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = User::find($id);
		return view('admin.user_detail', compact('data'));
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}


	/* Change User Language from TopBar*/
	public function changeLanguage(Request $request)
	{

		$userId = Auth::id();
		$userData = User::find($userId);
		$olddata = json_encode($userData);
		$userData->language_id = $request->language_id;
		if ($userData->save()) {
			$desc = "the user id ".Auth::user()->id." that was updated";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'update user',$desc,'users_admin','',$olddata,json_encode($request->all()));
			$this->reply['status'] = true;
			$this->reply['msg'] = 'Language updated successfully';
		} else {
			$this->reply['status'] = false;
			$this->reply['msg'] = 'Technical issue';
		}

		return response()->json($this->reply);
	}



	/* Multi Delete with checkbox */
	public function multiDelete(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){

		if ($request->user_ids && !empty($request->user_ids)) {
			User::destroy($request->user_ids);
			$desc = "the user id ".Auth::user()->id." that was deleted";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'delete user',$desc,'users_admin','','',json_encode($request->all()));
			$this->reply['status'] = true;
			$this->reply['msg'] = 'Record deleted successfully';
		} else {
			$this->reply['status'] = false;
			$this->reply['msg'] = 'Technical issue';
		}
		return response()->json($this->reply);
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}

	/* Multi Active with checkbox */
	public function multiActive(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		if ($request->user_ids && !empty($request->user_ids)) {

			User::whereIn('id', $request->user_ids)
				->update(['status' => 1]);

			$desc = "the user id ".Auth::user()->id." that was user active status updated";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'update user active status',$desc,'users_admin','','',json_encode($request->all()));

			$this->reply['status'] = true;
			$this->reply['msg'] = 'Record activated successfully';
		} else {
			$this->reply['status'] = false;
			$this->reply['msg'] = 'Technical issue';
		}
		return response()->json($this->reply);
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}

	/* Multi DeActive with checkbox */
	public function multiInActive(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		if ($request->user_ids && !empty($request->user_ids)) {

			User::whereIn('id', $request->user_ids)
				->update(['status' => 0]);
			$desc = "the user id ".Auth::user()->id." that was user inactive status updated";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'update user inactive status',$desc,'users_admin','','',json_encode($request->all()));
			$this->reply['status'] = true;
			$this->reply['msg'] = 'Record inactivated successfully';
		} else {
			$this->reply['status'] = false;
			$this->reply['msg'] = 'Technical issue';
		}
		return response()->json($this->reply);
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}

	/* Get Country Dropdown */
	public function getCountry()
	{

		$countryList = Country::all();

		return $countryList;
	}

	/* Get Country code from Country */
	public function getCountryCode(Request $request)
	{

		$country = Country::find($request->country_id);

		if ($country && $country->country_code != '') {

			$this->reply['status'] = true;
			$this->reply['country_code'] = $country->country_code;
		} else {
			$this->reply['status'] = true;
			$this->reply['country_code'] = "";
		}
		return response()->json($this->reply);
	}


	/* Assign User Skills */
	public function assignSkills($skills, $userId)
	{
            if (isset($skills) && !empty($skills)) {
            		$olddata = UsersSkills::where('user_id', $userId)->first();
                    $this->deleteSkills($userId);
                    foreach ($skills as $skillsId) {
                            $users_skills = new UsersSkills;
                            $users_skills->user_id = $userId;
                            $users_skills->skill_id = $skillsId;
                            $users_skills->save();
                    }

	            $desc = "the user id ".Auth::user()->id." that was skills Updated";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated skills',$desc,'users_admin','',json_encode($olddata),json_encode($skills));
            }

            return true;
	}

	/* Delete old users skills*/
	public function deleteSkills($user_id)
	{
		$olddata = UsersSkills::where('user_id', $user_id)->first();
		$data = UsersSkills::where('user_id', $user_id)->delete();
		$desc = "the user id ".Auth::user()->id." that was skills deleted";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted skills',$desc,'users_admin','','',json_encode($olddata));
		return true;
	}
	/* Table Migration */
	public function migration(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
                $countryList = $this->getCountry();
                $skills = Skills::all();
                $dataQuery = User::with('country', 'userSkills');

                if ($request->has('search_by_name') && $request->search_by_name != '') {
                            $explode = explode(' ', $request->search_by_name);
                            Session::forget("checkValue");
                            $dataQuery->Where(function ($query) use ($request, $explode) {
                                    $query->Where('first_name', 'like', '%' . $request->search_by_name . '%')
                                            ->orWhere('last_name', 'like', '%' . $request->search_by_name . '%')
                                            ->orWhere('first_name_en', 'like', '%' . $request->search_by_name . '%')
                                            ->orWhere('last_name_en', 'like', '%' . $request->search_by_name . '%');
                                            error_reporting(0);

                                    $combination = get_combination($request->search_by_name);
                                    for ($i = 0; $i < count($combination); $i++) {
                                            $query->orWhere('first_name', 'like', '%' . $combination[$i] . '%')
                                                    ->orWhere('last_name', 'like', '%' . $combination[$i] . '%')
                                                    ->orWhere('first_name_en', 'like', '%' . $combination[$i] . '%')
                                                    ->orWhere('last_name_en', 'like', '%' . $combination[$i] . '%');
                                    }
                            });
                            if(count($explode) > 1){
                                    $dataQuery->orWhere(function ($query) use ($request, $explode) {
                                            $query->where('record_type','Members')
                                            ->Where(function ($query) use ($request, $explode){
                                            $query->Where('first_name', 'like', '%' . $explode[0] .'%')
                                            ->Where('last_name', 'like', '%' . $explode[1] . '%')
                                            ->orWhere(function ($query) use ($request, $explode){
                                            $query->where('first_name_en', 'like', '%' . $explode[0] . '%')
                                            ->Where('last_name_en', 'like', '%' . $explode[1]. '%');
                                    });

                                    });
                            });
                            $dataQuery->orWhere(function ($query) use ($request, $explode) {
                                    $query->where('record_type','Members')
                                    ->Where(function ($query) use ($request, $explode){
                                    $query->Where('first_name', 'like', '%' . $explode[1] .'%')
                                    ->Where('last_name', 'like', '%' . $explode[0] . '%')
                                    ->orWhere(function ($query) use ($request, $explode){
                                    $query->where('first_name_en', 'like', '%' . $explode[1] . '%')
                                    ->Where('last_name_en', 'like', '%' . $explode[0]. '%');
                            });

                            });
                    });
                            }

                    }

                if ($request->has('search_by_email') && $request->search_by_email != '') {
                            Session::forget("checkValue");

                            $dataQuery->where('email', 'like', '%' . $request->search_by_email . '%');
                    }

                if ($request->has('search_by_phone') && $request->search_by_phone != '') {
                            Session::forget("checkValue");

                            $dataQuery->where(function ($query) use ($request) {
                                    $query->where('phone', 'like', '%' . $request->search_by_phone . '%')
                                            ->orWhere('mobile_phone', 'like', '%' . $request->search_by_phone . '%');
                            });
                    }

                if ($request->has('search_by_owner_code') && $request->search_by_owner_code != '') {
                            Session::forget("checkValue");

                            $dataQuery->where('owner_code', $request->search_by_owner_code . '.00000000');
                    }
                if ($request->has('search_by_dog_sagir') && $request->search_by_dog_sagir != '') {
                            Session::forget("checkValue");

                            $dataQuery->where('sagir_owner_id', $request->search_by_dog_sagir . '.00000000');
                    }
                if ($request->has('search_by_new_user') && $request->search_by_new_user != '') {
                            Session::forget("checkValue");

                            $dataQuery->where('record_type', 'Native');

                            if ($request->has('search_by_first_name') && $request->search_by_first_name != '') {
                                    $dataQuery->where(function ($query) use ($request) {
                                            $query->where('first_name', 'like', '%' . $request->search_by_first_name . '%')
                                                    ->orWhere('first_name_en', 'like', '%' . $request->search_by_first_name . '%');
                                    });
                            }
                            if ($request->has('search_by_last_name') && $request->search_by_last_name != '') {
                                    $dataQuery->where(function ($query) use ($request) {
                                            $query->where('last_name', 'like', '%' . $request->search_by_last_name . '%')
                                                    ->orWhere('last_name_en', 'like', '%' . $request->search_by_last_name . '%');
                                    });
                            }
                    } 
                else {

                    $dataQuery->where(function ($query) use ($request) {
                            $query->where('record_type', '!=', 'Native')
                                    ->orWhereNull('record_type');
                    });
                }

                $settings = Settings::where('field', 'Migration Record Per Page')->first();
                if (isset($request->record_per_page)) {
                        $settings->value = $request->record_per_page;
                        $settings->save();
                }
                $listing = $dataQuery->sortable()->paginate($settings->value);
                $record_per_page = $settings->value;
                $getCheckValue = Session::get('checkValue');
                if ($request->ajax()) {
                        return view('admin.ajax_migration_user_list', compact('listing', 'skills', 'request', 'countryList', 'getCheckValue', 'record_per_page'))->render();
                }
                return view('admin.migration_user_list', compact('listing', 'skills', 'request', 'countryList', 'getCheckValue', 'record_per_page'));
            }else{
                toastr()->error('You do not have permission to perform this action!');
		return redirect(frontUrl('error'));
           }
	}

	public function migrationCheckbox(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		if (Session::exists('checkValue')) {
			$getCheckValue = Session::get('checkValue');

			$convert_to_array = explode(',', $request->checkVal);
		
			for ($i = 0; $i < count($convert_to_array); $i++) {
				array_push($getCheckValue, $convert_to_array[$i]);
			}
			Session::put('checkValue', $getCheckValue);
			Session::save();
		} else {
			$newCheckValue = array();
			$convert_to_array = explode(',', $request->checkVal);
			for ($i = 0; $i < count($convert_to_array); $i++) {
				array_push($newCheckValue, $convert_to_array[$i]);
			}
			//array_push($newCheckValue,  $request->checkVal);
			Session::put('checkValue', $newCheckValue);
			Session::save();
		}
		$getFinalCheckValue = Session::get('checkValue');
		
		echo json_encode($getFinalCheckValue);
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	public function migrationCheckboxunChecked(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		if (Session::exists('checkValue')) {
			$getCheckValue = Session::get('checkValue');
			$convert_to_array = explode(',', $request->unCheckVal);
			for ($i = 0; $i < count($convert_to_array); $i++) {
				if (($key = array_search($convert_to_array[$i], $getCheckValue)) !== false) {
					unset($getCheckValue[$key]);
				}
			}
			Session::put('checkValue', $getCheckValue);
			Session::save();
		}
		$getFinalCheckValue1 = Session::get('checkValue');
		echo json_encode($getFinalCheckValue1);
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function migration_add_new_user(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = $request->all();
		$count_total_record = $data['m_prefix'];
		$settings_data = Settings::where('slug','country-code')->first();
		for ($i = 0; $i < count($count_total_record); $i++) {

			$user_details = User::where('record_type', 'Native')->where('mobile_phone', $data['m_prefix'][$i].$data['mobile_number'][$i])->first();
			if (!empty($user_details) && !empty($data['mobile_number'][$i])) {
				$new_user = $user_details;
			} else {
				$new_user = new User();
				$new_user->mobile_phone = $data['m_prefix'][$i].$data['mobile_number'][$i];
				$new_user->country_code = !empty($settings_data)?$settings_data->value:'';
				$new_user->first_name = $data['first_name'][$i];
				$new_user->first_name_en = $data['first_name_en'][$i];
				$new_user->last_name = $data['last_name'][$i];
				$new_user->last_name_en = $data['last_name_en'][$i];
				$new_user->email = $data['email'][$i];
				$new_user->address_city = $data['city'][$i];
				$new_user->address_city_en = $data['city_en'][$i];
				$new_user->social_id_number = $data['social_id'][$i];
				$new_user->birth_date = (empty($data['year'][$i]) || empty($data['month'][$i]) || empty($data['day'][$i])) ? NULL : $data['year'][$i] . '-' . $data['month'][$i] . '-' . $data['day'][$i];
				$new_user->address_street = $data['street'][$i];
				$new_user->house_number = $data['house_number'][$i];
				$new_user->address_zip = $data['zip_code'][$i];
				$new_user->country_id = $data['country'][$i];
				$new_user->is_superadmin = 0;
				$new_user->language_id = 1;
				$new_user->created_from = $data['checked_id'];
				$new_user->record_type = "Native";
				$new_user->created_at = Carbon::now();
				$new_user->save();
			}

			$getFinalCheckValue = Session::get('checkValue');
			
			if (!empty($getFinalCheckValue)) {
				$count_checked_id = array_values($getFinalCheckValue);
				
				if (!empty($count_checked_id)) {
					 
					User::whereIn('id', $count_checked_id)->update(array('migration_status' => 'Partial Migration'));
					
					for ($j = 0; $j < count($count_checked_id); $j++) {
						$user_details = User::find($count_checked_id[$j]);
						if (!empty($user_details)) {
							$dogs = DogsDB::where('CurrentOwnerId', $user_details->owner_code)->get();
							if (!empty($dogs)) {
								foreach ($dogs as $dog) {
									$dogs2user = new Dogs2Users();
									$dogs2user->user_id = $new_user->id;
									$dogs2user->sagir_id = $dog->SagirID;
									$dogs2user->status = 'current';
									$dogs2user->save();
								}
							}
						}
					}
				}
			}
		}
		 $desc = "the user id ".Auth::user()->id." that was created";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Created Migration User',$desc,'Migration_admin','',json_encode($request->all()),'');
		Session::forget("checkValue");
		Session::save();
		$request->session()->flash('success', 'Record added successfully');

		return redirect(backUrl('migration'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function migration_add_new_member_form(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$countryList = $this->getCountry();
		echo view('admin.migration_add_new_member_form', compact('request', 'countryList'))->render();
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	public function checking_user(Request $request)
	{
            
		$user_details = User::where('record_type', 'Native')->where('mobile_phone', $request->m_prefix.$request->phone_number)->first();
		if (!empty($user_details)) {
			echo json_encode($user_details->toArray());
		}
	}
	public function deleteRelatedDog(Request $request)
	{
		Dogs2Users::where(array('user_id' => $request->user_id, 'sagir_id' => $request->sagir_id))->delete();
		$desc = "the user id ".Auth::user()->id." that was Deleted Dog";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Deleted Dog',$desc,'Dog_admin','','',json_encode($request->all()));

	}
	public function data_compare(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		return view('admin.data_compare');
            }else{
                toastr()->error('You do not have permission to perform this action!');
                return redirect(frontUrl('error'));
            }
	}
	public function search_related_dogs(Request $request)
	{
		$dogs = Dogs2Users::where('user_id', $request->id)->pluck('sagir_id');
		$dogs_details = DogsDB::with('breed_name', 'color_name','user_data_id')->whereIn('SagirID', $dogs)->get();
		echo view('admin.dog_details', compact('dogs_details', 'request'))->render();
	}

	public function dogs_excel(Request $request)
	{   if(Auth::check() && Auth::user()->is_superadmin == 1){
		$desc = "the user id ".Auth::user()->id." that was Excel Dog created";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Excel Dog created',$desc,'Dog_admin','',json_encode($request->all()),'');
		return (new DogsExport($request->id))->download('dogs.xlsx');
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	public function exportUsers(Request $request)
	{   if(Auth::check() && Auth::user()->is_superadmin == 1){
		$desc = "the user id ".Auth::user()->id." that was Exports user created";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Exports user created',$desc,'user_admin','',json_encode($request->all()),'');
		return (new UsersExport)->download('users.xlsx');
            }else{
                toastr()->error('You do not have permission to perform this action!');
                return redirect(frontUrl('error'));
            }
		
	}
	public function delete_native_user(Request $request,$user_id){
		UsersSkills::where('user_id',$user_id)->delete();
		User::where('id',$user_id)->delete();
		Dogs2Users::where('user_id',$user_id)->delete();
		$desc = "the user id ".Auth::user()->id." that was delete native user";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Deleted Native User',$desc,'user_admin','','',json_encode($user_id));
		toastr()->error('User Deleted Successfully !');
		
	}
	 public function getStorageURL($filename)
    {
       
        $disk = Storage::disk('s3');
        if ($disk->exists($filename)) {
            $client = $disk->getDriver()->getAdapter()->getClient();
            //echo "123";exit;
            $bucket = \Config::get('filesystems.disks.s3.bucket');

            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key' => $filename
            ]);

            $request = $client->createPresignedRequest($command, '+120 minutes');
            return (string) $request->getUri();
        } else
            return '';
            
    }
}
