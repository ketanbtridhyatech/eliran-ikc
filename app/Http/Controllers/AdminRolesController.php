<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;
use JsValidator;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Traits\log;

class AdminRolesController extends Controller
{
	use log;
    public $moduleName = 'roles';

	public function __construct()
	{
        //$this->middleware('auth');
	}

	public function index()
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$listing = Roles::sortable()->paginate(settingParam('record-per-page'));

		return view('admin.roles_list',compact('listing'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function add()
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = [
			'role_name' => 'required|unique:roles',
			'role_name_en' => 'required|unique:roles',
		];

		$jsValidator = JsValidator::make($validator);
		return view('admin.roles_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function store(Request $request){

            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'role_name' => 'required|unique:roles',
			'role_name_en' => 'required|unique:roles',
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{

			$roles = new Roles;
			$roles->role_name = $request->role_name;
			$roles->role_name_en = $request->role_name_en;
			$roles->created_at = Carbon::now();
			
			if($roles->save()){
				$desc = "the user id ".Auth::user()->id." that was created roles";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created roles',$desc,'roles_admin','',json_encode($request->all()),'');

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Roles::find($id);

		$validator = [
			'role_name' => 'required|unique:roles,role_name,'.$id,
			'role_name_en' => 'required|unique:roles,role_name_en,'.$id,
		];

		$jsValidator = JsValidator::make($validator);
		return view('admin.roles_edit',compact('jsValidator','data'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function update(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'role_name' => 'required|unique:roles,role_name,'.$request->id,
			'role_name_en' => 'required|unique:roles,role_name_en,'.$request->id,
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$roles = Roles::find($request->id);
			$oldData = json_encode($roles);
			$roles->role_name = $request->role_name;
			$roles->role_name_en = $request->role_name_en;
			$roles->updated_at = Carbon::now();
			
			if($roles->save()){
				$desc = "the user id ".Auth::user()->id." that was updated roles";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'roles settings',$desc,'roles_admin','',$oldData,json_encode($request->all()));

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}


	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Roles::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			$desc = "the user id ".Auth::user()->id." that was deleted roles";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted roles',$desc,'roles_admin','',$oldData,json_encode($id));
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
}
