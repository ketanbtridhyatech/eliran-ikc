<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Country;
use JsValidator;
use Validator;
use Auth;
use App\DogsDB;
use App\Dogs2Users;
use App\BreedsDB;
use App\UsersSkills;
use App\User;
use Session;
use Carbon\Carbon;
use App\Traits\log;
use App\Exports\BreedExport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;
class BackendBreedController extends Controller
{

  use log;
  public $moduleName = "breeds";

  public function __construct()
  {
        $this->middleware('auth');
  }
  public function get_breeds_list(Request $request){

  // echo "here";

  	$dataQuery = new  BreedsDB();
  	if ($request->has('search_by_breed_name') && $request->search_by_breed_name != '') {

     $dataQuery = $dataQuery->where(function ($q)  use ($request) {
            $q->where('BreedName', 'like', '%' . $request->search_by_breed_name . '%')
                ->orWhere('BreedNameEN', 'like', '%' . $request->search_by_breed_name . '%');
        });
    }
    if ($request->has('search_by_breed_code') && $request->search_by_breed_code != '') {

      $dataQuery = $dataQuery->where('BreedCode', $request->search_by_breed_code);
    }


   	if ($request->submit_type == 'export_csv') {
                return Excel::download(new BreedExport($dataQuery->sortable()->get()), 'breeds.csv');
    }
    $listing = $dataQuery->sortable()->paginate();
   //	echo "<pre>";print_r($request->all());exit;
 	$userSkill = UsersSkills::where('user_id',Auth::user()->id)->where('skill_id','30')->first();
    return view('admin.get_breeds_list',compact('listing','request','userSkill'));
   
  }
  function validationRules(){
		$validator = [
			'breed_name' => 'required',
			'breed_name_en' => 'required',
			'breed_code' => 'required',
			'breed_desc' => 'required',
			'group_id' => 'required',
			'FCICODE' => 'required',
			'fci_group' => 'required',
			'breed_status' => 'required',
		];

		return $validator;
	}
  public function add(){
  	if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = $this->validationRules();
		$jsValidator = JsValidator::make($validator);
		return view('admin.breeds_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
  }

  public function store(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{
			$breedsdb = new BreedsDB;
			$breedsdb->BreedName = $request->breed_name;
			$breedsdb->BreedNameEN = $request->breed_name_en;
			$breedsdb->BreedCode = $request->breed_code;
			$breedsdb->Desc = $request->breed_desc;
			$breedsdb->GroupID = $request->group_id;
			$breedsdb->FCICODE = $request->FCICODE;
			$breedsdb->fci_group = $request->fci_group;
			$breedsdb->status = $request->breed_status;
			$breedsdb->created_at = Carbon::now();
			
			if($breedsdb->save()){
				$desc = "the user id ".Auth::user()->id." that was created breed";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created breed',$desc,'breeds_admin','',json_encode($request->all()),'');
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
	}else{
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
    }
  }

  public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = BreedsDB::find($id);

		$validator = $this->validationRules();

		$jsValidator = JsValidator::make($validator);
		
		return view('admin.breeds_edit',compact('jsValidator','data'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

 public function update(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$breedsdb = BreedsDB::find($request->id);
			$oldData = json_encode($breedsdb);
			$breedsdb->BreedName = $request->breed_name;
			$breedsdb->BreedNameEN = $request->breed_name_en;
			$breedsdb->BreedCode = $request->breed_code;
			$breedsdb->Desc = $request->breed_desc;
			$breedsdb->GroupID = $request->group_id;
			$breedsdb->FCICODE = $request->FCICODE;
			$breedsdb->fci_group = $request->fci_group;
			$breedsdb->status = $request->breed_status;
			$breedsdb->updated_at = Carbon::now();
			
			if($breedsdb->save()){

				$desc = "the user id ".Auth::user()->id." that was updated breed";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated breed',$desc,'breeds_admin','',$oldData,json_encode($request->all()));

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = BreedsDB::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			
			$desc = "the user id ".Auth::user()->id." that was deleted breed";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted breed',$desc,'breed_admin','',$oldData,json_encode($id));
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	public function export_csv(Request $request){
		echo "<pre>";print_r($request->all());exit;
	}
  

  
}
