<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Country;
use JsValidator;
use Validator;
use Auth;
use App\DogsDB;
use App\Dogs2Users;
use App\BreedsDB;
use App\UsersSkills;
use App\Breedinghouses;
use App\User;
use App\ColorsDB;
use Session;
use Carbon\Carbon;
use App\Traits\log;
use App\Exports\ColorsExport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;
class AdminColorController extends Controller
{

  use log;
  public $moduleName = "colors";

  public function __construct()
  {
        $this->middleware('auth');
  }
  public function get_color_list(Request $request){

  // echo "here";

  	$dataQuery = new  ColorsDB();
  	if ($request->has('search_by_colors_name') && $request->search_by_colors_name != '') {

     $dataQuery = $dataQuery->where(function ($q)  use ($request) {
            $q->where('ColorNameHE', 'like', '%' . $request->search_by_colors_name . '%')
                ->orWhere('ColorNameEN', 'like', '%' . $request->search_by_colors_name . '%');
        });
    }
    


   	if ($request->submit_type == 'export_csv') {
                return Excel::download(new ColorsExport($dataQuery->sortable()->get()), 'colors.csv');
              /*  return (new BreedinghousesExport($dataQuery->sortable()->get()))->download('breedinghouses.csv');*/
    }
    $listing = $dataQuery->sortable()->paginate();
   //	echo "<pre>";print_r($listing);exit;
 	$userSkill = UsersSkills::where('user_id',Auth::user()->id)->where('skill_id','30')->first();
    return view('admin.color_list',compact('listing','request','userSkill'));
   
  }
  function validationRules(){
		$validator = [
			'colors_HebName' => 'required',
			'colors_EngName' => 'required',
			'colors_OldCode' => 'required',
			'colors_status' => 'required',
			/*'breedinghouses_dataid' => 'required',*/

		];

		return $validator;
	}
  public function add(){
  	if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = $this->validationRules();
		$jsValidator = JsValidator::make($validator);
		return view('admin.color_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
  }

  public function store(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{
		//	echo "<pre>";print_r($request->all());exit;
			$ColorsDB = new ColorsDB;
			$ColorsDB->Remark = $request->colors_remark;
			$ColorsDB->ColorNameHE = $request->colors_HebName;
			$ColorsDB->ColorNameEN = $request->colors_EngName;
			$ColorsDB->OldCode = $request->colors_OldCode;
			$ColorsDB->status = $request->colors_status;

			
			$ColorsDB->created_at = Carbon::now();
			
			if($ColorsDB->save()){
				$desc = "the user id ".Auth::user()->id." that was created color";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created color',$desc,'color_admin','',json_encode($request->all()),'');
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
	}else{
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
    }
  }

  public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = ColorsDB::find($id);

		$validator = $this->validationRules();

		$jsValidator = JsValidator::make($validator);
		
		return view('admin.color_edit',compact('jsValidator','data'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

 public function update(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$ColorsDB = ColorsDB::find($request->id);
			$oldData = json_encode($ColorsDB);
			$ColorsDB->Remark = $request->colors_remark;
			$ColorsDB->ColorNameHE = $request->colors_HebName;
			$ColorsDB->ColorNameEN = $request->colors_EngName;
			$ColorsDB->OldCode = $request->colors_OldCode;
			$ColorsDB->status = $request->colors_status;
			$ColorsDB->updated_at = Carbon::now();
			
			if($ColorsDB->save()){

				$desc = "the user id ".Auth::user()->id." that was updated color";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated color',$desc,'color_admin','',$oldData,json_encode($request->all()));

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	} 

	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = ColorsDB::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			
			$desc = "the user id ".Auth::user()->id." that was deleted colot";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted color',$desc,'color_admin','',$oldData,json_encode($id));
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	
  

  
}
