<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules;
use JsValidator;
use Validator;
use Session;
use Auth;
use Carbon\Carbon;
use App\Traits\log;


class AdminModulesController extends Controller
{

	use log;
	public $moduleName = 'module';

	public function __construct()
	{
        //$this->middleware('auth');
	}

	public function index()
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$listing = Modules::sortable()->paginate(settingParam('record-per-page'));
		return view('admin.module_list',compact('listing'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function add()
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = [
			'name' => 'required|unique:modules',
		];

		$jsValidator = JsValidator::make($validator);
		return view('admin.module_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function store(Request $request){

            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'name' => 'required|unique:modules',
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{

			$modules = new Modules;
			$modules->name = $request->name;
			$modules->created_at = Carbon::now();
			
			if($modules->save()){
				$desc = "the user id ".Auth::user()->id." that was created modules";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created modules',$desc,'modules_admin','',json_encode($request->all()),'');
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
                $data = Modules::find($id);

                $validator = [
                        'name' => 'required|unique:modules,name,'.$id,
                ];

                $jsValidator = JsValidator::make($validator);
                return view('admin.module_edit',compact('jsValidator','data'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }	
	}

	public function update(Request $request){

            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'name' => 'required|unique:modules,name,'.$request->id,
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$modules = Modules::find($request->id);
			$oldData = json_encode($modules);
			$modules->name = $request->name;
			$modules->updated_at = Carbon::now();
			
			if($modules->save()){
				$desc = "the user id ".Auth::user()->id." that was updated modules";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated modules',$desc,'modules_admin','',$oldData,json_encode($request->all()));

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }

	}

	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Modules::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			$desc = "the user id ".Auth::user()->id." that was deleted modules";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted modules',$desc,'modules_admin','',$oldData,json_encode($id));
			$this->settingsDataRedis();
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
}
