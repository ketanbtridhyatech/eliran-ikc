<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\Request;
use App\Settings;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Redis;
use JsValidator;
use Validator;
use Auth;
use App\DynamicHtmlElement;
use App\DynamicHtmlElementOption;
use App\Traits\log;

class AdminHtmlElementController extends Controller
{
	use log;
	public $moduleName = 'admin_html_element';
	//
	public function __construct()
	{
		//$this->middleware('auth');
	}

	public function index(Request $request)
	{
		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$dataQuery = DynamicHtmlElement::sortable();

			//dd($dataQuery->toSql()); exit;

			$listing = $dataQuery->paginate(settingParam('record-per-page'));

			return view('admin.html_element_list', compact('listing', 'request'));
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}

	public function add()
	{
		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$validator = [
				'name' => 'required|unique:dynamic_html_element',
				'html_element' => 'required',
			];

			$jsValidator = JsValidator::make($validator);
			return view('admin.html_element_add', compact('jsValidator'));
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}

	public function store(Request $request)
	{

		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$validator = Validator::make($request->all(), [
				'name' => 'required|unique:dynamic_html_element',
				'html_element' => 'required',
			]);

			if ($validator->fails()) {
				return redirect(backUrl($this->moduleName . '/add'))
					->withErrors($validator)
					->withInput();
			} else {

				$html_element = new DynamicHtmlElement();
				$html_element->name = $request->name;
				$html_element->slug = $request->slug;
				$html_element->html_element = $request->html_element;

				if ($html_element->save()) {
					$desc = "the user id ".Auth::user()->id." that was created HtmlElement";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created HtmlElement',$desc,'htmlelement_admin','',json_encode($request->all()),'');

					toastr()->success('Data has been saved successfully!');
					return redirect(backUrl($this->moduleName));
				} else {
					toastr()->error('Technical Issue!');
					return redirect(backUrl($this->moduleName . '/add'))->withInput();
				}
			}
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}

	public function edit($id)
	{
		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$data = DynamicHtmlElement::find($id);

			$validator = [
				'name' => 'required|unique:dynamic_html_element,name,' . $id,
				'html_element' => 'required',
			];
			$jsValidator = JsValidator::make($validator);
			return view('admin.html_element_edit', compact('jsValidator', 'data'));
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}


	public function update(Request $request)
	{

		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$validator = Validator::make($request->all(), [
				'name' => 'required|unique:dynamic_html_element,name,' . $request->id,
				'html_element' => 'required',
			]);

			if ($validator->fails()) {
				return redirect(backUrl($this->moduleName . '/edit/' . $request->id))
					->withErrors($validator)
					->withInput();
			} else {

				$html_element = DynamicHtmlElement::find($request->id);
				$oldData = json_encode($html_element);
				$html_element->name = $request->name;
				$html_element->slug = $request->slug;
				$html_element->html_element = $request->html_element;

				if ($html_element->save()) {
					$desc = "the user id ".Auth::user()->id." that was updated HtmlElements";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated HtmlElements',$desc,'htmlelements_admin','',$oldData,json_encode($request->all()));

					toastr()->success('Data has been saved successfully!');
					return redirect(backUrl($this->moduleName));
				} else {
					toastr()->error('Technical Issue!');
					return redirect(backUrl($this->moduleName . '/edit/' . $request->id))->withInput();
				}
			}
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}

	public function delete($id)
	{
		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$data = DynamicHtmlElement::find($id);
			$oldData = json_encode($data);
			if ($data->delete()) {
				$desc = "the user id ".Auth::user()->id." that was deleted HtmlElements";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted HtmlElements',$desc,'htmlelements_admin','',$oldData,json_encode($id));
				toastr()->success('Data has been deleted successfully!');
				return redirect(backUrl($this->moduleName));
			} else {
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName));
			}
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}

	public function checkSlug(Request $request)
	{
		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$slug = str_slug($request->fieldTitle);

			$slug = SlugService::createSlug(DynamicHtmlElement::class, 'slug', $request->fieldTitle);

			return response()->json(['slug' => $slug]);
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}
	public function get_html_element_value(Request $request,$id)
	{
		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$dataQuery = DynamicHtmlElementOption::sortable();

			//dd($dataQuery->toSql()); exit;
			$dataQuery->where('html_element_id',$id);
			$listing = $dataQuery->paginate(settingParam('record-per-page'));
			return view('admin.html_element_value_list', compact('listing', 'request','id'));
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}

	public function html_element_value_add($element_id)
	{
		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$validator = [
				'option_value'=>'required',
				'option_text_english' => 'required',
				'option_text_hebrew' => 'required',
			];

			$jsValidator = JsValidator::make($validator);
			return view('admin.html_element_value_add', compact('jsValidator','element_id'));
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}

	public function html_element_value_store(Request $request)
	{

		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$validator = Validator::make($request->all(), [
				'option_value'=>'required',
				'option_text_english' => 'required',
				'option_text_hebrew' => 'required',
			]);

			if ($validator->fails()) {
				return redirect(backUrl($this->moduleName . '/add'))
					->withErrors($validator)
					->withInput();
			} else {

				$html_element = new DynamicHtmlElementOption();
				$html_element->html_element_id =  $request->html_element_id;
				$html_element->option_value = $request->option_value;
				$html_element->option_text_english = $request->option_text_english;
				$html_element->option_text_hebrew = $request->option_text_hebrew;

				if ($html_element->save()) {
					
					$desc = "the user id ".Auth::user()->id." that was created html element value";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created html element value',$desc,'HtmlElement_admin','',json_encode($request->all()),'');

					toastr()->success('Data has been saved successfully!');
					return redirect(backUrl($this->moduleName."/admin_html_element_option/".$request->html_element_id));
				} else {
					toastr()->error('Technical Issue!');
					return redirect(backUrl($this->moduleName . 'admin_html_element_option/add/'.$request->html_element_id))->withInput();
				}
			}
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}

	public function html_element_value_edit($element_id,$id)
	{
		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$data = DynamicHtmlElementOption::find($id);

			$validator = [
				'option_value'=>'required',
				'option_text_english' => 'required',
				'option_text_hebrew' => 'required',
			];
			$jsValidator = JsValidator::make($validator);
			return view('admin.html_element_value_edit', compact('jsValidator', 'data','element_id','id'));
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}


	public function html_element_value_update(Request $request)
	{

		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$validator = Validator::make($request->all(), [
				'option_value'=>'required',
				'option_text_english' => 'required',
				'option_text_hebrew' => 'required',
			]);

			if ($validator->fails()) {
				return redirect(backUrl($this->moduleName."/admin_html_element_option/edit/".$request->element_id .'/'. $request->id))
					->withErrors($validator)
					->withInput();
			} else {

				$html_element = DynamicHtmlElementOption::find($request->id);
				$oldData = json_encode($html_element);
				$html_element->option_value = $request->option_value;
				$html_element->option_text_english = $request->option_text_english;
				$html_element->option_text_hebrew = $request->option_text_hebrew;

				if ($html_element->save()) {
					$desc = "the user id ".Auth::user()->id." that was updated html element value";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated html element value',$desc,'HtmlElement_admin','',$oldData,json_encode($request->all()));

					toastr()->success('Data has been saved successfully!');
					return redirect(backUrl($this->moduleName."/admin_html_element_option/".$request->element_id));
				} else {
					toastr()->error('Technical Issue!');
					return redirect(backUrl($this->moduleName . '/edit/' . $request->id))->withInput();
				}
			}
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}

	public function html_element_value_delete($element_id,$id)
	{
		if (Auth::check() && Auth::user()->is_superadmin == 1) {
			$data = DynamicHtmlElementOption::find($id);
			$oldData = json_encode($data);
			if ($data->delete()) {
				$desc = "the user id ".Auth::user()->id." that was deleted html element value";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted html element value',$desc,'HtmlElement_admin','',$oldData,json_encode($id));
				toastr()->success('Data has been deleted successfully!');
				return redirect(backUrl($this->moduleName."/admin_html_element_option/".$element_id));
			} else {
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName."/admin_html_element_option/".$element_id));
			}
		} else {
			toastr()->error('You do not have permission to perform this action!');
			return redirect(frontUrl('error'));
		}
	}


	/* Store Data in Redis For SettingParams */

	
}
