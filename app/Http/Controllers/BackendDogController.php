<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Country;
use JsValidator;
use Validator;
use Auth;
use App\DogsDB;
use App\Dogs2Users;
use App\BreedsDB;
use App\User;
use Session;
use Carbon\Carbon;
use App\Traits\log;

class BackendDogController extends Controller
{

  use log;
  //public $moduleName = "country";

  public function __construct()
  {
        $this->middleware('auth');
  }
  public function get_dog_list(Request $request){
//DB::enableQueryLog();
     if ($request->has('search_by_owner_name') && $request->search_by_owner_name != '') {
        $userdata = User::where('first_name', 'like', '%' .$request->search_by_owner_name . '%')->orWhere('first_name_en', 'like', '%' .$request->search_by_owner_name . '%')->pluck('id');
        $userdogs = Dogs2Users::whereIn('user_id',$userdata)->where('status','current')->pluck('sagir_id');
         $dataQuery = DogsDB::with(['owner_name'])->whereIn('SagirID',$userdogs);
      }else{
         $dataQuery = DogsDB::with(['owner_name']);
      }
    if ($request->has('search_by_dog_name') && $request->search_by_dog_name != '') {

        $dataQuery->where(function ($q)  use ($request) {
            $q->where('Heb_Name', 'like', '%' . $request->search_by_dog_name . '%')
                ->orWhere('Eng_Name', 'like', '%' . $request->search_by_dog_name . '%');
        });
    }
    if ($request->has('search_by_chip') && $request->search_by_chip != '') {

      $dataQuery->where('Chip', 'like', '%' . $request->search_by_chip . '%');
    }
    if ($request->has('search_by_gender') && $request->search_by_gender != '') {

      $dataQuery->where('GenderID', $request->search_by_gender);
    }
    if ($request->has('search_by_sagirId') && $request->search_by_sagirId != '') {

      $dataQuery->where('SagirID', 'like', '%' . $request->search_by_sagirId . '%');
    }
    if ($request->has('search_by_breed_name') && $request->search_by_breed_name != '') {

      $dataQuery->where('RaceID', $request->search_by_breed_name);
    }
    if($request->has('search_by_birth_date') && $request->search_by_birth_date != ''){
      $value = (explode("-",$request->search_by_birth_date));
      $startDate = date("Y-m-d", strtotime(str_replace('/','-',$value[0])));
      $endDate = date("Y-m-d", strtotime(str_replace('/','-',$value[1])));
      $dataQuery->whereBetween('BirthDate', [$startDate, $endDate]);
    }
   
    $listing =  $dataQuery->sortable()->paginate(500);
   $breedsdb = BreedsDB::where('status','For Use')->get();
   //dd(DB::getQueryLog());
   //echo "<pre>";print_r($request->all());exit;
    return view('admin.get_dog_list',compact('listing','request','breedsdb'));
   
  }

  

  

 

  

  
}
