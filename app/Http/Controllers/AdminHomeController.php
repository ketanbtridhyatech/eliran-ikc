<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Config;
use Helper;
use Hash;

class AdminHomeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        if(Auth::check() && Auth::user()->is_superadmin == 1){
            return view('admin.index');
        }else{
                toastr()->error('You do not have permission to perform this action!');
                return redirect(frontUrl('error'));
        }
    }
}
