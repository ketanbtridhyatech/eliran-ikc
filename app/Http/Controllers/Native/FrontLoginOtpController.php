<?php

namespace App\Http\Controllers\Native;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use App\Country;
use JsValidator;
use Validator;
use Auth;
//use Illuminate\Support\Facades\Cookie;
use Session;
use Cookie;
use Twilio\Rest\Client;
use Illuminate\Http\Response;
class FrontLoginOtpController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('admin/logout');
        //$this->middleware('guest', ['except' => 'logout']);
    }


    public function showOtpLoginForm()
    {
        $countries = [];
        if (Auth::check()) {
            return redirect(frontUrl());
        }

        $countries = Country::all();

        return view('native.login_otp', compact('countries'));
    }

    public function checkUser(Request $request){
        $userId = User::where(['mobile_phone' => ltrim($request->mobile_phone, '0'), 'country_code' => $request->country_code,'record_type' => 'Native'])->first();
        if(empty($userId)){
            return response()->json(['success' => false, 'msg' => $request->User_invalid]);
        }
    }
    public function sendOTP(Request $request)
    {

        $userId = $this->checkMobileNumber($request->mobile_phone, $request->country_code);
        if ($userId == 0) {
            return response()->json(['success' => false, 'msg' => $request->Mobile_number_invalid]);
        }


        $otp = $this->generateOTP(ltrim($request->mobile_phone, '0'), $request->country_code);

        if (!empty($otp)) {
            if (isset($otp['code']) && $otp['code'] != "") {

                $userData = User::find($userId);
                $userData->otp = $otp['code'];
                $userData->save();

                return response()->json(['success' => true, 'msg' => $request->OTP_Successfully_sent]);
            } else {
                return response()->json(['success' => false, 'msg' => $otp['msg']]);
            }
        } else {
            return response()->json(['success' => false, 'msg' => translate('Technical issue!')]);
        }
    }

    public function checkMobileNumber($mobileNumber = "", $countryCode = "")
    {

        $userId = 0;
        $userMobile = User::where(['mobile_phone' => ltrim($mobileNumber, '0'), 'country_code' => $countryCode,'record_type'=>'Native'])->first();

        if ($userMobile) {
            $userId =  $userMobile->id;
        }

        return $userId;
    }

    public function generateOTP($mobileNumber = "", $countryCode = "")
    {
        $code = $msg = "";
        $mobileNumberWithCode = $countryCode . $mobileNumber;
        //$mobileNumber = '+919998842601';
        //$code = random_int(100000, 999999);
        if ($mobileNumberWithCode != '') {


            $client = new Client(settingParam('twillio-sid'), settingParam('twillio-token'));
            try {
                $code = random_int(100000, 999999);
                $msg = "";
                $client->messages->create(
                    $mobileNumberWithCode,
                    [
                        'from' => settingParam('twillio-registred-mobile'),
                        'body' => $code,
                    ]
                );
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                $code = "";
            }
        }

        return ['code' => $code, 'msg' => $msg];
    }


    /* OTP Login */
    public function otpLogin(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'mobile_phone' => 'required',
            'otp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'msg' => $validator->errors()]);
        } else {

            $user = User::where(['mobile_phone' => ltrim($request->mobile_phone, '0'), 'otp' => $request->otp, 'record_type' => 'Native'])->first();
            //$user = User::where(['mobile_phone' => $request->mobile_phone, 'record_type' => 'Native'])->first();
          
            if ($user) {
                Auth::login($user);
                $user->otp = null;
                $user->save();
                return response()->json(['success' => true, 'msg' => $request->Login_Successfully]);
            } else {
                return response()->json(['success' => false, 'msg' => $request->invalid_OTP]);
            }
        }
    }
    
}
