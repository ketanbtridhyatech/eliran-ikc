<?php

namespace App\Http\Controllers\Native;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Config;
use Helper;
use App\User;
use Hash;

class FrontHomeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

   
    public function changeLanguage(Request $request)
    {

            $userId = Auth::id();
            $userData = User::find($userId);

            $userData->language_id = $request->language_id;
            if ($userData->save()) {

                    $this->reply['status'] = true;
                    $this->reply['msg'] = 'Language updated successfully';
            } else {
                    $this->reply['status'] = false;
                    $this->reply['msg'] = 'Technical issue';
            }

            return response()->json($this->reply);
    }
}
