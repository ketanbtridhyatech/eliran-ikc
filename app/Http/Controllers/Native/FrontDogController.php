<?php

namespace App\Http\Controllers\Native;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Config;
use Helper;
use App\User;
use App\Dogs2Users;
use App\DogsDB;
use Hash;

class FrontDogController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');
    }
    public $moduleName = 'dog';

    public function getDogList(Request $request)
    {
        if (Auth::check() && Auth::user()->record_type == 'Native') {
            $userId = Auth::user()->id;
            $related_dogs = Dogs2Users::where('user_id', $userId)->where('status','current')->pluck('sagir_id');
            $breeddog = DogsDB::select('DogsDB.*', 'BreedsDB.BreedName')->join('BreedsDB', 'DogsDB.RaceID', 'BreedsDB.BreedCode')->whereIn("DogsDB.SagirID", $related_dogs)->groupBy('BreedsDB.BreedCode')->get();
            $dataQuery = DogsDB::with(['breed_name', 'breeding_name'=>function ($query) {
                $query->where('status', 0);
            }])->whereIn("SagirID", $related_dogs);


            if ($request->has('search_by_sagir_id') && $request->search_by_sagir_id != '') {
                $dataQuery->where('SagirID', 'like', '%' . $request->search_by_sagir_id . '%');
            }

            if ($request->has('search_by_dog_name') && $request->search_by_dog_name != '') {

                $dataQuery->where(function ($q)  use ($request) {
                    $q->where('Heb_Name', 'like', '%' . $request->search_by_dog_name . '%')
                        ->orWhere('Eng_Name', 'like', '%' . $request->search_by_dog_name . '%');
                });
            }


            if ($request->has('search_by_breed_name') && $request->search_by_breed_name != '') {

                $dataQuery->where('RaceID', $request->search_by_breed_name);
            }






            $dogs_details = $dataQuery->sortable()->paginate(settingParam('record-per-page'));
            //echo "<pre>";print_r($dogs_details->toArray());exit;
            return view('native.dog_list', compact('dogs_details', 'breeddog', 'request'));
        } else {
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
        }
    }
}
