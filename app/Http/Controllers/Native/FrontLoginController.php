<?php

namespace App\Http\Controllers\Native;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use JsValidator;
use Validator;
use Auth;
use Session;
class FrontLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    public function showLoginForm(){
        if (Auth::check()){
            return redirect(frontUrl());
        }
        $validator = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $jsValidator = JsValidator::make($validator);

        return view('native.login',compact('jsValidator'));
    }

    /* Login */
    public function doLogin(Request $request){


        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(frontUrl('login'))
            ->withErrors($validator)
            ->withInput();
        }else{

            $remember = ($request->remember) ? true : false;

            $credentials = [ 'email' => $request->email , 'password' => $request->password,'record_type' => 'Native' ];
            if(Auth::attempt($credentials,$remember)){
                return redirect(frontUrl('/'));
            }else{
                Session::flash('msg-class', 'alert-danger'); 
                Session::flash('message', 'Invalid credentials!'); 
                return redirect(frontUrl('login'))->withInput();
            }
        }


    }

    public function logOut(Request $request){
        if(isset($_COOKIE['sagirID']) && !empty($_COOKIE['sagirID'])){
            foreach ($_COOKIE['sagirID'] as $ckey => $cvalue) {
                    unset($_COOKIE['sagirID['.$ckey.']']);
                    setcookie('sagirID['.$ckey.']', '', time() - 3600, '/');
            }
        }
        Auth::logout();
        return redirect(frontUrl('login'));
    }




}
