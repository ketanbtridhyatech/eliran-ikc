<?php

namespace App\Http\Controllers\Native;

use App\Breed2Color;
use App\BreedClub;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Config;
use Helper;
use JsValidator;
use Validator;
use App\User;
use App\Dogs2Users;
use App\DogsDB;
use Hash;
use App\Roles;
use Carbon\Carbon;
use App\UsersSkills;
use App\Breeding;
use App\Breeds2clubs;
use App\Clubs;
use App\ColorsDB;
use App\Currency;
use App\Pricing;
use App\Settings;
use App\UserTasks;
use App\HairsDB;
use App\BreedingRelatedDog;
use App\BreedsDB;
use App\Country;
use App\TaskRelatedSkills;
use RealRashid\SweetAlert\Facades\Alert;
use File;
use Illuminate\Support\Facades\Storage;
use App\Traits\log;
use Twilio\Rest\Client;
use App\Templates;
use App\PuppieCards;


class BreedingController extends Controller
{
    use log;
    //public $breedingWarning = array();
    public function __construct()
    {
        $key = Settings::select('value')->where('slug','aws-access-key-id')->first();
        $secretKey = Settings::select('value')->where('slug','aws-secret-access-key')->first();
        $region = Settings::select('value')->where('slug','aws-default-region')->first();
        $bucket = Settings::select('value')->where('slug','aws-bucket')->first();
        $url = Settings::select('value')->where('slug','aws-url')->first();
        config(['filesystems.disks.s3.key' => $key['value'],'filesystems.disks.s3.secret' => $secretKey['value'],'filesystems.disks.s3.region' => $region['value'],'filesystems.disks.s3.bucket' => $bucket['value'],'filesystems.disks.s3.url' => $url['value']]);
        $this->middleware('auth');
    }
    public $moduleName = 'breeding';

    public function add(Request $request, $sagirId, $breedingWarning = null)
    {
        //echo \Route::currentRouteName();exit;

        if(\Route::currentRouteName() == 'breeding_done'){
            $breeding_data = Breeding::with('mother_detail')->find($sagirId);
            $sagirId = round($breeding_data->SagirId);
        }else{
            $breeding_data = Breeding::with('mother_detail')->where('SagirId', $sagirId)->where('status',0)->first();
        }
        //$femaleDogDetails = 
        $dogName = DogsDB::where('SagirID',$sagirId)->first();
        $text = translate('Active Breeding');
        //echo "<pre>";print_r($dogName);exit;
        if ($sagirId != 0) {
            $validationArray = [
                'breeding_date' => 'required',
                'maleId' => 'required|numeric',
            ];
            $jsValidator = JsValidator::make($validationArray);
           // $desc = "User go in breeding page";
            //$this->insertLog(Auth::user()->id,$sagirId, $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Add',$desc,'Breeding','',$sagirId,'');
            return view('native.breeding_add', compact('jsValidator', 'sagirId', 'breedingWarning', 'breeding_data','dogName'));
        } else {
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
        }
    }

    public function store(Request $request)
    {

        if (Auth::check()) {
           // echo "<pre>";print_r($request->all());exit;
            $validationArrayPassport = $jsValidator = [];
            $breedingWarning = array();
            $checkRegMsg =  $checkBreedMsg = $checkHistoryMsg = $checkFemaleStatusMsg =  $checkMaleStatusMsg = $checkForeginMaleMsg = '';
            $rules_IsOwner = $breedMismatch = 1;
            $male_DNA = $female_DNA = $maleBreedingNotApproved = $femaleBreedingNotApproved = $foregin_male = 0;
            $male_more_than_5 = $male_more_than_2 = 0;
            $getStartMonthOfSixteen = date("Y-m-d", strtotime("-16 month"));
            $getLastTwoYearDate = date("Y-m-d", strtotime("-2 year"));
            $getLastFiveYearDate = date("Y-m-d", strtotime("-5 year"));

            $checkRegMsg = 'שים לב, סגיר הנקבה אינו שייך למדווח ההרבעה';
            $checkBreedMsg = 'גזע סגיר הזכר וגזע סגיר הנקבה אינם תואמים';
            $checkHistoryMsg = 'תדירות ההמלטה של הנקבה חורגת מהמותר בתקנון הגידול, נקבה זו המליטה בתאריכים: ';
            $checkFemaleStatusMsg = 'סאגיר נקבה אינה מאושרת לגידול';
            $checkMaleStatusMsg = 'סאגיר נקבה אינה מאושרת לגידול';
            $checkForeginMaleMsg = 'זכר מחו"ל ללא פרטים בספר הגידול';
            $validationArray = [
                'breeding_date' => 'required',
                'maleId' => 'required|numeric',
            ];
            $validator = Validator::make($request->all(), $validationArray);
            $validator->getTranslator()->setLocale('ro');
            if ($validator->fails()) {
                return redirect(frontUrl('breeding/add/' . $request->hidden_femaleId))
                    ->withErrors($validator)
                    ->withInput();
            } else {

                if (!isset($request->submit)) {
                    if(isset($_COOKIE['sagirID'][$request->hidden_femaleId])){
                        $value = $_COOKIE['sagirID'][$request->hidden_femaleId];
                        if($value >= 3){
                            if(isset($_COOKIE['sagirID']) && !empty($_COOKIE['sagirID'])){
                                foreach ($_COOKIE['sagirID'] as $ckey => $cvalue) {
                                        unset($_COOKIE['sagirID['.$ckey.']']);
                                        setcookie('sagirID['.$ckey.']', '', time() - 3600, '/');
                                }
                            }

                            $text = translate('Too many tests for the same female dog, you are redirected to the login page');
                            $data = '';
                            $button_title_1 = translate('OK');
                                alert()->alert($text, $data, 'error')->showConfirmButton($button_title_1, '#3085d6')->autoClose(3000)->toHtml();
                            Auth::logout();
                            return redirect('/login');
                        }
                    }
                }
                 if (!isset($request->submit)) {
                    if(isset($_COOKIE['sagirID'][$request->hidden_femaleId])){
                        $value = $_COOKIE['sagirID'][$request->hidden_femaleId];
                        $value = $value + 1;
                     }else{
                        $value = 1;
                     }
                     setcookie('sagirID['.$request->hidden_femaleId.']', $value , time()+3600, '/');
                 }
               /* echo "<pre>";print_r($_COOKIE['sagirID'][$request->hidden_femaleId]);exit;
                exit;*/
                $checkExits = DogsDB::where('SagirID',$request->hidden_femaleId)->select('RaceID')->first();
                if(isset($checkExits) && !empty($checkExits)){
                }else{

                   if(currentLanguage() == 1){
                        $text = "סגיר הזכר אינו קיים במאגר";
                   }else{
                        $text = "We didnt find the male ID in our system";
                   }
                    $data = '';
                    $button_title_1 = translate('OK');
                    alert()->alert($text, $data, 'error')->showConfirmButton($button_title_1, '#3085d6')->autoClose(false)->toHtml();
                    return redirect(frontUrl('breeding/add/' . $request->hidden_femaleId))
                    ->withInput();
                }
                $checkFemaleMaleExits =  DogsDB::where('SagirID', $request->maleId)->where('GenderID','2')->select('RaceID')->first();
                if(isset($checkFemaleMaleExits) && !empty($checkFemaleMaleExits)){
                    $textalert = translate('Female and Female selected');
                    $data = '';
                    $button_title_1 = translate('OK');
                    alert()->alert($textalert, $data, 'error')->showConfirmButton($button_title_1, '#3085d6')->autoClose(false)->toHtml();
                    return redirect(frontUrl('breeding/add/' . $request->hidden_femaleId))
                    ->withInput();
                }
                $breedingWarning = array();
                $sagirId = $request->hidden_femaleId;

                //check Registered 
                $checkReg = Dogs2Users::where('user_id', Auth::user()->id)->where('sagir_id', $request->hidden_femaleId)->where('status','current')->first();
  
                $checkSkitll =  UsersSkills::where('user_id', Auth::user()->id)
                    ->pluck('skill_id')->toArray();
                if (empty($checkReg)) {
                    $breedingWarning['checkReg']  = $checkRegMsg;
                    $rules_IsOwner = 0;
                }
             

                //check Breed
                $checkFemaleBreedId = DogsDB::where('SagirID', $request->hidden_femaleId)->select('RaceID')->first();
                $checkMaleBreedId = DogsDB::where('SagirID', $request->maleId)->select('RaceID')->first();
              //  echo "<pre>";print_r($checkMaleBreedId);exit;
                if(empty($checkMaleBreedId)){
                     if (!isset($request->foreignMale)) {
                        
                           /*if(currentLanguage() == 1){
                                $text = "סגיר הזכר אינו קיים במאגר";
                           }else{
                                $text = "We didnt find the male ID in our system";
                           }*/
                            $text = '';
                            $dataalert = '';
                            $dataalert = "<ul class='alert alert-warning'>";
                            $dataalert .= '<li>' . translate('Dog Not Exits') . '</li>';
                            $dataalert .= '<li>' . translate('Valid For Today Date') . '</li>';
                            $dataalert .= "</ul>";
                            $button_title_1 = translate('OK');
                            alert()->alert($text, $dataalert, 'error')->showConfirmButton($button_title_1, '#3085d6')->autoClose(false)->toHtml();
                            return redirect(frontUrl('breeding/add/' . $request->hidden_femaleId))
                            ->withInput();
                           
                    }
                }




                if ($checkFemaleBreedId && $checkMaleBreedId) {
                    $breed_female = BreedsDB::where('BreedCode',$checkFemaleBreedId->RaceID)->first();
                    $breed_male = BreedsDB::where('BreedCode',$checkMaleBreedId->RaceID)->first();
                /*echo "<pre>";print_r($checkMaleBreedId);exit;
                echo "<pre>";print_r($checkFemaleBreedId);*/ 	
                        if(currentLanguage() == 1){
                            $newMessage = "גזע הזכר הוא ".$breed_male->BreedName." וגזע הנקבה הוא ".$breed_female->BreedName." האם אתה בטוח שצריכים להתקדם?";
                        }else{
                            $newMessage = "Male Breed is ".$breed_male->BreedNameEN." and female breed is ".$breed_female->BreedNameEN.", are you sure you want to continue?";
                        }
                    if ($checkFemaleBreedId != $checkMaleBreedId) {
                        //echo "not equal";exit;
                        $breedingWarning['checkBreed']  = $newMessage;
                        $breedMismatch = 1;
                    }else
                    {
                       // echo "equal";exit;
                       // $breedingWarning['checkBreed']  = $newMessage;
                        $breedMismatch = 0;

                    }
                } else {
                    $breedingWarning['checkBreed']  = $checkBreedMsg;
                    $breedMismatch = 1;
                }

                //check history
                $totalHistoryCount = $newDataHistory = $oldDataHistory =   0;
                $historyDate = array();
                $historyCheckInNew = Breeding::where('created_at', '>=', $getStartMonthOfSixteen)
                    ->where('SagirId', $request->hidden_femaleId)
                    ->pluck('created_at')->toArray();

                $historyCheckInOld = DB::table('idune_508_raurotoddqetdjphslhr_Users_Tables_229251')
                    ->where('CreationDateTime', '>=', $getStartMonthOfSixteen)
                    ->where('fSagirID', $request->hidden_femaleId)
                    ->pluck('CreationDateTime')->toArray();

                if (count($historyCheckInNew) > 0) {
                    $newDataHistory = count($historyCheckInNew);
                    foreach ($historyCheckInNew as $val) {
                        $historyDate[] = date("Y-m-d", strtotime($val));
                    }
                }
                if (count($historyCheckInOld) > 0) {
                    $oldDataHistory = count($historyCheckInNew);
                    foreach ($historyCheckInOld as $val) {
                        $historyDate[] = date("Y-m-d", strtotime($val));
                    }
                }
                $totalHistoryCount = $newDataHistory + $oldDataHistory;
                $history_note = '';
                if ($totalHistoryCount > 1) {
                    $breedingWarning['checkHistory']  = $checkHistoryMsg.' '.implode(',',array_unique($historyDate)); 
                    $history_note = $checkHistoryMsg . ' ' . implode(',', array_unique($historyDate));
                }

                //check Male breeding data in last 2 & 5 year
                $breeding_date_note_2 = '';
                $breeding_date_note_5 = '';
                
                if (!isset($request->foreignMale)) {
                $totalLastTwoYearBreedingData =  $totalOfLastTwoYearOfSagirId = $totalOfLastFiveYearOfSagirId =
                    $totalLastFiveYearBreedingData = $lastTwoYearPerc = $lastFiveYearPerc = 0;

                $getTotalLastTwoYearBreedingInNew = Breeding::where('BreddingDate', '>=', $getLastTwoYearDate)->whereIn('status',array(0,3))->get()->count();
                $getTotalLastTwoYearBreedingInOld = DB::table('idune_508_raurotoddqetdjphslhr_Users_Tables_229251')
                    ->where('MateDate', '>=', $getLastTwoYearDate)->where('BreedID',$checkMaleBreedId->RaceID)->get()->count();
                $totalLastTwoYearBreedingData = $getTotalLastTwoYearBreedingInNew + $getTotalLastTwoYearBreedingInOld;

                $getTotalLastFiveYearBreedingInNew = Breeding::where('BreddingDate', '>=', $getLastFiveYearDate)->whereIn('status',array(0,3))->get()->count();
                $getTotalLastFiveYearBreedingInOld = DB::table('idune_508_raurotoddqetdjphslhr_Users_Tables_229251')
                    ->where('MateDate', '>=', $getLastFiveYearDate)->where('BreedID',$checkMaleBreedId->RaceID)->get()->count();
                $totalLastFiveYearBreedingData = $getTotalLastFiveYearBreedingInNew + $getTotalLastFiveYearBreedingInOld;

                $checkTwoYearMaleBreedingInNew = Breeding::where('BreddingDate', '>=', $getLastTwoYearDate)->whereIn('status',array(0,3))
                    ->where('MaleSagirId', $request->maleId)
                    ->get()->count();
                $checkTwoYearMaleBreedingInOld = DB::table('idune_508_raurotoddqetdjphslhr_Users_Tables_229251')
                    ->where('MateDate', '>=', $getLastTwoYearDate)
                    ->where('MSagirID', $request->maleId)
                    ->where('BreedID',$checkMaleBreedId->RaceID)
                    ->get()->count();
                $totalOfLastTwoYearOfSagirId = $checkTwoYearMaleBreedingInNew + $checkTwoYearMaleBreedingInOld;
                if ($totalOfLastTwoYearOfSagirId != 0 && $totalLastTwoYearBreedingData != 0) {
                    $lastTwoYearPerc = ($totalOfLastTwoYearOfSagirId * 100) / $totalLastTwoYearBreedingData;
                }

                $checkFiveYearMaleBreedingInNew = Breeding::where('BreddingDate', '>=', $getLastFiveYearDate)->whereIn('status',array(0,3))
                    ->where('MaleSagirId', $request->maleId)
                    ->get()->count();
                $checkFiveYearMaleBreedingInOld = DB::table('idune_508_raurotoddqetdjphslhr_Users_Tables_229251')
                    ->where('MateDate', '>=', $getLastFiveYearDate)
                    ->where('MSagirID', $request->maleId)
                    ->where('BreedID',$checkMaleBreedId->RaceID)
                    ->get()->count();
                $totalOfLastFiveYearOfSagirId = $checkFiveYearMaleBreedingInNew + $checkFiveYearMaleBreedingInOld;
                if ($totalOfLastFiveYearOfSagirId != 0 && $totalLastFiveYearBreedingData != 0) {
                    $lastFiveYearPerc = ($totalOfLastFiveYearOfSagirId * 100) / $totalLastFiveYearBreedingData;
                }
                if (round($lastTwoYearPerc, 2) > 5) {
                    $breedingWarning['checkTwoYearData']  = 'The Breeding male was found in {'.round($lastTwoYearPerc,2).'%} in the last 2 years out of '.$totalLastTwoYearBreedingData.' total breeding';
                    $breeding_date_note_2 = 'The Breeding male was found in {' . round($lastTwoYearPerc, 2) . '%} in the last 2 years out of ' . $totalLastTwoYearBreedingData . ' total breeding';
                    $male_more_than_2  = 1;
                }
                if (round($lastFiveYearPerc, 2) > 10) {
                    $breedingWarning['checkFiveYearData']  = 'The Breeding male was found in {'.round($lastFiveYearPerc,2).'%} in the last 5 years out of '.$totalLastFiveYearBreedingData.' total breeding';
                    $breeding_date_note_5 = 'The Breeding male was found in {' . round($lastFiveYearPerc, 2) . '%} in the last 5 years out of ' . $totalLastFiveYearBreedingData . ' total breeding';
                    $male_more_than_5 = 1;
                }
            }
                //check DNA
                $checkFemaleDna = DogsDB::where('SagirID', $request->hidden_femaleId)->select('DnaID')->first();
                $checkMaleDna = DogsDB::where('SagirID', $request->maleId)->select('DnaID')->first();

                if ($checkFemaleDna) {
                    if (!empty($checkFemaleDna->DnaID)) {
                        $female_DNA = 1;
                    }
                }
                if ($checkMaleDna) {
                    if (!empty($checkMaleDna->DnaID)) {
                        $male_DNA = 1;
                    }
                }

                //check generation
                $generations_note = array();
                $femaleGenerationData = array();
                $femaleGenerationData =  $this->getGenerationData($request->hidden_femaleId);
                $maleGenerationData = $this->getGenerationData($request->maleId);
                $repeat_array = array();
                if (!empty($femaleGenerationData) && !empty($maleGenerationData)) {
                    foreach ($maleGenerationData as $key => $m) {
                        $array_male = explode(',', $m);
                        for ($i = 0; $i < count($array_male); $i++) {
                            $repeat = array();
                            foreach ($femaleGenerationData as $key1 => $f) {
                                $array_female = explode(',', $f);

                                if (in_array($array_male[$i], $array_female)) {
                                    $repeat[] = $key1;
                                }
                            }
                            if ($array_male[$i] != '' && !empty($repeat)) {
                                if(round($array_male[$i]) != '0' || round($array_male[$i]) != 0){
                                    $breedingWarning['repeat_'.$array_male[$i]]="Sagir ".round($array_male[$i])." in generation ".implode(',',$repeat)." of the female and in generation ".$key ." of the male.";
                                    $generations_note[] = "Sagir " . round($array_male[$i]) . " in generation " . implode(',', $repeat) . " of the female and in generation " . $key . " of the male.";
                                 }
                            }
                        }
                    }
                }
                

                //check Status
                $checkFemaleStatus = DogsDB::where('SagirID', $request->hidden_femaleId)->select('Status')->first();
                $checkMaleStatus = DogsDB::where('SagirID', $request->maleId)->select('Status')->first();

                if ($checkFemaleStatus) {
                    if ($checkFemaleStatus->Status == 'notapproved') {
                        $femaleBreedingNotApproved = 1;
                        $breedingWarning['checkFemaleStatus']  = $checkFemaleStatusMsg;
                    }
                }
                if ($checkMaleStatus) {
                    if ($checkMaleStatus->Status == 'notapproved') {
                        $maleBreedingNotApproved = 1;
                        $breedingWarning['checkMaleStatus']  = $checkMaleStatusMsg;
                    }
                }

                //Check Foregin Male
                if (isset($request->foreignMale)) {
                    if ($request->foreignMale == 1) {
                        $checkForeignMale  = DogsDB::where('SagirID', $request->maleId)->first();
                        if (!$checkForeignMale) {
                            $breedingWarning['checkForeginStatus']  = $checkForeginMaleMsg;
                        } else {
                            $foregin_male = 1;
                        }
                    }
                }
                $checkFemaleBreedId = DogsDB::where('SagirID', $request->hidden_femaleId)->select('*')->first();

                $date1 = strtotime($checkFemaleBreedId->BirthDate);
                $date2 = strtotime(date("Y-m-d"));
                $diff = abs($date2 - $date1); 
                $years = floor($diff / (365*60*60*24)); 
                $months = floor(($diff - $years * 365*60*60*24) 
                               / (30*60*60*24));
                $days = floor(($diff - $years * 365*60*60*24 -  
                        $months*30*60*60*24)/ (60*60*24)); 
               /* echo "<pre>";print_r($years);
                echo "<pre>";print_r($months);
*/
                if($years != 0){
                    $totalmonth = ($years * 12);
                }else{
                    $totalmonth = 0;
                }
                $totalmonth = $totalmonth + $months;
                if($totalmonth >= 96){
                    $yearflag = 'no';
                    $monthflag = 'yes';
                }else{
                   $yearflag  = 'yes';
                    if($totalmonth > 18){
                        $monthflag = 'yes';
                    }else{
                        if($totalmonth == 18 && $days >=1){
                            $monthflag = 'yes';
                        }else{
                            $monthflag = 'no';
                        }
                    }
                }
                if($totalmonth >= 18 && $totalmonth <= 96){
                }else{
                     if(currentLanguage() == 1){
                        $breedingWarning['birthdayStatus']  = "גיל הנקבה אינו מעל 18 חודש ומתחת לגיל 8 שנים";
                    }else{
                        $breedingWarning['birthdayStatus']  = "female age is NOT under 8 years and above 18 months";
                    }
                }
                
                $check_breeding = Breeding::where('SagirId', $request->hidden_femaleId)->where('status',0)->first();

                if (empty($check_breeding) && isset($request->submit)) {
                    $breedingObj = new Breeding;
                    $breedingObj->SagirId = $request->hidden_femaleId;
                    $breedingObj->BreddingDate = date('Y-m-d', strtotime(str_replace('/', '-', $request->breeding_date)));
                    $breedingObj->MaleSagirId = $request->maleId;
                    $breedingObj->Rules_IsOwner = $rules_IsOwner;
                    $breedingObj->BreedMismatch = $breedMismatch;
                    $breedingObj->Male_DNA = $male_DNA;
                    $breedingObj->Female_DNA =  $female_DNA;
                    $breedingObj->Female_Breeding_Not_Approved =  $femaleBreedingNotApproved;
                    $breedingObj->Male_Breeding_Not_Approved =  $maleBreedingNotApproved;
                    $breedingObj->Foreign_Male_Records =  $foregin_male;
                    $breedingObj->Male_More_Than_5 =  $male_more_than_5;
                    $breedingObj->Male_More_Than_2 =  $male_more_than_2;
                    $breedingObj->female_rate =  $history_note;
                    $breedingObj->male_rebreed_5  =  $breeding_date_note_5;
                    $breedingObj->male_rebreed_2  =  $breeding_date_note_2;
                    $breedingObj->generations_note =  implode(', ', $generations_note);
                    $breedingObj->filled_step = 1;
                    $breedingObj->more_than_18_months = $monthflag;
                    $breedingObj->less_than_8_years = $yearflag;
                    $breedingObj->created_at =  Carbon::now();
                    $breedingObj->created_by = Auth::user()->id;
                    $breedingObj->save();
                    $title = translate('The details of the stud were successfully saved');
                    if(isset($_COOKIE['sagirID'][$request->hidden_femaleId])){
                        if(isset($_COOKIE['sagirID']) && !empty($_COOKIE['sagirID'])){
                            foreach ($_COOKIE['sagirID'] as $ckey => $cvalue) {
                                    if($ckey == $request->hidden_femaleId){
                                        unset($_COOKIE['sagirID['.$ckey.']']);
                                        setcookie('sagirID['.$ckey.']', '', time() - 3600,'/');
                                    }
                                    
                            }
                        }
                    }
                }else{
                     $title = translate('Test results');
                     
                }
                if (isset($request->submit)) {
                    toastr()->success('Data has been saved successfully!');
                }
                $request->session()->push('breedingWarning', $breedingWarning);
               
               // $button_title = translate('All right, take me to my escape stand');
                $button_title_1 = translate('OK for breeding');

                $data = '';
                if (count($breedingWarning) > 0) {
                    $data = "<ul class='alert alert-warning'>";
                    if (session()->has('breedingWarning')) {
                        $warnings = session()->pull('breedingWarning')[0];
                    }

                  
                    if (count($breedingWarning) > 0) {
                        foreach ($warnings as $val) {
                            $data .= '<li>' . $val . '</li>';
                        }
                    }
                    $data .= "</ul>";
                }
                if (!isset($request->submit) && count($breedingWarning) <= 0) {
                     $data = "<ul class='alert alert-warning'>";
                     $data .= '<li>' . translate('all is ok') . '</li>';
                     $data .= "</ul>";    
                }

                alert()->alert($title, $data, 'success')->showConfirmButton($button_title_1, '#3085d6')->width('48rem')->autoClose(false)->toHtml();
                $desc = "Store Breeding details in table";
                $this->insertLog(Auth::user()->id,$request->hidden_femaleId, $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Store in table',$desc,'Breeding','',json_encode($request->all()),'');
                return redirect('breeding/add/' . $sagirId)->withInput();
            }
        } else {
            toastr()->error('Technical Issue!');
            return redirect(frontUrl('breeding/add/' . $request->hidden_femaleId))->withInput();
        }
    }


    public function getGenerationData($sagirId)
    {
      
        $generation_array = array();
        $generation_array[1] = $sagirId;
        for ($f = 2; $f <= 5; $f++) {
            if (!empty($sagirId)) {
                if ($f == 1) {
                    $result = DogsDB::where('SagirID', $sagirId)->select('FatherSAGIR', 'MotherSAGIR')->first();
                   
                    //$generation_array[$f]=$femaleGenerationData[$female_id];
                    $generation_array[$f] = !empty($result) ? $result->FatherSAGIR . ',' . $result->MotherSAGIR : "";
                } else if ($f == 2) {
                    if (!empty($generation_array[$f - 1])) {
                        $explod = explode(',', $generation_array[$f - 1]);
                        if (!empty($explod)) {
                            $parent = array();
                            for ($i = 0; $i < count($explod); $i++) {
                                $result = DogsDB::where('SagirID', $explod[$i])->select('FatherSAGIR', 'MotherSAGIR')->first();

                                $parent[] = !empty($result) ? $result->FatherSAGIR . ',' . $result->MotherSAGIR : "";
                            }
                            $generation_array[$f] = implode(',', $parent);
                        }
                    }
                } else if ($f == 3) {
                    if (!empty($generation_array[$f - 1])) {
                        $explod = explode(',', $generation_array[$f - 1]);
                        if (!empty($explod)) {
                            $parent = array();
                            for ($i = 0; $i < count($explod); $i++) {
                                $result = DogsDB::where('SagirID', $explod[$i])->select('FatherSAGIR', 'MotherSAGIR')->first();
                                $parent[] = !empty($result) ? $result->FatherSAGIR . ',' . $result->MotherSAGIR : "";
                            }
                            $generation_array[$f] = implode(',', $parent);
                        }
                    }
                } else if ($f == 4) {
                    if (!empty($generation_array[$f - 1])) {
                        $explod = explode(',', $generation_array[$f - 1]);
                        if (!empty($explod)) {
                            $parent = array();
                            for ($i = 0; $i < count($explod); $i++) {
                                $result = DogsDB::where('SagirID', $explod[$i])->select('FatherSAGIR', 'MotherSAGIR')->first();
                                $parent[] = !empty($result) ? $result->FatherSAGIR . ',' . $result->MotherSAGIR : "";
                            }
                            $generation_array[$f] = implode(',', $parent);
                        }
                    }
                }
            }
        }

        return $generation_array;
    }
    public function add_birthing(Request $request, $sagirId)
    {

        if(\Route::currentRouteName() == 'birthing_done'){
            $breeding_data = Breeding::with('mother_detail')->find($sagirId);
            $sagirId = round($breeding_data->SagirId);
        }else{
            $breeding_data = Breeding::with('mother_detail')->where('SagirId', $sagirId)->where('status',0)->first();
            if(isset($breeding_data) && $breeding_data->status == 1){
                return redirect('breeding/add/' . $sagirId);
            }
        }
        //$breeding_data = Breeding::where('SagirId', $sagirId)->where('status',0)->first();
        
        $dogs_data = DogsDB::where('SagirID', $sagirId)->first();
        /* $femaleDogData = $dogs_data;*/
        if(isset($breeding_data->MaleSagirId) && !empty($breeding_data->MaleSagirId)){
            $maleDogData = DogsDB::where('SagirId', $breeding_data->MaleSagirId)->first();
        }else{
            $maleDogData = array();
        }
        $udBreedPromoter = array();
        if (!empty($dogs_data)) {
            $breed = BreedsDB::where('BreedCode', $dogs_data->RaceID)->where('status', 'For Use')->first();
            if (!empty($breed))
                $user_related_breed = \DB::table("users_skills")->whereRaw("find_in_set('" . $breed->id . "',breed_id)")->pluck('user_id')->toArray();
               
             
                if (!empty($user_related_breed)) {
                    $udBreedPromoter =  User::find($user_related_breed[0]);
                    //$task_manager_id = $related_user->user_id;
                }
            
        }

        if (!empty($breeding_data)) {
            if ($sagirId != 0) {
                
                return view('native.birthing_add', compact('sagirId', 'breeding_data','maleDogData','udBreedPromoter'));
            }
        } else {
            return redirect('breeding/add/' . $sagirId);
        }
    }
    public function store_birthing(Request $request)
    {
       // echo "<pre>";print_r($request->all());exit;
        $breeding_data = Breeding::where('SagirId', $request->hidden_femaleId)->where('status',0)->first();
        if (!empty($breeding_data)) {

            $breeding_data->birthing_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->birthing_date)));
            $breeding_data->live_male_puppie = $request->live_male_puppie;
            $breeding_data->live_female_puppie = $request->live_female_puppie;
            $breeding_data->dead_male_puppie = $request->dead_male_puppie;
            $breeding_data->dead_female_puppie = $request->dead_female_puppie;
            //$breeding_data->review_type = $request->review_type;
            $breeding_data->publish_data = isset($request->publish_data) ? $request->publish_data : 0;
            $breeding_data->share_data = isset($request->share_data) ? $request->share_data : 0;
            $breeding_data->payment_status = "waiting for payment";
           // $breeding_data->payment_type = $request->payment_type;
            $breeding_data->filled_step = 2;
            //$breeding_data->total_payment = $request->total_cart_price;
            //$breeding_data->price_per_dog = $request->store_price_per_puppie;
            //$breeding_data->review_price = $request->store_review_price;
            //$breeding_data->certificate_price = $request->store_certificate_price;
            $breeding_data->save();


            
           
        }
       /* $title = translate('The details was saved and a notification was sent to the right manager');*/
        $title = translate('birth successfully message');

         $button_title_1 = translate('OK for breeding');

        $data = '';


        alert()->alert($title, $data, 'success')->showConfirmButton($button_title_1, '#3085d6')->autoClose(false)->toHtml();
        $desc = "Store Birthing details in table";
        $this->insertLog(Auth::user()->id,$breeding_data->SagirId, $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Store in table',$desc,'Birthing','',json_encode($request->all()),'');
        toastr()->success('Data has been saved successfully!');
        $id = encrypt($breeding_data->id);

        if ($request->payment_type == 'credit_card') {
            return redirect('birthing/payment/' . $id);
        } else {
            return redirect('/active_breeding');
        }
    }
    public function payment(Request $request)
    {
        $id = decrypt($request->id);
        $settings = Settings::where('slug', 'currency')->first();
        $currency = !empty($settings) ? Currency::find($settings->value) : array();
        $breeding_data = Breeding::find($id);
        if (!empty($breeding_data)) {
            return view('native.payment', compact('breeding_data', 'currency'));
        } else {
            toastr()->error('Something went wrong !');
            return redirect('/active_breeding');
        }
    }
    public function add_chipping(Request $request, $id)
    {
        //$breeding_data = Breeding::where('SagirId', $id)->where('payment_status', 'payment done')->first();

        if(\Route::currentRouteName() == 'chipping_done'){
            $breeding_data = Breeding::with('mother_detail')->find($id);
            $id = round($breeding_data->SagirId);
        }else{
            $breeding_data = Breeding::with('mother_detail')->where('SagirId', $id)->where('status',0)->first();
            if(isset($breeding_data) && $breeding_data->status == 1 || $breeding_data->status == 2){
                return redirect('breeding/add/' . $id);
            }
        }

        
        if (!empty($breeding_data)) {
            $chipping_data = BreedingRelatedDog::where('mother_sagir_id', $id)->where('breeding_id', $breeding_data->id)->get();
            $total_dogs = 0;
            $settings = Settings::where('slug', 'currency')->first();
            $currency = !empty($settings) ? Currency::find($settings->value) : array();
            if (!empty($breeding_data)) {
                $total_dogs = $breeding_data->live_male_puppie + $breeding_data->live_female_puppie;
            }
            $sagirId = $id;
            $hairs = HairsDB::get();
            $dogs_data = DogsDB::where('SagirID', $sagirId)->first();
            $colors = array();
            if (!empty($dogs_data)) {
                $breed = BreedsDB::where('BreedCode', $dogs_data->RaceID)->where('status', 'For Use')->first();
           // echo "<pre>";print_r($breed);exit;
                if (!empty($breed)) {
                    $colors = Breed2Color::where('breed_id', $breed->id)->pluck('color_id');

                }
            }
            $colors = ColorsDB::whereIn('id', $colors)->get();
            //echo "<pre>";print_r($colors);exit;

            $pricing_data = Pricing::where('name', 'Certification')->first();
            $review_price_settings = Pricing::where('name', 'Ikc Review')->first();
            $price_per_dog_setting = Pricing::where('name','ikc_per_dog')->first();
            $udBreedPromoter = array();
            if (!empty($dogs_data)) {
                $breed = BreedsDB::where('BreedCode', $dogs_data->RaceID)->where('status', 'For Use')->first();
                if (!empty($breed))
                    $user_related_breed = \DB::table("users_skills")->whereRaw("find_in_set('" . $breed->id . "',breed_id)")->pluck('user_id')->toArray();
                   
                 
                    if (!empty($user_related_breed)) {
                        $udBreedPromoter =  User::find($user_related_breed[0]);
                        //$task_manager_id = $related_user->user_id;
                    }
                
            }
            $userData = array ();
            $user_related_skills = Settings::where('slug', 'office-target-skill-id')->first();
            //echo "<pre>";print_r($user_related_skills);exit;
            if (!empty($user_related_skills)) {
                $user_details = User::find($user_related_skills->value);
                if (!empty($user_details)) {
                    $userData = $user_details ;
                   // $task_manager_id = $user_details->id;
                }
            }
         $club_details = array();
            if (!empty($dogs_data)) {
                $review_data = BreedsDB::where('BreedCode', $dogs_data->RaceID)->where('status', 'For Use')->first();
                if (!empty($review_data)) {
                    $related_club = BreedClub::where('breed_id', $review_data->id)->first();
                    $club_details = Clubs::find($related_club->club_id);
                }
            }

            if ($sagirId != 0) {
                $validationArray = [
                    'temparory_name[]' => 'required',
                    'chip_number[]' => 'required',
                    'color[]' => 'required',
                    'hair_type[]' => 'required',

                ];
                $customMessages = [
                    'required' => 'This field is required'
                ];
                $jsValidator = JsValidator::make($validationArray, $customMessages);
                /*  $validationArray = [
                    'payment_type' => 'required',
                    'review_type' => 'required'
                ];
                $jsValidator = JsValidator::make($validationArray);*/
               //echo "<pre>";print_r($dogs_data);exit;
                return view('native.chipping', compact('breeding_data', 'chipping_data', 'jsValidator', 'sagirId', 'hairs', 'colors', 'id', 'total_dogs', 'currency','dogs_data','pricing_data','review_price_settings','udBreedPromoter','userData','club_details','price_per_dog_setting'));
            }
        } else {
            return redirect('birthing/' . $id);
        }
    }
    public function add_new_dog_form(Request $request)
    {
        $hairs = HairsDB::get();
        $colors = ColorsDB::get();
        echo view('native.add_new_dog_form', compact('request', 'hairs', 'colors'))->render();
    }
    public function store_chipping(Request $request)
    {
      //  echo "<pre>";print_r($request->all());exit;
        $breeding_data = Breeding::where('SagirId', $request->SagirId)->where('status',0)->first();
        $related_task = UserTasks::where('related_breeding_process_id', $breeding_data->id)->first();
        if (!empty($related_task)) {
           /* TaskRelatedSkills::where('task_id', $related_task->id)->whereNull('is_manager')->update(array('is_editable' => 1));*/
            $desc = "Task is now editable";
            $this->insertLog(Auth::user()->id,$breeding_data->SagirId, $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Change Task Permission',$desc,'Tasks',$related_task->id,'is_editable=0','is_editable=1');
        }
        $total_dog = $request->temparory_name;
        $is_dead = array();
        if (isset($request->dead)) {
            for ($k = 0; $k < count($request->dead); $k++) {
                $array = explode('_', $request->dead[$k]);
                if (!empty($array)) {
                    $is_dead[] = $array[1];
                }
            }
        }
        $breeding_data->filled_step = 3;


        $breeding_data->review_type = $request->review_type;
        $breeding_data->payment_type = $request->payment_type;
        $breeding_data->total_payment = $request->total_cart_price;
        $breeding_data->price_per_dog = $request->store_price_per_puppie;
        $breeding_data->review_price = $request->store_review_price;
        $breeding_data->certificate_price = $request->store_certificate_price;
        $breeding_data->total_dead = $request->quantity_22;

        $checkExitsDead = UserTasks::where('related_breeding_process_id',$breeding_data->id)->where('task_name','Finding a litter credit for dead puppies')->get();
        if(count($checkExitsDead) == 0){
            if (!empty($is_dead)) {
                $new_task = new UserTasks();
                $new_task->related_to_user_id = Auth::user()->id;
                $new_task->related_breeding_process_id = $breeding_data->id;
                $new_task->task_name = "Finding a litter credit for dead puppies";
                //$task->due_date_time = date('Y-m-d', strtotime("+" . $due_date_settings->value . " day"));
                $new_task->read_status = 0;
                $new_task->status = "New";
                $new_task->is_editable = 0;
                $new_task->save();
                $related_skill = new TaskRelatedSkills();
                $related_skill->task_id = $new_task->id;
                $related_skill->skill_id = 25;
                $related_skill->is_editable = 1;
                $related_skill->save();
                $breeding_data->total_refund = $request->total_refund;
                $desc = "Finding a litter credit for dead puppies";
                $this->insertLog(Auth::user()->id,$breeding_data->SagirId, $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Task Created for refund',$desc,'Tasks',$new_task->id,json_encode($new_task),'');
            } else {
                $breeding_data->total_refund = NULL;
            }
        }
        /*$check_tasks = UserTasks::where('related_breeding_process_id', $breeding_data->id)->first();*/
        $check_tasks = UserTasks::where('related_breeding_process_id', $breeding_data->id)->where('task_name','!=','Finding a litter credit for dead puppies')->first();

            if (empty($check_tasks)) {
                $task_manager_id = NULL;
                if ($request->review_type == 'breeding_promoter') {
                    $dogs_data = DogsDB::where('SagirID',$request->SagirId)->first();
                    if (!empty($dogs_data)) {
                        $breed = BreedsDB::where('BreedCode', $dogs_data->RaceID)->where('status', 'For Use')->first();
                        if (!empty($breed))
                            $related_club = BreedClub::where('breed_id', $breed->id)->first();
                        if (!empty($related_club)) {
                            $related_user = UsersSkills::where('club_id', $related_club->id)->first();
                            if (!empty($related_user)) {
                                $task_manager_id = $related_user->user_id;
                            }
                        }
                    }
                } else {

                    $user_related_skills = Settings::where('slug', 'office-target-skill-id')->first();
                    if (!empty($user_related_skills)) {
                        $user_details = User::find($user_related_skills->value);
                        if (!empty($user_details)) {
                            $task_manager_id = $user_details->id;
                        }
                    }
                }

                $due_date_settings = Settings::where('slug', 'breeding-manager-due-date')->first();
                $task = new UserTasks();
                //$task->manager_user_id = $task_manager_id;
                $task->related_to_user_id = Auth::user()->id;
                $task->related_breeding_process_id = $breeding_data->id;
                $task->task_name = "A new birth form for";
                //$task->due_date_time = date('Y-m-d', strtotime("+" . $due_date_settings->value . " day"));
                $task->read_status = 0;
                $task->status = "New";
                $task->is_editable = 1;
                $task->task_type = "breeding_task";
                $task->save();
                $subtask = new TaskRelatedSkills;
                $subtask->task_id = $task->id;
                $subtask->user_id = $task_manager_id;
                $subtask->is_editable = 1;
                $subtask->save();
                // send message
                if(!empty($task_manager_id) && ($task_manager_id != '231475' || $task_manager_id != 231475)){
                        if(currentLanguage() == 1){
                            $userData = Templates::select(['translated_text as templateText'])->where('slug','sms-template')->first();
                            $userDetails = User::select(['first_name as name','country_code','mobile_phone'])->where('id',$task_manager_id)->first();
                        }else{
                            $userData = Templates::select(['english_text as templateText'])->where('slug','sms-template')->first();
                            $userDetails = User::select(['first_name_en as name','country_code','mobile_phone'])->where('id',$task_manager_id)->first();
                        }
                        if(!empty($userData) && !empty($userDetails)){
                            $sendText = str_replace('{Firstname}',$userDetails->name,$userData->templateText);
                            $mobileNumber = $userDetails->country_code.$userDetails->mobile_phone;
                           /* $mobileNumberWithCode = '+917600432721';
                            $code = "test";*/
                            //echo settingParam('twillio-sid');exit;
                            $client = new Client(settingParam('twillio-sid'), settingParam('twillio-token'));
                            try {
                                
                                $msg = "";
                                /*$client->messages->create(
                                    $mobileNumber,
                                    [
                                        'from' => settingParam('twillio-registred-mobile'),
                                        'body' => $sendText,
                                    ]
                                );*/
                            } catch (\Exception $e) {
                                $msg = $e->getMessage();
                                $code = "";
                            }
                        }
                }
                
                if ($task_manager_id != '231475' || $task_manager_id != 231475) {
                    $subtask_1 = new TaskRelatedSkills;
                    $subtask_1->task_id = $task->id;
                    $subtask_1->user_id = 231475;
                    $subtask_1->is_editable = 1;
                    $subtask_1->save();
                }

                $subtask_2 = new TaskRelatedSkills;
                $subtask_2->task_id = $task->id;
                $subtask_2->user_id = 227053;
                $subtask_2->is_editable = 1;
                $subtask_2->save();

                $subtask_3 = new TaskRelatedSkills;
                $subtask_3->task_id = $task->id;
                $subtask_3->user_id = 230910;
                $subtask_3->is_editable = 1;
                $subtask_3->save();

                $subtask_4 = new TaskRelatedSkills;
                $subtask_4->task_id = $task->id;
                $subtask_4->user_id = 232154;
                $subtask_4->is_editable = 1;
                $subtask_4->save();

                 $desc = "New task create and assign to approprite user with readonly permission";
                 $this->insertLog(Auth::user()->id,$breeding_data->SagirId, $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Create Task',$desc,'Tasks',$task->id,json_encode($task),'');
            }
        $breeding_data->save();
        
        


        $title = translate('Thanks! The details of the puppies were kept');
         $button_title_1 = translate('OK for breeding');

        $data = '';

        $data = translate('Details sent successfully');

        alert()->alert($title, $data, 'success')->showConfirmButton($button_title_1, '#3085d6')->autoClose(false)->toHtml();

        $desc = "Store Chipping data with puppie details in table";
        $this->insertLog(Auth::user()->id,$breeding_data->SagirId, $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Store Chipping and puppie details',$desc,'Chipping','',json_encode($request->all()),'');
        /* for ($i = 0; $i < count($total_dog); $i++) {

            $text = 'gender_' . $i;
            $dog = new BreedingRelatedDog();
            $dog->temparory_name = isset($request->temparory_name[$i]) ? $request->temparory_name[$i] : NULL;
            $dog->chip_number = isset($request->chip_number[$i]) ? $request->chip_number[$i] : NULL;
            $dog->color = isset($request->color[$i]) ? $request->color[$i] : NULL;
            $dog->gender = isset($request->$text[0]) ? $request->$text[0] : NULL;
            $dog->approval_status = 0;
            $dog->mother_sagir_id = $request->SagirId;
            $dog->breeding_id = $breeding_data->id;
            $dog->is_dead = in_array($i, $is_dead) ? 1 : NULL;
            $dog->save();
        }*/
        toastr()->success('Dogs save successfully !');
        $id = encrypt($breeding_data->id);

        if ($request->payment_type == 'credit_card') {
            return redirect('birthing/payment/' . $id);
        } else {
            return redirect('/active_breeding');
        }
    }
    public function chcked_dogsDB(Request $request){
      // echo "<pre>";print_r($request->chip_number);
      // echo "<pre>";print_r($request->chip_id[0]);exit;
        $data = [];
        $a = array();
        $count  = 1;
        if(!empty($request->chip_number)){
            foreach ($request->chip_number as $key => $value) {
                if($value != 0 || $value != '0'){
                    $data = DogsDB::select('SagirID')->where('Chip',$value)->first();
                    if(!empty($data)){
                        array_push($a,$request->chip_id[$key]);
                        $count = 0;  
                    }
                }
                //echo "in";
            }
        }
        $data['count'] = $count;
        $data['key_return'] = $a;
       // echo $count;exit;
        return $data;
        
    }
    public function chcked_dogsDB_single(Request $request){
       // echo "<pre>";print_r($request->all());exit;
        $count  = 1;
        if($request->chip_number != 0){
            $dog_details = BreedingRelatedDog::find($request->id);
            //echo "<pre>";print_r($dog_details->sagir_id);exit;
            if(empty($dog_details->sagir_id)){
                $data = DogsDB::select('SagirID')->where('Chip',$request->chip_number)->first();
            }else{
                $data = DogsDB::select('SagirID')->where('Chip',$request->chip_number)->where('SagirID','!=',$dog_details->sagir_id)->first();
            }
            
            if(!empty($data)){
                $count = 0;  
            }
            // echo "<pre>";print_r($request->task_details_id);exit;
            if($count == 1){
                    $dataQuery = UserTasks::with('breed_details_many')->where('id',$request->task_details_id)->first();
                   
           // echo $count;exit;
                    if(!empty($dataQuery)){
                        if(isset($dataQuery->breed_details_many) && !empty($dataQuery->breed_details_many))
                        foreach ($dataQuery->breed_details_many as $key => $value) {
                            if($value->id != $request->id){
                                if($value->chip_number == $request->chip_number){
                                    $count = 0;
                                }
                            }
                        }
                    }
            }
        }
        
        
        return $count;
    }
     public function chcked_dogsDB_validation(Request $request){
       // echo "<pre>";print_r($request->all());exit;
        $data = DogsDB::select('SagirID')->where('Chip',$request->chip_number[0])->first();
        if(!empty($data)){
            return 0;
        }else{
            return 1;
        }
    }
    function ajax_store_dog(Request $request)
    {
        $breeding_data = Breeding::where('SagirId', $request->SagirId)->where('status',0)->first();
        if (empty($request->dog_id)) {
            $dog = new BreedingRelatedDog();
        } else {
            $dog = BreedingRelatedDog::find($request->dog_id);
        }

        if ($request->field == 'temparory_name[]') {
            $dog->temparory_name = $request->value;
        }
        if ($request->field == 'chip_number[]') {
           // echo $request->value;exit;
            $dog->chip_number = $request->value;
        }
        if ($request->field == 'color[]') {
            $dog->color = $request->value;
        }
        if ($request->field == 'gender') {
            $dog->gender = $request->value;
        }
        if ($request->field == 'other[]') {
            $dog->other_color = $request->value;
        }
        if ($request->field == 'dead[]') {
            $dog->is_dead = $request->value;
            if ($dog->is_dead == 1) {
                //$number = $breeding_data->total_dead;
                //$breeding_data->total_dead = $number + 1;
                $breeding_data->total_refund = $request->cart_price;
                $breeding_data->save();
            } else {
                //$number = $breeding_data->total_dead;
                //$breeding_data->total_dead = $number - 1;
                $breeding_data->total_refund = Null;
                $breeding_data->save();
            }
        }

        $dog->approval_status = NULL;
        $dog->mother_sagir_id = $request->SagirId;
        $dog->breeding_id = $breeding_data->id;
        /*$dog->is_dead = in_array($i, $is_dead) ? 1 : NULL;*/
        $dog->save();
        echo $dog->id;
    }
    public function supervisor_checkup(Request $request, $id)
    {
        if(\Route::currentRouteName() == 'supervisor_checkup_done'){
            $breeding_data = Breeding::with('mother_detail')->find($id);
            $id = round($breeding_data->SagirId);
        }else{
            $breeding_data = Breeding::with('mother_detail')->where('SagirId', $id)->where('filled_step','3')->where('status',0)->first();
            if(isset($breeding_data) && ($breeding_data->status == 1 || $breeding_data->status == 2)){
                return redirect('breeding/add/' . $id);
            }
        }

       /* $breeding_data = Breeding::where('SagirId', $id)->where('filled_step','3')->where('status',0)->first();
        if(isset($breeding_data) && ($breeding_data->status == 1 || $breeding_data->status == 2)){
            return redirect('breeding/add/' . $id);
        }*/
        if (!empty($breeding_data)) {

            $sagirId = $id;
            /*$breeding = Breeding::where('SagirId', $sagirId)->first();*/
            $related_task = array();
            /*if (!empty($breeding)) {*/
                $related_task = UserTasks::where('related_breeding_process_id', $breeding_data->id)->where('task_type', 'breeding_task')->first();
                $related_manager = TaskRelatedSkills::with('user_details')->where('task_id', $related_task->id)->where('is_manager', 1)->first();
           /* }*/
           $pdf_details = PuppieCards::where('breeding_id', $breeding_data->id)->get();
           if(!$pdf_details->isEmpty()){
               foreach($pdf_details as $key=>$pdf){
                   $pdf_details[$key]->download_link = $this->getStorageURL($pdf->file_name);
               }
           }
            return view('native.supervisor_checkup', compact('sagirId', 'related_task', 'related_manager','breeding_data','pdf_details'));
        } else {
            return redirect('breeding/add/' . $id);
        }
    }
    public function active_breeding(Request $request)
    {
        if (Auth::check() && Auth::user()->record_type == 'Native') {
            $userId = Auth::user()->id;
            $related_dogs = Dogs2Users::where('user_id',$userId)->where('status','current')->pluck('sagir_id');
           // $related_dogs_new = Breeding::whereIn('SagirId',$related_dogs)->pluck('SagirId');

          

            $dataQuery = Breeding::with('father_detail','mother_detail')->whereIn('SagirId',$related_dogs)->where(function ($query) {
    $query->where('status', '=', 0)
          ->orWhere('status', '=', 3);
})->orderByRaw("FIELD(status , 0, 3) ASC");
        

            $dogs_details = $dataQuery->sortable()->paginate(settingParam('record-per-page'));
    // echo "<pre>";print_r($dogs_details->toArray());exit;
            return view('native.breeding_list', compact('dogs_details'));
        } else {
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
        }
    }
    public function delete_dog(Request $request){
        //$bredingId = Breeding::where('id',$request->id)->first();
        $bredingIddetele = Breeding::where('id',$request->id)->delete();
        $data = BreedingRelatedDog::where('breeding_id',$request->id)->delete();
        $delete = UserTasks::where('related_breeding_process_id',$request->id)->pluck('id');
        if(!empty($delete)){
            $colors = TaskRelatedSkills::whereIn('task_id', $delete)->delete();
        }
        $deleteNew = UserTasks::where('related_breeding_process_id',$request->id)->delete();
        //return redirect('/active_breeding');
    }
    public function getStorageURL($filename)
    {

        $disk = Storage::disk('s3');
        if ($disk->exists($filename)) {
            $client = $disk->getDriver()->getAdapter()->getClient();
        //echo "123";exit;
            $bucket = \Config::get('filesystems.disks.s3.bucket');

            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key' => $filename
            ]);

            $request = $client->createPresignedRequest($command, '+120 minutes');
            return (string) $request->getUri();
        }
        else 
            return '';
    }
    public function owner_transfer($id){
        $dog_details = DogsDB::find($id);
        $countries = Country::get();
        $usercount = 0;
        if(!empty($dog_details)){
            $checkuser = Dogs2Users::where('sagir_id',round($dog_details->SagirID))->where('status','current')->get();
            if(count($checkuser) > 1){
                $usercount = 1;
            }
        }
        
        //echo "<pre>";print_r($dog_details);exit;
        return view('native.owner_transfer', compact('dog_details','countries','usercount'));

    }
    public function add_new_owner_form(Request $request){
       // $dog_details = DogsDB::find($request->);
        $countries = Country::get();
        //echo "123";exit;
       echo view('admin.add_new_owner_form', compact('request', 'countries'))->render();
           
    }
    public function getCountry()
    {

        $countryList = Country::all();

        return $countryList;
    }

    /* Get Country code from Country */
    public function getCountryCode(Request $request)
    {

        $country = Country::find($request->country_id);

        if ($country && $country->country_code != '') {

            $this->reply['status'] = true;
            $this->reply['country_code'] = $country->country_code;
        } else {
            $this->reply['status'] = true;
            $this->reply['country_code'] = "";
        }
        return response()->json($this->reply);
    }
    public function owner_exits(Request $request){
        $data = User::where("country_code",$request->country_code)->where('mobile_phone',ltrim($request->number, '0'))->where('record_type','Native')->first();
        if(!empty($data)){
            return 1;
        }else{
            return 0;
        }
    }   
    public function otp_send(Request $request){

        $mobileNumber = $request->country_code.ltrim($request->number, '0');
       /* $mobileNumberWithCode = '+917600432721';
        $code = "test";*/
        //echo settingParam('twillio-sid');exit;
        $msg = "";
        $code = random_int(100000, 999999);
        $client = new Client(settingParam('twillio-sid'), settingParam('twillio-token'));
        try {
            
            $msg = "";
            $client->messages->create(
                $mobileNumber,
                [
                    'from' => settingParam('twillio-registred-mobile'),
                    'body' => $code,
                ]
            );
        } catch (\Exception $e) {
           // toastr()->error('Enter wrong mobile number');
            $msg = $e->getMessage();
            $code = 0;
        }
        return ['code' => $code, 'msg' => $msg];
    }
    public function append_userdata(Request $request){
        $countries = Country::get();    
        echo view('admin.add_new_user_data', compact('request', 'countries'))->render();
    }
    public function owner_transfer_store(Request $request){
        $input = $request->all();
       
        $data = Dogs2Users::where('sagir_id', $input['sagir_id'])->update(array('status' => 'old'));
        $j = 0;
        for ($i=0; $i < $input['total_div']; $i++) { 

             $dogDetails = DogsDB::find($input['dog_id']);
                if(currentLanguage() == 1){
                    $dogDetails->Heb_Name = $input['dog_name'][$i];
                }else{
                    $dogDetails->Eng_Name = $input['dog_name'][$i];

                }
                $dogDetails->save();

             $data = User::where("country_code",$input['country_code'][$i])->where('mobile_phone',ltrim($input['mobile_number'][$i], '0'))->where('record_type','Native')->first();
            try {
            if(!empty($data)){
                $Dogs2Users = new Dogs2Users();
                $Dogs2Users->user_id = $data->id;
                $Dogs2Users->sagir_id = $input['sagir_id'];
                $Dogs2Users->status = "current";
                $Dogs2Users->save();
                $data->grower_remarks = $input['remark'][$i];
                $data->save();
            }else{
                $user = new User();
                $user->first_name = $input['first_name'][$j];
                $user->last_name = $input['last_name'][$j];
                $user->first_name_en = $input['first_name_en'][$j];
                $user->last_name_en = $input['last_name_en'][$j];
                $user->email =  $input['email'][$j];
                $user->mobile_phone =  ltrim($input['mobile_number'][$i], '0');
                $user->country_code =  $input['country_code'][$i];
                $user->address_city = $input['city'][$j];
                $user->address_city_en = $input['city_en'][$j];
                $user->address_street_number = $input['house_number'][$j];
                $user->address_street = $input['street'][$j];
               // $user->address_zip = $input['zip_code'][$j];
                $user->social_id_number = $input['social_id'][$j];
                $user->grower_remarks = $input['remark'][$i];
                $user->created_at = Carbon::now();
                $user->record_type = 'Native';
                $user->save();
                $Dogs2Users = new Dogs2Users();
                $Dogs2Users->user_id = $user->id;
                $Dogs2Users->sagir_id = $input['sagir_id'];
                $Dogs2Users->status = "current";
                $Dogs2Users->save();
                $j++;
            }
        }catch (\Exception $e) {
            echo "<pre>";print_r($e->getMessage());exit;
            DB::rollback();
            /*\Session::flash('message', 'Not Successfully');
            \Session::flash('class', 'danger');
            return redirect()->back();*/
        }
        }
           return redirect('dog');
    }
    public function check_sagir_exits(Request $request){
        $maleIdExits = DogsDB::where('SagirID',$request->id)->first();
        if(!empty($maleIdExits)){
           $data['status'] = 'match';
           if(!empty($maleIdExits->Heb_Name)){
                $Heb_Name = explode(" ",$maleIdExits->Heb_Name);
                $data['Heb_Name'] = end($Heb_Name);
           }else{
            $data['Heb_Name'] = $maleIdExits->Heb_Name;
           }
           if(!empty($maleIdExits->Eng_Name)){
                $Eng_Name = explode(" ",$maleIdExits->Eng_Name);
                $data['Eng_Name'] = $Eng_Name[0];
           }else{
            $data['Eng_Name'] = $maleIdExits->Eng_Name;
           }
        }else{
           $data['status'] = 'nomatch';
           $data['Heb_Name'] = '';
           $data['Eng_Name'] = '';
        }
        return response()->json($data);
    }
    public function breeding_status_change(Request $request){
        $data = Breeding::where('id',$request->breeding_id)->where('status',0)->first();
        $data->status = 1;
        $data->save();
        return "1";
    }
    public function birthing_status_change(Request $request){
        $data = Breeding::where('id',$request->breeding_id)->where('status',0)->first();
        $data->status = 2;
        $data->save();
        return "1";
    }
    public function breed_related_dog(Request $request){
        $skills = UsersSkills::where('user_id',Auth::user()->id)->where('skill_id',3)->first();

        if (Auth::check() && Auth::user()->record_type == 'Native' && !empty($skills) && !empty($skills->breed_id)) {
            $userId = Auth::user()->id;
            $related_breed = explode(',',$skills->breed_id);
            $related_breed_code = BreedsDB::whereIn('id',$related_breed)->pluck('BreedCode');
            $related_sagir_id = DogsDB::whereIn('RaceID',$related_breed_code)->pluck('SagirID');
            $related_dogs = Dogs2Users::where('user_id',$userId)->where('status','current')->pluck('sagir_id');
           // $related_dogs_new = Breeding::whereIn('SagirId',$related_dogs)->pluck('SagirId');

          if(!empty($related_sagir_id)){
              
            $dataQuery = Breeding::with('father_detail','mother_detail')->whereIn('breedings.SagirId',$related_dogs)->whereIn('breedings.SagirId',$related_sagir_id)->where(function ($query) {
                $query->where('breedings.status', '=', 0)
                      ->orWhere('breedings.status', '=', 3);
            })->orderByRaw("FIELD(breedings.status , 0, 3) ASC");
          }else{
            $dataQuery = Breeding::with('father_detail','mother_detail')->whereIn('SagirId',$related_dogs)->where(function ($query) {
                $query->where('status', '=', 0)
                      ->orWhere('status', '=', 3);
            })->orderByRaw("FIELD(status , 0, 3) ASC");
          }

            
        

            $dogs_details = $dataQuery->sortable()->paginate(settingParam('record-per-page'));
    // echo "<pre>";print_r($dogs_details->toArray());exit;
            return view('native.related_breeding_list', compact('dogs_details'));
        } else {
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
        }

    }
    
}
