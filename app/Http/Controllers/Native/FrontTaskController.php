<?php

namespace App\Http\Controllers\Native;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use Config;
use Helper;
use App\User;
use App\UserTasks;
use Hash;
use Sortable;
use App\Breeding;
use App\Settings;
use App\BreedingRelatedDog;
use Illuminate\Support\Facades\DB;
use App\DogsDB;
use App\BreedsDB;
use App\Dogs2Users;
use App\Breed2Color;
use App\ColorsDB;
use App\TaskRelatedSkills;
use App\ActionLog;
use App\UsersSkills;
use File;
use Illuminate\Support\Facades\Storage;
use App\Traits\log;
use App\PuppieCards;
use Twilio\Rest\Client;
use App\Templates;

class FrontTaskController extends Controller
{
    use log;

    public function __construct()
    {
        $key = Settings::select('value')->where('slug', 'aws-access-key-id')->first();
        $secretKey = Settings::select('value')->where('slug', 'aws-secret-access-key')->first();
        $region = Settings::select('value')->where('slug', 'aws-default-region')->first();
        $bucket = Settings::select('value')->where('slug', 'aws-bucket')->first();
        $url = Settings::select('value')->where('slug', 'aws-url')->first();
        config(['filesystems.disks.s3.key' => $key['value'], 'filesystems.disks.s3.secret' => $secretKey['value'], 'filesystems.disks.s3.region' => $region['value'], 'filesystems.disks.s3.bucket' => $bucket['value'], 'filesystems.disks.s3.url' => $url['value']]);
        //echo "<pre>"; print_r(config());exit;
        $this->middleware('auth');
    }
    public $moduleName = 'task';

    public function getTaskList(Request $request)
    {
        if (Auth::check() && Auth::user()->record_type == 'Native') {
            $userId = Auth::user()->id;
            $other_related_task = TaskRelatedSkills::where('user_id', $userId)->pluck('task_id');
            $dataQuery = UserTasks::with('user_details', 'breed_details')->WhereIn('id', $other_related_task)->orderByRaw("FIELD(is_editable , '0', '1') DESC")->orderBy("created_at","DESC");
            $tasks = $dataQuery->sortable()->paginate(settingParam('record-per-page'));
            $editable_user = array();
            foreach($tasks as $key=>$value){
                $related_user = TaskRelatedSkills::with('user_details')->where('task_id',$value->id)->where('is_editable',1)->get();
                if(!empty($related_user)){
                    $editable_user = array();
                    foreach($related_user as $ru){
                        if(!empty($ru->user_details)){
                            if(currentLanguage() == 1){
                                $editable_user[] = $ru->user_details->first_name." ".$ru->user_details->last_name;

                            }else{
                                 $editable_user[] = $ru->user_details->first_name_en." ".$ru->user_details->first_name_en;
                            }
                           
                        }
                    }
                    $tasks[$key]['editable_users'] = implode(', ',$editable_user);


                    $related_user_edit_permisstion = TaskRelatedSkills::where('task_id',$value->id)->where('user_id',Auth::user()->id)->first();
                    if(isset($related_user_edit_permisstion) && !empty($related_user_edit_permisstion)){
                        $tasks[$key]['related_user_edit_permisstion'] = $related_user_edit_permisstion->is_editable;
                    }else{
                        $tasks[$key]['related_user_edit_permisstion'] = 0;
                    }
                }

            }
            // $desc = "Task List which is realted to logged in user";
            // $this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Task List',$desc,'Tasks','','','');
            //echo "<pre>";print_r($tasks);exit;
            return view('native.task_list', compact('tasks'));
        } else {
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
        }
    }
    public function getTaskDetails(Request $request, $id)
    {

        //  print_r(config('filesystem.disks.s3'));exit;
        if (Auth::check() && Auth::user()->record_type == 'Native') {
            $userId = Auth::user()->id;
           /* $skill_details = UsersSkills::where('user_id', Auth::user()->id)->pluck('skill_id')->toArray();*/
            $skill_details = TaskRelatedSkills::where('task_id',$id)->where('user_id', Auth::user()->id)->pluck('skill_id')->toArray();
            $setting_details = Settings::where('slug', 'task-assign-to-skill-after-puppies-review')->first();
            $user_skills =
                $task_details = UserTasks::with('user_details')->find($id);
            $review_manager = TaskRelatedSkills::where('task_id', $id)->where('is_manager', 1)->first();
            /* if (!empty($task_details)) {
                if ($task_details->is_editable == 0) {
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect('/tasks');
                }
            }*/
            $breeding_data = array();
            $related_dogs = array();
            $related_user = array();
            $edit_permission = array();
            $unupdated_dog = 0;
            if (!empty($task_details)) {
                $edit_permission = TaskRelatedSkills::where('task_id', $task_details->id)->where('is_editable', 1)->pluck('user_id')->toArray();
                $breeding_data = Breeding::find($task_details->related_breeding_process_id);
                $related_dogs = BreedingRelatedDog::with('dog_color', 'dog_status')->where('breeding_id', $task_details->related_breeding_process_id)->where(function ($query) {
                    $query->whereNull('is_dead')->orwhere('is_dead', 0);
                })->get();
               //echo "<pre>";print_r($related_dogs);exit;
                $updated_dog_data = BreedingRelatedDog::with('dog_color', 'dog_status')->where('breeding_id', $task_details->related_breeding_process_id)->whereNotNull('approval_status')->where(function ($query) {
                    $query->whereNull('is_dead')->orwhere('is_dead', 0);
                })->count();
                $total_dog = BreedingRelatedDog::with('dog_color', 'dog_status')->where('breeding_id', $task_details->related_breeding_process_id)->where(function ($query) {
                    $query->whereNull('is_dead')->orwhere('is_dead', 0);
                })->count();
                $unupdated_dog = $total_dog - $updated_dog_data;
                $dogs_data = DogsDB::where('SagirID', $breeding_data->SagirId)->first();
                if (!empty($dogs_data)) {
                    $breed_id = BreedsDB::where('BreedCode', $dogs_data->RaceID)->where('status', 'For Use')->first();
                    if (!empty($breed_id)) {
                        $user_related_skills = \DB::table("users_skills")->whereRaw("find_in_set('" . $breed_id->id . "',breed_id)")->pluck('user_id')->toArray();
                        if (!empty($user_related_skills)) {
                            if (empty($review_manager)) {
                                $related_user = User::whereIn('id', $user_related_skills)->orWhere('id', Auth::user()->id)->get();
                            } else {
                                
                                    $related_user = User::where('id', Auth::user()->id)->orWhere(function($query) use ($user_related_skills,$review_manager){
									$query->whereIn('id', $user_related_skills)->orWhere('id',$review_manager->user_id);})->get();
                              //echo "<pre>";print_r($related_user);exit;  
                            }   
                        } else {
                            if (!empty($review_manager)) {
                                $related_user = User::where('id', Auth::user()->id)->orWhere('id', $review_manager->user_id)->get();
                            } else {
                                $related_user = User::where('id', Auth::user()->id)->get();
                            }
                        }
                    }
                }
            }
            $pdf_details = PuppieCards::where('breeding_id', $breeding_data->id)->get();
            if(!$pdf_details->isEmpty()){
                foreach($pdf_details as $key=>$pdf){
                    $pdf_details[$key]->download_link = $this->getStorageURL($pdf->file_name);
                }
            }
            //$desc = "User Go in Task Detail page";
            //$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Task details',$desc,'Tasks',$id,'','');

            $taskHistory = ActionLog::select('action_logs.date_time', 'action_logs.action_name', 'users.first_name_en', 'users.first_name')
                ->leftJoin('users', 'action_logs.user_id', '=', 'users.id')
                ->where('action_logs.related_task_id', $id)->get();
            //  dd(DB::getQueryLog());  
            //  echo "<pre>";print_r($taskHistory);exit;

            return view('native.task_details', compact('task_details', 'id', 'breeding_data', 'related_dogs', 'related_user', 'edit_permission', 'unupdated_dog', 'review_manager', 'setting_details', 'skill_details', 'taskHistory', 'pdf_details','dogs_data'));
        } else {
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
        }
    }
    public function update_task(Request $request)
    {
        $id = $request->task_details_id;
        if (isset($request->submit_form) || (isset($request->submit_form_review) && $request->submit_form_review == 1)) {
        //echo "<pre>";print_r($request->all());exit;
            $old_task_details =  UserTasks::find($id);
            $task = UserTasks::find($id);
            //$task->task_name = $request->task_name;
            $task->full_details = $request->full_details;
            $task->due_date_time = !empty($request->due_date) ? date('Y-m-d', strtotime(str_replace('/', '-', $request->due_date))) : NULL;
            $task->status = $request->task_status;
            $task->review_place = $request->review_place;
            $task->review_date = !empty($request->review_date) ? date('Y-m-d', strtotime(str_replace('/', '-', $request->review_date))) : NULL;
            $task->male_owner_agree = isset($request->male_owner_agree) ? $request->male_owner_agree : NULL;
            $task->save();
            if (isset($request->review_manager)) {
                $update_task_status = UserTasks::find($id);
                $update_task_status->status = 'In process';
                $update_task_status->save();
                $check_user = TaskRelatedSkills::where('task_id', $id)->where('user_id', $request->review_manager)->first();
                if (empty($check_user)) {
                    $assign_task = new TaskRelatedSkills();
                    $assign_task->task_id = $task->id;
                    $assign_task->user_id = $request->review_manager;
                    $assign_task->is_editable = 1;
                    $assign_task->is_manager = 1;
                    $assign_task->save();
                    // send message
                    if(!empty($request->review_manager)){
                            if(currentLanguage() == 1){
                                $userData = Templates::select(['translated_text as templateText'])->where('slug','sms-template')->first();
                                $userDetails = User::select(['first_name as name','country_code','mobile_phone'])->where('id',$request->review_manager)->first();
                            }else{
                                $userData = Templates::select(['english_text as templateText'])->where('slug','sms-template')->first();
                                $userDetails = User::select(['first_name_en as name','country_code','mobile_phone'])->where('id',$request->review_manager)->first();
                            }
                            if(!empty($userData) && !empty($userDetails)){
                                $sendText = str_replace('{Firstname}',$userDetails->name,$userData->templateText);
                                $mobileNumber = $userDetails->country_code.$userDetails->mobile_phone;
                               /* $mobileNumberWithCode = '+917600432721';
                                $code = "test";*/
                                //echo settingParam('twillio-sid');exit;
                                $client = new Client(settingParam('twillio-sid'), settingParam('twillio-token'));
                                try {
                                    
                                    $msg = "";
                                    $client->messages->create(
                                        $mobileNumber,
                                        [
                                            'from' => settingParam('twillio-registred-mobile'),
                                            'body' => $sendText,
                                        ]
                                    );
                                } catch (\Exception $e) {
                                    $msg = $e->getMessage();
                                    $code = "";
                                }
                            }
                            
                    }
                } else {
                    TaskRelatedSkills::where('task_id', $id)->where('user_id', $request->review_manager)->update(array('is_editable' => 1, 'is_manager' => 1));
                }
            }
            $desc = "User Update task details and assign task to Review Manager";
            $this->insertLog(Auth::user()->id, '', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Task details Updated By User', $desc, 'Tasks', $id, json_encode($old_task_details), json_encode($task));
            toastr()->success('Data has been saved successfully!');
            return redirect(frontUrl('tasks'));
        } elseif (isset($request->update_status)) {
            $task =  $old_task_details =  UserTasks::find($id);
            $task->status = $request->task_status;
            $task->pedigree_color = $request->pedigree_color;
            $task->save();
            if($request->task_status == 'Competed'){
                $breedingstatus = Breeding::where('id',$task->related_breeding_process_id)->first();
                $breedingstatus->status = 3;
                $breedingstatus->filled_step = 4;
                $breedingstatus->save();
                TaskRelatedSkills::where('task_id', $id)->update(array('is_editable' => 0));
            }
            
            DogsDB::where('sheger_id', $task->related_breeding_process_id)->update(array('pedigree_color' => $request->pedigree_color));
            
            $desc = "Supervisor Update task details and Pedigree Color";
            $this->insertLog(Auth::user()->id, '', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Task details Updated By supervisor', $desc, 'Tasks', $id, json_encode($old_task_details), json_encode($task));
            $this->generate_pdf($task->related_breeding_process_id, '2');
            toastr()->success('Data has been saved successfully!');
            return redirect(frontUrl('tasks'));
        } else {
            $breed_id = NULL;
            $id = $request->task_details_id;
            $task = $old_task_details = UserTasks::find($id);
            $task->male_owner_agree = isset($request->male_owner_agree) ? $request->male_owner_agree : NULL;
            $task->save();
            $breeding_data = Breeding::find($task->related_breeding_process_id);
            $related_dogs = BreedingRelatedDog::where('breeding_id', $task->related_breeding_process_id)->where(function ($query) {
                $query->where('is_dead', '!=', 1)
                    ->orWhereNull('is_dead');
            })->whereNull('sagir_id')->get();
            $dogs_data = DogsDB::where('SagirID', $breeding_data->SagirId)->first();
            if (!empty($dogs_data)) {
                //$review_data = BreedsDB::where('BreedCode', $dogs_data->RaceID)->where('status', 'For Use')->first();
                $breed_id = $dogs_data->RaceID;
            }
            if (!$related_dogs->isEmpty()) {
                foreach ($related_dogs as $rd) {
                    $new_dog = new DogsDB();
                    $check_sagir_id = DogsDB::orderBy('SagirID', 'desc')->first();
                    $sagir_id = round($check_sagir_id->SagirID) + 1;
                    $new_dog = new DogsDB();
                    $new_dog->Heb_name = $rd->temparory_name;
                    $new_dog->BirthDate = !empty($breeding_data->birthing_date) ? date('Y-m-d', strtotime($breeding_data->birthing_date)) : NULL;
                    $new_dog->FatherSAGIR = $breeding_data->MaleSagirId;
                    $new_dog->MotherSAGIR = $breeding_data->SagirId;
                    $new_dog->Chip = $rd->chip_number;
                    $new_dog->ColorID = $rd->color;
                    $new_dog->GenderID = $rd->gender == "female" ? 2 : 1;
                    $new_dog->RaceID = $breed_id;
                    $new_dog->sheger_id = $breeding_data->id;
                    $new_dog->sagir_prefix = "BRD";
                    $new_dog->SagirID = $sagir_id;
                    $new_dog->save();
                    $relate_dog = new Dogs2Users();
                    $relate_dog->user_id = $breeding_data->created_by;
                    $relate_dog->sagir_id = $new_dog->SagirID;
                    $relate_dog->status = 'current';
                    $relate_dog->save();

                    $related_puppie = BreedingRelatedDog::find($rd->id);
                    $related_puppie->sagir_id = $new_dog->SagirID;
                    $related_puppie->save();
                }
            }
            TaskRelatedSkills::where('task_id', $id)->where('user_id', $request->review_manager)->WhereNotIn('user_id',array('231475','227053','230910','232154'))->update(array('is_editable' => 0));
            $setting_details = Settings::where('slug', 'task-assign-to-skill-after-puppies-review')->first();
            $assign_task = new TaskRelatedSkills();
            $assign_task->task_id = $id;
            $assign_task->skill_id = $setting_details->value;
            if (!empty($setting_details)) {
                $skill_related_user = UsersSkills::where('skill_id', $setting_details->value)->first();
                if (!empty($skill_related_user)) {
                    $assign_task->user_id = $skill_related_user->user_id;
                }
            }
            $assign_task->is_editable = 1;
            $assign_task->save();
            // send message
            if(!empty($assign_task->user_id)){
                if(currentLanguage() == 1){
                    $userData = Templates::select(['translated_text as templateText'])->where('slug','sms-template')->first();
                    $userDetails = User::select(['first_name as name','country_code','mobile_phone'])->where('id',$assign_task->user_id)->first();
                }else{
                    $userData = Templates::select(['english_text as templateText'])->where('slug','sms-template')->first();
                    $userDetails = User::select(['first_name_en as name','country_code','mobile_phone'])->where('id',$assign_task->user_id)->first();
                }

                 if(!empty($userData) && !empty($userDetails)){
                    $sendText = str_replace('{Firstname}',$userDetails->name,$userData->templateText);
                    $mobileNumber = $userDetails->country_code.$userDetails->mobile_phone;
                   /* $mobileNumberWithCode = '+917600432721';
                    $code = "test";*/
                    //echo settingParam('twillio-sid');exit;
                    $client = new Client(settingParam('twillio-sid'), settingParam('twillio-token'));
                    try {
                        
                        $msg = "";
                        $client->messages->create(
                            $mobileNumber,
                            [
                                'from' => settingParam('twillio-registred-mobile'),
                                'body' => $sendText,
                            ]
                        );
                    } catch (\Exception $e) {
                        $msg = $e->getMessage();
                        $code = "";
                    }
                }
            }
            TaskRelatedSkills::where('task_id', $id)->where('is_editable',1)->update(array('skill_id' => $setting_details->value));
            $desc = "Review Manager update related puppie details and another fields and New dogs added in dog table";
            $this->insertLog(Auth::user()->id, '', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'Task details Updated By Review Manager', $desc, 'Tasks', $id, json_encode($old_task_details), json_encode($task));
            $this->generate_pdf($task->related_breeding_process_id, '1');
            toastr()->success('Data has been saved successfully!');

            return redirect(frontUrl('tasks'));
        }
    }
    public function updateDogStatus(Request $request)
    {
        //BreedingRelatedDog::where('id',$request->dog_id)->update(array('approval_status'=>$request->status));
       // echo "<pre>";print_r($request->all());exit;
        $dog_details = $old_dog_details = BreedingRelatedDog::find($request->dog_id);
        $dog_details->chip_number = $request->chip_number;
        if(isset($request->supervisor_notes)){
            $dog_details->supervisor_notes = $request->supervisor_notes;
        }
        $dog_details->note = isset($request->note) ? $request->note : '';
        $dog_details->approval_status = isset($request->approval_status) ? $request->approval_status : Null;
        if(isset($request->color)){
            if($request->color == '0' || $request->color == 0){
                $dog_details->color = $request->color;
                $dog_details->other_color = $request->other;
            }else{
                $dog_details->color = $request->color;
                $dog_details->other_color = Null;
            }
        }
        if (isset($request->image)) {
            $file = $request->file('image');

            $destinationPath = 'puppie_images';
            $file->store('puppie_images', 's3');
            Storage::disk('s3')->put($file->getClientOriginalName(), file_get_contents($file->getRealPath()));
            //$file->move($destinationPath, $file->getClientOriginalName());
            $dog_details->image = $file->getClientOriginalName();
        }
        if (isset($request->document)) {
            $file_document = $request->file('document');
            $destinationPath = 'puppie_document';
            $file_document->store('puppie_document', 's3');
            //echo "";print_r($file)
            Storage::disk('s3')->put($file_document->getClientOriginalName(), file_get_contents($file_document->getRealPath()));
            // $file_document->move($destinationPath, $file_document->getClientOriginalName());
            $dog_details->document = $file_document->getClientOriginalName();
        }
        $dog_details->updated_by = Auth::user()->id;
        $dog_details->save();
        $related_dogs = BreedingRelatedDog::with('dog_color', 'dog_status')->where('breeding_id', $dog_details->breeding_id)->where(function ($query) {
                    $query->whereNull('is_dead')->orwhere('is_dead', 0);
                })->get();
        $updated_dog_data = BreedingRelatedDog::with('dog_color', 'dog_status')->where('breeding_id', $dog_details->breeding_id)->whereNotNull('approval_status')->where(function ($query) {
            $query->whereNull('is_dead')->orwhere('is_dead', 0);
        })->count();
        $total_dog = BreedingRelatedDog::with('dog_color', 'dog_status')->where('breeding_id', $dog_details->breeding_id)->where(function ($query) {
            $query->whereNull('is_dead')->orwhere('is_dead', 0);
        })->count();
        $unupdated_dog = $total_dog - $updated_dog_data;
        $user_skills = UsersSkills::where('user_id', Auth::user()->id)->first();
        $setting_details = Settings::where('slug', 'task-assign-to-skill-after-puppies-review')->first();
        if (!empty($user_skills) && $user_skills->skill_id == $setting_details->id) {
            $desc = "Supervisor update related information of puppie";
            $subject = "Puppie Details updated by Supervison";
        } else {
            $desc = "Review Manager update related information of puppie";
            $subject = "Puppie Details updated by Review Manager";
        }

        $this->insertLog(Auth::user()->id, '', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), $subject, $desc, 'Tasks', $request->task_id, json_encode($old_dog_details), json_encode($dog_details));
        echo view('native.ajax_dog_list', compact('related_dogs', 'unupdated_dog'))->render();
    }
    function get_dog_details(Request $request)
    {
        $color = ColorsDB::where('status','for use')->get();
        $dog_details = BreedingRelatedDog::find($request->dog_id);
       /* $skill_details = UsersSkills::where('user_id', Auth::user()->id)->pluck('skill_id')->toArray();*/
       $skill_details = TaskRelatedSkills::where('task_id',$request->task_id)->where('user_id', Auth::user()->id)->pluck('skill_id')->toArray();
        $setting_details = Settings::where('slug', 'task-assign-to-skill-after-puppies-review')->first();
        $dog_details['image'] = $this->getStorageURL($dog_details['image']);
        $dog_details['document_url'] = $this->getStorageURL($dog_details['document']);
        $task_id = $request->task_id;
        echo view('native.ajax_dog_details', compact('dog_details', 'skill_details', 'setting_details', 'task_id','color'))->render();
    }
    public function getStorageURL($filename)
    {
       
        $disk = Storage::disk('s3');
        if ($disk->exists($filename)) {
            $client = $disk->getDriver()->getAdapter()->getClient();
            //echo "123";exit;
            $bucket = \Config::get('filesystems.disks.s3.bucket');

            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key' => $filename
            ]);

            $request = $client->createPresignedRequest($command, '+120 minutes');
            return (string) $request->getUri();
        } else
            return '';
            
    }
    public function generate_pdf($id, $file_name)
    {

        $related_dog = BreedingRelatedDog::where('breeding_id', $id)->get();
        $breeding_data = Breeding::find($id);
        $dog_data = DogsDB::with('breed_name', 'color_name_new')->where('sheger_id', $id)->get();
       //echo "<pre>";print_r($breeding_data);exit;
        if (!empty($dog_data)) {
            foreach ($dog_data as $key => $dog) {
                echo $dog->MotherSAGIR;
                $dog_data[$key]['mother_details'] = !empty($dog->MotherSAGIR) ? DogsDB::with('breed_name', 'color_name_old')->where('SagirID',$dog->MotherSAGIR)->first() : '';
                $dog_data[$key]['father_details'] = !empty($dog->FatherSAGIR) ? DogsDB::with('breed_name', 'color_name_old')->where('SagirID',$dog->FatherSAGIR)->first() : '';
                $dog_data[$key]['user_mother_details'] = !empty($dog->MotherSAGIR) ? Dogs2Users::with('user_detail')->where('sagir_id',$dog->MotherSAGIR)->where('status','current')->first() : '';
                $dog_data[$key]['puppie_details'] = !empty($dog->SagirID) ? BreedingRelatedDog::where('sagir_id', $dog->SagirID)->first() : '';
                if (!empty($dog_data[$key]['puppie_details'])) {
                    $dog_data[$key]['puppie_details']->image = !empty($dog_data[$key]['puppie_details']->image) ? $this->getStorageURL($dog_data[$key]['puppie_details']->image) : '';
                }
            }
        }
        if (!empty($related_dog)) {
            $currentTime = date('dmY_Hi');
            if(!empty($breeding_data)){
                $new_file_name = round($breeding_data->SagirId) . '_' . $currentTime;
            }else{
                $new_file_name = $currentTime;
            }
            $data = view('native.generate_pdf', compact('dog_data', 'breeding_data'))->render();
            $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4']);
            $mpdf->SetDirectionality('rtl');
            $mpdf->autoScriptToLang = true;
            $mpdf->autoLangToFont = true;
            $mpdf->WriteHTML($data);
            $pdf_path = public_path() . '/puppie_cards/'.$new_file_name.'.pdf';
            $mpdf->Output($pdf_path, 'F');
            Storage::disk('s3')->put('puppie_cards/'.$new_file_name.'.pdf', file_get_contents($pdf_path));
            unlink($pdf_path);

            $save_file = new PuppieCards();
            $save_file->breeding_id = $id;
            $save_file->file_name = 'puppie_cards/'.$new_file_name.'.pdf';
            $save_file->save();
        }
    }
    public function download($id){
        $save_file = PuppieCards::find($id);
        $file_name  = "puppie_card.pdf";
        $explode_data = explode('/',$save_file->file_name);
    $headers = [
          'Content-Type'        => 'application/pdf',            
          'Content-Disposition' => 'attachment; filename="'. $explode_data[1] .'"',
    ];

    return \Response::make(Storage::disk('s3')->get($save_file->file_name), 200, $headers);
    }
    public function check_breed_color(Request $request){
        $count = 0;
        $dogs_data = DogsDB::where('SagirID', $request->female_sagirid_hidden)->first();
        $colors = array();
        if (!empty($dogs_data)) {
            $breed = BreedsDB::where('BreedCode', $dogs_data->RaceID)->where('status', 'For Use')->first();
       // echo "<pre>";print_r($breed);exit;
            if (!empty($breed)) {
                $colors = Breed2Color::where('breed_id', $breed->id)->pluck('color_id');

            }
        }
        $colors = ColorsDB::whereIn('id', $colors)->get();
        if(!empty($colors)){
            foreach ($colors as $key => $value) {
                if($value->id == $request->color){
                    $count = 1;
                }
            }
        }
       return $count;
    }
}
