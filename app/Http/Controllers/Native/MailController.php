<?php

namespace App\Http\Controllers\Native;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use JsValidator;
use App\TaskRelatedSkills;
use Validator;
use Auth;
use Mail;
use Session;
class MailController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    public function sendSignUpMail(Request $request){
     /* $data = array('name'=>"Virat Gandhi");*/
        $userData = TaskRelatedSkills::with('user_details')->where('skill_id','26')->whereNotNull('user_id')->first();
        $toEmail = '';
        if(!empty($userData->user_details)){
            $toEmail = $userData->user_details->email;
        }else{
             $userDataadmin = TaskRelatedSkills::with('user_details')->where('skill_id','28')->whereNotNull('user_id')->first();
             if(!empty($userDataadmin->user_details)){
                $toEmail = $userDataadmin->user_details->email;
             }
        }
        if(!empty($toEmail)){
             Mail::send('native.mail', $request->all(), function($message) {
                 $message->to($toEmail, 'SignUp')->subject
                    ('New User SignUp');
                $message->from('phptesting2019@gmail.com');
              });
              
        }
        return response()->json(['success' => true, 'msg' => 'Mail send Successfully.']);
    }




}
