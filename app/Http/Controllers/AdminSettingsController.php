<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\Request;
use App\Settings;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Redis;
use JsValidator;
use Validator;
use Auth;
use App\Traits\log;

class AdminSettingsController extends Controller
{
	use log;
	public $moduleName = 'settings';
    //
	public function __construct()
	{
        //$this->middleware('auth');
	}

	public function index(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$dataQuery = Settings::sortable();

		if ($request->has('search_by_field') && $request->search_by_field != '') {

			$dataQuery->where('field','like','%'.$request->search_by_field.'%');
		}

		//dd($dataQuery->toSql()); exit;

		$listing = $dataQuery->paginate(settingParam('record-per-page'));

		return view('admin.settings_list',compact('listing','request'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function add()
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
                $validator = [
			'field' => 'required|unique:settings',
			'value' => 'required',
		];
		
		$jsValidator = JsValidator::make($validator);
		return view('admin.settings_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	public function store(Request $request){
            
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'field' => 'required|unique:settings',
			'value' => 'required',
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{

			$settings = new Settings;
			$settings->field = $request->field;
			$settings->slug = $request->slug;
			$settings->value = $request->value;
			
			if($settings->save()){
				$desc = "the user id ".Auth::user()->id." that was created settings";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created settings',$desc,'settings_admin','',json_encode($request->all()),'');

				$this->settingsDataRedis();
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }

	}

	public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Settings::find($id);

		$validator = [
			'field' => 'required|unique:settings,field,'.$id,
			'value' => 'required',
		];
		$currency = Currency::get();
		$jsValidator = JsValidator::make($validator);
		return view('admin.settings_edit',compact('jsValidator','data','currency'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}


	public function update(Request $request){

            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'field' => 'required|unique:settings,field,'.$request->id,
			'value' => 'required',
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$settings = Settings::find($request->id);
			$oldData = json_encode($settings);
			$settings->field = $request->field;
			$settings->slug = $request->slug;
			$settings->value = $request->value;
			
			if($settings->save()){
				$desc = "the user id ".Auth::user()->id." that was updated settings";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated settings',$desc,'settings_admin','',$oldData,json_encode($request->all()));
				$this->settingsDataRedis();
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }

	}

	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Settings::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			$desc = "the user id ".Auth::user()->id." that was deleted settings";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted settings',$desc,'settings_admin','',$oldData,json_encode($id));
			$this->settingsDataRedis();
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                toastr()->error('You do not have permission to perform this action!');
                return redirect(frontUrl('error'));
            }
	}

	public function checkSlug(Request $request)
	{   
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$slug = str_slug($request->fieldTitle);

		$slug = SlugService::createSlug(Settings::class, 'slug', $request->fieldTitle);

		return response()->json(['slug' => $slug]);
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Store Data in Redis For SettingParams */

	public function settingsDataRedis(){

		$data = Settings::get()->toArray();

		if(!empty($data)){
			foreach($data as $dataval){
				$setting[$dataval['slug']] = $dataval['value'];
			}
			Redis::set('settingParams', json_encode($setting));
		}
		return true;
		
		
	}
}
