<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\Request;
use App\Templates;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Support\Facades\Redis;
use JsValidator;
use Validator;
use Auth;
use App\Traits\log;

class AdminTemplateController extends Controller
{
	use log;
	public $moduleName = 'template';
    //
	public function __construct()
	{
        //$this->middleware('auth');
	}

	public function index(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$dataQuery = Templates::sortable();

		if ($request->has('search_by_field') && $request->search_by_field != '') {

			$dataQuery->where('field','like','%'.$request->search_by_field.'%');
		}

		//dd($dataQuery->toSql()); exit;

		$listing = $dataQuery->paginate(settingParam('record-per-page'));

		return view('admin.templates_list',compact('listing','request'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	public function add()
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
                $validator = [
			'field' => 'required|unique:templates',
			'english_text' => 'required',
			'translated_text' => 'required',
		];
		
		$jsValidator = JsValidator::make($validator);
		return view('admin.templates_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	public function store(Request $request){
            
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'field' => 'required|unique:templates',
			'english_text' => 'required',
			'translated_text' => 'required',
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{

			$settings = new Templates;
			$settings->field = $request->field;
			$settings->slug = $request->slug;
			//$settings->value = $request->value;
			$settings->english_text = $request->english_text;
			$settings->translated_text = $request->translated_text;
			
			if($settings->save()){
				$desc = "the user id ".Auth::user()->id." that was created template";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created template',$desc,'templates_admin','',json_encode($request->all()),'');

				$this->settingsDataRedis();
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }

	}
	public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Templates::find($id);

		$validator = [
			'field' => 'required|unique:templates,field,'.$id,
			'english_text' => 'required',
			'translated_text' => 'required',
		];
		$currency = Currency::get();
		$jsValidator = JsValidator::make($validator);
		return view('admin.templates_edit',compact('jsValidator','data','currency'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	public function update(Request $request){

            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), [
			'field' => 'required|unique:templates,field,'.$request->id,
			'english_text' => 'required',
			'translated_text' => 'required',
		]);

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$settings = Templates::find($request->id);
			$oldData = json_encode($settings);
			$settings->field = $request->field;
			$settings->english_text = $request->english_text;
			$settings->translated_text = $request->translated_text;
			
			if($settings->save()){
				$desc = "the user id ".Auth::user()->id." that was updated templates";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated templates',$desc,'templates_admin','',$oldData,json_encode($request->all()));
				$this->settingsDataRedis();
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }

	}
	public function checkSlug(Request $request)
	{   
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$slug = str_slug($request->fieldTitle);

		$slug = SlugService::createSlug(Templates::class, 'slug', $request->fieldTitle);

		return response()->json(['slug' => $slug]);
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	public function settingsDataRedis(){

		$data = Templates::get()->toArray();

		if(!empty($data)){
			foreach($data as $dataval){
				$setting[$dataval['slug']] = $dataval['english_text'];
			}
			Redis::set('templateParams', json_encode($setting));
		}
		return true;
		
		
	}

	
}
