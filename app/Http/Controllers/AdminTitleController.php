<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Country;
use JsValidator;
use Validator;
use Auth;
use App\DogsDB;
use App\Dogs2Users;
use App\BreedsDB;
use App\UsersSkills;
use App\Breedinghouses;
use App\Titles;
use App\User;
use Session;
use Carbon\Carbon;
use App\Traits\log;
use App\Exports\TitleExport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Facades\Excel;
class AdminTitleController extends Controller
{

  use log;
  public $moduleName = "titles";

  public function __construct()
  {
        $this->middleware('auth');
  }
  public function get_title_list(Request $request){

  // echo "here";exit;
DB::enableQueryLog();
  	$dataQuery = new  Titles();
  	if ($request->has('search_by_title_name') && $request->search_by_title_name != '') {
  		$dataQuery = $dataQuery->where('TitleName', 'like', '%' . $request->search_by_title_name  . '%');
     
    }
    if ($request->has('search_by_title_remark') && $request->search_by_title_remark != '') {

      $dataQuery = $dataQuery->where('Remark', 'like', '%' .  $request->search_by_title_remark . '%');
    }


   	if ($request->submit_type == 'export_csv') {
                return Excel::download(new TitleExport($dataQuery->sortable()->get()), 'titles.csv');
              /*  return (new BreedinghousesExport($dataQuery->sortable()->get()))->download('breedinghouses.csv');*/
    }
    $listing = $dataQuery->sortable()->paginate();
   // dd(DB::getQueryLog());
   	//echo "<pre>";print_r($listing);exit;
 	$userSkill = UsersSkills::where('user_id',Auth::user()->id)->where('skill_id','30')->first();
    return view('admin.titles_list',compact('listing','request','userSkill'));
   
  }
  function validationRules(){
		$validator = [
			'titles_name' => 'required',
			'titles_code' => 'required',

		];

		return $validator;
	}
  public function add(){
  	if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = $this->validationRules();
		$jsValidator = JsValidator::make($validator);
		return view('admin.titles_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
  }

  public function store(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{
		//	echo "<pre>";print_r($request->all());exit;
			$Titles = new Titles;
			$Titles->TitleName = $request->titles_name;
			$Titles->TitleCode = $request->titles_code;
			$Titles->Remark = $request->titles_remark;
			$Titles->TitleDesc = $request->titles_desc;
			
			$Titles->created_at = Carbon::now();
			
			if($Titles->save()){
				$desc = "the user id ".Auth::user()->id." that was created title";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created title',$desc,'title_admin','',json_encode($request->all()),'');
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
	}else{
            toastr()->error('You do not have permission to perform this action!');
            return redirect(frontUrl('error'));
    }
  }

  public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Titles::find($id);

		$validator = $this->validationRules();

		$jsValidator = JsValidator::make($validator);
		
		return view('admin.titles_edit',compact('jsValidator','data'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

 public function update(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$Titles = Titles::find($request->id);
			$oldData = json_encode($Titles);
			$Titles->TitleName = $request->titles_name;
			$Titles->TitleCode = $request->titles_code;
			$Titles->Remark = $request->titles_remark;
			$Titles->TitleDesc = $request->titles_desc;
			$Titles->updated_at = Carbon::now();
			
			if($Titles->save()){

				$desc = "the user id ".Auth::user()->id." that was updated title";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated title',$desc,'title_admin','',$oldData,json_encode($request->all()));

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	} 

	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Titles::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			
			$desc = "the user id ".Auth::user()->id." that was deleted title";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted title',$desc,'title_admin','',$oldData,json_encode($id));
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
	
  

  
}
