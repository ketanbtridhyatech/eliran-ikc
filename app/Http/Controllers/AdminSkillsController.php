<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Skills;
use JsValidator;
use Validator;
use Session;
use Carbon\Carbon;
use Auth;
use App\Traits\log;

class AdminSkillsController extends Controller
{
	use log;
	public $moduleName = "skills";

	public function __construct()
	{
        //$this->middleware('auth');
	}

	/* Listing View */
	public function index(Request $request)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$dataQuery = skills::sortable();

		if ($request->has('search_by_skill_name') && $request->search_by_skill_name != '') {
			
			$dataQuery->where('skill_name','like','%'.$request->search_by_skill_name.'%');
			$dataQuery->orWhere('skill_name_en','like','%'.$request->search_by_skill_name.'%');
			
		}

		$listing = $dataQuery->paginate(settingParam('record-per-page'));
		return view('admin.skills_list',compact('listing','request'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}

	/* Common Validation Rules */
	function validationRules(){
		$validator = [
			'skill_name' => 'required',
			'skill_name_en' => 'required',
			'skill_access_level' => 'required',
			'skill_status' => 'required',
		];

		return $validator;
	}

	/* Add View */
	public function add()
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = $this->validationRules();
		$jsValidator = JsValidator::make($validator);
		return view('admin.skills_add',compact('jsValidator'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}


	/* Store Function for Save values */
	public function store(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/add'))
			->withErrors($validator)
			->withInput();
		}else{

			$skills = new Skills;
			$skills->skill_name = $request->skill_name;
			$skills->skill_name_en = $request->skill_name_en;
			$skills->skill_access_level = $request->skill_access_level;
			$skills->skill_status = $request->skill_status;
			$skills->created_at = Carbon::now();
			
			if($skills->save()){
				$desc = "the user id ".Auth::user()->id." that was created skills";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'created skills',$desc,'skills_admin','',json_encode($request->all()),'');
				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/add'))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}


	/* Edit View */
	public function edit($id)
	{
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Skills::find($id);

		$validator = $this->validationRules();

		$jsValidator = JsValidator::make($validator);
		return view('admin.skills_edit',compact('jsValidator','data'));
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}


	/* Update Function for Update values */
	public function update(Request $request){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$validator = Validator::make($request->all(), $this->validationRules());

		if ($validator->fails()) {
			return redirect(backUrl($this->moduleName.'/edit/'.$request->id))
			->withErrors($validator)
			->withInput();
		}else{

			$skills = Skills::find($request->id);
			$oldData = json_encode($skills);
			$skills->skill_name = $request->skill_name;
			$skills->skill_name_en = $request->skill_name_en;
			$skills->skill_access_level = $request->skill_access_level;
			$skills->skill_status = $request->skill_status;
			$skills->updated_at = Carbon::now();
			
			if($skills->save()){

				$desc = "the user id ".Auth::user()->id." that was updated skills";
	        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'updated skills',$desc,'skills_admin','',$oldData,json_encode($request->all()));

				toastr()->success('Data has been saved successfully!');
				return redirect(backUrl($this->moduleName));
			}else{
				toastr()->error('Technical Issue!');
				return redirect(backUrl($this->moduleName.'/edit/'.$request->id))->withInput();
			}
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}


	/* Soft Delete */
	public function delete($id){
            if(Auth::check() && Auth::user()->is_superadmin == 1){
		$data = Skills::find($id);
		$oldData = json_encode($data);
		if($data->delete()){
			
			$desc = "the user id ".Auth::user()->id." that was deleted skills";
        	$this->insertLog(Auth::user()->id,'', $_SERVER['REMOTE_ADDR'], date('Y-m-d H:m:s'), 'deleted skills',$desc,'skills_admin','',$oldData,json_encode($id));
			toastr()->success('Data has been deleted successfully!');
			return redirect(backUrl($this->moduleName));
		}else{
			toastr()->error('Technical Issue!');
			return redirect(backUrl($this->moduleName));
		}
            }else{
                    toastr()->error('You do not have permission to perform this action!');
                    return redirect(frontUrl('error'));
            }
	}
}
