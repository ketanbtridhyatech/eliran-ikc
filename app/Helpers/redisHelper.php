<?php
use Illuminate\Support\Facades\Redis;

function translate($string = ''){
	
	
	$transVal = "";
	$returnVal = $string; 

	if(Auth::check()){
		$langId = isset(Auth::user()->language_id)?Auth::user()->language_id:0;

	}else{
		if(isset($_COOKIE['language_id'])){
            $langId = $_COOKIE['language_id'];
         }else{
         	$langId = 1;
         }
	}
	//$langId = isset(Auth::user()->language_id)?Auth::user()->language_id:0;
	

	$moduleId = (Session::has('module_id'))?Session::get('module_id'):0;
	$response = Redis::get('translate');

	
	if($response!=''){
		$langData = json_decode($response, true);
		if(isset($langData) && !empty($langData)){


			/* Check Module base language translation */
			if($langId == 0){
				

				if(isset($langData[$moduleId][1]) && array_key_exists($string,$langData[$moduleId][1])){
					
					$convert_to_array = explode('###',$langData[$moduleId][1][$string]);
					$transVal = $convert_to_array[0];
					//$transVal = $string;
				}

			}else{
			
			
				//$transVal = (isset($langData[$moduleId][$langId][$string]))?$langData[$moduleId][$langId][$string]:"";
				if(isset($langData[$moduleId][$langId][$string])){
					$convert_to_array = explode('###',$langData[$moduleId][$langId][$string]);
					$transVal = $convert_to_array[1];
				}else{
					$transVal = "";
				}
				
			}

			/* IF Module have no value found then check the value from General Module */

			if($transVal == ""){
				if($langId == 0){
					
					if(isset($langData['General'][1]) && array_key_exists($string,$langData['General'][1])){
						$convert_to_array = explode('###',$langData['General'][1][$string]);
						$returnVal = $convert_to_array[0];
					}
				}else{

					//$returnVal = (isset($langData['General'][$langId][$string]))?$langData['General'][$langId][$string]:$string;
					
					if(isset($langData['General'][$langId][$string])){
						$convert_to_array = explode('###',$langData['General'][$langId][$string]);
						$returnVal = $convert_to_array[1];
					}else{
						return $string;
					}
					
						
				}
			}else{
				$returnVal = $transVal;
			}
		}
	}

	return $returnVal;
	//Redis::del('translate');
	
}

function settingParam($string = ''){

	$returnVal = "";
	$response = Redis::get('settingParams');
	if($response!=''){
		$settingsData = json_decode($response, true);

		if(isset($settingsData) && !empty($settingsData)){
			if(isset($settingsData[$string]) && $settingsData[$string]!=''){
				$returnVal = $settingsData[$string];
			}

		}
		

	}
	return $returnVal;
}

function currentLanguage(){
	
	if(Auth::check()){
		$langId = isset(Auth::user()->language_id)?Auth::user()->language_id:0;

	}else{
		if(isset($_COOKIE['language_id'])){
            $langId = $_COOKIE['language_id'];
         }else{
         	$langId = 1;
         }
	}

	
	return $langId;
}



