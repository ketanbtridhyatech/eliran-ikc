<?php

/* Dynamimc admin URL */

use App\DynamicHtmlElement;
use App\TaskRelatedSkills;
use App\UsersSkills;
use Illuminate\Support\Facades\Auth;

function backUrl($url = '')
{

	$url = ($url != '') ? '/' . $url : '';
	return url(config('constant.backend') . $url);
}
function frontUrl($url = '')
{

	$url = ($url != '') ? '/' . $url : '';
	return url($url);
}
function convertDateInYMD($date)
{

	$date = strtotime($date);

	return date("Y-m-d", $date);
}

function convertDate($date)
{
	//echo $date;exit;
	$date = strtotime($date);

	return date(settingParam('date-format'), $date);
}

function displayName()
{
	$langId = isset(Auth::user()->language_id) ? Auth::user()->language_id : 0;
	$name = isset(Auth::user()->first_name_en) ? Auth::user()->first_name_en : '';
	if ($langId == 1) {
		$name = isset(Auth::user()->first_name) ? Auth::user()->first_name : '';
	}

	return $name;
}

function displayProfile()
{
	$profilePicture = "";
	if (isset(Auth::user()->profile_photo) && Auth::user()->profile_photo != '' && file_exists(storage_path('app\public\users\thumb_' . Auth::user()->profile_photo))) {

		$profilePicture = url('storage/users/thumb_' . Auth::user()->profile_photo);
	}

	return $profilePicture;
}
function pc_next_permutation($p, $size)
{
	for ($i = $size - 1; $p[$i] >= $p[$i + 1]; --$i) {
	}

	if ($i == -1) {
		return false;
	}
	for ($j = $size; $p[$j] <= $p[$i]; --$j) {
	}
	$tmp = $p[$i];
	$p[$i] = $p[$j];
	$p[$j] = $tmp;

	for (++$i, $j = $size; $i < $j; ++$i, --$j) {
		$tmp = $p[$i];
		$p[$i] = $p[$j];
		$p[$j] = $tmp;
	}
	return $p;
}
function get_combination($string)
{
	$set = explode(' ', $string);
	if (count($set) == 1) {
		$set[] = " ";
	}
	$size = count($set) - 1;
	$perm = range(0, $size);
	$j = 0;
	do {
		foreach ($perm as $i) {
			$perms[$j][] = $set[$i];
		}
	} while ($perm = pc_next_permutation($perm, $size) and ++$j);
	$new_array = array();
	foreach ($perms as $p) {

		$new_array[] = implode(' ', $p);
	}
	return $new_array;
}
function generate_html_element($ele_name, $id = "", $name = "", $value_type = "int", $value = "", $extra_class = array(), $extra_attr = array())
{
	$html_element = DynamicHtmlElement::with('dynamic_html_element_value')->where('slug', $ele_name)->first();
	$ele_name = !empty($html_element) ? $html_element->html_element : '';
	if ($ele_name == "textbox") {
		echo '<input type="text" class="form-control ' . implode(" ", $extra_class) . '" id="{{$id}}" name="{{$name}}" value="{{$value}}" ' . implode(" ", $extra_attr) . '>';
	} else if ($ele_name == "checkbox") {
	} else if ($ele_name == "select_list") {

		$html_ele = " ";
		if (!empty($html_element)) {
			$html_ele .= "<select name='$name' id='$id' class='form-control '" . implode(' ', $extra_class) . "  " . implode(' ', $extra_attr) . ">";
			$html_ele .= "<option value=''>" . translate('Select') . "</option>";
			if (!$html_element->dynamic_html_element_value->isEmpty()) {
				foreach ($html_element->dynamic_html_element_value as $ht) {
					$lable = (currentLanguage() == 1) ? $ht->option_text_hebrew : $ht->option_text_english;
					if ($value_type == 'int') {
						$selected = (!empty($value) && $value == $ht->id) ? 'selected' : '';
						$html_ele .= "<option value='$ht->id'" . $selected . ">" . $lable . "</option>";
					} else {
						
						$selected = (!empty($value) && $value == $ht->option_value) ? 'selected' : '';
						$html_ele .= "<option value='$ht->option_value'" . $selected . ">" . $lable . "</option>";
					}
				}
			}
			$html_ele .= "</select>";
			
		}
		echo $html_ele;
	} else if ($ele_name == "radio_button") {
	} else if ($ele_name == "file_upload") {
		echo '<input type="file" class="form-control ' . implode(" ", $extra_class) . '" id="{{$id}}" name="{{$name}}" value="{{$value}}" ' . implode(" ", $extra_attr) . '>';
	} else if ($ele_name == "textarea") {
		echo '<textarea class="form-control ' . implode(" ", $extra_class) . '" id="{{$id}}" name="{{$name}}" ' . implode(" ", $extra_attr) . '>{{$value}}</textarea>';
	}
}
function related_task()
{
	return TaskRelatedSkills::where('user_id', Auth::user()->id)->count();
}
function user_related_skill(){
	$skills = UsersSkills::where('user_id',Auth::user()->id)->where('skill_id',3)->first();
	if(empty($skills)){
		return false;
	}else{
		if(empty($skills->breed_id)){
			return false;
		}
		return true;
	}
}
