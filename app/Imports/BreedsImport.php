<?php

namespace App\Imports;

use App\BreedingDB;
use Maatwebsite\Excel\Concerns\ToModel;

class BreedsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new BreedingDB([
            //
        ]);
    }
}
