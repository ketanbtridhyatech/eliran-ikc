<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dogs2Users extends Authenticatable
{

    public $table = "dogs2users";
    use Notifiable;
    use Sortable;
    use SoftDeletes;
    protected $casts = [
    'sagir_id' => 'int',
];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function user_detail() {
    return $this->hasOne('App\User', 'id', 'user_id');
}
    

}
