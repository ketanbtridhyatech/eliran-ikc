<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BreedClub extends Model
{
    use SoftDeletes;
    public $table = "breed_club";
    public $timestamps = true;
    
}
