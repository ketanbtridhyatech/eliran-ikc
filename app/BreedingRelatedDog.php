<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class BreedingRelatedDog extends Authenticatable
{

    public $table = "breeding_related_dog";
    use Notifiable;
    use Sortable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    function dog_color(){
        return $this->hasOne('App\ColorsDB', 'id','color');
    }
    function dog_status(){
        return $this->hasOne('App\DynamicHtmlElementOption', 'id','approval_status');
    }
  
}
