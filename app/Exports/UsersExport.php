<?php

namespace App\Exports;

use App\User;
use App\Country;
use App\Skills;
use App\UsersSkills;
use App\Roles;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

ini_set('max_execution_time', '0');

class UsersExport implements FromView
{
    use Exportable;
    
    public function view(): View
    {
        ini_set('max_execution_time', '0');
    	$dataQuery = User::with('country','userSkills');

        $listing = $dataQuery->get();
        
        return view('export.export_user', [
            'listing' => $listing
        ]);
    }
}
