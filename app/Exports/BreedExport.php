<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BreedExport implements FromView,ShouldAutoSize
{
    use Exportable;

    public function __construct($data)
    {
        $this->data = $data;
    }
    public function view(): View
    {
        return view('export.export_breeds', [
            'breeds_details' => $this->data
        ]);
    }
}
