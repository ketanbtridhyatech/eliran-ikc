<?php

namespace App\Exports;

use App\User;
use App\Country;
use App\Skills;
use App\UsersSkills;
use App\Roles;
use Illuminate\Contracts\View\View;
//use Maatwebsite\Excel\Concerns\FromView;

class UsersExportPDF
{
    
    public static function view($listing)
    {
    	//$dataQuery = User::with('country','userSkills');

    	////$listing = $dataQuery->get();

        return view('export.export_user_pdf', [
            'listing' => $listing
        ]);
    }
}
