<?php

namespace App\Exports;
use App\Dogs2Users;
use App\DogsDB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DogsExport implements FromView,ShouldAutoSize
{
    use Exportable;

    public function __construct(int $user_id)
    {
        $this->user_id = $user_id;
    }
    public function view(): View
    {
    
        $dogs = Dogs2Users::where('user_id',$this->user_id)->pluck('sagir_id');
		$dogs_details = DogsDB::with('breed_name','color_name','user_data_id')->whereIn('SagirID',$dogs)->get();

        return view('export.export_dogs', [
            'dogs_details' => $dogs_details
        ]);
    }
}
