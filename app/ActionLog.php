<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActionLog extends Authenticatable
{

    public $table = "action_logs";
    public $timestamps = false;
    use Notifiable;
    use Sortable;
    use SoftDeletes;



}
