<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaskRelatedSkills extends Authenticatable
{

    public $table = "task_related_skills";
    use Notifiable;
    use Sortable;
    use SoftDeletes;

    public function user_details(){
        return $this->hasOne('App\User', 'id','user_id');
    }

}
