<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class PuppieCards extends Authenticatable
{

    public $table = "puppie_cards";
    public $timestamps = true;
    use Notifiable;
    use Sortable;
    use SoftDeletes;

   


}
