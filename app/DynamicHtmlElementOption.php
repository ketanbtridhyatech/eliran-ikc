<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class DynamicHtmlElementOption extends Authenticatable
{

    public $table = "dynamic_html_element_value";
    //public $timestamps = false;
    use Notifiable;
    use Sortable;
    use SoftDeletes;
    public $sortable = ['option_text_hebrew','option_text_english','option_value']; 
}
