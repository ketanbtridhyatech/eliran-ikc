<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersSkills extends Model
{
    use SoftDeletes;
    public $table = "users_skills";
    public $timestamps = true;
    


    public function skills()
    {
        return $this->belongsTo('App\Skills');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }


}
