<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Authenticatable
{

    public $table = "currency";
    public $timestamps = true;
    use Notifiable;
    use Sortable;
    use SoftDeletes;
}
