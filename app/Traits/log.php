<?php
namespace App\Traits;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Application;
use App\ActionLog;

trait log
{
    public function insertLog($userId,$sagirid = '',$ip,$date,$actionName, $actionFullDesc, $actionTopic = '', $reletedTaskId = '',$currentField = '',$updatedField = '')
    {
            $obj             = new ActionLog;
            $obj->user_id    = $userId;
            if(!empty($sagirid)){
                $obj->sagir_id = $sagirid;
            }
            $obj->user_ip       = $ip;
            $obj->date_time     = $date;
            $obj->action_name     = $actionName;
            $obj->action_full_desc     = $actionFullDesc;
            $obj->action_topic     = $actionTopic;
            if(!empty($reletedTaskId)){
                $obj->related_task_id     = $reletedTaskId;
            }
            $obj->current_field    = $currentField;
            $obj->updated_field     = $updatedField;
            $obj->save();
    
        return true;
    }
}