<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewUser extends Authenticatable
{

    public $table = "new_users";
    public $timestamps = false;
    use Notifiable;
    use Sortable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id','id');
    }

    public function userSkills()
    {
        return $this->hasMany('App\UsersSkills');
        //return $this->hasMany('App\UsersSkills', 'skill_id','user_id');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skills','users_skills','user_id', 'skill_id');
    }


}
