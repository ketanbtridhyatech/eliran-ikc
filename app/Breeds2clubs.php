<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Breeds2clubs extends Authenticatable
{

    public $table = "breeds2clubs";
    public $timestamps = false;
    use Notifiable;
    use Sortable;
    use SoftDeletes;

   


}
