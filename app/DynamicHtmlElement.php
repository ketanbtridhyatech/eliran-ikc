<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class DynamicHtmlElement extends Authenticatable
{

    public $table = "dynamic_html_element";
    //public $timestamps = false;
    use Notifiable;
    use Sortable;
    use SoftDeletes;
    use Sluggable;
    public $sortable = ['name','slug','html_element'];
  
    public function dynamic_html_element_value(){
        return $this->hasMany('App\DynamicHtmlElementOption', 'html_element_id','id');
    }
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
